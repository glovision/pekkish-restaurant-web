<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class HomeController extends Controller
{



    public function testing(){
        //dd(env('BASE_URL'));
        $response = Http::get(env('BASE_URL').'/v1/cuisines-filter');
        dd($response->json()["data"]);
    }

    public function home(Request $request){
       // $today_deals = Http::get(env('BASE_URL').'/v1/today-deals')->json()["data"];
        //$pekkish_picks = Http::get(env('BASE_URL').'/v1/shops')->json()["data"];
       $pekkish_picks = Http::get('http://localhost/pekkish/api/v1/filter_options')->json()["data"];
       // $top_picks = Http::get(env('BASE_URL').'/v1/shops')->json()["data"];
       // $buy_one_get_one = Http::get(env('BASE_URL').'/v1/shops')->json()["data"];

        /*$curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://localhost/pekkish/api/v1/filter_options',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmNmE0NTQwYzM0OTg5OGFkM2I5ODBjOGI2ZTI4M2E3NDg4OTY5NWM4MTIyNDEwYzhkNGRhOTMxMzk3NjBhOTI4OWUyMjVmMTU3ZjgxY2FkIn0.eyJhdWQiOiIxIiwianRpIjoiZGY2YTQ1NDBjMzQ5ODk4YWQzYjk4MGM4YjZlMjgzYTc0ODg5Njk1YzgxMjI0MTBjOGQ0ZGE5MzEzOTc2MGE5Mjg5ZTIyNWYxNTdmODFjYWQiLCJpYXQiOjE2MDk1Nzc5ODMsIm5iZiI6MTYwOTU3Nzk4MywiZXhwIjoxNjQxMTEzOTgzLCJzdWIiOiIxOCIsInNjb3BlcyI6W119.drB1jTAyYdCVc-KN5u3_6r_CFJn207bSJl7NGeIjOsySMbm8fJdhsDiBoJZYvAuSsV_ND_YdLjjqyf5VZ-SjCQO-fRq1Okl0ZC9-O4NJibTSQQzTme5-g1wHKil7Uh48NrgN5KCjc0XTioShRtohDtmNKuNFx7VpBB8nJQKqnM-CD0IKFVA6UzFP4wstMKsyrr2bTJBykA4lzkjE38qe6pF0GYD4wKYBaJSw8EJv88N6XOj0iIstWMLs1LtRLXXYhHJXN_cJ3DJj_bY8L60hWOgKWTztSAp9c_q7FAvjRQxubYGA7rsr2fnujFoQk2pGtIQSLRdTQxTfJRA9UdzR5qHc7_IhWvpo4wDlbzi3alTzn1eCLZmSe8AEzXkIW7iEFlTiOj7_5DahOqKG6MW4c7hMth5eEc_fQpPGZpMSaZHe3mrQd5BZcAoNMGJLd93pNRPw4wN2kDnyr8OBzPkX2_9qLVN8cj2sDnyHUe_6RNZgjlE2MOIsuqlRgfvhFnxh5Gv7yOe74eX1MHHMUSgd8tD6nnaSYejzpt73GBAREhxKe6fqGg1BYedjhEMJ014_wxMzffASSzZnLtf_jst7CsPIj2yxOqXgfhtSW7s1rG4SP4k5Bi2tA2QGswltSy2yYCUSIpUp3Cku3a0sC_C6OD9df0OkkCCRlmF2J0Leqt0'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;*/

        //dd($pekkish_picks);
        return view('home',compact('today_deals','pekkish_picks'))->with('address',$request->address);
    }

    public function login(Request $request){
        $response = Http::post(env('BASE_URL').'/v1/auth/login', [
            'email' => $request->email,
            'password' => $request->password,
            'fcm_id' => "111111",
            'request_type' => "111111",
            'device_type' => "android",
            'role' => "customer"
        ]);
        $result=$response->json();
        session(['token' => $result["access_token"],'user_details'=>$result["user"]]);
        return view('home_page');
    }


}
