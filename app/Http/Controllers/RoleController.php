<?php

namespace App\Http\Controllers;

use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class RoleController extends Controller
{
    public $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->user->roles();
        //dd($roles);
        if($roles['success']){
            $roles = $roles['data'];
        }
        $tblfeatures = $this->user->tblfeatures();
        $tblfeatures = $tblfeatures['data'];
        //dd($tblfeatures);
        return view('employees.roles', compact('roles','tblfeatures'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $user = session()->get('user_details');
        if($user->type == "shop"){
            $request->shop_id = array($user->id);
        }
        $validator = Validator::make($request->all(),
            [
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                'emailid' => ['required'],
                'phone' => ['required'],
                'role_id' => ['required'],
                //'shop_id' => ['required'] ,
                'joiningdate' => ['required']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['phone'] = $request->phone;
        $data['email'] = $request->emailid;
        $data['role_id'] = $request->role_id;
        $data['shop_id'] = $request->shop_id;
        $data['joining_date'] = $request->joiningdate;

       try {
           $result = $this->user->addUser($data);
           if($result['success']){
               return redirect()->route('employees.index')->with('success',"User has been inserted successfully!!!");
           }else{
               return redirect()->back()->with('failed',"Failed insert");
           }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Insert Failed'], 400);
        }
    }


    public function edit(Request $request)
    {
        $data = [];
        $user = session()->get('user_details');
        if($user->type == 'shop'){
            $request->shop_id = array($user->id);
        }
        $validator = Validator::make($request->all(),
            [
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                //'emailid' => ['required'],
                //'phone' => ['required'],
                'role_id' => ['required'],
                //'shop_id' => ['required'] ,
                'joiningdate' => ['required']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['id'] = $request->id;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['phone'] = $request->phone;
        $data['email'] = $request->emailid;
        $data['role_id'] = $request->role_id;
        $data['shop_id'] = $request->shop_id;
        $data['joining_date'] = $request->joiningdate;

        try {
            $result = $this->user->EditUser($data);
            //dd($result['Response']);
            if($result['success']){
                return redirect()->route('employees.index')->with('success',"User has been updated successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed Update");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }

    public function users_destroy($id)
    {
        try {
            $adduser = $this->user->deleteUser($id);
            return redirect()->route('employees.index')->with('success',"User has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

    public function addrole(Request $request)
    {
        $data = [];
        $validator = Validator::make($request->all(),
            [
                'rolename' => ['required', 'string'],
                'permission_id' => ['required'],
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['name'] = $request->rolename;
        $data['apptype'] = 2;
        $data['permissions'] = $request->permission_id;

        try {
            $result = $this->user->addRole($data);
            if($result['success']){
                return redirect()->route('employees.roles')->with('success',"Role has been inserted successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed insert");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Insert Failed'], 400);
        }
    }

    public function editrole(Request $request)
    {
        $data = [];
        $validator = Validator::make($request->all(),
            [
                'rolename' => ['required', 'string'],
                'permission_id' => ['required'],
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['id'] = $request->id;
        $data['name'] = $request->rolename;
        $data['apptype'] = 2;
        $data['permissions'] = $request->permission_id;
        try {
            $result = $this->user->editRole($data);
            if($result['success']){
                return redirect()->route('employees.roles')->with('success',"Role has been updated successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed update");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Update Failed'], 400);
        }
    }

    public function role_destroy($id)
    {
        try {
            $result = $this->user->deleteRole($id);
            return redirect()->route('employees.roles')->with('success',"Role has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }


}
