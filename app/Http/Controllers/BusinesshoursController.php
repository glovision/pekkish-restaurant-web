<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Repositories\Businesshour\BusinesshourInterface as BusinesshourInterface;
use Illuminate\Support\Facades\Validator;
class BusinesshoursController extends Controller
{
    public $businesshour;

    public function __construct(BusinesshourInterface $businesshour)
    {
        $this->businesshour = $businesshour;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dutyhistory = $this->businesshour->dutyHistory();
       // dd($dutyhistory);
        if($dutyhistory=='Internal server error'){
            return redirect()->back()->with('message', 'No data Available');
        }else{
            $dutyhistory = $dutyhistory->data;
            //dd($orders->label);
        }
        $days = [['id'=>0,'name'=>'Sun'],['id'=>1,'name'=>'Mon'],['id'=>2,'name'=>'Tue'],['id'=>3,'name'=>'Wed'],['id'=>4,'name'=>'Thu'],['id'=>5,'name'=>'Fri'],['id'=>6,'name'=>'Sat']];
        return view('business_hours', compact('dutyhistory','days'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxBusinessTiming()
    {
        $shoptiming = $this->businesshour->shopTiming();
        return $shoptiming;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'working_status'    => ['required'],
                //'timing'            => ['required'],
               // 'foralldays'        => ['required'],
               // 'days_to_run'       => 'required',
            ]
        );
        if($request->working_status==2){

            $validator = Validator::make($request->all(),
                [
                    'working_status'    => ['required'],
                    //'timing'            => ['required'],
                    // 'foralldays'        => ['required'],
                     'days_to_run'       => 'array|min:1|required'
                ]
            );

        }
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }
        //dd($request->working_status);
        $data['day_status'] = $request->working_status;
        $data['timing'] = $request->timing;
        $data['foralldays'] = $request->foralldays;
        $data['days_to_run'] = json_encode($request->days_to_run);
        $timing = json_encode($request->timing);
        $data['timing'] = $timing;
        $data['set_off_time_key'] = 0;
        $data['status'] = 1;

        try {
            $result = $this->businesshour->addBusinesshour($data);
            if($result->success){
                return redirect()->route('business_hours')->with('success',"Timing has been inserted successfully!!! ");
            }else{
                return redirect()->back()->with('failed','Failed to insert');
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Failed'], 200);
        }
    }

    public function shopAvailableStatusChange(Request $request)
    {
        $data=[];
        $data['id'] = $request->id;
        try {
            $result = $this->businesshour->shopAvailableStatusChange($data);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
