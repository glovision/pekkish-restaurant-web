<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CusineController extends Controller
{
    public $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cusines = $this->user->cusines();

        if($cusines['success']){
            $cusines = $cusines['data'];
        }else{
            //return view('cusine',compact('cusines'));
        }

        return view('cusine', compact( 'cusines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        $data = [];
//        $user = session()->get('user_details');
//        if($user->type == "shop"){
//            $request->shop_id = array($user->id);
//        }
        $validator = Validator::make($request->all(),
            [
                'name' => ['required', 'string'],
                'meta_description' => ['required', 'string'],

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = session()->get('user_details');
        //$shop_id = Shop::where('user_id',$user->id)->first()->id;
        $data['name'] = $request->name;
        $data['meta_description'] = $request->meta_description;
        $data['shop_id'] =$user->id;

        try {
            $result = $this->user->addCusine($data);

            if($result['success']){
                return redirect()->route('cusine')->with('success',"Cusine has been inserted successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Cusine has been inserted failed!!!");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Insert Failed'], 400);
        }
    }


    public function update(Request $request)
    {
        $data = [];
        $user = session()->get('user_details');
        if($user->type == 'shop'){
            $request->shop_id = array($user->id);
        }
        $validator = Validator::make($request->all(),
            [
                'name'=>'required',
                'meta_description'=>'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data['name'] = $request->name;
        $data['meta_description'] = $request->meta_description;
        $user = session()->get('user_details');
        $data['shop_id'] = $user->id;
        $data['id'] = $request->id;

        try {
            $result = $this->user->updateCusine($data);

            if($result['success']){
                return redirect()->route('cusine')->with('success',"Cusine has been updated successfully!!!");
            }else{
                return redirect()->route('cusine')->with('success',"Cusine has been updated failed!!!");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Failed to add Cusine'], 400);
        }
    }


    public function deleteCusine($id)
    {

        try {
            $deleteCusine = $this->user->deleteCusine($id);
            if($deleteCusine) {
                return redirect()->route('cusine');
            }else{
                return redirect()->route('cusine');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to Delete Cusine'], 200);
        }
    }

}
