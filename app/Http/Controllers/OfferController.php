<?php

namespace App\Http\Controllers;

use App\Repositories\Offer\OfferInterface as OfferInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OfferController extends Controller
{
    public $offer;

    public function __construct(OfferInterface $offer)
    {
        $this->offer = $offer;
    }

    public function index()
    {
        $offers = [];
        $offers = $this->offer->offers();
        //dd($offers);
        if($offers->success){
            $offers = $offers->data;
        }
        $percentages = [['percent' => 5], ['percent' => 10], ['percent' => 15], ['percent' => 20], ['percent' => 25], ['percent' => 30]];
        $percentages = json_encode($percentages);
        $percentages = json_decode($percentages);
        $days = [['id' => 0, 'name' => 'Sun'], ['id' => 1, 'name' => 'Mon'], ['id' => 2, 'name' => 'Tue'], ['id' => 3, 'name' => 'Wed'], ['id' => 4, 'name' => 'Thu'], ['id' => 5, 'name' => 'Fri'], ['id' => 6, 'name' => 'Sat']];
        $categories = $this->offer->categoryProduct();
        $restaurants = $this->offer->shops();
        $restaurants  = $restaurants->data;
        if($categories){
            $categories = [];
        }else{
            return redirect()->back()->with('message',$categories->message);
        }

        return view('offer_list', compact('offers','percentages', 'days','categories','restaurants'));
        //return redirect()->route('offer_list', compact('percentages', 'days'));
    }


    public function addOffer(Request $request)
    {
        $data = [];
        /*$validator = Validator::make($request->all(),
            [
                'Promocode'    => ['required'],
                'description'    => ['required'],
                'days_to_run'    => ['required'],
                'StartDate' => ['required'],
                'end_date' => ['required']
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }*/
        //dd($request);
        $data['code'] = $request->Promocode;
        $data['description'] = $request->description;
        $data['discount_type'] = $request->discount_type;
        $data['discount_max_amount'] = (int)$request->discount_max_amount;
        $data['discount'] = (int)$request->discount;
        dd(array_values($request->days_to_run));
        $data['days_to_run'] = array_values($request->days_to_run);
        $data['start_date'] = $request->start_date;
        $data['end_date'] = $request->end_date;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['min_spending_amount'] = (int)$request->min_spending_amount;
        $data['max_spending_amount'] = (int)$request->max_spending_amount;
        $data['all_categories'] = $request->all_categories == "All" ? 1 :0;
        //dd($request->recommendedproducts);
        $data['allowed_categories'] = $request->recommendedcategory;
        $data['allowed_products'] = $request->recommendedproducts;
        $data['max_usage_per_user'] = (int)$request->max_usage_per_user;
        $data['max_users'] = (int)$request->max_users;
        $data['shops'] = array_values($request->restaurant);

        try {
            $result = $this->offer->addOffer($data);
            if($result['success']){
                return redirect()->route('offer_list')->with('success',"Offer has been inserted successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed to insert");
            }
        } catch (Exception $e) {
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add coupon'], 200);
        }
    }

    public function editOffer(Request $request)
    {
        $data = [];
        /* $validator = Validator::make($request->all(),
             [
                 'Promocode'    => ['required'],
                 'description'    => ['required'],
                // 'days_to_run'    => ['required'],
                 'StartDate' => ['required'],
                 'end_date' => ['required'],
             ]
         );
         if ($validator->fails()) {
             return redirect()->back()
                 ->withErrors($validator)
                 ->withInput();
         }*/
//dd($request);
        $data['id'] = $request->id;
        $data['code'] = $request->Promocode;
        $data['description'] = $request->description;
        $data['discount_type'] = $request->discount_type;
        $data['discount_max_amount'] = (int)$request->discount_max_amount;
        $data['discount'] = (int)$request->discount;
        $data['days_to_run'] = array_values($request->days_to_run);
        $data['start_date'] = $request->start_date;
        $data['end_date'] = $request->end_date;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['min_spending_amount'] = (int)$request->min_spending_amount;
        $data['max_spending_amount'] = (int)$request->max_spending_amount;
        $data['all_categories'] = $request->all_categories;
        $data['allowed_categories'] = $request->recommendedcategory;
        $data['allowed_products'] = $request->recommendedproducts;
        $data['max_usage_per_user'] = (int)$request->max_usage_per_user;
        $data['max_users'] = (int)$request->max_users;
        $data['shops'] = array_values($request->restaurant);
        //dd($data);

        try {
            $result = $this->offer->editOffer($data);
            if ($result['success']) {
                return redirect()->route('offer_list')->with('success', "Offer has been updated successfully!!!");
            } else {
                return redirect()->back()->with('failed', "Failed to update");
            }
        } catch (Exception $e) {
            return response()->json(['Status' => 'failed', 'message' => 'Failed to edit coupon'], 200);
        }
    }

    public function destroy($id)
    {
        try {
            $deleteoffer = $this->offer->deleteCoupon($id);
            return redirect()->route('offer_list');
        } catch (Exception $e) {
            return response()->json(['Status' => 'failed', 'message' => 'Failed to Delete Coupon'], 200);
        }
    }

    public function shop_categories($id){
        $categories = $this->offer->shop_categories($id);
        return $categories;
    }
    public function category_items($id){
        $items = $this->offer->category_items($id);
        return $items;

    }
    public function couponverify($id){
        $coupons = $this->offer->coupon_verify($id);
        return $coupons;

    }
}
