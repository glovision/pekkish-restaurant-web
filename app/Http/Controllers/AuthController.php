<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Repositories\Auth\AuthInterface as AuthInterface;
use Illuminate\Support\Facades\Validator;
use Log;
class AuthController extends Controller
{
    public $auth;
    public $token;

    public function __construct(AuthInterface $auth)
    {
        $this->auth = $auth;
        $this->token = session('token');
    }

    public function index()
    {

        try {
            $result = $this->auth->countrycodes();
            $brand_result = $this->auth->brands();
            $brands = $brand_result->data;
            $apk_result = $this->auth->apklinks();
            $apklinks = $apk_result->data;
            $ss_result = $this->auth->success_stories();
            $success_stories = $ss_result->data;
            $itworks_result = $this->auth->itworks();
            $itworks = $itworks_result->data;

            if ($result->success) {
                $countries = $result->data;
                return view('welcome',compact('countries','brands','apklinks','success_stories','itworks'));
            } else {
                $countries = $result->data;
                $brands = [];
                $apklinks = [];
                $success_stories = [];
                $itworks = [];
                return view('welcome', compact('countries','brands','apklinks','success_stories','itworks'));
            }
        }catch(\Exception $e){
			Log::info($e->getMessage());
            $countries = [];
            $brands = [];
            $apklinks = [];
            $success_stories = [];
            $itworks = [];
            return view('welcome', compact('countries','brands','apklinks','success_stories','itworks'));
            //return response()->json(['Status' => 'failed', 'message' => 'Countries list'], 200);
        }
    }
    public function two_way_signin($request){
        $data=[];

        $data['email'] = $request->email;
        $data['id'] = $request->id;
        $data['fcm_id'] = "web";
        $data['request_type'] = "web";
        $data['device_type'] = "web";
        $data['role'] = "seller";

        try {
            $result = $this->auth->two_way_signin($data);
               return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Signin Failed'], 200);
        }
    }

    public function signin(Request $request){

    try {

            if ($request->isMethod('GET')) {
                return redirect('/index');
            }
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'email'    => ['required', 'string', 'email'],
                'password' => ['required', 'string'],
                'user_type'=>'required'
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $data['user_type'] = $request->user_type;
        $data['fcm_id'] = "web";
        $data['request_type'] = "web";
        $data['device_type'] = "web";
        $data['role'] = "seller";

        $result = $this->auth->signIn($data);
            if($result->message=='Success'){
                session(['token' => $result->access_token,'user_details'=>$result->user]);
                if($result->user->type =='shop'){
                 $usershops =  $this->auth->userShops($result->user->id);
                    if($usershops->success){
                        //return $usershops->data[0]->user_id;
                        session(['token' => $result->access_token,'user_details'=>$result->user,'storeid'=>$usershops->data[0]->id]);                        
                    }              
                }                
                return redirect()->route('dashboard');                
            }else{
                return redirect()->back()->with('failed',$result->message);
            }
        }
        catch (Exception $e){
            return redirect()->back();
        }
    }

    public function signup(Request $request)
    {
        if ($request->isMethod('GET')) {
            return redirect('/index');
        }
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'store_name' => ['required', 'string'],
                'store_address' => ['required', 'string'],
                'firstname' => ['required', 'string'],
                'lastname' => ['required'],
                'email' => ['required'],
                'mobile_id' => 'required',
                //'country_code'=>['required']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['user_id'] = 'null';
        $data['email'] = $request->email;
        $data['user_type'] = 'seller';

        $email = $this->auth->EmailVerify($data);
        if(!$email->success){
            return redirect()->back()->with('failed','email already exists');
        }



        $data['email'] = $request->email;
       // $data['phone'] = $request->phone;
        $data['mobile_id'] = $request->mobile_id;
        $data['store_name'] = $request->store_name;
        $data['store_address'] = $request->store_address;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['role'] = "seller";

        try {
            $result = $this->auth->signUp($data);
            //dd($result);
            if($result->success){
                return redirect()->back()->with('success',"Signup successfully. Please verify your email.");
            }else{
                return redirect()->back()->with('failed',"Signup Failed.");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Signup Failed'], 400);
        }
    }

    public function ajaxEmailvarification(Request $request)
    {
        $data = [];
        $data['user_id'] = $request->user_id;
        $data['email'] = $request->email;
        $data['user_type'] = 'seller';
        $result = $this->auth->EmailVerify($data);
        return $result->success;
    }

    public function ajaxmobilevarification(Request $request)
    {
        $data = [];
        $data['user_id'] = $request->user_id;
        $data['mobile'] = $request->mobile;
        $data['user_type'] = 'seller';
        $result = $this->auth->MobileVerify($data);
        return $result;
    }
    public function verifyshopemail($request){
        try {
        $result = $this->auth->verifyemail($request);
        if($result->success){

           return redirect('index')->with('success',$result->Response);
        }else{
            return redirect('index')->with('failed','Your Email is Not Verified Please Try Again Later');
        }
      }
      catch (\Exception $e){
          return redirect('index')->with('failed' , 'Your Email is Not Verified Please Try Again Later');
      }
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => ['required', 'string', 'email'],
                'user_type' =>'required'
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data["email"]=$request->email;
        $data['user_type'] = $request->user_type;

        $result = $this->auth->forgotPassword($data);
        //dd($result);
        return $result;
        /*if($result["success"]) {
            return  redirect()->back()->with('mobile',$result["data"]["phone_number"] ?? '')->with('email',$data["email"]);
        }
        else{
            return redirect()->back()->with('message', $result["message"]);
        }*/

    }
    public function OTPverification(Request $request)
    {

        $data["otp"]=$request->otp;
        $data["email"]=$request->email;
        $result = $this->auth->OTPverification($data);
        return $result;
        /*if($result["success"]) {
            return  redirect()->back()->with('id',$result["data"]["id"] ?? '');
        }
        else{
            return redirect()->back()->with('otp-message', $result["message"]);
        }*/
    }

    public function logout(Request $request)
    {
        $result = $this->auth->logout($this->token);
        $request->session()->flush();
        return redirect()->route('signin');
    }

    public function twoWayLogin($id)
    {
        $result = $this->auth->getUserDetails($id);
        session(['token' => $id,'user_details'=>$result->data]);
        return redirect()->route('dashboard');
    }

    public function sendSignUpotp(Request $request){

        $data["phone"]=$request->phone;
        $data["country_code"]=$request->country_code;
        $data['user_type'] = 'seller';
        $result = $this->auth->SendOtpVerify($data);
        return $result;
    }
    public function verifysignupotp(Request $request){
        $data["otp"]=$request->otp;
        $data["id"]=$request->id;
        $result = $this->auth->Verifysignupotp($data);
        return $result;
    }

    public function storechange($id)
    {

        // $user = session()->get('token');
        $result = $this->dashboard->getUserDetails($id);
        session(['user_details'=>$result->data,'storeid'=>$id]);
        return redirect()->route('dashboard');
    }

    public function storyDetails($id)
    { 
        try {
            if($id == 'all'){
                $story = $this->auth->storyDetails($id);
                if($story->success){
                    $story = $story->data;                    
                    return view('stories', compact('story'));
                }else{
                    return redirect('stories', compact('story'))->with('message','No data Available');
                }    
                }else{
                $story = $this->auth->storyDetails($id);
                if($story->success){
                    $story = $story->data;
                    return view('storyDetails', compact('story'));
                }else{
                    return redirect('storyDetails', compact('story'))->with('message','No data Available');
                }
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }


}
