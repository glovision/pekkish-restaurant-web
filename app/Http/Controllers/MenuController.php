<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Menu\MenuInterface as MenuInterface;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
class MenuController extends Controller
{
    public $menu;

    public function __construct(MenuInterface $menu)
    {
        $this->menu = $menu;
    }

    public function index(Request $request)
    {

        $search = 'all';
        if ($request->has('search')){
                $search = $request->search;
        }
        //dd($search);
        $user = session()->get('user_details');
        $cuisines = $this->menu->cuisines($user->id);
        if($cuisines->success) {
            $cuisines = $cuisines->data;
        }else{
            $cuisines = [];
        }

        $categories = $this->menu->categoryProduct();
        //dd($categories);
            if ($categories->success) {
                $categories = $categories->data;
            }else{
                $categories = [];
            }

        $weekMap = [0 => 'SU',1 => 'MO',2 => 'TU',3 => 'WE',4 => 'TH',5 => 'FR',6 => 'SA'];

        $itemlist = $this->menu->itemlist($user->id,$search);
        //dd($itemlist->data);
        if($itemlist->success){
            $itemlist = $itemlist->data;

            return view('menu-new',compact('itemlist','cuisines','categories','weekMap'));
        }else{
            $itemlist = [];
            return view('menu',compact('itemlist','cuisines','categories','weekMap'))->with('message',$itemlist);
        }
    }

    public function addItem(Request $request)
    {
        //dd($request->all());
        $data=[];
        $products=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'description' => ['required', 'string'],
                'unit_price' => ['required'],
                'veg_nonveg' => ['required'],
                'set_max_order_number' => ['required'],
                'available_days'=> ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $image = $request->file('photos');
        $data['unit_price'] = $request->unit_price;
        $data['menu_items_desc'] = $request->menu_items_desc;
        $data['set_max_order_number'] = $request->set_max_order_number;
        $data['cuisine_id'] = $request->cuisine_id;
        //$data['available_ststus'] = $request->available_ststus;
        $data['category_addon'] = $request->category_addon;
        $data['Modifier'] = $request->Modifier;
        $data['recommended'] = $request->recommended;
        //$data['Modifier_name'] = $request->Modifier_name;
        //$data['optional'] = $request->optional;
        //$data['selection'] = $request->selection;
        //$data['variant'] = $request->variant;
        //$data['price'] = $request->price;
        $data['added_by'] = $user->id;
        $data['status'] = $request->available_ststus;
        $data['featured'] = ($request->featured ==1)?1:0;
        $data['healthpicks'] = ($request->healthpicks ==1)?1:0;
        $data['veg_nonveg'] = $request->veg_nonveg;
        $data['user_id'] = $user->id;
        if(is_null($request->allowed_category_add_ons)){
            $data['allowed_category_add_ons']=null;
        }else {
            $data['allowed_category_add_ons'] = json_encode($request->allowed_category_add_ons);
        }

        $products = $data;
        $products['available_days'] = json_encode($request->available_days);
        $products['tags'] = json_encode($request->recommendedproducts);
        //$variantdata = $request->variants;
//        if(is_array($request->variants)){
//                if($request->variants[0]['variant']==null){
//                    $variantdata = null;
//                }else{
//                $variantdata = json_encode($request->variants);
//                }
//            }else{
//                $variantdata = null;
//            }
//        $products['variants'] = $variantdata;
        if(is_array($request->items)) {
            $itemsdata = json_encode($request->items);
            if(count($request->items)==1){
                if($request->items[0]["ModifierName"]==null){
                    $itemsdata=NULL;
                }
            }
        }
        else{
            $itemsdata=NULL;
        }
        $products['items'] = $itemsdata;

        if($request->combo == 1){
            $products['allowed_combo_add_ons'] = json_encode($request->allowed_combo_add_ons);
        }else {
            $products['allowed_combo_add_ons']=null;
        }

        //dd($products);
        try {
            $result = $this->menu->addItem($products, $image, $token);
//dd($result);
            if($result['success']){
                return redirect()->route('menu.index')->with('success',"Item has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function update(Request $request)
    {
        //dd($request->file('photos'));
        $data=[];
        $products=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'description' => ['required', 'string'],
                'unit_price' => ['required'],
                'veg_nonveg' => ['required'],
                'set_max_order_number' => ['required'],
                'available_days'=> ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $image = $request->file('photos');
        $data['unit_price'] = $request->unit_price;
        $data['menu_items_desc'] = $request->menu_items_desc;
        $data['set_max_order_number'] = $request->set_max_order_number;
        $data['cuisine_id'] = $request->cuisine_id;
        //$data['available_ststus'] = $request->available_ststus;
        $data['Modifier'] = $request->Modifier;
        $data['Modifier_name'] = $request->Modifier_name;
        $data['optional'] = $request->optional;
        $data['selection'] = $request->selection;
        //$data['variant'] = $request->variant;
        //$data['price'] = $request->price;
        $data['added_by'] = $user->id;
        $data['status'] = $request->available_ststus;
        $data['featured'] = ($request->featured ==1)?1:0;
        $data['healthpicks'] = ($request->healthpicks ==1)?1:0;
        $data['veg_nonveg'] = $request->veg_nonveg;
        $data['user_id'] = $user->id;

        if(is_null($request->allowed_category_add_ons)){
            $data['allowed_category_add_ons']=null;
        }else {
            $data['allowed_category_add_ons'] = json_encode($request->allowed_category_add_ons);
        }

        $products = $data;
        $products['available_days'] = json_encode($request->available_days);
        $products['tags'] = json_encode($request->recommendedproducts);
        //$variantdata = json_encode($request->variants);
        //$products['variants'] = $variantdata;

        if(is_array($request->variants)){
            if($request->variants[0]['variant']==null){
                $variantdata = null;
            }else{
            $variantdata = json_encode($request->variants);
            }
        }else{
            $variantdata = null;
        }
        $products['variants'] = $variantdata;
        if(is_array($request->items)) {
            $itemsdata = json_encode($request->items);
            if(count($request->items)==1){
                if($request->items[0]["ModifierName"]==null){
                    $itemsdata=NULL;
                }
            }
        }
        else{
            $itemsdata=NULL;
        }
        $products['items'] = $itemsdata;

        if($request->combo == 1){
            $products['allowed_combo_add_ons'] = json_encode($request->allowed_combo_add_ons);
        }else {
            $products['allowed_combo_add_ons']=null;
        }

        //dd($products);
        try {
            $result = $this->menu->editSellerItem($products, $image, $token);
            if($result['success']){
                return redirect()->route('menu.index')->with('success',"Item has been updated successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to update Item'], 200);
        }

    }

    public function getProductDetails($id)
    {
        try {
            $user = session()->get('user_details');
            $token = session()->get('token');
            $result = $this->menu->getProductDetails($id, $token);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed'], 200);
        }
    }

    public function menu_destroy($id)
    {
        try {
            $user = session()->get('user_details');
            $token = session()->get('token');
            $deleteitem = $this->menu->deleteItem($id, $token);
            return redirect()->route('menu.index')->with('success',"Item has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to Delete'], 200);
        }
    }

    public function productstatus($id)
    {
       try {
           $user = session()->get('user_details');
           $token = session()->get('token');
            $status = $this->menu->product_status($id, $token);
            return redirect()->route('menu.index');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }

    public function categorymenu($id)
    {
       try {
        $user = session()->get('user_details');
        $itemlist = $this->menu->itemlist($user->id, $id);
        //dd($itemlist);
            if($itemlist->success){
                $itemlist = $itemlist->data;
                return $itemlist;
            }else{
                $itemlist = $itemlist->data;
                return $itemlist;
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function edit_category($id){
        try {

            $category = $this->menu->category_edit($id);
            return $category;

//                return $category['data'];
//            }else{
//                $category = $category['data'];
//                return $category;
//            }

        }catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function category_destroy($id)
    {
        try {
            $deleteitem = $this->menu->deleteCategory($id);
            return redirect()->route('menu.index');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

    /*New Menu functions for seller to shop migration*/

    public function indexNew(Request $request)
    {
        $cuisines = [];
        $categories = [];
        $items = [];
        $user = session()->get('user_details');
        $token = session()->get('token');
        if ($user->type == "seller") {
            $cuisines_data = $this->menu->cuisines(0, $token);
            if (isset($cuisines_data["success"])) {
                if ($cuisines_data["success"]) {
                    $cuisines = $cuisines_data["data"];
                }
            }
            $categories_data = $this->menu->sellerCategories($user->id, $token);
            if (isset($categories_data["success"])) {
                if ($categories_data["success"]) {
                    $categories = $categories_data["data"];
                }
            }
            $weekMap = [0 => 'SU', 1 => 'MO', 2 => 'TU', 3 => 'WE', 4 => 'TH', 5 => 'FR', 6 => 'SA'];

            $items_data = $this->menu->sellerItems($user->id, $token);
            if (isset($items_data["success"])) {
                if ($items_data["success"]) {
                    $items = $items_data["data"];
                    //dd($items);
                }
            }

            return view('seller-menu', compact('items', 'cuisines', 'categories', 'weekMap'));

        } else if ($user->type == "shop") {

            $cuisines_data = $this->menu->cuisines(0, $token);
            if (isset($cuisines_data["success"])) {
                if ($cuisines_data["success"]) {
                    $cuisines = $cuisines_data["data"];
                }
            }
            $categories_data = $this->menu->shopCategories($user->id, $token);           
            if (isset($categories_data["success"])) {
                if ($categories_data["success"]) {
                    $categories = $categories_data["data"];
                }
            }
            $weekMap = [0 => 'SU', 1 => 'MO', 2 => 'TU', 3 => 'WE', 4 => 'TH', 5 => 'FR', 6 => 'SA'];

            $items_data = $this->menu->shopItems($user->id, $token);
            if (isset($items_data["success"])) {
                if ($items_data["success"]) {
                    $items = $items_data["data"];
                }
            }
            //dd($items);
            return view('shop-menu', compact('items', 'cuisines', 'categories', 'weekMap'));
        }
    }


    public function sellerCategoryAddon()
    {
        try {
            $user = session()->get('user_details');
            $token = session()->get('token');
            $categories_data = $this->menu->sellerCategories($user->id, $token);
            if (isset($categories_data["success"])) {
                if ($categories_data["success"]) {
                    $categories = $categories_data["data"];
                }
            }

            $categoryaddons = $this->menu->sellerCategoryAddOns($user->id, $token);
            //dd($categoryaddons);
            if($categoryaddons['success']){
                $categoryaddons = $categoryaddons['data'];

                return view('category_addons', compact('categoryaddons', 'categories'));
            }else{
                return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function addCategoryAddOn(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(),
            [
                'section_name'    => ['required', 'string'],
                'subcategory_name' => ['required', 'string'],
                'subcategory_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=[];
        $image = "";
        $items_data=[];
        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['section_name'] = $request->section_name;
        $data['subcategory_name'] = $request->subcategory_name;
        $data['subcategory_description'] = $request->subcategory_description;
        $data['user_id'] = $user->id;
        if(is_array($request->items)){
            foreach($request->items as $item){
               if($item['items_name']!=null && $item['items_name'] != "") {
                   array_push($items_data,$item);
               }
            }
        }
        $data['items']  = $items_data;
        //dd($data);
        try {
            $result = $this->menu->addSellerCategoryAddOn($data, $image, $token);
            //dd($result);
            if($result['success']){
                return redirect()->route('seller-menu.indexNew')->withInput($request->input())->with('success',"Category Addon has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function editCategoryAddOn(Request $request){
        $validator = Validator::make($request->all(),
            [
                'id'    => ['required'],
                'section_name'    => ['required', 'string'],
                'subcategory_name' => ['required', 'string'],
                'subcategory_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data=[];
        $image = "";
        $items_data=[];
        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['section_name'] = $request->section_name;
        $data['subcategory_name'] = $request->subcategory_name;
        $data['subcategory_description'] = $request->subcategory_description;
        $data['user_id'] = $user->id;
        if(is_array($request->items)){
            foreach($request->items as $item){
                if($item['items_name']!=null && $item['items_name'] != "") {
                    array_push($items_data,$item);
                }
            }
        }
        $data['items']  = $items_data;
        //dd($data);
        try {
            $result = $this->menu->editSellerCategoryAddOn($data, $image, $token);
            //dd($result);
            if($result['success']){
                return redirect()->route('seller-menu.indexNew')->withInput($request->input())->with('success',"Category Addon has been updated successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function categoryAddonDestroy($id)
    {
        try {
            $token = session()->get('token');
            $destroycategoryaddon = $this->menu->deleteSellerCategoryAddOn($id, $token);
            return redirect()->route('seller-menu.indexNew')->with('success',"Category Addon has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

    public function addSellerCategory(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['name'] = $request->name;
        $data['user_id'] = $user->id;
        $data['related_to'] = 'seller';
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $image = $request->file('icon');

        try {
            $result = $this->menu->addSellerCategory($data, $image, $token);
            //dd($result);
            if($result['success']){
                return redirect()->route('seller-menu.indexNew')->withInput($request->input())->with('success',"Category has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function editSellerCategory($id){
        try {
            $token = session()->get('token');
            $category = $this->menu->editSellerCategory($id, $token);
            return $category;
        }catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function updateSellerCategory(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'id'    => ['required'],
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['user_id'] = $user->id;
        $data['related_to'] = 'seller';
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $image = $request->file('icon');

        try {
            $result = $this->menu->updateSellerCategory($data, $image, $token);
            if($result['success']){
                return redirect()->route('seller-menu.indexNew')->withInput($request->input())->with('success',"Category has been updated successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }

    public function sellerCategoryDestroy($id)
    {
        try {
            $token = session()->get('token');
            $destroysellercategory = $this->menu->sellerCategoryDestroy($id, $token);
            return redirect()->route('seller-menu.indexNew')->with('success',"Category has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }


    public function addShopCategory(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
                'start_time' => ['required'],
                'end_time' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['name'] = $request->name;
        $data['user_id'] = $user->id;
        $data['related_to'] = 'shop';        
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $image = $request->file('icon');

        try {
            $result = $this->menu->addShopCategory($data, $image, $token);
            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->withInput($request->input())->with('success',"Category has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function editShopCategory($id){
        try {
            $token = session()->get('token');
            $category = $this->menu->editShopCategory($id, $token);
            return $category;
        }catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function updateShopCategory(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'id'    => ['required'],
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
                'start_time' => ['required'],
                'end_time' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['user_id'] = $user->id;
        $data['related_to'] = 'shop';
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $image = $request->file('icon');

        try {
            $result = $this->menu->updateShopCategory($data, $image, $token);
            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->withInput($request->input())->with('success',"Category has been updated successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }

    public function deleteShopCategory($id)
    {
        try {
            $token = session()->get('token');
            $destroyshopcategory = $this->menu->deleteShopCategory($id, $token);
            return redirect()->route('shop-menu.indexNew')->with('success',"Category has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

    public function addSellerCategoryAddOn(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'category_id' => ['required', 'string'],
                'description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['user_id'] = $user->id;
        $data['name'] = $request->name;
        $data['category_id'] = $request->category_id;
        $data['description'] = $request->description;
        $data['addons'] = $request->addons;

        try {
            $result = $this->menu->addSellerCategoryAddOn($data, $token);
            if($result['success']){
                return redirect()->route('seller-menu.indexNew')->withInput($request->input())->with('success',"Subcategory has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }


    public function addShopItem(Request $request)
    {
        $data=[];
        $products=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'description' => ['required', 'string'],
                'unit_price' => ['required'],
                'veg_nonveg' => ['required'],
                'set_max_order_number' => ['required'],
                'available_days'=> ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $image = $request->file('photos');
        $data['unit_price'] = $request->unit_price;
        $data['menu_items_desc'] = $request->menu_items_desc;
        $data['set_max_order_number'] = $request->set_max_order_number;
        $data['cuisine_id'] = $request->cuisine_id;
        //$data['available_ststus'] = $request->available_ststus;
        $data['category_addon'] = $request->category_addon;
        $data['Modifier'] = $request->Modifier;
        $data['recommended'] = $request->recommended;
        //$data['Modifier_name'] = $request->Modifier_name;
        //$data['optional'] = $request->optional;
        //$data['selection'] = $request->selection;
        //$data['variant'] = $request->variant;
        //$data['price'] = $request->price;
        $data['added_by'] = $user->id;
        $data['status'] = $request->available_ststus;
        $data['featured'] = ($request->featured ==1)?1:0;
        $data['healthpicks'] = ($request->healthpicks ==1)?1:0;
        $data['veg_nonveg'] = $request->veg_nonveg;
        $data['user_id'] = $user->id;
        if(is_null($request->allowed_category_add_ons)){
            $data['allowed_category_add_ons']=null;
        }else {
            $data['allowed_category_add_ons'] = json_encode($request->allowed_category_add_ons);
        }

        $products = $data;
        $products['available_days'] = json_encode($request->available_days);
        $products['tags'] = json_encode($request->recommendedproducts);
        //$variantdata = $request->variants;
//        if(is_array($request->variants)){
//                if($request->variants[0]['variant']==null){
//                    $variantdata = null;
//                }else{
//                $variantdata = json_encode($request->variants);
//                }
//            }else{
//                $variantdata = null;
//            }
//        $products['variants'] = $variantdata;
        if(is_array($request->items)) {
            $itemsdata = json_encode($request->items);
            if(count($request->items)==1){
                if($request->items[0]["ModifierName"]==null){
                    $itemsdata=NULL;
                }
            }
        }
        else{
            $itemsdata=NULL;
        }
        $products['items'] = $itemsdata;

        if($request->combo == 1){
            $products['allowed_combo_add_ons'] = json_encode($request->allowed_combo_add_ons);
        }else {
            $products['allowed_combo_add_ons']=null;
        }

        //dd($products);
        try {
            $result = $this->menu->addShopItem($products, $image, $token);

            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->with('success',"Item has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function updateShopItem(Request $request)
    {
        $data=[];
        $products=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'description' => ['required', 'string'],
                'unit_price' => ['required'],
                'veg_nonveg' => ['required'],
                'set_max_order_number' => ['required'],
                'available_days'=> ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $image = $request->file('photos');
        $data['unit_price'] = $request->unit_price;
        $data['menu_items_desc'] = $request->menu_items_desc;
        $data['set_max_order_number'] = $request->set_max_order_number;
        $data['cuisine_id'] = $request->cuisine_id;
        //$data['available_ststus'] = $request->available_ststus;
        $data['Modifier'] = $request->Modifier;
        $data['Modifier_name'] = $request->Modifier_name;
        $data['optional'] = $request->optional;
        $data['selection'] = $request->selection;
        //$data['variant'] = $request->variant;
        //$data['price'] = $request->price;
        $data['added_by'] = $user->id;
        $data['status'] = $request->available_ststus;
        $data['featured'] = ($request->featured ==1)?1:0;
        $data['healthpicks'] = ($request->healthpicks ==1)?1:0;
        $data['veg_nonveg'] = $request->veg_nonveg;
        $data['user_id'] = $user->id;

        $products = $data;
        $products['available_days'] = json_encode($request->available_days);
        $products['tags'] = json_encode($request->recommendedproducts);
        //$variantdata = json_encode($request->variants);
        //$products['variants'] = $variantdata;

        if(is_array($request->variants)){
            if($request->variants[0]['variant']==null){
                $variantdata = null;
            }else{
                $variantdata = json_encode($request->variants);
            }
        }else{
            $variantdata = null;
        }
        $products['variants'] = $variantdata;
        if(is_array($request->items)) {
            $itemsdata = json_encode($request->items);
            if(count($request->items)==1){
                if($request->items[0]["ModifierName"]==null){
                    $itemsdata=NULL;
                }
            }
        }
        else{
            $itemsdata=NULL;
        }
        $products['items'] = $itemsdata;
        //dd($products);
        try {
            $result = $this->menu->editShopItem($products, $image, $token);
            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->with('success',"Item has been updated successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to update Item'], 200);
        }
    }

    public function destroyShopItem($id)
    {
            try {
                $user = session()->get('user_details');
                $token = session()->get('token');
                $deleteitem = $this->menu->deleteShopItem($id, $token);
                return redirect()->route('shop-menu.indexNew')->with('success',"Item has been deleted successfully!!!");
            }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

    public function shopCategoryAddon()
    {
        try {
            $user = session()->get('user_details');
            $token = session()->get('token');
            $categories_data = $this->menu->shopCategories($user->id, $token);
            if (isset($categories_data["success"])) {
                if ($categories_data["success"]) {
                    $categories = $categories_data["data"];
                }
            }

            $categoryaddons = $this->menu->shopCategoryAddOns($user->id, $token);
            if($categoryaddons['success']){
                $categoryaddons = $categoryaddons['data'];

                return view('category_addons', compact('categoryaddons', 'categories'));
            }else{
                return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 200);
        }
    }

    public function addShopCategoryAddOn(Request $request){
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'section_name'    => ['required', 'string'],
                'subcategory_name' => ['required', 'string'],
                'subcategory_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['section_name'] = $request->section_name;
        $data['subcategory_name'] = $request->subcategory_name;
        $data['subcategory_description'] = $request->subcategory_description;
        $data['user_id'] = $user->id;
        $items_image = $request->file('items_image');
        if(is_array($request->items)){
            if($request->items[0]['items_name']==null){
                $itemdata = null;
            }else{
                $itemdata = $request->items;
            }
        }else{
            $itemdata = null;
        }
        $data['items']  = $itemdata;
        //dd($data);
        try {
            $result = $this->menu->addShopCategoryAddOn($data, $items_image, $token);
            //dd($result);
            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->withInput($request->input())->with('success',"Category Addon has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function editShopCategoryAddOn(Request $request){
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'id'    => ['required'],
                'section_name'    => ['required', 'string'],
                'subcategory_name' => ['required', 'string'],
                'subcategory_description' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['section_name'] = $request->section_name;
        $data['subcategory_name'] = $request->subcategory_name;
        $data['subcategory_description'] = $request->subcategory_description;
        $data['user_id'] = $user->id;
        $image = ($request->file('items_image'))?'':'';
        if(is_array($request->items)){
            if($request->items[0]['items_name']==null){
                $itemdata = null;
            }else{
                $itemdata = $request->items;
            }
        }else{
            $itemdata = null;
        }
        $data['items']  = $itemdata;
        dd($data);
        try {
            $result = $this->menu->editShopCategoryAddOn($data, $image, $token);
            //dd($result);
            if($result['success']){
                return redirect()->route('shop-menu.indexNew')->withInput($request->input())->with('success',"Category Addon has been updated successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function shopCategoryAddonDestroy($id)
    {
        try {
            $token = session()->get('token');
            $destroycategoryaddon = $this->menu->deleteShopCategoryAddOn($id, $token);
            return redirect()->route('shop-menu.indexNew')->with('success',"Category Addon has been deleted successfully!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }

}
