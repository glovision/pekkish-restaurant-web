<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Dashboard\DashboardInterface as DashboardInterface;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct(DashboardInterface $dashboard)
    {
        $this->dashboard = $dashboard;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = session()->get('user_details');
        $date = date( 'Y' ) . '-01-01';
        $datework = new Carbon($date);
        $now = Carbon::now();
        $ytd = $datework->diffInDays($now);
        $days = [1 => '1D',5 => '5D', 30=> '1M',90 => '3M',180 => '6M', $ytd=>'YTD'];

        $usershops = '';
        if($user->type=='shop') {
            $result =  $this->dashboard->userShops($user->id);
            $usershops = $result->data;
                return view('dashboard', compact('user','days', 'usershops'));

        }
        if($user->type=='seller'){
           // dd($days);
            $id = $user->id;
            $shops = $this->dashboard->shops($id);
            //dd($shops);
            if($shops['success']){
                $shops = $shops['data'];
                //dd($days);
                return view('dashboard', compact('shops','days', 'usershops'));
            }else{
                //dd($days);
                //return redirect()->route('dashboard')->with('message', 'No Data Found');
                return view('dashboard', compact( 'shops'))->with('message',$shops->message);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dayWiseSalesDetails1($days)
    {
        $result = $this->dashboard->dayWiseSalesDetails($days);
        return $result;
    }
    public function dayWiseSalesDetails(Request $request)
    {
        $data =[];
        $data['shop_id'] = $request->shop_id;
        $data['days'] = $request->days;
        $result = $this->dashboard->dayWiseSalesDetails($data);
        return $result;
    }
    public function linechartDetails(Request $request)
    {
        $data =[];
        $data['shop_id'] = $request->shop_id;
        $data['days'] = $request->days;
        $result = $this->dashboard->linechartdataDetails($data);
        return $result;
    }    
    public function dashboardCountsDetails(Request $request)
    {
        $data =[];
        $data['shop_id'] = $request->shop_id;
        $data['days'] = $request->days;
        $result = $this->dashboard->dashboardCountsDetails($data);
        return $result;
    }
    public function AjaxOrdersHistoryCounts1(Request $request)
    {
        $data =[];
        $data['shop_id'] = $request->shop_id;
        $data['days'] = $request->days;
        $result = $this->dashboard->AjaxOrdersHistoryCounts1($data);
        return $result;
    }
    public function storechange($id)
    {

        $user = session()->get('token');
        $result = $this->dashboard->getUserDetails($user);
        session(['user_details'=>$result->data,'storeid'=>$id]);
        return redirect()->route('dashboard');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
