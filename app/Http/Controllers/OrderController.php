<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Order\OrderInterface as OrderInterface;
use Illuminate\Support\Facades\Validator;
class OrderController extends Controller
{
    public $order;

    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = session()->get('user_details');
        $id =$user->id;
        $orders = $this->order->orders($id);
            if($orders->success){
                $orders = $orders->data;
            }else{

                return redirect('orders')->with('message','No data Available');
            }
        $search['key'] = '';
        if ($request->has('key')){
            $search['key'] = $request->key;
        }
       // dd($request);
        $allorders = $this->order->allorders($search);
            if($allorders=='Internal server error'){
                return redirect()->back()->with('message', 'No data Available');
            }else{
                $allorders = $allorders->data;
            }
        return view('orders',compact('orders','allorders'));
    }

    public function new_orders()
    {

        $user = session()->get('user_details');
        $id =$user->id;
        $orders = $this->order->orders($id);
        //dd($orders);
        if($orders->success){
            $orders = $orders->data;
        }else{

            return response()->json(['success' => true, 'data' => $orders], 200);
        }
        //$allorders = $this->order->allorders();
        //dd($allorders);
//        if($allorders=='Internal server error'){
//            return response()->json(['success' => false, 'message' => 'No data Available','data'=>[]], 200);
//            //return redirect()->back()->with('message', 'No data Available');
//        }else{
//            $allorders = $allorders->data;
//            //dd($orders->label);
//        }
       // return $allorders;
        return response()->json(['success' => true, 'data' => $orders], 200);
       // return view('orders',compact('orders','allorders'));
    }

    public function getinvoicedtls($id)
    {
        try {
        $getorders = $this->order->getInvoiceDtls($id);
        return $getorders;
            if($getorders=='Internal server error'){
                //return response()->json(['success' => 'failed', 'message' => 'No data available'], 400);
                return '';
            }else{
                if($getorders['success']) {
                    return $getorders;
                }
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'No data available'], 400);
        }
    }
    public function history()
    {
        try {
        $user = session()->get('user_details');
        $id =$user->id;
        $orders = $this->order->orderHistory($id);
        //dd($orders);
            if($orders->success){
                $orders = $orders->data;
                return view('order_history',compact('orders'));
            }else{
                return redirect('orders')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }
    public function orderhistory()
    {
        $user = session()->get('user_details');
        $id =$user->id;                
        try {
            $result = $this->order->order_History($id);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed'], 400);
        }      
    }    
    public function FoodReady($id)
    {
        try {
            $foodreadystatus = $this->order->FoodReady($id);
             if($foodreadystatus['success']) {
                return redirect()->route('orders.index')->with('success',"Food has been prepared successfully.");
                //return redirect()->back()->with('message',$result->message);
            }else{
                return redirect()->back()->with('failed',$foodreadystatus['message']);
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function FoodDispatched($id)
    {
        try {
            $fooddispatchstatus = $this->order->FoodDispatch($id);
            if($fooddispatchstatus->success) {
                return redirect()->route('orders.index')->with('success',"Food has been dispatched successfully.");
                //return redirect()->back()->with('message',$result->message);
            }else{
                return redirect()->back()->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function food_dispatch_delivey($id){
        try {

            $fooddispatchstatus = $this->order->FoodDispatchDelivery($id);
            return $fooddispatchstatus;
//            if($fooddispatchstatus->success) {
//                return redirect()->route('orders.index')->with('success',"Food has been dispatched successfully.");
//                //return redirect()->back()->with('message',$result->message);
//            }else{
//                return redirect()->back()->with('failed',"Failed to update");
//            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function AssignOrderDriver($orderid,$id)
    {

        try {
            $assigndriver = $this->order->AssignOrderDriver($orderid,$id);
            if($assigndriver['success']) {
                return redirect()->route('orders.index')->with('success',"Assing New Driver Successfully.");
                //return redirect()->back()->with('message',$result->message);
            }else{
                return redirect()->back()->with('failed',$assigndriver['message']);
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed Assign Driver'], 400);
        }
    }
    public function AssignDriver($id)
    {

        try {
            $assigndriver = $this->order->AssignDriver($id);
            if($assigndriver['success']) {
                return redirect()->route('orders.index')->with('success',"Assing New Driver Successfully.");
                //return redirect()->back()->with('message',$result->message);
            }else{
                return redirect()->back()->with('failed',$assigndriver['message']);
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed Assign Driver'], 400);
        }
    }
    public function FoodReject(Request $request)
    {

        $data =[];
        $data['reason'] = $request->rejectstatus;
	    $data['order_id'] = $request->orderid;
	//dd($data);
        try {
            $result = $this->order->FoodReject($data);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function OrderItemReject(Request $request)
    {
        $data =[];
        $data['reason'] = $request->reason;
        $data['order_id'] = $request->order_id;
        try {
            $result = $this->order->OrderItemReject($data);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function FoodAccept(Request $request)
    {
        $data =[];
        $data['add_time'] = $request->add_time;
        $data['order_id'] = $request->orderid;
        try {
            $result = $this->order->FoodAccept($data);
            return $result;         

        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function AjaxOrdersHistoryCounts($days)
    {
        $result = $this->order->AjaxOrdersHistoryCounts($days);
        return $result;
    }

    public function AjaxTotalOrdersCounts($days)
    {
        $result = $this->order->AjaxTotalOrdersCounts($days);
        return $result;
    }

    public function get_drivers(){
        $result = $this->order->get_drivers();
        return $result;
    }
}
