<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Account\AccountInterface as AccountInterface;
use Illuminate\Support\Facades\Validator;
class AcountsController extends Controller
{
    public $account;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $salehist = $this->account->salesHistory();
            $itemizedsalehist = $this->account->ItemizedSalesHistory();

            if($salehist->success){
                $salehist = $salehist->data;
                $itemizedsalehist = $itemizedsalehist->data;
                return view('accounts',compact('salehist','itemizedsalehist'));
            }else{
                return redirect('accounts',compact('salehist','itemizedsalehist'));
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }

    public function ajaxupdateemail(Request $request)
    {
        $data =[];
        $data['email'] = $request->newemail;
        try {
            $result = $this->account->profileEmailUpdate($data);
            if($result->success) {
                return redirect()->back()->with('success',$result->message);
                //return $result;
            }else{
                return redirect()->back()->with('failed',$result->message);
               // return $result;
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }
    public function ajaxpasswordreset(Request $request)
    {

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'oldpassword' => ['required', 'string'],
                'newpassword' => ['required', 'string'],
                'confirmpassword'=>['required','string','same:newpassword']

            ]
        );
        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['oldpassword'] = $request->oldpassword;
        $data['password'] = $request->newpassword;

        try {
            $result = $this->account->PasswordReset($data);
            //return $result;
            return redirect()->back()->with('success',"Password Successfully Updated!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Password Update Failed'], 200);
        }
    }

    public function resendotp(Request $request)
    {

        $data['id'] = $request->id;
        try {
            $result = $this->account->ResendOtp($data);
            //return $result;
            return redirect()->back()->with('success',"Password Successfully Updated!!!");
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Password Update Failed'], 200);
        }
    }
    public function resetpassword(Request $request)
    {
        //dd($request);
        $data=[];
        $validator = Validator::make($request->all(),
            [
                //'id' => ['required'],
                'password' => ['required', 'string'],
                'confirmpassword'=>['required','string','same:password']
            ]
        );
        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['password'] = $request->password;
        $data['id'] = $request->id;
        try {
            $result = $this->account->ResetPassword($data);
           // return $result;
            return redirect()->back()->with('success',"Password Successfully Updated!!!");


        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Password Update Failed'], 200);
        }
    }
    public function ajaxshopscheduleoff(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'reason' => ['required'],
                'from_time' => ['required', 'string'],
                'to_time' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }
        $data['reason'] = $request->reason;;
        $data['set_off_time_key'] = 0;
        $data['from_time'] = $request->from_time;
        $data['to_time'] = $request->to_time;
        $data['status'] = 0;
        try {
            $result = $this->account->ShopScheduleoff($data);
            if($result->success){
                return redirect()->back()->with('success',"Timing has been inserted successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Signup Failed'], 200);
        }
    }

    public function ajaxshopstatuschange(Request $request)
    {
        $data=[];
       // $data['status'] = $request->status;
        $data['status'] = $request->status==1 ? 0: 1;
        try {
            $result = $this->account->ShopManualloff($data);
            return $result;
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Signup Failed'], 200);
        }
    }
    public function profupd(Request $request)
    {

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'id' => ['required'],
                'name' => ['required', 'string'],
                'last_name' => ['required', 'string'],
                'address' => ['required', 'string'],
                //'city' => ['required', 'string'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }
        $data['id'] = $request->id;;
        $data['name'] = $request->name;
        $data['last_name'] = $request->last_name;
        $data['address'] = $request->address;
      //  $data['city'] = $request->city;
        $data['avatar'] = $request->file('avatar');
        try {

            $result = $this->account->ProfileUpdate($data);
            if($result->success){
                session(['user_details'=>$result->data]);
                return redirect()->back()->with('success',"Profile has been Successfully updated");
               // session(['user_details'=>$result->data]);
              // Session::set('avatar')=$value;session('user_details')->type
              //session(['token' => $result->access_token,'user_details'=>$result->user]);
             // Session::put(('user_details')->avatar, $result->data->avatar);        
            }else{
                return redirect()->back()->with('failed',"Failed to update");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed'], 200);
        }
    }
   public function term_conditions()
    {
        //return view('term_conditions');
       try {
        //$user = session()->get('user_details');
        //$id =$user->id;
        $terms = $this->account->termsConditions();        
            if($terms->success){ //dd($terms->data[0]);
                $terms = $terms->data[0];
                return view('term_conditions',compact('terms'));
            }else{
                return redirect('term_conditions')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }

    public function terms_conditions()
    {
       try {
         $terms = $this->account->termsConditions();

            if($terms->success){ //dd($terms->data[0]);
                $terms = $terms->data[0];
                return view('terms_conditions',compact('terms'));
            }else{
                return redirect('terms_conditions')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
   }


   /* public function term_conditions()
    {
        //return view('term_conditions');
       try {
         $terms = $this->account->termsConditions();

            if($terms->success){ //dd($terms->data[0]);
                $terms = $terms->data[0];
                return view('term_conditions',compact('terms'));
            }else{
                return redirect('term_conditions')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }*/

    public function help()
    {
        //return view('help');
       try {
        $help = $this->account->help();
       // dd($help);
            if($help->success){ 
                $help = $help->data[0];
                return view('help',compact('help'));
            }else{
                return redirect('help')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }
    public function helpatlogin()
    {
       
       try {
        $help = $this->account->help();        
            if($help->success){ 
                $help = $help->data[0];
                return view('helpatlogin',compact('help'));
            }else{
                return redirect('helpatlogin')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }

    
    public function faq()
    {
       try {
         $faq = $this->account->faq();
            if($faq->success){

                $faq = $faq->data[0];
                return view('faq',compact('faq'));
            }else{
                return redirect('faq')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }
    public function faqs()
    {
       try {
         $faq = $this->account->faq();

            if($faq->success){
                $faq = $faq->data[0];
                return view('faqs',compact('faq'));
            }else{
                return redirect('faq')->with('message','No data Available');
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'No data available'], 400);
        }
    }
}
