<?php

namespace App\Http\Controllers;
use App\Repositories\Menu\MenuInterface;
use App\Repositories\Shop\ShopInterface as ShopInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class RestaurantController extends Controller
{
    public $shop;
    public $menu;

    public function __construct(ShopInterface $shop, MenuInterface $menu)
    {
        $this->shop = $shop;
        $this->menu = $menu;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('restaurants.index');
        $googleapi ='';
        $user = session()->get('user_details');
        $token = session()->get('token');
        $id = $user->id;
        $country_id = $user->country_id ?? 0;
        $cities   = $this->shop->cities($country_id);
        $googleapi_data   = $this->shop->googleapi($token);        
        if($googleapi_data->success){
            $googleapi = $googleapi_data->data;
        }
        if($cities->success){
            $cities = $cities->data->cities;

        }
        $shops = $this->shop->shops($id);
        if($shops['message']=='Success'){
            $shops = $shops['data'];

        $result = $this->shop->countrycodes();
        if ($result->success) {
            $countries = $result->data;                
        } else {
            $countries = $result->data;                
        }

            return view('restaurants.index',compact('shops','cities','countries', 'googleapi'));
        }else{
            return redirect()->back()->with('message',$shops->message);
        }
    }

    public function add()
    {
        //return view('restaurants.index');
        $googleapi = '';
        $user = session()->get('user_details');
        $token = session()->get('token');
        $id = $user->id;
        $country_id = $user->country_id ?? 0;
        $cities   = $this->shop->cities($country_id);
        $googleapi_data   = $this->shop->googleapi($token);
        //dd($googleapi);
        if($googleapi_data->success){
            $googleapi = $googleapi_data->data;
        }
        if($cities->success){
            $cities = $cities->data->cities;

        }
        $shops = $this->shop->shops($id);
        if($shops['message']=='Success'){
            $shops = $shops['data'];

        $result = $this->shop->countrycodes();
        if ($result->success) {
            $countries = $result->data;
        } else {
            $countries = $result->data;
        }
            return view('restaurants.add',compact('shops','cities','countries', 'googleapi'));
        }else{
            return redirect()->back()->with('message',$shops->message);
        }
    }

    public function store(Request $request)
    {

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'email' => ['required', 'string'],
                'phone' => ['required'],
                'address' => ['required'],
                'cityLat' => ['required'],
                'cityLng' => ['required'],
                'bank_acno' => ['required'],
                'bank_code' => ['required'],
                'bank_name' => ['required'],
                'radius' => ['required'],
                'gct'=>['required'],
                'seller_access'=>['required']
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $user->id;
        $data['name'] = $request->name;
        $data['meta_title'] = $request->shortname;
        $data['email'] = $request->email;
        $data['country_code'] = $request->country_code;
        $data['phone'] = $request->phone;
        $data['address'] = $request->address;
        $data['landmark'] = $request->landmark;
        $data['city'] = $request->city;
        $data['cityLat'] = $request->cityLat;
        $data['cityLng'] = $request->cityLng;
        $data['bank_acno'] = $request->bank_acno;
        $data['bank_code'] = $request->bank_code;
        $data['bank_name'] = $request->bank_name;
        $data['radius'] = $request->radius;
        $data['gct'] = $request->gct;
        $data['seller_access'] = $request->seller_access;
        $data['pickup'] = $request->pickup;
        $data['delivery'] = $request->delivery;
        $data['foodatwork'] = $request->food_work;
        $logo = $request->file('logo');
	try {
            $result = $this->shop->addShop($data, $logo, $token);
           //dd($result);
            if($result['success']){
                if($request->seller_access == 1){
                    $shopid = $result['data'];
                    return redirect()->route('storeTest', compact('shopid'))->with('success',"Restaurant has been inserted successfully!!!");
                }
                if($request->seller_access == 0){
                    return redirect()->route('restaurants.index')->with('success',"Restaurant has been inserted successfully!!!");
                }
            }else{
		    return redirect()->back()->with('failed',"Failed to insert Restaurant");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function edit($id)
    {
        $googleapi = '';
        $user = session()->get('user_details');
        $token = session()->get('token');
        $country_id = $user->country_id ?? 0;
        $cities   = $this->shop->cities($country_id);
        $googleapi_data   = $this->shop->googleapi($token);
        if($googleapi_data->success){
            $googleapi = $googleapi_data->data;
        }
        if($cities->success){
            $cities = $cities->data->cities;

        }
        $shop = $this->shop->editShop($id, $token);
        if($shop['message']=='Success'){
            $shop = $shop['data'];

        $result = $this->shop->countrycodes();
        if ($result->success) {
            $countries = $result->data;
        } else {
            $countries = $result->data;
        }
            return view('restaurants.edit',compact('shop','cities','countries', 'googleapi'));
        }else{
            return redirect()->back()->with('message',$shop->message);
        }
    }

    public function update(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'email' => ['required', 'string'],
                'phone' => ['required'],
                'address' => ['required'],
                'cityLat' => ['required'],
                'cityLng' => ['required'],
                'bank_acno' => ['required'],
                'bank_code' => ['required'],
                'bank_name' => ['required'],
                'radius' => ['required'],
                'seller_access' => ['required'],
                'gct'=>['required']
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = session()->get('user_details');
        $token = session()->get('token');
        $data['id'] = $request->id;
        $data['name'] = $request->name;
        $data['meta_title'] = $request->shortname;
        $data['email'] = $request->email;
        $data['country_code'] = $request->country_code;
        $data['phone'] = $request->phone;
        $data['address'] = $request->address;
        $data['cityLat'] = $request->cityLat;
        $data['cityLng'] = $request->cityLng;
        $data['landmark'] = $request->landmark;
        $data['city'] = $request->city;
        $data['bank_acno'] = $request->bank_acno;
        $data['bank_code'] = $request->bank_code;
        $data['bank_name'] = $request->bank_name;
        $data['radius'] = $request->radius;
        $data['gct'] = $request->gct;
        $data['seller_access'] = $request->seller_access;
        $data['pickup'] = $request->pickup;
        $data['delivery'] = $request->delivery;
        $data['foodatwork'] = $request->food_work;
        $logo = $request->file('logo');
        $shopid = $request->id;
        try {
            $result = $this->shop->updateShop($data, $logo, $token);
            if($result['success']){
                if($request->seller_access == 1){
                    return redirect()->route('storeTest', compact('shopid'))->with('success',"Restaurant has been updated successfully!!!");
                    //return redirect('storeTest', compact('shopid'))->with('success',"Restaurant has been updated successfully!!!");
                    //return view('storeTest',compact('data'))->with('success',"Restaurant has been updated successfully!!!");
                }
                if($request->seller_access == 0){
                    return redirect()->route('restaurants.index')->with('success',"Restaurant has been updated successfully!");
                }
            }else{
                return redirect()->back()->with('failed',"Failed to updated Restaurant");
            }

        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }

    public function shop_destroy($id)
    {
        try {
            $deleteshop = $this->shop->deleteShop($id);
            return redirect()->route('restaurants.index');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to delete shop'], 400);
        }
    }
    public function EmailIdvarification(Request $request)
    {
        $data = [];
        $data['user_id'] = $request->user_id;
        $data['email'] = $request->email;
        $result = $this->shop->EmailVerify($data);
        return $result;
       // return $result->success;
    }
    public function dutyHistoryStatus()
    {
        $result = $this->shop->dutyHistoryStatus();
        return $result;
    }

    public function storeTest(Request $request)
    {
        $cuisines = [];
        $categories = [];
        $items = [];
        $user = session()->get('user_details');
        $token = session()->get('token');

            $cuisines_data = $this->menu->cuisines(0, $token);
            if (isset($cuisines_data["success"])) {
                if ($cuisines_data["success"]) {
                    $cuisines = $cuisines_data["data"];
                }
            }
            $categories_data = $this->menu->sellerCategories($user->id, $token);
            if (isset($categories_data["success"])) {
                if ($categories_data["success"]) {
                    $categories = $categories_data["data"];
                }
            }
            $weekMap = [0 => 'SU', 1 => 'MO', 2 => 'TU', 3 => 'WE', 4 => 'TH', 5 => 'FR', 6 => 'SA'];

            $items_data = $this->menu->sellerItems($user->id, $token);
            if (isset($items_data["success"])) {
                if ($items_data["success"]) {
                    $items = $items_data["data"];
                    //dd($items);
                }
            }

            return view('add_menu_to_restaurant', compact('items', 'cuisines', 'categories', 'weekMap'));
    }

    public function addSellerItemsToStore(Request $request){
        try {
            $data = [];
            $user = session()->get('user_details');
            $token = session()->get('token');
            $data['shopid'] = $request->shopid;
            $data['category'] = $request->category;
            $data['item_override'] = $request->item_override;
            $data['item_access'] = $request->item_access;
            $data['add_on_access'] = $request->add_on_access;
            $data['add_on_override'] = $request->add_on_override;
            $data['user_id'] = $user->id;
            $result = $this->shop->addSellerItemsToStore($data, $token);
            if($result['success']){
                    return redirect()->route('restaurants.index')->with('success',"Restaurant items has been updated successfully!");
            }else{
                return redirect()->back()->with('failed',"Failed to updated Restaurant items ");
            }

        }catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }
    }


}
