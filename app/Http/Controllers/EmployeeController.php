<?php

namespace App\Http\Controllers;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->users();
        //dd($users);
        if($users['success']){
            $users = $users['data'];
        }else{
            return redirect('employees.index')->with('message','No data Available');
        }
        $user = session()->get('user_details');
        $id =$user->id;
        $type = $user->type;
        $shops = $this->user->shopslist($id);
        //dd($shops);
        if($shops['success']){
            $shops = $shops['data'];
        }
        $roles = $this->user->roles($id);

	if($roles['success']){
            $roles = $roles['data'];
        }else{
	    $roles = [];
    }

        return view('employees.index', compact('users','roles', 'shops','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $user = session()->get('user_details');
        if($user->type == "shop"){
            $request->shop_id = array($user->id);
        }
        $validator = Validator::make($request->all(),
            [
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                'emailid' => ['required'],
                'phone' => ['required'],
                'role_id' => ['required'],
                //'shop_id' => ['required'] ,
                'joiningdate' => ['required']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['phone'] = $request->phone;
        $data['email'] = $request->emailid;
        $data['role_id'] = $request->role_id;
        $data['shop_id'] = $request->shop_id;
        $data['joining_date'] = $request->joiningdate;

       try {
           $result = $this->user->addUser($data);           
           if($result['success']){
               return redirect()->route('employees.index')->with('success',"User has been inserted successfully!!!");
           }else{
               return redirect()->back()->with('failed',"Failed insert");
           }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Insert Failed'], 400);
        }
    }


    public function edit(Request $request)
    {
        $data = [];
        $user = session()->get('user_details');
        if($user->type == 'shop'){
            $request->shop_id = array($user->id);
        }
        $validator = Validator::make($request->all(),
            [
                'firstname' => ['required', 'string'],
                'lastname' => ['required', 'string'],
                //'emailid' => ['required'],
                //'phone' => ['required'],
                'role_id' => ['required'],
                //'shop_id' => ['required'] ,
                'joiningdate' => ['required']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data['id'] = $request->id;
        $data['firstname'] = $request->firstname;
        $data['lastname'] = $request->lastname;
        $data['phone'] = $request->phone;
        $data['email'] = $request->emailid;
        $data['role_id'] = $request->role_id;
        $data['shop_id'] = $request->shop_id;
        $data['joining_date'] = $request->joiningdate;

        try {
            $result = $this->user->EditUser($data);

            if($result['success']){
                return redirect()->route('employees.index')->with('success',"User has been updated successfully!!!");
            }else{
                return redirect()->back()->with('failed',"Failed Update");
            }
        }
        catch (Exception $e){
            return response()->json(['success' => 'failed', 'message' => 'Failed to add User'], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function users_destroy($id)
    {
        try {
            $adduser = $this->user->deleteUser($id);
            return redirect()->route('employees.index');
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add User'], 200);
        }
    }
    public function userEmailIdvarification(Request $request)
    {
        $data = [];
        $data['user_id'] = $request->user_id;
        $data['email'] = $request->email;
        $result = $this->user->EmailVerify($data);
        return $result->success;
    }
    public function ajaxuserpasswordreset(Request $request)
    {
        $data = [];
        $data['id'] = $request->userid;
        $result = $this->user->UserPasswordReset($data);
        return $result->success;
    }
}
