<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Category\CategoryInterface as CategoryInterface;
use Illuminate\Support\Facades\Validator;
class CategoryController extends Controller
{
    public $category;

    public function __construct(CategoryInterface $category)
    {
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_category(Request $request)
    {
        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
                'start_time' => ['required'],
                'end_time' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $data['name'] = $request->name;
        $data['shop_id'] = $user->id;
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['digital'] = 0;
        //$data['banner'] = $request->file('banner');
       // $data['icon'] = $request->file('icon');
        //return redirect()->back()->withInput($request->input())->with('error_code', 5);
        //dd($data);
        try {
            $result = $this->category->add_category($data);
           // dd($result);
            if($result['success']){
                return redirect()->route('menu.index')->withInput($request->input())->with('success',"Category has been inserted successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    public function update_category(Request $request)
    {

        $data=[];
        $validator = Validator::make($request->all(),
            [
                'name'    => ['required', 'string'],
                'meta_title' => ['required', 'string'],
                'meta_description' => ['required', 'string'],
                'start_time' => ['required'],
                'end_time' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = session()->get('user_details');
        $data['name'] = $request->name;
        $data['shop_id'] = $user->id;
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['digital'] = 0;
        $data['id'] = $request->id;
        //dd($request);
        //$data['banner'] = $request->file('banner');
        // $data['icon'] = $request->file('icon');
        //return redirect()->back()->withInput($request->input())->with('error_code', 5);
        //dd($data);
        try {
            $result = $this->category->update_category($data);
             //dd($result);
            if($result['success']){
                return redirect()->route('menu.index')->withInput($request->input())->with('success',"Category has been updated successfully!!!");
            }else{
                return redirect()->back()->withInput($request->input())->with('failed',"Failed to insert");
            }
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to add Item'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
