<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Timing\TimingInterface as TimingInterface;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class SettingController extends Controller
{
    public $timing;

    public function __construct(TimingInterface $timing)
    {
        $this->timing = $timing;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timings = $this->timing->getTiming();
        $timings = $timings['data'];
//dd($timings);
       // return view('time_settings');
        return view('time_settings',compact('timings'));
    }

    public function average_time(Request $request)
    {
        $data=[];
        /*$validator = Validator::make($request->all(),
            [
                'go_average_time' => ['required'],
                'ordertype' => ['required'],
            ]
        );
        if ($validator->fails()) {
            return redirect()->back();
        }*/
        $data['go_average_time'] = $request->go_average_time;
        $data['ordertype'] = $request->ordertype;

        try {
            $result = $this->timing->averageTime($data);
            //return redirect()->route('time_settings.')->with('message', 'Time has been inserted successfully!!!');
            return "Success";
        }
        catch (Exception $e){
            return response()->json(['Status' => 'failed', 'message' => 'Failed to Update'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notifications(){
      try {
          $notifications = $this->timing->notifications();
          if ($notifications->success) {
              //dd($notifications);
              $notifications = $notifications->data;
                //dd($notifications);
              return view('notifications', compact('notifications'));
          } else {
              $notifications = [];
              return view('notifications', compact('notifications'));
          }
      }catch (Exception $e){
          $notifications = [];
          return view('notifications', compact(
              'notifications'));
      }
    }
}
