<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Businesshour;
use App\Repositories\Businesshour\BusinesshourInterface as BusinesshourInterface;
use Illuminate\Support\Facades\Http;

class BusinesshourRepository implements BusinesshourInterface
{
    public function shopTiming()
    {
       //dd(getUrl('SHOPTIMING'));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOPTIMING'));
        $result = $response->json();
        return $result;
    }
    public function dutyHistory()
    {
       // dd(getUrl('ADDBUSINESSHOUR'),json_encode($data));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DUTYHISTORY'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
       if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addBusinesshour($data)
    {
        //dd(getUrl('ADDBUSINESSHOUR'),json_encode($data));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDBUSINESSHOUR'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
       if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

    public function shopAvailableStatusChange($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('SHOPAVAILABLETATUS'), $data);
        $result = $response->json();
        return $result;
    }
}
