<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Businesshour;


interface BusinesshourInterface
{
    public function shopTiming();
    public function dutyHistory();
    public function addBusinesshour($data);
    public function shopAvailableStatusChange($data);

}
