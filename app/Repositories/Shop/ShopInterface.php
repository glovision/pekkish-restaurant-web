<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Shop;


interface ShopInterface
{
    public function shops($id);
    public function addShop($data, $logo, $token);
    public function editShop($id, $token);
    public function updateShop($data, $logo, $token);
    public function deleteShop($id);
    public function EmailVerify($data);
    public function dutyHistoryStatus();
    public function  cities($id);
    public function countrycodes();
    public function googleapi($token);
    public function addSellerItemsToStore($data, $token);
}
