<?php
/**
 * Created by PhpStorm.
 * User: vyadati
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Shop;
use App\Repositories\Shop\ShopInterface as ShopInterface;
use Illuminate\Support\Facades\Http;

class ShopRepository implements ShopInterface
{
    public function shops($id)
    {
       $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOPS').'/'.$id);
        $result = $response->json();
        if($result['message'] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function addShop($data, $logo, $token)
    {
        //dd(getUrl('ADDSHOP'),$data);
        if($logo == ""){
            $response = Http::withToken($token)->post(getUrl('ADDSHOP'),$data);
        }else{
            $logo = fopen($logo, 'r');
            $response = Http::withToken($token)->attach('logo', $logo, 'logo.jpg')->post(getUrl('ADDSHOP'), $data);
        }
        $result = $response->json();
        return $result;
    }
    public function editShop($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('EDITSHOP').'/'.$id);
        $result = $response->json();      
        return $result;
       
    }
    public function updateShop($data, $logo, $token)
    {
        //dd(getUrl('UPDATESHOP'),$data);
        if($logo == ""){
            $response = Http::withToken($token)->post(getUrl('UPDATESHOP'),$data);
        }else{
            $logo = fopen($logo, 'r');
            $response = Http::withToken($token)->attach('logo', $logo, 'logo.jpg')->post(getUrl('UPDATESHOP'), $data);
        }

        $result = $response->json();      
        return $result;
       
    }
    public function deleteShop($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DELETESHOP').'/'.$id);
        $result = $response->json();
        //dd($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function EmailVerify($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->POST(getUrl('EMAILVERIFY'), $data);

        $result = $response->json();
        //return $result;
//        $result = json_encode($result);
//        $result =json_decode($result);
        return $result;
    }
    public function dutyHistoryStatus()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DUTYHISTORYSTATUS'));
        $result = $response->json();
        return $result;
    }

    public function cities($id){

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('CITIES').'/'.$id);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function countrycodes()
    {
        $response = Http::get(getUrl('COUNTRYCODES'));
        $result=$response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;

    }
    public function googleapi($token)
    {       
        $response = Http::withToken($token)->get(getUrl('GOOGLEAPI'));
        $result=$response->json();
        //dd($result);
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;

    }
    public function addSellerItemsToStore($data, $token)
    {
        $response = Http::withToken($token)->post(getUrl('ADDSELLERITEMSTOSTORE'), $data);
        $result = $response->json();
        return $result;
    }



}
