<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Category;
use App\Repositories\Category\CategoryInterface as CategoryInterface;
use Illuminate\Support\Facades\Http;

class CategoryRepository implements CategoryInterface
{
    public function add_category($data)
    {
        //dd(getUrl('CATEGORYADD'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token')
        ])->post(getUrl('CATEGORYADD'),$data);
        $result = $response->json();
        //dd($result);
        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function update_category($data)
    {
        //dd(getUrl('CATEGORYADD'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token')
        ])->post(getUrl('CATEGORYUPDATE'),$data);
        $result = $response->json();

        if($result["message"] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function rating()
    {
        $user = session()->get('user_details');
       // dd(getUrl('RATINGS'));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('RATINGS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result->success);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}
