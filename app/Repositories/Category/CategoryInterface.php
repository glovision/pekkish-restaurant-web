<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Category;


interface CategoryInterface
{
   public function add_category($data);
   public function rating();
   public function update_category($data);
}
