<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Menu;

use App\Repositories\Menu\MenuInterface as MenuInterface;
use Illuminate\Support\Facades\Http;

class MenuRepository implements MenuInterface
{

    public function itemlist1($id, $categoryid)

    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('ITEMLIST') . '/' . $id/*.'/'.$categoryid*/);
        $result = $response->json();

        $result = json_encode($result);
        $result = json_decode($result);
        if ($result) {
            return $result;
        } else {
            return "";
        }
    }

    public function cuisines1($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('CUISINES') . '/' . $id);
        $result = $response->json();
        $result = json_encode($result);
        $result = json_decode($result);
        //dd($result);
        if ($result) {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    public function category1($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('CATEGORIES') . '/' . $id);
        $result = $response->json();
        $result = json_encode($result);
        $result = json_decode($result);
        if ($result) {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    public function addItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('ADDSELLERITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photo.jpg')
                ->post(getUrl('ADDSELLERITEM'), $data);
        }
        $result = $response->json();
        return $result;

    }

    public function EditItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('EDITSELLERITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photos.jpeg')
                ->post(getUrl('EDITSELLERITEM'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function deleteItem($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETEITEM') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function product_status1($id, $token)
    {
        $pid = explode("@", $id);
        $response = Http::withToken($token)->get(getUrl('PRODUCT_STATUS') . '/' . $pid[0] . '/' . $pid[1]);
        $result = $response->json();
        if ($result["message"] == "Success") {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    public function categoryProduct1()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->post(getUrl('CATEGORYPRODUCT'));
        $result = $response->json();
        $result = json_encode($result);
        $result = json_decode($result);
        if ($result) {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    public function category_edit1($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('CATEGORYEDIT') . '/' . $id);
        $result = $response->json();
        // return $result;
//        $result = json_encode($result);
//        $result =json_decode($result);
//        //dd($result);
        if ($result) {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    public function deleteCategory1($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('DELETECATEGORY') . '/' . $id);
        $result = $response->json();
        //dd($result);
        if ($result) {
            return $result;
        } else {
            return "Internal server error";
        }
    }

    /*New functions modified*/
    public function product_status($id, $token)
    {
        $pid = explode("@", $id);
        $response = Http::withToken($token)->get(getUrl('PRODUCT_STATUS') . '/' . $pid[0] . '/' . $pid[1]);
        $result = $response->json();
        return $result;
    }

    public function cuisines($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('CUISINES') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function sellerItems($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SELLERITEMS') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function shopItems($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SHOPITEMS') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function addSellerItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('ADDSELLERITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photo.jpg')->post(getUrl('ADDSELLERITEM'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function editSellerItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('EDITSELLERITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photo.jpg')->post(getUrl('EDITSELLERITEM'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function addShopItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('ADDSHOPITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photo.jpg')->post(getUrl('ADDSHOPITEM'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function editShopItem($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('EDITSHOPITEM'), $data);
        } else {
            $photo = fopen($image, 'r');
            $response = Http::withToken($token)->attach('photos', $photo, 'photo.jpg')->post(getUrl('EDITSHOPITEM'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function deleteSellerItem($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESELLERITEM') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function getProductDetails($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('GETPRODUCTDETAILS') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function deleteShopItem($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESHOPITEM') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function shopCategories($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SHOPCATEGORIES') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function sellerCategories($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SELLERCATEGORIES') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function addSellerCategory($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('ADDSELLERCATEGORY'), $data);
        } else {
            $icon = fopen($image, 'r');
            $response = Http::withToken($token)->attach('icon', $icon, 'icon.jpg')->post(getUrl('ADDSELLERCATEGORY'), $data);
        }
        $result = $response->json();
        return $result;
    }

    public function addShopCategory($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('ADDSHOPCATEGORY'), $data);
        } else {
            $icon = fopen($image, 'r');
            $response = Http::withToken($token)->attach('icon', $icon, 'icon.jpg')->post(getUrl('ADDSHOPCATEGORY'), $data);
        }
        $result = $response->json();
        return $result;
    }
    
    public function editSellerCategory($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('EDITSELLERCATEGORY') . '/' . $id);
        $result = $response->json();
        return $result;
    }
    public function updateSellerCategory($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('UPDATESELLERCATEGORY'), $data);
        } else {
            $icon = fopen($image, 'r');
            $response = Http::withToken($token)->attach('icon', $icon, 'icon.jpg')->post(getUrl('UPDATESELLERCATEGORY'), $data);
        }
        $result = $response->json();
        return $result;
    }

public function sellerCategoryDestroy($id, $token)
{
    $response = Http::withToken($token)->get(getUrl('SELLERCATEGORYDESTROY') . '/' . $id);
    $result = $response->json();   
    return $result;
}

    public function editShopCategory($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('EDITSHOPCATEGORY') . '/' . $id);
        $result = $response->json();
        return $result;
    }
    public function updateShopCategory($data, $image, $token)
    {
        if ($image == "") {
            $response = Http::withToken($token)->post(getUrl('UPDATESHOPCATEGORY'), $data);
        } else {
            $icon = fopen($image, 'r');
            $response = Http::withToken($token)->attach('icon', $icon, 'icon.jpg')->post(getUrl('UPDATESHOPCATEGORY'), $data);
        }
        $result = $response->json();
        return $result;
    }
    public function deleteSellerCategory($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESELLERCATEGORY') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function deleteShopCategory($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESHOPCATEGORY') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function shopCategoryAddOns($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SHOPCATEGORYADDONS') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function addShopCategoryAddOn($data, $image, $token)
    {
        $items=[];
        $items_data=[];
        $response = Http::withToken($token);
        foreach($data["items"] as $key=>$item){
            if(isset($item["items_image"]) && file_exists($item["items_image"])){
                $photo = fopen($item["items_image"], 'r');
                $response = $response->attach('sub_add_on_images['.$key.']',$photo);
            }
            $items["items_name"]=$item["items_name"];
            $items["price"]=$item["price"];
            $items["discount"]=$item["discount"];
            array_push($items_data,$items);
        }
        $data["items"]=json_encode($items_data);
        $response = $response->post(getUrl('ADDSHOPCATEGORYADDON'), $data);
        $result = $response->json();
        //dd($result);
        return $result;
    }

    public function editShopCategoryAddOn($data, $image, $token)
    {
        $items=[];
        $items_data=[];
        $response = Http::withToken($token);
        foreach($data["items"] as $key=>$item){
            if(isset($item["items_image"]) && file_exists($item["items_image"])){
                $photo = fopen($item["items_image"], 'r');
                $response = $response->attach('sub_add_on_images['.$key.']',$photo);
            }
            $items["items_id"]=$item["items_id"];
            $items["items_name"]=$item["items_name"];
            $items["price"]=$item["price"];
            $items["discount"]=$item["discount"];
            array_push($items_data,$items);
        }
        //dd($items_data);
        $data["items"]=json_encode($items_data);
        $response = $response->post(getUrl('EDITSHOPCATEGORYADDON'), $data);

        $result = $response->json();
        //dd($result);
        return $result;
    }

    public function deleteShopCategoryAddOn($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESHOPCATEGORYADDON') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function sellerCategoryAddOns($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('SELLERADDONS') . '/' . $id);
        $result = $response->json();
        return $result;
    }

    public function addSellerCategoryAddOn($data, $image, $token)
    {
        $items=[];
        $items_data=[];
        $response = Http::withToken($token);
        foreach($data["items"] as $key=>$item){
            if(isset($item["items_image"]) && file_exists($item["items_image"])){
                $photo = fopen($item["items_image"], 'r');
                $response = $response->attach('sub_add_on_images['.$key.']',$photo);
            }
            $items["items_name"]=$item["items_name"];
            $items["price"]=$item["price"];
            $items["discount"]=$item["discount"];
            array_push($items_data,$items);
        }
        $data["items"]=json_encode($items_data);
        $response = $response->post(getUrl('ADDSELLERCATEGORYADDON'), $data);
        $result = $response->json();
        //dd($result);
        return $result;
    }
    public function editSellerCategoryAddOn($data, $image, $token)
    {
        $items=[];
        $items_data=[];
        $response = Http::withToken($token);
        foreach($data["items"] as $key=>$item){
            if(isset($item["items_image"]) && file_exists($item["items_image"])){
                $photo = fopen($item["items_image"], 'r');
                $response = $response->attach('sub_add_on_images['.$key.']',$photo);
            }
            $items["items_id"]=$item["items_id"];
            $items["items_name"]=$item["items_name"];
            $items["price"]=$item["price"];
            $items["discount"]=$item["discount"];
            array_push($items_data,$items);
        }
        //dd($items_data);
        $data["items"]=json_encode($items_data);
        $response = $response->post(getUrl('EDITSELLERCATEGORYADDON'), $data);

        $result = $response->json();
        //dd($result);
        return $result;
    }

    public function deleteSellerCategoryAddOn($id, $token)
    {
        $response = Http::withToken($token)->get(getUrl('DELETESELLERCATEGORYADDON') . '/' . $id);
        $result = $response->json();
        return $result;
    }

}
