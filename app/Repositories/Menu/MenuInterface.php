<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Menu;


interface MenuInterface
{

    public function itemlist1($id, $search);

    public function cuisines1($id);

    public function addItem($data, $image, $token);

    public function EditItem($data, $image, $token);

    public function category1($data);

    public function categoryProduct1();

    public function product_status1($id, $token);

    public function deleteItem($id, $token);

    public function category_edit1($id);

    public function deleteCategory1($id);

    /**/
    public function product_status($id, $token);

    public function cuisines($id, $token);

    public function sellerItems($id, $token);

    public function shopItems($id, $token);

    public function addSellerItem($data, $image, $token);

    public function addShopItem($data, $image, $token);

    public function editSellerItem($data, $image, $token);

    public function getProductDetails($id, $token);

    public function editShopItem($data, $image, $token);

    public function deleteSellerItem($id, $token);

    public function deleteShopItem($id, $token);

    public function shopCategories($id, $token);

    public function sellerCategories($id, $token);

    public function addSellerCategory($data, $image, $token);

    public function addShopCategory($data, $image, $token);

    public function editSellerCategory($id, $token);

    public function updateSellerCategory($data, $image, $token);
    
    public function sellerCategoryDestroy($id, $token);

    public function editShopCategory($id, $token);

    public function updateShopCategory($data, $image, $token);

    public function deleteSellerCategory($id, $token);

    public function deleteShopCategory($id, $token);

    public function shopCategoryAddOns($id, $token);

    public function sellerCategoryAddOns($id, $token);

    public function addSellerCategoryAddOn($data, $image, $token);

    public function addShopCategoryAddOn($data, $image, $token);

    public function editSellerCategoryAddOn($data, $image, $token);

    public function editShopCategoryAddOn($data, $image, $token);

    public function deleteSellerCategoryAddOn($id, $token);

    public function deleteShopCategoryAddOn($id, $token);

}
