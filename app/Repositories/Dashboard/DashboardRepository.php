<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Dashboard;
use App\Repositories\Dashboard\DashboardInterface as DashboardInterface;
use Illuminate\Support\Facades\Http;
use Session;

class DashboardRepository implements DashboardInterface
{
    public function rating()
    {
        $user = session()->get('user_details');
        //dd(getUrl('RATINGS'));
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('RATINGS'));
        //$response = Http::withToken(session()->get('token'))->get(getUrl('RATINGS'));
        $result = $response->json();

        $result = json_encode($result);
        $result =json_decode($result);
           return $result;

    }
    public function dayWiseSalesDetails1($days)
    {
        $user = session()->get('user_details');
        //dd(getUrl('DAYWISESALEDTLS').'/'.$days);  
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DAYWISESALEDTLS').'/'.$days);       
        $result = $response->json();
        return $result;    
    }
    public function dayWiseSalesDetails($data)
    {
        $user = session()->get('user_details');
        //dd(getUrl('DAYWISESALEDTLS').'/'.$days);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('DAYWISESALEDTLS'),$data);
        $result = $response->json();
        return $result;
    }
    public function linechartdataDetails($data)
    {       
        //dd(getUrl('LINECHARTORDERDATA').'/'.$days);  
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('LINECHARTORDERDATA'),$data);
        $result = $response->json();
        return $result;    
    }
    public function dashboardCountsDetails($data)
    {
        $user = session()->get('user_details');
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('DASHBOARDCOUNTDTLS'),$data);
        $result = $response->json();
        return $result;    
    }
    public function tblfeatures()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('TBLFEATURES'));
        $result = $response->json();
        //dd($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function roles()
    {
        $user = session()->get('user_details');
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERROLES'));
        $result = $response->json();
        //dd($result);
        return $result;
    }

    public function shops($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOPS').'/'.$id);
        //$response = Http::get(getUrl('SHOPS').'/'.$id);
        $result = $response->json();
        /* $result = json_encode($result);
         $result =json_decode($result);*/
        //dd($result);
        if($result['message'] == "Success") {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function AjaxOrdersHistoryCounts1($data){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ORDERHISTORYCOUNT1'),$data);
        $result = $response->json();
        return $result;
    }
    public function userShops($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERSHOPS').'/'.$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function getUserDetails($data)
    {
        $response = Http::withHeaders([
            //'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERDETAILS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
}
