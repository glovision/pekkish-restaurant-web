<?php
    /**
     * Created by PhpStorm.
     * User: venkat
     * Date: 22-03-2021
     * Time: 09:16
     */

namespace App\Repositories\Dashboard;


interface DashboardInterface
{
    public function rating();

    public function dayWiseSalesDetails($data);

    public function dashboardCountsDetails($data);

    public function linechartdataDetails($data);

    public function tblfeatures();

    public function roles();

    public function shops($id);

    public function AjaxOrdersHistoryCounts1($data);

    public function userShops($data);
    
    public function getUserDetails($data);
}
