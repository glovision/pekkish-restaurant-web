<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Order;
use App\Repositories\Order\OrderInterface as OrderInterface;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Http;

class OrderRepository implements OrderInterface
{
    public function orders($id)
    {
        $image_type = 'svg';
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
       // dd($result);
            return $result;
    }
    public function allorders($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ORDERS_STATUS'),$id);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function getInvoiceDtls($id)
    {//return getUrl('GETINVOICEDETAILS').'/'.$id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('GETINVOICEDETAILS').'/'.$id);
       // return getUrl('GETINVOICEDETAILS').'/'.$id;
        $result = $response->json();

//        $result = json_encode($result);
//        $result =json_decode($result);
        return $result;
//       if($result->success) {
//            return $result;
//        }
//        else{
//            return "Internal server error";
//        }
    }

    public function orderHistory($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERHISTORY'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
       //dd($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function order_History($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERHISTORY'));
        $result = $response->json();
        return $result;
    }
    public function FoodReady($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('FOODREADY').'/'.$id);
        $result = $response->json();
        return $result;
    }
    public function FoodDispatch($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('FOODDISPATCH').'/'.$id);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function FoodReject($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('FOODREJECT'),$data);
        $result = $response->json();
        return $result;
    }
    public function OrderItemReject($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ORDERITEMREJECT'),$data);

        $result = $response->json();
        return $result;
    }
    public function FoodAccept($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('FOODACCEPT'),$data);
        $result = $response->json();
        return $result;
    }
    public function AssignOrderDriver($orderid,$id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('ASSIGNDRIVER').'/'.$orderid.'/'.$id);
        $result = $response->json();
        return $result;
    }
    public function AssignDriver($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('ASSIGNDRIVER').'/'.$id);
        $result = $response->json();
        return $result;
    }
    public function AjaxOrdersHistoryCounts($days)
    {
        $user = session()->get('user_details');
       // return getUrl('ORDERHISTORYCOUNT').'/'.$days;  
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERHISTORYCOUNT').'/'.$days);
        $result = $response->json();
        return $result;    
    }
    public function AjaxTotalOrdersCounts($days)
    {
        $user = session()->get('user_details');
        $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ORDERCOUNT').'/'.$days);       
        $result = $response->json();
        return $result;    
    }

    public function AjaxTotalOrdersCounts1($data)
    {
       $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ORDERHISTORYCOUNT'),$data);
        $result = $response->json();
        return $result;
    }


    public function get_drivers(){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('GETDRIVERS'));
        $result = $response->json();
        return $result;
    }
    public function FoodDispatchDelivery($id){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->POST(getUrl('FOODDISPATCHDELIVERY').'/'.$id);
       // return getUrl('FOODDISPATCHDELIVERY').'/'.$id;
        $result = $response->json();
        return $result;
    }
}
