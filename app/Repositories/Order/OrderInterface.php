<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Order;


interface OrderInterface
{
    public function orders($id);
    public function allorders($id);
    public function getInvoiceDtls($id);
    public function orderHistory($id);
    public function FoodReady($id);
    public function FoodDispatch($id);
    public function FoodReject($data);
    public function OrderItemReject($data);
    public function FoodAccept($data);
    public function AssignOrderDriver($orderid,$id);
    public function AjaxOrdersHistoryCounts($days);
    public function AjaxTotalOrdersCounts($days);
    public function AjaxTotalOrdersCounts1($data);
    public function get_drivers();
    public function AssignDriver($id);
    public function FoodDispatchDelivery($id);
    public function order_History($id);
}
