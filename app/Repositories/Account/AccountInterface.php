<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Account;


interface AccountInterface
{
    public function profileEmailUpdate($data);
    public function PasswordReset($data);
    public function ShopScheduleoff($data);
    public function salesHistory();
    public function ItemizedSalesHistory();
    public function ShopManualloff($data);
    public function ProfileUpdate($data);
    public function termsConditions();
    public function help();
    public function faq();
    public function ResetPassword($data);
    public function ResendOtp($data);

}
