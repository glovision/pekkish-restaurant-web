<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Account;
use App\Repositories\Account\AccountInterface as AccountInterface;
use Illuminate\Support\Facades\Http;

class AccountRepository implements AccountInterface
{
    public function salesHistory()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SALESHISTORY'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        return $result;
    }
    public function ItemizedSalesHistory()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ITEMIZEDSALESHISTORY'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        return $result;
    }
    public function profileEmailUpdate($data)
    {
        //dd(getUrl('PROFEMAILUPDATE'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('PROFEMAILUPDATE'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
            return $result;
    }
    public function PasswordReset($data)
    {//dd(getUrl('PASSWORDRESET'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('PASSWORDRESET'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function ShopScheduleoff($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('SHOPSCHEDULEOFF'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function ShopManualloff($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('SHOPMANUALOFF'),$data);
        $result = $response->json();
        return $result;
    }
    public function ProfileUpdate($data)
    {
        //dd(getUrl('PROFILEUPDATE'),$data);
        if($data["avatar"]=="") {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . session()->get('token')
            ])->post(getUrl('PROFILEUPDATE'), $data);
        }else {
            $avatar = fopen($data["avatar"], 'r');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . session()->get('token')
            ])->attach('avatar',$avatar, 'avatar.jpg')
                ->post(getUrl('PROFILEUPDATE'), $data);
        }
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function help()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('HELP'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
       // dd($result);
        return $result;
    }
    public function faq()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('FAQ'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);

        return $result;
    }
   public function termsConditions()
   {
       $response = Http::withHeaders([
           'Authorization' => 'Bearer '.session()->get('token'),
           'Content-Type' =>'application/json',
       ])->get(getUrl('TERMCONDITIONS'));
       $result = $response->json();
       $result = json_encode($result);
       $result =json_decode($result);
       //dd($result);
       return $result;
   }

    public function ResetPassword($data)
    {//dd(getUrl('PASSWORDRESET'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('RESETPASSWORD'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function ResendOtp($data){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('RESENDOTP'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}
