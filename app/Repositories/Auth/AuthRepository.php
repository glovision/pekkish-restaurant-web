<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Auth;
use App\Repositories\Auth\AuthInterface as AuthInterface;
use Illuminate\Support\Facades\Http;
class AuthRepository implements AuthInterface
{
    public function signIn($data)
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->POST(getUrl('SIGNIN'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function two_way_signin($data)
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->POST(getUrl('TWO_WAY_SIGNIN'),$data);
        $result = $response->json();

//        $result = json_encode($result);
//        $result =json_decode($result);
        return $result;
    }
    public function signUp($data)
    {
       //dd(getUrl('SIGNUP'),$data);
        $response = Http::POST(getUrl('SIGNUP'),$data);
        $result=$response->json();
        //return $result;
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function EmailVerify($id)
    {
        $response = Http::POST(getUrl('EMAILVERIFY'),$id);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function MobileVerify($id)
    {
        $response = Http::POST(getUrl('MOBILEVERIFY'),$id);
        $result = $response->json();
        return $result;
//        $result = json_encode($result);
//        $result =json_decode($result);
//        return $result;
    }
    public function countrycodes()
    {
        $response = Http::get(getUrl('COUNTRYCODES'));
        $result=$response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;

    }

    public function verifyemail($data)
    {

	    $response = Http::get(getUrl('VERIFYEMAIL').'/'.$data);
	$result = $response->json();
	$result = json_encode($result);
	$result = json_decode($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

    public function forgotPassword($data)
    {
        //dd(getUrl('FORGOTPASSWORD'),$data);
        $response = Http::post(getUrl('FORGOTPASSWORD'),$data);
        $result=$response->json();
        return $result;
    }
    public function OTPverification($data)
    {
        $response = Http::post(getUrl('OTPVERIFICATION'),$data);
        $result=$response->json();
        return $result;
    }

    public function getUserDetails($token)
    {
        $response = Http::withToken($token)->get(getUrl('USERDETAILS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function logout($data)
    {
        $response = Http::withToken($data)->get(getUrl('LOGOUT'));
        $result=$response->json();
        //dd($result);

    }
    public function brands()
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->get(getUrl('BRANDS'));
        $result = $response->json();        
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function apklinks()
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->get(getUrl('APKLINKS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function success_stories()
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->get(getUrl('SUCCESS_STORIES'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function storyDetails($id)
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->get(getUrl('STORY_DETAILS').'/'.$id);
        $result = $response->json();        
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function itworks()
    {
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->get(getUrl('ITWORKS'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function userShops($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERSHOPS').'/'.$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }

    public function SendOtpVerify($data){
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->post(getUrl('SENDOTPSIGNUP'),$data);
        $result = $response->json();
        return $result;
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function Verifysignupotp($data){
        $response = Http::withHeaders([
            'Content-Type' =>'application/json',
        ])->post(getUrl('VERIFYSINGUPOTP'),$data);
        $result = $response->json();
        return $result;
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }

}
