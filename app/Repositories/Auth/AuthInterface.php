<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Auth;


interface AuthInterface
{
    public function signIn($data);
    public function two_way_signin($data);
    public function signUp($data);
    public function logout($data);
    public function EmailVerify($id);
    public function MobileVerify($id);
    public function countrycodes();
    public function verifyemail($id);
    public function forgotPassword($data);
    public function OTPverification($data);
    public function getUserDetails($id);
    public function brands();
    public function apklinks();
    public function success_stories();
    public function itworks();
    public function SendOtpVerify($data);
    public function Verifysignupotp($data);
    public function userShops($data);
    public function storyDetails($id);

}
