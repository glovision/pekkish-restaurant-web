<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Offer;
use App\Repositories\Offer\OfferInterface as OfferInterface;
use Illuminate\Support\Facades\Http;

class OfferRepository implements OfferInterface
{

    public function offers()
    {
        $user = session()->get('user_details');
        $id = $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('OFFERS').'/'.$id);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function addOffer($data)
    {
        //dd(getUrl('ADDOFFERS'),json_encode($data));
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDOFFERS'),$data);

        $result = $response->json();
        //dd($result);
        return $result;
    }
    public function editOffer($data)
    {
        //dd(getUrl('EDITOFFER'),$data,$data['id']);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('EDITOFFER').'/'.$data['id'], $data);
        $result = $response->json();
       //dd($result);
        return $result;
    }

    public function categoryProduct()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('CATEGORYPRODUCT'));
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        //dd($result->success);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function shops()
    {
        $user = session()->get('user_details');
        $id = $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . session()->get('token'),
            'Content-Type' => 'application/json',
        ])->get(getUrl('SHOPS') . '/' . $id);
        $result = $response->json();
        $result = json_encode($result);
        $result = json_decode($result);
        return $result;
    }

    public function shop_categories($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOP_CATEGORIES').'/'.$id);
       // return getUrl('SHOP_CATEGORIES').'/'.$id;
        $result = $response->json();
        //return $result;
//        $result = json_encode($result);
//        $result =json_decode($result);
        return $result;
    }

    public function category_items($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('CATEGORY_ITEMS').'/'.$id);
        $result = $response->json();
        //return $result;
//        $result = json_encode($result);
//        $result =json_decode($result);
        return $result;
    }
    public function deleteCoupon($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DELETECOUPON').'/'.$id);
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function coupon_verify($id){
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('COUPONVERIFY').'/'.$id);
        $result = $response->json();
        return $result;

    }
}
