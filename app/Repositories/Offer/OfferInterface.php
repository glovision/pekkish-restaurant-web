<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Offer;


interface OfferInterface
{
    public function offers();
    public function addOffer($data);
    public function editOffer($data);
    public function deleteCoupon($id);
    public function categoryProduct();
    public function  shop_categories($id);
    public function shops();
    public function category_items($id);
    public function coupon_verify($id);
}
