<?php
/**
 * Created by PhpStorm.
 * User: vyadati
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Coupon;


interface CouponInterface
{
    public function getAll();

    public function find($id);

    public function delete($id);
}