<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\Timing;
use App\Repositories\Timing\TimingInterface as TimingInterface;
use Illuminate\Support\Facades\Http;

class TimingRepository implements TimingInterface
{

    function __construct() {

    }
    public function getTiming()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('TIMINGS'));
        $result = $response->json();
        if($result['success']) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function averageTime($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('SETAVGTIME'),$data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

    public function notifications(){
        $user = session()->get('user_details');
        $id =$user->id;

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('NOTIFICATIONS').'/'.$id);
        $result = $response->json();

        $result = json_encode($result);
        $result =json_decode($result);
        if($result->success) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

}
