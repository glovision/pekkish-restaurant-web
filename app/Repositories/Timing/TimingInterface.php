<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\Timing;


interface TimingInterface
{
    public function getTiming();
    public function averageTime($data);
    public function notifications();
}
