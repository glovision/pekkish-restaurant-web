<?php
/**
 * Created by PhpStorm.
 * User: Venkat
 * Date: 22-03-2021
 * Time: 09:16
 */

namespace App\Repositories\User;


interface UserInterface
{
    public function users();
    public function addUser($data);
    public function EditUser($data);
    public function deleteUser($id);
    public function roles();
    public function shopslist($id);
    public function EmailVerify($data);
    public function UserPasswordReset($data);
    public function tblfeatures();
    public function addRole($data);
    public function editRole($data);
    public function deleteRole($id);
    public function cusines();
    public function addCusine($id);
    public function updateCusine($id);
    public function deleteCusine($id);

//    public function apklinks();
//    public function successstories();

}
