<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:17
 */

namespace App\Repositories\User;
use App\Repositories\User\UserInterface as UserInterface;
use Illuminate\Support\Facades\Http;

class UserRepository implements UserInterface
{
    public function users()
    {
        $user = session()->get('user_details');
        //dd($user->id);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('USERS').'/'.$user->id);
        //dd(getUrl('USERS').'/'.$user->id);
        $result = $response->json();
        return $result;
    }
    public function addUser($data)
    {
     //dd(getUrl('ADDUSER'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDUSER'),$data);
        $result = $response->json();
        return $result;
    }
    public function EditUser($data)
    {

        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('EDITUSER'),$data);
        $result = $response->json();
        return $result;
    }
    public function roles()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('ROLES'));
        $result = $response->json();
        return $result;
    }
    public function tblfeatures()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('TBLFEATURES'));
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function deleteUser($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DELETEUSER').'/'.$id);
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }
    public function shopslist($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('SHOPS').'/'.$id);
        $result = $response->json();
        //dd($result);
            return $result;
    }
    public function EmailVerify($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->POST(getUrl('EMAILVERIFY'), $data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }
    public function UserPasswordReset($data)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->POST(getUrl('USERPWDRESET'), $data);
        $result = $response->json();
        $result = json_encode($result);
        $result =json_decode($result);
        return $result;
    }

    public function addRole($data)
    {
        //dd(getUrl('ADDROLE'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDROLE'),$data);
        $result = $response->json();
        return $result;
    }
    public function editRole($data)
    {
        //dd(getUrl('EDITROLE'),$data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('EDITROLE').'/'.$data['id'], $data);
        $result = $response->json();
        return $result;
    }
    public function deleteRole($id)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DELETEROLE').'/'.$id);
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

    public function cusines()
    {
        $user = session()->get('user_details');
        $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('CUSINES').'/'.$user->id);
        $result = $response->json();
        if($result) {
            return $result;
        }
        else{
            return "Internal server error";
        }
    }

    public function addCusine($data)
    {

        $user = session()->get('user_details');
        $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('ADDCUSINE'), $data);
        $result = $response->json();
        return $result;
    }

    public function updateCusine($data)
    {
        $user = session()->get('user_details');
        $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->post(getUrl('UPDATECUSINE'), $data);
        $result = $response->json();
        return $result;
    }
    public function deleteCusine($data)
    {
        $user = session()->get('user_details');
        $user->id;
        $response = Http::withHeaders([
            'Authorization' => 'Bearer '.session()->get('token'),
            'Content-Type' =>'application/json',
        ])->get(getUrl('DELETECUSINE').'/'.$data);
        $result = $response->json();
        return $result;
    }


}
