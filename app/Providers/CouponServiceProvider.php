<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CouponServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Coupon\CouponInterface', 'App\Repositories\Coupon\CouponRepository');
        $this->app->bind('App\Repositories\Auth\AuthInterface', 'App\Repositories\Auth\AuthRepository');
        $this->app->bind('App\Repositories\Order\OrderInterface', 'App\Repositories\Order\OrderRepository');
        $this->app->bind('App\Repositories\Menu\MenuInterface', 'App\Repositories\Menu\MenuRepository');
        $this->app->bind('App\Repositories\Shop\ShopInterface', 'App\Repositories\Shop\ShopRepository');
        $this->app->bind('App\Repositories\Offer\OfferInterface', 'App\Repositories\Offer\OfferRepository');
        $this->app->bind('App\Repositories\User\UserInterface', 'App\Repositories\User\UserRepository');
        $this->app->bind('App\Repositories\Category\CategoryInterface', 'App\Repositories\Category\CategoryRepository');
        $this->app->bind('App\Repositories\Account\AccountInterface', 'App\Repositories\Account\AccountRepository');
        $this->app->bind('App\Repositories\Businesshour\BusinesshourInterface', 'App\Repositories\Businesshour\BusinesshourRepository');
        $this->app->bind('App\Repositories\Timing\TimingInterface', 'App\Repositories\Timing\TimingRepository');
        $this->app->bind('App\Repositories\Dashboard\DashboardInterface', 'App\Repositories\Dashboard\DashboardRepository');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
