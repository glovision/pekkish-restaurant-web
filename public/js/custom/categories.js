/**
 * Created by vyadati on 28-06-2021.
 */
function selectcatagory(id, name) {
    $("#search").val(id);
    $('#search_category').val(name);
    $('#category_filter').submit();
}

function edit_category(url) {
    var meta_title_edit=$('#meta_title_edit');
    var meta_description_edit=$('#meta_description_edit');
    var category_name_edit=$('#category_name_edit');
    var categoryEditImagePreview=$('#categoryEditImagePreview');
    meta_title_edit.attr('value', '');
    meta_title_edit.val('');
    meta_description_edit.attr('value', '');
    meta_description_edit.val('');
    category_name_edit.attr('value', '');
    category_name_edit.val('');
    categoryEditImagePreview.css('background-image', 'url("")');
    categoryEditImagePreview.hide();
    categoryEditImagePreview.fadeIn(650);
    categoryEditImagePreview.html('');
    $('#icon_edit').html('');
    $('#edit_section').modal({backdrop: 'static', keyboard: false});

    $.ajax({
        cache: false,
        type: "GET",
        url: url,
        success: function (res) {
            console.log("Seller_Category " +JSON.stringify(res));
            var  category = res.data[0];
            console.log(category['name']);
            $('#category_id').val(category.id);
            meta_title_edit.attr('value', category.meta_title);
            meta_title_edit.val(category.meta_title);
            meta_description_edit.attr('value', category.meta_description);
            meta_description_edit.val(category.meta_description);
            category_name_edit.attr('value', category.name);
            category_name_edit.val(category.name);
            categoryEditImagePreview.css('background-image', 'url('+category.icon +')');
            categoryEditImagePreview.hide();
            categoryEditImagePreview.fadeIn(650);
            categoryEditImagePreview.html('');
            $('#icon_edit').html(category.icon);
            //$('#edit_section').modal({backdrop: 'static', keyboard: false});
        }, error: function (res) {

        }
    });
}


function deletecategory(id) {

}