/**
 * Created by Venkat on 28-06-2021.
 */

var sub_add_ons_in_add_on_count_category=1;
var edit_sub_add_ons_in_add_on_count_category=0;

function open_add_on_popup_model(){
    var add_category_add=$('#add_category_add-ons');
    add_category_add.modal({backdrop: 'static', keyboard: false});
    add_category_add.modal('show');
    $('#category-add-on-model').trigger("reset");
}

function add_a_sub_add_on() {
    var a=sub_add_ons_in_add_on_count_category;
    var sub_add_ons = document.getElementById("add_on_div_d").innerHTML;
    var sub_add_ons_data = sub_add_ons.replace(/::::/g, a);

    $('#category_add_ons_sub_add_ons').append(sub_add_ons_data);

    sub_add_ons_in_add_on_count_category++;
}

function a_sub_add_on_image_upload(input,a) {
    var img_preview = $('#imagePreview_'+a);
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")){
        var reader = new FileReader();
        reader.onload = function (e) {
            img_preview.css('background-image', 'url('+e.target.result +')');
            img_preview.hide();
            img_preview.fadeIn(650);
            img_preview.html('');
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        img_preview.html('');
    }
}

function edit_category_add_on_model(data) {

    var foodsubaddon = [];
    var param = JSON.parse(data);
    edit_sub_add_ons_in_add_on_count_category=0;
    var edit_category_add_ons=$('#edit_category_add-ons');
    var section_name = $('#edit_section_name');
    var subcategory_name = $('#edit_subcategory-name');
    var subcategory_description = $('#edit_subcategory-description');

    edit_category_add_ons.modal({backdrop: 'static', keyboard: false});
    edit_category_add_ons.modal('show');
    $('#category-add-on-model').trigger("reset");
    $('#edit_category_add_ons_sub_add_ons').html('');
    //$('#categoryaddonid').val(param.id);
    $('#editcategoryaddonid').val(param.id);
    section_name.attr('value', param.category_id);
    section_name.val(param.category_id);
    subcategory_name.attr('value', param.name);
    subcategory_name.val(param.name);
    subcategory_description.attr('value', param.description);
    subcategory_description.val(param.description);
    foodsubaddon = param.foodsubaddon;
    for (var a = edit_sub_add_ons_in_add_on_count_category; a < foodsubaddon.length; a++, edit_sub_add_ons_in_add_on_count_category++) {

        var content = '<div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay" id="edit_a_sub_add_on_' + a + '">';
        if (a > 0) {
            content = content + '<div class="overlay-close mouse-pointer" onclick="delete_product_add_on_row(\'edit_a_sub_add_on_\',' + a + ');"><a class="hover-switch"><img src="./images/delete-svg.svg" alt="X"><img src="./images/delete.svg" alt="X"></a> </div>';
        }
        content = content + '<div class="col-3 no-left-padding"> <div class="avatar-upload"><div class="avatar-edit">' +
        '<input type="file" id="editImageUpload_' + a + '" name="items[' + a + '][items_image]" accept=".png, .jpg, .jpeg" onchange="edit_a_sub_add_on_image_upload(this,' + a + ')"/>' +
        '<label for="editImageUpload_' + a + '"></label>' +
        '</div><div class="avatar-preview">';
        if (foodsubaddon[a].photo == "" || foodsubaddon[a].photo == null) {
            content = content + '<div id="editImagePreview_' + a + '"  class="p_a_sub_add_on_img">+ ICON';
        } else {
            content = content + '<div id="editImagePreview_' + a + '"  style="background-image:url(' + foodsubaddon[a].photo + ');border-radius:10px;">';
        }
        content = content + '</div></div></div></div>' +
            '<div class="col-9"><div class="form-group input-material mr-2">' +
            '<input type="hidden" name="items[' + a + '][items_id]" class="form-control"id="EASAAddOnId_' + a + '" value="' + foodsubaddon[a].id + '" required>' +
            '<input type="text" name="items[' + a + '][items_name]" class="form-control"id="EASAAddOnName_' + a + '" value="' + foodsubaddon[a].name + '" required>' +
            '<label for="EASAAddOnName_' + a + '">Item Name</label></div>' +
            '<div class="row">' +
            '<div class="col-6"><div class="form-group input-material mr-2">' +
            '<input type="text" name="items[' + a + '][price]" class="form-control" id="EASAAddOnPrice_' + a + '"  value="' + foodsubaddon[a].price + '" required>' +
            '<label for="price">Price</label></div>' +
            '</div><div class="col-6">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="text" name="items[' + a + '][discount]" class="form-control" id="EASAAddOnDiscount_' + a + '" value="' + foodsubaddon[a].discount + '" required>' +
            '<label for="EASAAddOnDiscount_' + a + '">Discount</label>' +
            '</div></div></div></div></div>';
        $('#edit_category_add_ons_sub_add_ons').append(content);
    }
}

function edit_a_sub_add_on_image_upload(input,a) {
    var img_preview = $('#editImagePreview_'+a);
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")){
        var reader = new FileReader();
        reader.onload = function (e) {
            img_preview.css('background-image', 'url('+e.target.result +')');
            img_preview.hide();
            img_preview.fadeIn(650);
            img_preview.html('');
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        img_preview.html('');
    }
}

function edit_add_a_sub_add_on() {
    var a=edit_sub_add_ons_in_add_on_count_category;
    var sub_add_ons = document.getElementById("edit_add_on_div_d").innerHTML;
    var sub_add_ons_data = sub_add_ons.replace(/::::/g, a);

    $('#edit_category_add_ons_sub_add_ons').append(sub_add_ons_data);

    edit_sub_add_ons_in_add_on_count_category++;
}