// var table = document.getElementsByTagName("table")[0];
// var datatable = new DataTable(table, {
//     perPage: 10,
// });
var key = [0,1,2];
for(var j=0;j<key.length;j++) {
    var table = document.getElementsByTagName("table")[j];
    var datatable = new DataTable(table, {
        perPage: 10,
    });
}
    var inputs = [], visible = [], hidden = [];
    var checkboxes = document.getElementById("controls-dropdown-menu");
    var tbutton = document.getElementById("table-button");
// var checkboxes1 = document.getElementById("controls-dropdown-menu1");
// var tbutton1 = document.getElementById("table-button1");
// var checkboxes2 = document.getElementById("controls-dropdown-menu");
// var tbutton2 = document.getElementById("table-button");


    function setCheckboxes() {
        inputs = [];
        visible = [];
        checkboxes.innerHTML = "";
        var fragment = document.createDocumentFragment();
        var isVisible = false;

        document.onclick = function (e) {
            if (!tbutton.contains(e.target.parentNode)) {
                checkboxes.style.cssText = "display:none;";
                isVisible = false;
            }
        };

        tbutton.onclick = function (e) {
            if (checkboxes.contains(e.target)) {
                return true;
            }
            if (!isVisible) {
                checkboxes.style.cssText = "display:block;height: 200px; position: absolute; right: 0;  background: #fff; z-index: 1000; width: 200px; list-style-type: none; padding: 10px;";
                isVisible = true;
            } else {
                checkboxes.style.cssText = "display:none;";
                isVisible = false;
            }
        };

        [].forEach.call(datatable.headings, function (heading, i) {
            var checkbox = document.createElement("li");
            checkbox.className = "checkbox";
            var input = document.createElement("input");
            input.type = "checkbox";
            input.id = "checkbox-" + i;
            input.name = "checkbox";
            var label = document.createElement("label");
            label.htmlFor = "checkbox-" + i;
            label.className = "checkbox-label";
            label.appendChild(
                document.createTextNode(heading.textContent || heading.innerText)
            );

            // IE 8 supports `Object.defineProperty` on DOM nodes so no p!
            if (Object.defineProperty) {
                Object.defineProperty(input, "idx", {
                    value: i,
                    writable: false,
                    configurable: true,
                    enumerable: true
                });
            } else if (input.__defineGetter__) {
                input.__defineGetter("idx", function () {
                    return i;
                })
            }

            if (datatable.columns().visible(heading.cellIndex)) {
                input.checked = true;
                visible.push(i);
            } else {
                if (hidden.indexOf(i) < 0) {
                    hidden.push(i);
                }
            }

            checkbox.appendChild(input)
            checkbox.appendChild(label)

            fragment.appendChild(checkbox);

            inputs.push(input);
        });

        [].forEach.call(inputs, function (input, i) {

            input.onchange = function (e) {
                if (input.checked) {
                    hidden.splice(hidden.indexOf(input.idx), 1);
                    visible.push(input.idx);
                } else {
                    visible.splice(visible.indexOf(input.idx), 1);
                    hidden.push(input.idx);
                }

                updateColumns();
            };
        });

        checkboxes.appendChild(fragment);
    }
//     function setCheckboxes1() {
//     inputs = [];
//     visible = [];
//         checkboxes1.innerHTML = "";
//     var fragment = document.createDocumentFragment();
//     var isVisible = false;
//
//     document.onclick = function (e) {
//         if (!tbutton1.contains(e.target.parentNode)) {
//             checkboxes1.style.cssText = "display:none;";
//             isVisible = false;
//         }
//     };
//
//     tbutton1.onclick = function (e) {
//         if (checkboxes1.contains(e.target)) {
//             return true;
//         }
//         if (!isVisible) {
//             checkboxes1.style.cssText = "display:block;height: 200px; position: absolute; right: 0;  background: #fff; z-index: 1000; width: 200px; list-style-type: none; padding: 10px;";
//             isVisible = true;
//         } else {
//             checkboxes1.style.cssText = "display:none;";
//             isVisible = false;
//         }
//     };
//
//     [].forEach.call(datatable.headings, function (heading, i) {
//         var checkbox = document.createElement("li");
//         checkbox.className = "checkbox";
//         var input = document.createElement("input");
//         input.type = "checkbox";
//         input.id = "checkbox-" + i;
//         input.name = "checkbox";
//         var label = document.createElement("label");
//         label.htmlFor = "checkbox-" + i;
//         label.className = "checkbox-label";
//         label.appendChild(
//             document.createTextNode(heading.textContent || heading.innerText)
//         );
//
//         // IE 8 supports `Object.defineProperty` on DOM nodes so no p!
//         if (Object.defineProperty) {
//             Object.defineProperty(input, "idx", {
//                 value: i,
//                 writable: false,
//                 configurable: true,
//                 enumerable: true
//             });
//         } else if (input.__defineGetter__) {
//             input.__defineGetter("idx", function () {
//                 return i;
//             })
//         }
//
//         if (datatable.columns().visible(heading.cellIndex)) {
//             input.checked = true;
//             visible.push(i);
//         } else {
//             if (hidden.indexOf(i) < 0) {
//                 hidden.push(i);
//             }
//         }
//
//         checkbox.appendChild(input)
//         checkbox.appendChild(label)
//
//         fragment.appendChild(checkbox);
//
//         inputs.push(input);
//     });
//
//     [].forEach.call(inputs, function (input, i) {
//
//         input.onchange = function (e) {
//             if (input.checked) {
//                 hidden.splice(hidden.indexOf(input.idx), 1);
//                 visible.push(input.idx);
//             } else {
//                 visible.splice(visible.indexOf(input.idx), 1);
//                 hidden.push(input.idx);
//             }
//
//             updateColumns();
//         };
//     });
//
//     checkboxes.appendChild(fragment);
// }



function updateColumns() {
	try {
		datatable.columns().show(visible);
		datatable.columns().hide(hidden);	
	} catch(e) {
		console.log(e);
	}
}

setCheckboxes();
//setCheckboxes1();
