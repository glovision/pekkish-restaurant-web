/**
 * Created by Aditya on 28-06-2021.
 */

var i = 1;
var m = 1;
var cat=0;
var p_data=[];
var product_add_ons=[];
var available_flag = [];
var p_combo_item_count=0;
var recommended_product = [];
var new_recommended_product = [];

var product_category_add_on = [];
var product_category_add_on_length = 0;
var sub_add_ons_in_add_on_count=[];
var add_ons_in_product_count=0;
sub_add_ons_in_add_on_count[0]=1;

var combo_item_product = [];
var combo_product_category_add_on = [];
var combo_product_category_add_on_length = [];
var combo_product_category_a_sub_add_on = [];
var combo_product_category_a_sub_add_on_length = [];



var product_add_ons_for_item_edit=[];
var available_flag_for_item_edit = [];
var p_combo_item_count_for_item_edit =0;
var recommended_product_for_item_edit = [];
var new_recommended_product_for_item_edit = [];
var combo_item_product_for_item_edit = [];
var product_category_add_on_for_item_edit = [];
var sub_add_ons_in_add_on_count_for_item_edit =[];
var combo_product_category_add_on_for_item_edit = [];
var add_ons_in_product_count_for_item_edit =0;
sub_add_ons_in_add_on_count_for_item_edit[0]=1;



/*while displaying items check whether they are exists or not 'Black screen data'*/
/*when click on add sub category we need to refresh products*/

function get_category_products(data) {
    var category_products = data.items;
    var products = [];
    for (var i = 0; i < category_products.length; i++) {
        var is_exist = new_recommended_product.includes(parseInt(category_products[i].id)) ? "checked" : "";
        //console.log(new_recommended_product + category_products[i].id);
        var append_div = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + category_products[i].name +
            '<input type="checkbox" name="itemnameid" value="' + category_products[i].id + '@@' + category_products[i].name +
            '" onclick="add_recommended_item_to_product(this.value);" id="' + category_products[i].id + '" '+ is_exist +'>' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        products.push(append_div);
    }
    $('#category_product_list').html(products);
}

function add_recommended_item_to_product(data) {
    var recommended_item_div=$('#newrecommended');
    var item_data = data.split('@@');
    if($('#' + item_data[0]).is(":checked")) {
        if(!recommended_item_div.is(":checked")){
            recommended_item_div.prop('checked', true);
        }
        if (new_recommended_product.includes(parseInt(item_data[0]))) {
            return false;
        }else {
            new_recommended_product.splice(0, 0, parseInt(item_data[0]));

            $('#selected_recommended_items').append('<div class="employee-options option-selected" id="card' + item_data[0] + '">' + item_data[1] + '' +
                '<input type="checkbox" name="allowed_products[]" value=' + item_data[0] + ' style="display:none;"><span class="ml-2">' +
                '<img src="./images/clearoption.svg" alt="" onclick="remove_recommended_item(' + item_data[0] + ')"></div>');

            $("#recommended_products").val(new_recommended_product);
        }
    }else{
        remove_recommended_item(item_data[0]);
    }

}

function remove_recommended_item(data) {
    var recommended_item_div=$('#newrecommended');
    var recommended_products=$("#recommended_products");
    recommended_products.val(data);
    $("#card" + data).remove();
    $("#" + data).prop("checked", false);
    for (var i = 0; i < new_recommended_product.length; i++) {
        if (new_recommended_product[i] == parseInt(data)) {
            new_recommended_product.splice(i, 1);
        }
    }
    if(new_recommended_product.length == 0){
        recommended_item_div.prop('checked', false);
    }
    recommended_products.val(new_recommended_product);
}


function get_combo_category_products(data) {
    p_data[cat]="";
    p_data[cat]=data;
    //console.log(p_data[cat]);
    var c_product = data.items;
    var combo_products = [];
    $('#combo_category_product_add_ons').html('');
    $('#combo_category_product_a_sub_add_ons').html('');
    for (var i = 0; i < c_product.length; i++) {
        product_add_ons[c_product[i].id]=[];
        for (var j = 0; j < c_product[i].add_ons.length; j++) {
            product_add_ons[c_product[i].id]=c_product[i].add_ons;
        }
        var is_exist = combo_item_product.includes(parseInt(c_product[i].id)) ? "checked" : "";
        var append_combo_products = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + c_product[i].name + '<input type="checkbox" name="combo_item" value="' + c_product[i].id +'@@' + c_product[i].name +
            '" onclick="add_combo_item_to_product(this.value,\' ' + cat + ' \');" id="combo_item' + c_product[i].id + '" ' + is_exist + '>' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        combo_products.push(append_combo_products);
    }
    cat++;
    $('#combo_category_products').html(combo_products);
}

function add_combo_item_to_product(data,products_data){
    var append_data ="";
    var combo_div=$('#combo');
    var item_data = data.split('@@');
    var i = p_combo_item_count;
    if($('#combo_item' + item_data[0]).is(":checked")) {
        if(!combo_div.is(":checked")){
            combo_div.prop('checked', true);
            document.getElementById('categoryAddOnDiv').style.display = 'none';
        }
        if (combo_item_product.includes(parseInt(item_data[0]))) {
            return false;
        }else {
            combo_item_product.splice(0, 0, parseInt(item_data[0]));
            if(typeof combo_product_category_add_on[item_data[0]] === 'undefined' || combo_product_category_add_on[item_data[0]].length === 0) {
                combo_product_category_add_on[item_data[0]] = [];
                combo_product_category_add_on_length[item_data[0]] = 0;
                combo_product_category_a_sub_add_on[item_data[0]]=[];
                combo_product_category_a_sub_add_on_length[item_data[0]]=[];
            }

            append_data = '<div id="combocard' + item_data[0] + '"><div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay category-add-on-delete' + p_combo_item_count + '">' +
                '<div class="overlay-close mouse-pointer" onclick="remove_product_in_combo(' + item_data[0] + ');">' +
                '<a class="hover-switch" id="category-add-on-delete' + p_combo_item_count + '"><img src="./images/delete-svg.svg" alt="X"><img src="./images/delete.svg" alt="X"></a></div>' +
                '<div class="col-12"><div class="row">' +
                '<div class="col-12"><div class="form-group input-material mr-2">' +
                '<span>' + item_data[1] + '</span>'+
                '<input type="text" class="form-control hide" name="allowed_combo_add_ons[' + i + '][product_id]" value="' + item_data[0] + '">' +
                '<br><span class="span-slash" style="color:#898989;" id="combo_item_description'+item_data[0]+'"></span>' +
                '</div></div>' +
                '</div></div>' +
                '<div class="col-12"><div class="row">' +
                '<div class="col-6"><div class="form-group input-material mr-2">' +
                '<input type="text" name="allowed_combo_add_ons[' + i + '][offer]" class="form-control" id="price">' +
                '<label for="price">Offer Percentage</label></div>' +
                '</div><div class="col-6">' +
                '<div class="form-group input-material mr-2" style="visibility: hidden;">' +
                // '<input type="text" name="items[' + p_combo_item_count + '][discount]"' +
                // 'class="form-control" id="disscount">' +
                // '<label for="discount">Discount</label>' +
                '</div></div></div></div>';

            if(product_add_ons[item_data[0]].length > 0) {
                for (var z = 0; z <product_add_ons[item_data[0]].length; z++) {
                    if(combo_product_category_a_sub_add_on[item_data[0]].length === 0) {
                        combo_product_category_a_sub_add_on[item_data[0]][z] = [];
                        combo_product_category_a_sub_add_on_length[item_data[0]][z] = 0;
                    }
                }
                append_data = append_data + '<div class="col-12"><div class="check"><label class="mouse-pointer no-padding"' +
                    'onclick="get_combo_item_add_on_to_product_inner(' + item_data[0] + ',' + i + ',\'' + products_data + '\');"> Add Subcategory </label>' +
                    '<label class="mouse-pointer"> <input type="checkbox" name="add_subcategory[' + p_combo_item_count + ']" value="" ' +
                    'onclick="get_combo_item_add_on_to_product(' + item_data[0] + ',' + i + ',\'' + products_data + '\');" id="combo_item_add_on_subcategory' + item_data[0] + '">' +
                    '<span class="checkmark"></span>' +
                    '</label></div></div>';

            }
            append_data = append_data + '<div id="category_add_ons_for_combo_product' + item_data[0] + '">' +
                '<input type="hidden" name="combo_add_on_for_product[]" id="category_add_on_for_product">' +
                '</div></div></div></div>';

            $('#combo_selected').append(append_data);

            $("#products_for_combo").val(combo_item_product);

            p_combo_item_count++;
            $('#combo_category_product_add_ons').html('');
            $('#combo_category_product_a_sub_add_ons').html('');

        }
    }else{
        remove_product_in_combo(item_data[0]);
    }

}

function get_combo_item_add_on_to_product(product_id,product_input_index,products_data){

    if($('#combo_item_add_on_subcategory'+product_id).is(":checked")) {
        get_combo_item_add_on_to_product_inner(product_id,product_input_index,products_data);
    }else{
        $('#combo_category_product_add_ons').html('');
        for (i = 0; i < product_add_ons[product_id].length; i++) {
            remove_combo_add_on_for_product(product_id,product_add_ons[product_id][i].id,i);
        }
    }
}

function get_combo_item_add_on_to_product_inner(product_id,product_input_index,products_data){
    var combo_products = [];
    var item_add_ons= product_add_ons[product_id];
    get_combo_category_products(p_data[parseInt(products_data)]);
    for (var i = 0; i <item_add_ons.length; i++) {
        if(typeof combo_product_category_a_sub_add_on[product_id][i] === 'undefined' || combo_product_category_a_sub_add_on[product_id][i].length === 0) {
            combo_product_category_a_sub_add_on[product_id][i] = [];
            combo_product_category_a_sub_add_on_length[product_id][i] = 0;
        }
        // no need to intialize these values
        var is_exist = combo_product_category_add_on[product_id].includes(parseInt(item_add_ons[i].id)) ? "checked" : "";
        var append_combo_products = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<div class="col-12"><div class="check"><label class="mouse-pointer no-padding"' +
            'onclick="get_combo_item_a_sub_add_on_to_product(this.id,' + product_id + ','+i+','+product_input_index+',\'' + products_data + '\');" id="' + item_add_ons[i].id + '@@' + item_add_ons[i].name +'"> ' + item_add_ons[i].name + ' </label>' +
            '<label class="mouse-pointer">'+
            '<input type="checkbox" name="combo_item" value="' + item_add_ons[i].id + '@@' + item_add_ons[i].name +
            '" onclick="add_combo_item_add_on_to_product(this.value,' + product_id + ','+i+','+product_input_index+',\'' + products_data + '\');" id="combo_item_add_on' + item_add_ons[i].id +
            '" ' + is_exist + '>' +
            '<span class="checkmark"></span>' +
            '</label></div></div>'+
            '</div>' +
            '</a>';
        combo_products.push(append_combo_products);
        $('#combo_category_product_add_ons').html(combo_products);
    }
}

function add_combo_item_add_on_to_product(data,product_id,add_on_index,product_input_index,products_data) {
    var item_data = data.split('@@');
    var i=combo_product_category_add_on_length[product_id];

    get_combo_item_a_sub_add_on_to_product(data,product_id,add_on_index,product_input_index,products_data);

    if($('#combo_item_add_on' + item_data[0]).is(":checked")) {
        if(!$('#combo_item_add_on_subcategory'+product_id).is(":checked")){
            $('#combo_item_add_on_subcategory'+product_id).prop('checked', true);
        }
        if (combo_product_category_add_on[product_id].includes(parseInt(item_data[0]))) {
            return false;
        }else {
            combo_product_category_add_on[product_id].splice(0, 0, parseInt(item_data[0]));
            $('#category_add_ons_for_combo_product' + product_id).append('<div id="combo_add_on_card' + item_data[0] + '"><div class="col-12 add-on-div combo-add-on-delete' + i + '">' +
                '<div class="row" style="display: block;">' +
                '<div class="col-4 vcenter">' +
                '<div class="employee-options option-selected" id="combo-add-on-delete' + i + '"><span class="max-length_word" title="' + item_data[1] + '" onclick="change_p_a_s_a_divs(\''+data+'\','+product_id+','+add_on_index+','+product_input_index+',\'' + products_data + '\')">' + item_data[1] + '</span>' +
                '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_combo_add_on_for_product(' + product_id + ',' + item_data[0] + ','+add_on_index+')"></span></div>' +
                '<input type="checkbox" name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][add_on_id]" value=' + item_data[0] + ' style="display:none;" checked="checked">' +
                '</div><div class="col-4 vcenter">' +
                '<div>' +
                '<label class="radio">Optional <input type="radio"  name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon\',' + item_data[0] + ', true)"> <span class="checkround"></span> </label></div>' +
                '<div>' +
                '<label class="radio">Required <input type="radio"  name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon\',' + item_data[0] + ', false)"> <span class="checkround"></span> </label></div>' +
                '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforcomboaddon' + item_data[0] + '" style="visibility: hidden;">' +
                '<input type="text" name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][max_no_of_changes]" class="form-control" id="max_no_of_changes' + item_data[0] + '">' +
                '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
                '</div></div>');
            combo_product_category_add_on_length[product_id]++;
            $("#combo_add_on_for_product").val(combo_product_category_add_on[product_id]);
        }
    }else{
        remove_combo_add_on_for_product(product_id ,item_data[0],add_on_index);
    }
}

function add_combo_item_add_on_to_product_from_sub_add_ons(data,product_id,add_on_index,product_input_index,products_data) {
    //console.log(data);
    var item_data = data.split('@@');
    var i=combo_product_category_add_on_length[product_id];

    if(!$('#combo_item_add_on' + item_data[0]).is(":checked")) {
        $('#combo_item_add_on' + item_data[0]).prop('checked', true);
        if(!$('#combo_item_add_on_subcategory'+product_id).is(":checked")){
            $('#combo_item_add_on_subcategory'+product_id).prop('checked', true);
        }
        if (combo_product_category_add_on[product_id].includes(parseInt(item_data[0]))) {
            //console.log(3);
            return false;
        }else {
            //console.log(4);
            combo_product_category_add_on[product_id].splice(0, 0, parseInt(item_data[0]));
            $('#category_add_ons_for_combo_product' + product_id).append('<div id="combo_add_on_card' + item_data[0] + '"><div class="col-12 add-on-div combo-add-on-delete' + i + '">' +
                '<div class="row" style="display: block;">' +
                '<div class="col-4 vcenter">' +
                '<div class="employee-options option-selected" id="combo-add-on-delete' + i + '"><span class="max-length_word" title="' + item_data[1] + '" onclick="change_p_a_s_a_divs(\''+data+'\','+product_id+','+add_on_index+','+product_input_index+',\'' + products_data + '\')">' + item_data[1] + '</span>' +
                '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_combo_add_on_for_product(' + product_id + ',' + item_data[0] + ','+add_on_index+')"></span></div>' +
                '<input type="checkbox" name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][add_on_id]" value=' + item_data[0] + ' style="display:none;" checked="checked">' +
                '</div><div class="col-4 vcenter">' +
                '<div>' +
                '<label class="radio">Optional <input type="radio"  name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon\',' + item_data[0] + ', true)"> <span class="checkround"></span> </label></div>' +
                '<div>' +
                '<label class="radio">Required <input type="radio"  name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon\',' + item_data[0] + ', false)"> <span class="checkround"></span> </label></div>' +
                '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforcomboaddon' + item_data[0] + '" style="visibility: hidden;">' +
                '<input type="text" name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + i + '][max_no_of_changes]" class="form-control" id="max_no_of_changes' + item_data[0] + '">' +
                '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
                '</div></div>');
            combo_product_category_add_on_length[product_id]++;
            $("#combo_add_on_for_product").val(combo_product_category_add_on[product_id]);
        }
    }
}

function get_combo_item_a_sub_add_on_to_product(data,product_id,add_on_index,product_input_index,products_data) {
    var combo_sub_add_ons = [];
    var item_data = data.split('@@');
    var i=combo_product_category_add_on_length[product_id];
    var s_a=product_add_ons[product_id][add_on_index].sub_add_ons;

    for (var j = 0; j < s_a.length; j++) {

        var is_exist = combo_product_category_a_sub_add_on[product_id][add_on_index].includes(parseInt(s_a[j]["id"])) ? "checked" : "";
        var append_combo_products = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + s_a[j]["name"] +
            '<input type="checkbox" name="combo_item" value="' + s_a[j]["id"] + '@@' + s_a[j]["name"] +
            '" onclick="add_combo_item_a_sub_add_on_to_product(this.value,' + product_id + ',' + add_on_index + ',' + item_data[0] + ',\'' + data + '\',' + i + ','+product_input_index+',\'' + products_data + '\');" id="combo_item_a_sub_add_on' + s_a[j]["id"] +
            '" ' + is_exist + '>' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        combo_sub_add_ons.push(append_combo_products);

        $('#combo_category_product_a_sub_add_ons').html(combo_sub_add_ons);
    }
}

function change_p_a_s_a_divs(data,product_id,add_on_index,product_input_index,products_data) {
    /*console.log(products_data+'@@'+cat);
     console.log(p_data);
     console.log(p_data[parseInt(products_data)]);*/
    //get_combo_category_products(p_data[parseInt(products_data)]);
    get_combo_item_add_on_to_product_inner(product_id,product_input_index,products_data);
    get_combo_item_a_sub_add_on_to_product(data,product_id,add_on_index,product_input_index,products_data);
}

function add_combo_item_a_sub_add_on_to_product(data,product_id,add_on_index,add_on_id,add_on_data,input_index,product_input_index,products_data) {
    var item_data = data.split('@@');
    var i=combo_product_category_a_sub_add_on_length[product_id][add_on_index];
    var split='';
    if($('#combo_item_a_sub_add_on' + item_data[0]).is(":checked")) {
        add_combo_item_add_on_to_product_from_sub_add_ons(add_on_data,product_id,add_on_index,product_input_index,products_data);
        if (combo_product_category_a_sub_add_on[product_id][add_on_index].includes(parseInt(item_data[0]))) {
            return false;
        }else {
            combo_product_category_a_sub_add_on[product_id][add_on_index].splice(0, 0, parseInt(item_data[0]));
            $('#combo_item_description'+product_id).append('<span id="c_p_a_sub_add_on_'+product_id+'_'+add_on_index+'_'+item_data[0]+'">'+split+item_data[1]+
                '<input type="text" class="form-control hide" name="allowed_combo_add_ons['+product_input_index+'][add_ons][' + input_index + '][sub_add_ons][' + i + ']" id="sub_add_ons' + item_data[0] + '" value="' + item_data[0] + '">' +
                '</span>');
            combo_product_category_a_sub_add_on_length[product_id][add_on_index]++;
            //$("#).val(combo_product_category_add_on);
        }
    }else{
        remove_combo_a_sub_add_on_to_product(product_id,add_on_index,add_on_id,item_data[0]);
    }
}

function remove_combo_a_sub_add_on_to_product(product_id , add_on_index , add_on_id , sub_add_on_id) {
    //alert(id);
    // var combo_add_on_selected=$("#"+id);
    // combo_add_on_selected.val(data);
    $("#c_p_a_sub_add_on_" + product_id + '_' + add_on_index + '_' + sub_add_on_id).remove();
    $("#combo_item_a_sub_add_on" + sub_add_on_id).prop("checked", false);
    for (var i = 0; i < combo_product_category_a_sub_add_on[product_id][add_on_index].length; i++) {
        if (parseInt(combo_product_category_a_sub_add_on[product_id][add_on_index][i]) == parseInt(sub_add_on_id)) {
            combo_product_category_a_sub_add_on[product_id][add_on_index].splice(i, 1);
        }
    }
    //console.log(combo_product_category_add_on);
    // combo_add_on_selected.val(combo_product_category_add_on);
}

function remove_combo_add_on_for_product(product_id,add_on_id,add_on_index){
    var combo_add_on_selected=$("#category_add_ons_for_combo_product"+product_id);
    combo_add_on_selected.val(add_on_id);
    $("#combo_add_on_card" + add_on_id).remove();
    $("#combo_item_add_on" + add_on_id).prop("checked", false);
    for (var i = 0; i < combo_product_category_add_on[product_id].length; i++) {
        if (combo_product_category_add_on[product_id][i] == parseInt(add_on_id)) {
            combo_product_category_add_on[product_id].splice(i, 1);
        }
    }
    combo_add_on_selected.val(combo_product_category_add_on[product_id]);
    var sub_add_ons=product_add_ons[product_id][add_on_index].sub_add_ons;
    for (var z = 0; z <sub_add_ons.length; z++) {
        remove_combo_a_sub_add_on_to_product(product_id , add_on_index , add_on_id , sub_add_ons[z].id);
    }
    $('#combo_category_product_add_ons').html('');
    $('#combo_category_product_a_sub_add_ons').html('');
}

function remove_product_in_combo(data) {
    var products_for_combo=$("#products_for_combo");
    products_for_combo.val(data);
    $("#combocard" + data).remove();
    $("#combo_item" + data).prop("checked", false);
    for (var i = 0; i < combo_item_product.length; i++) {
        if (parseInt(combo_item_product[i]) == parseInt(data)) {
            combo_item_product.splice(i, 1);
        }
    }
    combo_product_category_add_on[data]=[];
    combo_product_category_add_on_length[data]=0;
    products_for_combo.val(combo_item_product);
    combo_product_category_a_sub_add_on[data] = [];
    combo_product_category_a_sub_add_on_length[data] =[];
    $('#combo_category_product_add_ons').html('');
    $('#combo_category_product_a_sub_add_ons').html('');
}





function change_category_add_on_div(id,url){
    var dummy="";
    for(var i=0;i<10;i++){
        dummy=dummy+'<div class="nav-link text-truncate active-links"><div class="skeleton-post"><div class="skeleton-line"></div></div></div>';
    }
    $('#category_based_add_ons').html(dummy);
    $.ajax({
        cache: false,
        type: "GET",
        url: url+'/'+id,
        success: function (res) {
            var addons = res.data;
            var addons_data="";
            $.each(addons, function(k,v) {
                //console.log(k+v);
                var is_exist = product_category_add_on.includes(parseInt(v.id)) ? "checked" : "";
                addons_data=addons_data+'<div class="nav-link text-truncate active-links">'+
                    '<label class="check">' + v.name +
                    '<input type="checkbox" name="addonnameid" value="' + v.id + '@@' + v.name +
                    '" onclick="add_add_on_to_product(this.value)" id="cat_add_on'+v.id+'" ' + is_exist + '>'+
                    '<span class="checkmark"></span></label></div>';
            });
            if(addons_data == ""){
                $("#category_based_add_ons").fadeOut(400, function() {
                    $(this).html('<div class="nav-link text-truncate active-links check">No data available</div>').fadeIn(400);
                });
            }else {
                $("#category_based_add_ons").fadeOut(400, function() {
                    $(this).html(addons_data).fadeIn(400);
                });
                //need to clear category add on div while changing category in product
                //$('#category_based_add_ons').html(addons_data);
            }
        }, error: function (res) {

        }
    });
}

function add_add_on_to_product(data) {
    var item_category_add_on_div=$('#category_addon');
    var item_data = data.split('@@');
    var i=product_category_add_on_length;
    if($('#cat_add_on' + item_data[0]).is(":checked")) {
        if (!item_category_add_on_div.is(":checked")) {
            item_category_add_on_div.prop('checked', true);
        }
        if (product_category_add_on.includes(parseInt(item_data[0]))) {
            return false;
        } else {
            product_category_add_on.splice(0, 0, parseInt(item_data[0]));
            $('#category_add_on_selected').append('<div id="category_add_on_card' + item_data[0] + '"><div class="col-12 add-on-div category-add-on-delete' + item_data[0] + '">' +
                '<div class="row" style="display: block;">' +
                '<div class="col-4 vcenter">' +
                '<div class="employee-options option-selected" id="category-add-on-delete' + item_data[0] + '"><span class="max-length_word" title="' + item_data[1] + '">' + item_data[1] + '</span>' +
                '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_category_add_on_for_product(' + item_data[0] + ')"></span></div>' +
                '<input type="checkbox" name="allowed_category_add_ons[][item]" value=' + item_data[0] + ' style="display:none;" checked="checked">' +
                '</div><div class="col-4 vcenter">' +
                '<div>' +
                '<label class="radio">Optional <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(\'visibleforaddon\',' + item_data[0] + ', true)"> <span class="checkround"></span> </label></div>' +
                '<div>' +
                '<label class="radio">Required <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(\'visibleforaddon\',' + item_data[0] + ', false)"> <span class="checkround"></span> </label></div>' +
                '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforaddon' + item_data[0] + '" style="visibility: hidden;">' +
                '<input type="text" name="allowed_category_add_ons[' + i + '][offer]" class="form-control" id="price">' +
                '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
                '</div></div>');
            product_category_add_on_length++;
            $("#category_add_on_for_product").val(product_category_add_on);
        }
    }else{
        remove_category_add_on_for_product(item_data[0]);
    }

}

function remove_category_add_on_for_product(data) {
    var item_category_add_on_div=$('#category_addon');
    var category_add_on_selected=$("#category_add_on_for_product");
    category_add_on_selected.val(data);
    $("#category_add_on_card" + data).remove();
    $("#cat_add_on" + data).prop("checked", false);
    for (var i = 0; i < product_category_add_on.length; i++) {
        if (parseInt(product_category_add_on[i]) == parseInt(data)) {
            product_category_add_on.splice(i, 1);
        }
    }
    if(product_category_add_on.length == 0){
        item_category_add_on_div.prop('checked', false);
    }
    category_add_on_selected.val(product_category_add_on);
}



function changeVisibility(id,val,condition){
    //console.log(id+ '->' +val+ '->' +condition);
    if(condition){
        document.getElementById(id+val).style.visibility = "visible";
    }else{
        document.getElementById(id+val).style.visibility = "hidden";
    }
}

function changeDisplay(id,val,condition){
    if(condition){
        document.getElementById(id+val).style.display = "block";
    }else{
        document.getElementById(id+val).style.display = "none";
    }
}

function add_p_a_sub_add_on(val) {
    var a=sub_add_ons_in_add_on_count[val];
    var product_sub_add_ons = document.getElementById("product_sub_add_ons_####_!!!!").innerHTML;
    var add_ons = product_sub_add_ons.replace(/####/g, val);
    var sub_add_ons = add_ons.replace(/!!!!/g, a);
    //alert('product_sub_add_ons_'+val+'_0');
    $('#product_sub_add_ons_'+val+'_0').append(sub_add_ons);

    sub_add_ons_in_add_on_count[val]++;
}

function add_p_add_on() {

    sub_add_ons_in_add_on_count[add_ons_in_product_count]=0;
    var a=sub_add_ons_in_add_on_count[add_ons_in_product_count];
    var product_sub_add_ons = document.getElementById("product_based_add_on_div_d").innerHTML;
    var add_ons = product_sub_add_ons.replace(/####/g, add_ons_in_product_count);
    var sub_add_ons = add_ons.replace(/!!!!/g, a);
    $('#product_based_add_on_div').append(sub_add_ons);

    sub_add_ons_in_add_on_count[add_ons_in_product_count]++;
    add_ons_in_product_count++;
}

function  p_a_sub_add_on_image_upload(input,a,b) {
    var img_preview = $('#PASAImagePreview_'+a+'_'+b);
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")){
        var reader = new FileReader();
        reader.onload = function (e) {
            img_preview.css('background-image', 'url('+e.target.result +')');
            img_preview.hide();
            img_preview.fadeIn(650);
            img_preview.html('');
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        img_preview.html('');
    }
}

/*Item edit functions*/

function get_category_products_for_item_edit(data) {
    var category_products = data.items;
    var products = [];
    for (var i = 0; i < category_products.length; i++) {
        var append_div = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + category_products[i].name +
            '<input type="checkbox" name="itemnameid" value="' + category_products[i].id + '@@' + category_products[i].name + '" onclick="add_recommended_item_to_product_for_item_edit(this.value);" id="' + category_products_for_item_edit[i].id + '">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        products.push(append_div);
    }
    $('#category_product_list_for_item_edit').html(products);
}

function add_recommended_item_to_product_for_item_edit(data) {
    var item_data = data.split('@@');
    if($('#' + item_data[0]).is(":checked")) {
        var recommend_flag_for_item_edit = false;
        for (var i = 0; i < new_recommended_product.length; i++) {
            if (parseInt(new_recommended_product[i]) == parseInt(item_data[0])) {
                recommend_flag_for_item_edit = true;
            }
        }
        if (recommend_flag_for_item_edit)
            return false;

        new_recommended_product_for_item_edit.splice(0, 0, item_data[0]);

        $('#selected_recommended_items').append('<div class="employee-options option-selected" id="card_for_item_edit' + item_data[0] + '">' + item_data[1] + '' +
            '<input type="checkbox" name="allowed_products[]" value=' + item_data[0] + ' style="display:none;"><span class="ml-2">' +
            '<img src="./images/clearoption.svg" alt="" onclick="remove_recommended_item_for_item_edit(' + item_data[0] + ')"></div>');

        $("#recommended_products_for_item_edit").val(new_recommended_product_for_item_edit);
    }else{
        remove_recommended_item_for_item_edit(item_data[0]);
    }

}

function remove_recommended_item_for_item_edit(data) {

    var recommended_products=$("#recommended_products_for_item_edit");
    recommended_products.val(data);
    $("#card_for_item_edit" + data).remove();
    $("#category_products_for_item_edit" + data).prop("checked", false);
    for (var i = 0; i < new_recommended_product_for_item_edit.length; i++) {
        if (parseInt(new_recommended_product_for_item_edit[i]) == parseInt(data)) {
            new_recommended_product_for_item_edit.splice(i, 1);
        }
    }
    recommended_products.val(new_recommended_product_for_item_edit);
}

function get_combo_category_products_for_item_edit(data) {
    var c_product = data.items;
    var combo_products = [];
    $('#combo_category_product_add_ons_for_item_edit').html('');
    for (var i = 0; i < c_product.length; i++) {
        product_add_ons_for_item_edit[i]=[];
        for (var j = 0; j < c_product[i].add_ons.length; j++) {
            product_add_ons_for_item_edit[i][j]=[];
            product_add_ons_for_item_edit[i][j]["id"] = c_product[i].add_ons[j].id;
            product_add_ons_for_item_edit[i][j]["name"] = c_product[i].add_ons[j].name;
        }
        var append_combo_products = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + c_product[i].name +
            '<input type="checkbox" name="combo_item" value="' + c_product[i].id +'@@' + c_product[i].name + '" onclick="add_combo_item_to_product_for_item_edit(this.value,'+ i +');" id="combo_item_for_item_edit' + c_product[i].id + '">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        combo_products.push(append_combo_products);
    }
    $('#combo_category_products_for_item_edit').html(combo_products);
}

function add_combo_item_to_product_for_item_edit(data,add_ons){
    var item_data = data.split('@@');
    var combo_products = [];
    for (var i = 0; i < product_add_ons_for_item_edit[add_ons].length; i++) {
        var append_combo_products = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + product_add_ons[add_ons][i].name +
            '<input type="checkbox" name="combo_item" value="' + product_add_ons[add_ons][i].id + '@@' + product_add_ons[add_ons][i].name +
            '" onclick="add_combo_item_add_on_to_product_for_item_edit(this.value,\'category_add_ons_for_combo_product_for_item_edit' + item_data[0] + '\');" id="combo_item_add_on_for_item_edit' + product_add_ons[add_ons][i].id + '">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        combo_products.push(append_combo_products);
    }

    $('#combo_category_product_add_ons_for_item_edit').html(combo_products);
    if($('#combo_item_for_item_edit' + item_data[0]).is(":checked")) {
        var recommend_flag_for_item_edit = false;
        for ( i = 0; i < combo_item_product_for_item_edit.length; i++) {
            if (parseInt(combo_item_product_for_item_edit[i]) == parseInt(item_data[0])) {
                recommend_flag_for_item_edit = true;
            }
        }
        if (recommend_flag_for_item_edit)
            return false;

        combo_item_product_for_item_edit.splice(0, 0, item_data[0]);

        $('#combo_selected_for_item_edit').append('<div id="combocard' + item_data[0] + '"><div class="modifiactions-required addonsstaticsubdiv row add-on-div category-add-on-delete' + i + '">' +
            '<div class="col-12"><div class="row">' +
            '<div class="col-11"><div class="form-group input-material mr-2">' +
            '<span>' + item_data[1] + '</span><br><span style="color:#898989;">Description</span>' +
            '</div></div><div class="col-1" style="margin-top: -15px;">' +
            '<div class="form-group input-material mr-2">' +
            '<a class="mouse-pointer" id="category-add-on-delete_for_item_edit' + i + '" onclick="remove_product_in_combo_for_item_edit(' + item_data[0] + ')"><img class="mr-2" src="./images/delete.svg" alt=""></a>' +
            '</div></div></div></div>' +
            '<div class="col-12"><div class="row">' +
            '<div class="col-6"><div class="form-group input-material mr-2">' +
            '<input type="text" name="items[' + p_combo_item_count + '][offer]" class="form-control" id="price">' +
            '<label for="price">Offer Percentage</label></div>' +
            '</div><div class="col-6">' +
            '<div class="form-group input-material mr-2" style="visibility: hidden;">' +
            '<input type="text" name="items[' + p_combo_item_count + '][discount]"' +
            'class="form-control" id="disscount">' +
            '<label for="discount">Discount</label>' +
            '</div></div></div></div>' +
            '<div class="row" id="category_add_ons_for_combo_product_for_item_edit' + item_data[0] + '">' +
            '<input type="hidden" name="combo_add_on_for_product[]" id="category_add_on_for_product_for_item_edit">'+
            '</div></div></div></div>');
        $("#products_for_combo_for_item_edit").val(combo_item_product);
        p_combo_item_count++;
    }else{
        remove_product_in_combo_for_item_edit(item_data[0]);
    }

}

function add_combo_item_add_on_to_product_for_item_edit(data,id) {
    var item_data = data.split('@@');
    if($('#combo_item_add_on' + item_data[0]).is(":checked")) {
        var recommendFlag = false;
        for (var i = 0; i < combo_product_category_add_on_for_item_edit.length; i++) {
            if (parseInt(combo_product_category_add_on_for_item_edit[i]) == parseInt(item_data[0])) {
                recommendFlag = true;
            }
        }
        if (recommendFlag)
            return false;

        combo_product_category_add_on_for_item_edit.splice(0, 0, item_data[0]);
        $('#'+id).append('<div id="combo_add_on_card_for_item_edit' + item_data[0] + '"><div class="col-12 add-on-div combo-add-on-delete_for_item_edit' + i + '">' +
            '<div class="row" style="display: block;">' +
            '<div class="col-4 vcenter">' +
            '<div class="employee-options option-selected" id="combo-add-on-delete_for_item_edit' + i + '"><span class="max-length_word" title="' + item_data[1] + '">' + item_data[1] + '</span>' +
            '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_combo_add_on_for_product_for_item_edit(' + item_data[0] + ',\'' + id + '\')"></span></div>'+
            '<input type="checkbox" name="allowed_combo_add_ons[' + i + '][item]" value=' + item_data[0] + ' style="display:none;" checked="checked">' +
            '</div><div class="col-4 vcenter">' +
            '<div>' +
            '<label class="radio">Optional <input type="radio"  name="allowed_combo_add_ons[' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon_for_item_edit\',' + i + ', true)"> <span class="checkround"></span> </label></div>'+
            '<div>' +
            '<label class="radio">Required <input type="radio"  name="allowed_combo_add_ons[' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(\'visibleforcomboaddon_for_item_edit\',' + i + ', false)"> <span class="checkround"></span> </label></div>'+
            '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforcomboaddon_for_item_edit' + i + '" style="visibility: hidden;">' +
            '<input type="text" name="allowed_combo_add_ons[' + i + '][offer]" class="form-control" id="price">' +
            '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
            '</div></div>');

        $("#combo_add_on_for_product_for_item_edit").val(combo_product_category_add_on);
    }else{
        remove_combo_add_on_for_product_for_item_edit(item_data[0],id);
    }
}

function remove_combo_add_on_for_product_for_item_edit(data,id) {
    //alert(id);
    var combo_add_on_selected=$("#"+id);
    combo_add_on_selected.val(data);
    $("#combo_add_on_card_for_item_edit" + data).remove();
    $("#combo_item_add_on_for_item_edit" + data).prop("checked", false);
    for (var i = 0; i < combo_product_category_add_on_for_item_edit.length; i++) {
        if (parseInt(combo_product_category_add_on_for_item_edit[i]) == parseInt(data)) {
            combo_product_category_add_on_for_item_edit.splice(i, 1);
        }
    }
    //console.log(combo_product_category_add_on);
    combo_add_on_selected.val(combo_product_category_add_on_for_item_edit);
}

function remove_product_in_combo_for_item_edit(data) {
    var products_for_combo=$("#products_for_combo_for_item_edit");
    products_for_combo.val(data);
    $("#combocard" + data).remove();
    $("#combo_item" + data).prop("checked", false);
    for (var i = 0; i < combo_item_product_for_item_edit.length; i++) {
        if (parseInt(combo_item_product_for_item_edit[i]) == parseInt(data)) {
            combo_item_product.splice(i, 1);
        }
    }
    products_for_combo.val(combo_item_product);
}

function change_category_add_on_div_for_item_edit(id,url){
    var dummy="";
    for(var i=0;i<10;i++){
        dummy=dummy+'<div class="nav-link text-truncate active-links"><div class="skeleton-post"><div class="skeleton-line"></div></div></div>';
    }
    $('#category_based_add_ons').html(dummy);
    $.ajax({
        cache: false,
        type: "GET",
        url: url+'/'+id,
        success: function (res) {
            var addons = res.data;
            var addons_data="";
            $.each(addons, function(k,v) {
                console.log(k+v);
                addons_data=addons_data+'<div class="nav-link text-truncate active-links">'+
                    '<label class="check">' + v.name +
                    '<input type="checkbox" name="addonnameid" value="' + v.id + '@@' + v.name + '" onclick="add_add_on_to_product_for_item_edit(this.value)" id="cat_add_on'+v.id+'">'+
                    '<span class="checkmark"></span></label></div>';
            });
            if(addons_data == ""){
                $("#category_based_add_ons_for_item_edit").fadeOut(400, function() {
                    $(this).html('<div class="nav-link text-truncate active-links check">No data available</div>').fadeIn(400);
                });
            }else {
                $("#category_based_add_ons_for_item_edit").fadeOut(400, function() {
                    $(this).html(addons_data).fadeIn(400);
                });
                //$('#category_based_add_ons').html(addons_data);
            }
        }, error: function (res) {

        }
    });
}

function add_add_on_to_product_for_item_edit(data) {
    var item_data = data.split('@@');
    if($('#cat_add_on_for_item_edit' + item_data[0]).is(":checked")) {
        var recommendFlag = false;
        for (var i = 0; i < product_category_add_on_for_item_edit.length; i++) {
            if (parseInt(product_category_add_on_for_item_edit[i]) == parseInt(item_data[0])) {
                recommendFlag = true;
            }
        }
        if (recommendFlag)
            return false;

        product_category_add_on_for_item_edit.splice(0, 0, item_data[0]);
        //console.log(product_category_add_on);
        $('#category_add_on_selected_for_item_edit').append('<div id="category_add_on_card_for_item_edit' + item_data[0] + '"><div class="col-12 add-on-div category-add-on-delete_for_item_edit' + i + '">' +
            '<div class="row" style="display: block;">' +
            '<div class="col-4 vcenter">' +
            '<div class="employee-options option-selected" id="category-add-on-delete_for_item_edit' + i + '"><span class="max-length_word" title="' + item_data[1] + '">' + item_data[1] + '</span>' +
            '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_category_add_on_for_product_for_item_edit(' + item_data[0] + ')"></span></div>'+
            '<input type="checkbox" name="allowed_category_add_ons[' + i + '][item]" value=' + item_data[0] + ' style="display:none;" checked="checked">' +
            '</div><div class="col-4 vcenter">' +
            '<div>' +
            '<label class="radio">Optional <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(\'visibleforaddon_for_item_edit\',' + i + ', true)"> <span class="checkround"></span> </label></div>'+
            '<div>' +
            '<label class="radio">Required <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(\'visibleforaddon_for_item_edit\',' + i + ', false)"> <span class="checkround"></span> </label></div>'+
            '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforaddon_for_item_edit' + i + '" style="visibility: hidden;">' +
            '<input type="text" name="allowed_category_add_ons[' + i + '][offer]" class="form-control" id="price">' +
            '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
            '</div></div>');

        $("#category_add_on_for_product_for_item_edit").val(product_category_add_on_for_item_edit);
    }else{
        remove_category_add_on_for_product_for_item_edit(item_data[0]);
    }
}

function remove_category_add_on_for_product_for_item_edit(data) {
    var category_add_on_selected=$("#category_add_on_selected_for_item_edit");
    category_add_on_selected.val(data);
    $("#category_add_on_card_for_item_edit" + data).remove();
    $("#cat_add_on_for_item_edit" + data).prop("checked", false);
    for (var i = 0; i < product_category_add_on_for_item_edit.length; i++) {
        if (parseInt(product_category_add_on_for_item_edit[i]) == parseInt(data)) {
            product_category_add_on_for_item_edit.splice(i, 1);
        }
    }
    category_add_on_selected.val(product_category_add_on_for_item_edit);
}

function add_p_a_sub_add_on_for_item_edit(val) {
    var a=sub_add_ons_in_add_on_count[val];
    var product_sub_add_ons = document.getElementById("product_sub_add_ons_####_!!!!").innerHTML;
    var add_ons = product_sub_add_ons.replace(/####/g, val);
    var sub_add_ons = add_ons.replace(/!!!!/g, a);
    //alert('product_sub_add_ons_'+val+'_0');
    $('#product_sub_add_ons_'+val+'_0').append(sub_add_ons);

    sub_add_ons_in_add_on_count[val]++;
}

function add_p_add_on_for_item_edit() {

    sub_add_ons_in_add_on_count[add_ons_in_product_count]=0;
    var a=sub_add_ons_in_add_on_count[add_ons_in_product_count];
    var product_sub_add_ons = document.getElementById("product_based_add_on_div_d").innerHTML;
    var add_ons = product_sub_add_ons.replace(/####/g, add_ons_in_product_count);
    var sub_add_ons = add_ons.replace(/!!!!/g, a);
    $('#product_based_add_on_div').append(sub_add_ons);

    sub_add_ons_in_add_on_count[add_ons_in_product_count]++;
    add_ons_in_product_count++;
}

function  p_a_sub_add_on_image_upload_for_item_edit(input,a,b) {
    var img_preview = $('#PASAImagePreview_'+a+'_'+b);
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")){
        var reader = new FileReader();
        reader.onload = function (e) {
            img_preview.css('background-image', 'url('+e.target.result +')');
            img_preview.hide();
            img_preview.fadeIn(650);
            img_preview.html('');
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        img_preview.html('');
    }
}

/*need to change*/
function itemAvaailableCancelModel(input) {
    $('#product_sts').hide();
    location.reload();
}
$('#image-preview').hide();
$('#varientaddon').hide();
$("#Modifiercheck").click(function () {
    if ($('#Modifiercheck').is(":checked")) {
        $('#varientaddon').show();
    } else {
        $('#varientaddon').hide();
    }
});

function readURL(input) {
    var fileUpload = document.getElementById("photos");
    if (typeof (fileUpload.files) != "undefined") {
        var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
        console.log(size);
        if (size > 2000) {
            $('#imagesizevaliderror').html("Image size should be below 2mb.");
            $('#image-preview').attr('src', '');
            return false;
        }
    }

    $('#image-preview').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-preview').attr('src', e.target.result);
        }
        $('#imagesizevaliderror').html("");
        reader.readAsDataURL(input.files[0]);
    }
}


$(document).ready(function () {
    var i = 0;
    var j = 1;
    var a = 0;
    $('#addmorevarients').on('click', function () {
        console.log($('#dynamicvariant').val());
        if ($('#dynamicvariant').val() > 0) {
            i++;
            i = parseInt($('#dynamicvariant').val()) + parseInt(i);
        }
        var newadditemrowsdelete = "block";
        if(i==0) {
            var newadditemrowsdelete = "none";
        }
        $('#addvariants').append('<div id="' + i + '" class="col-6 variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="text" name="variants[' + i + '][variant]" class="form-control" id="opt2" required>' +
            '<label for="opt2">Name the Option </label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="variants[' + i + '][price]" step="0.01" class="form-control" id="Price2" required>' +
            '<label for="Price2">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p class="f-10 mb-2" style="display: '+newadditemrowsdelete+'">Remove</p>' +
            '<a id="variantsdelete' + i + '" onclick="deleterow(this.id, ' + i + ');" style="display: '+newadditemrowsdelete+'">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>');
        i++;
        $('#dynamicvariant').val(0);
    });
    $('#addmoreaddons').on('click', function () {
        console.log(a);
        if ($('#dynamicaddonincr').val() > 0) {
            a++;
            a = parseInt($('#dynamicaddonincr').val()) + parseInt(a);
        }
        //else{
        //     a =0;
        //     $('#dynamicaddonincr').val(0);
        // }
        var addmoreaddonsdelete = "block";
        if(a==0) {
            var addmoreaddonsdelete = "none";
        }
        $('#addons_subdiv').append('<div id="' + a + '" class="modifiactions-required addonsdelete' + a + '">' +
            '<div class="d-flex align-items-end justify-content-end mb-0">' +
            '<label class="radio mx-3">Optional' +
            '<input type="radio" name="items[' + a + '][optional]" value="true" checked>' +
            '<span class="checkround"></span>' +
            '</label>' +
            '<label class="radio">Required' +
            '<input type="radio" name="items[' + a + '][optional]" value="false">' +
            '<span class="checkround"></span>' +
            '</label>' +
            '<button type="button" style="display: '+addmoreaddonsdelete+'" class="close closebtn" id="addonsdelete' + a + '" onclick="deleteaddonrow(this.id, ' + a + ');">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" ></button>' +
            '</div>' +
            '<div class="form-group input-material mb-5">' +
            '<input type="text" name="items[' + a + '][ModifierName]" class="form-control" id="ModifierName" required>' +
            '<label for="Modifier Name">Modifier Name </label>' +
            '</div>' +
            '<div class="my-5">' +
            '<p class="mb-3 text-color-grey f-14">Options for Selection</p>' +
            '<div class="container-fluid p-0">' +
            '<div class="row">' +
            '<div class="col-6">' +
            '<label class="radio">Select at least one' +
            '<input type="radio" name="items[' + a + '][selection]" value="true" checked>' +
            '<span class="checkround"></span>' +
            '</label>' +
            '</div>' +
            '<div class="col-6">' +
            '<label class="radio">Multiple Selection Required' +
            '<input type="radio" name="items[' + a + '][selection]" value="false">' +
            '<span class="checkround"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
            '<p >Options</p>' +
            '<p  onclick="addmoreitmrows(' + a + ')">+ Add row</p>' +
            '</div>' +
            '<div class="container-fluid p-0">' +
            '<div id="additemrows' + a + '">' +
            '<div class="row">' +
            '<div class="col-6 addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="hidden" name="items[' + a + '][items][0][id]" value = "' + (a + 1) + '" class="form-control" id="opt1">' +
            '<input type="text" name="items[' + a + '][items][0][name]" class="form-control" id="opt1">' +
            '<label for="opt1">Name the Option</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="items[' + a + '][items][0][price]" step="0.01" class="form-control" id="Price">' +
            '<label for="Price">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p style="display: none" class="f-10 mb-2">Remove</p>' +
            '<a style="display: none" id="addonoptiosdelete' + a + '" onclick="deleterow(this.id, ' + a + ');">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        a++;
        $('#dynamicaddonincr').val(0);
    });
    k = 1;
    $('#addmoreitems').on('click', function () {
        j++;
        $('#additemrows').append('<div id="' + j + '" class="col-6 addonoptiosdelete' + j + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="hidden" name="items[0][items][' + k + '][id]" value = "' + j + '" class="form-control" id="opt1">' +
            '<input type="text" name="items[0][items][' + k + '][name]" class="form-control" id="opt2" required>' +
            '<label for="opt2">Name the Option </label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 addonoptiosdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="items[0][items][' + k + '][price]" step="0.01" class="form-control" id="Price2" required>' +
            '<label for="Price2">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic addonoptiosdelete' + j + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p class="f-10 mb-2">Remove</p>' +
            '<a id="addonoptiosdelete' + j + '" onclick="deleterow(this.id, ' + j + ');">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>');
        k++;
    });


    $("#variants_div").hide();
    $("#addons_div").hide();

    $('#varientsitem').on('click', function () {
        $("#variants_div").show();
        $("#addons_div").hide();
    });

    $('#addonitem').on('click', function () {
        $("#variants_div").hide();
        $("#addons_div").show();
        $("#addons_subdiv").show();
        $("#addmoreaddons").show();
    });

    $("#newvariants_div").hide();
    $("#newaddons_div").hide();
    $("#newaddmoreaddons").hide();
    $('#newvarientsitem').on('click', function () {
        $("#newvariants_div").show();
        $("#newaddons_div").hide();
    });
    $('#newaddonitem').on('click', function () {
        $("#newvariants_div").hide();
        $("#newaddons_div").show();
        $("#newaddons_subdiv").show();
        $("#newaddmoreaddons").show();
    });
});

function product_available_sts(product_url) {
    jQuery('#product_sts').modal('show', {backdrop: 'static'});
    document.getElementById('productsts_link').setAttribute('href', product_url);
}

var availavleflag = [];
var recommenditems = [];

function update_modal(param, item_edit_url, itemurl) {
    $('#edit-Shop-Item').modal({backdrop: 'static', keyboard: false});
    $("#variants_div").hide();
    $("#addons_div").hide();
    var data = JSON.parse(param);
    //console.log("23243434 " + JSON.stringify(data));
    //console.log("23243434 " + JSON.stringify(url));

    var editItemName = $('#editItemName');
    var editItemDescription = $('#editItemDescription');
    var editItemunit_price = $('#editItemunit_price');
    var editItemmSet_max_order_number = $('#editItemmSet_max_order_number');
    var image_preview = $('#image-preview');
    var editItemmCuisine_id = $('#editItemmCuisine_id option[value=' + data.cuisine_id + ']');
    $('#addons_subdiv').html("");
    $('#addvariants').html("");
    $('#additemrows').html("");
    //console.log("APP_URL" + data.thumbnail_img);
    if (data.thumbnail_img) {
        image_preview.show();
        $('#photos').attr('src', data.thumbnail_img);
        image_preview.attr('src', data.thumbnail_img);
    }
    //$('#image-preview').attr('src', source);
    $('#edit_item_form').attr('action', item_edit_url);
    $('#edit-Shop-Item').modal('show');
    $('#editItemid').val(data.id);
    editItemName.attr('value', data.description);
    editItemName.val(data.name);
    editItemDescription.attr('value', data.description);
    editItemDescription.val(data.description);
    editItemunit_price.attr('value', data.base_price);
    editItemunit_price.val(data.base_price);
    editItemmCuisine_id.prop('selected', 'selected');
    editItemmSet_max_order_number.attr('value', data.max_qty);
    editItemmSet_max_order_number.val(data.max_qty);
    $('input[name="featured"][value=' + data.featured + ']').prop('checked', 'checked');
    $('input[name="healthpicks"][value=' + data.healthy_picks + ']').prop('checked', 'checked');
    $('input[name="veg_nonveg"][value=' + data.type + ']').prop('checked', 'checked');
    $('#edit_menu_items_desc option[value=' + data.category_id + ']').prop('selected', 'selected');
    $('#selected_recommended_items_edit').html("");
    $('#recommended_products_for_item_edit').val("");
    $('#category_add_on_selected_for_item_edit').html("");
    $('#product_based_add_on_div_for_item_edit').html("");
    $('#combo_selected_for_item_edit').html("");
    $('#category_add_on_for_item_edit').prop('checked', false);
    $('#modifier_check_for_item_edit').prop('checked', false);
    $('#recommended_for_item_edit').prop('checked', false);
    $('#combo_for_item_edit').prop('checked', false);


    if (data.Modifier == 1) {
        $('input[name="Modifier"]').prop('checked', 'checked');
        $('input[name="varientsitem"]').prop('checked', 'checked');
        $('#varientaddon').show();
        $("#variants_div").show();
    } else {
        $('input[name="Modifier"]').attr('checked', false);
        $('#varientaddon').hide();
        $("#variants_div").hide();
    }


    var url = itemurl;
    //console.log("23243434 " + JSON.stringify(url));
    $.ajax({
        cache:false,
        type: "GET",
        url: url,
        success: function (res) {
            //console.log("product_data "+JSON.stringify(res));
            if (res.success) {
                var output = res.data;
                console.log("23243434 " + JSON.stringify(output));
                var itemtiming = output.PRODUCTTIMING;
                var recommends = output.RECOMMENDS;
                var caregoryaddons = output.CATEGORYADDON;
                var modifiers = output.MODIFIERS;
                var comboitems = output.COMBOITEMS;

                //$("#edit_recommendedproducts").val(recommends);//edititemmenu
                for (var i = 0; i < recommends.length; i++) {
                    recommenditems.splice(0, 0, recommends[i].recommended_product_id);
                    $('#recommended_products_for_item_edit').val(recommenditems);
                    $('#selected_recommended_items_edit').append('<div class="employee-options option-selected" id="card' + recommends[i].recommended_product_id + '">' + recommends[i].name + '' +
                        '<input type="checkbox" name="allowed_products[]" value=' + recommends[i].recommended_product_id + ' style="display:none;"><span class="ml-2">' +
                        '<img src="./images/clearoption.svg" alt="" onclick="new_remove_rec(' + recommends[i].recommended_product_id + ')"></div>');
                    $('#recommended_for_item_edit').prop('checked', 'checked');
                }
                for (var i = 0; i < itemtiming.length; i++) {
                    availavleflag.splice(0, 0, itemtiming[i].day_id);
                    console.log("23 " + availavleflag);
                    $('#editItemavailable_days').val(availavleflag).trigger('change')
                }

                for (var i = 0; i < caregoryaddons.length; i++) {
                    $('#category_add_on_for_item_edit').prop('checked', 'checked');
                    console.log(JSON.stringify(modifiers[i]));
                    var optselect = "";
                    if (caregoryaddons[i]['optional'] == 1) {
                        var optselect = "checked";
                    }
                    var optreqselect = "";
                    if (caregoryaddons[i]['optional'] == 0) {
                        var optreqselect = "checked";
                    }
                    $('#category_add_on_selected_for_item_edit').append('<div id="category_edit_on_card' + i +'"><div class="col-12 edit-on-div category-edit-on-delete' + i + '">' +
                        '<div class="row" style="display: block;">' +
                        '<div class="col-4 vcenter">' +
                        '<div class="employee-options option-selected" id="category-edit-on-delete' + i + '"><span class="max-length_word" title="' + caregoryaddons[i]['name'] + '">' + caregoryaddons[i]['name'] + '</span>' +
                        '<span class="max-length_word">&ensp;<img src="./images/clearoption.svg" onclick="remove_category_add_on_for_product_for_item_edit(' + i + ')"></span></div>' +
                        '<input type="checkbox" name="allowed_category_add_ons[' + i + '][item]" value=' + i + ' style="display:none;" checked="checked">' +
                        '</div><div class="col-4 vcenter">' +
                        '<div>' +
                        '<label class="radio">Optional <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="1" data-validation="required" required onclick="changeVisibility(visibleforaddon,' + i + ', true)" ' + optselect + '> <span class="checkround"></span> </label></div>' +
                        '<div>' +
                        '<label class="radio">Required <input type="radio"  name="allowed_category_add_ons[' + i + '][optional]" id="optional" value="0" data-validation="required" required onclick="changeVisibility(visibleforaddon,' + i + ', false)" ' + optreqselect + '> <span class="checkround"></span> </label></div>' +
                        '</div><div class="col-4 vcenter"><div class="form-group input-material mr-2" id="visibleforaddon' + i + '" style="visibility: hidden;">' +
                        '<input type="text" name="allowed_category_add_ons[' + i + '][offer]" value="' + caregoryaddons[i]['name'] + '" class="form-control" id="price">' +
                        '<label for="price" style="font-size: 10px;">Max no.of changes</label></div></div></div>' +
                        '</div></div>');

                }

                for (var i = 0; i < modifiers.length; i++) {
                    $('#modifier_check_for_item_edit').prop('checked', 'checked');
                    console.log("Modifier_Add_on_div " + JSON.stringify(modifiers[i]));
                    var sub_add_ons = modifiers[i].sub_add_ons;
                    console.log("sub_Add_on_div " + JSON.stringify(sub_add_ons));
                    console.log("sub_Add_on_div_sub_add_ons.length " + JSON.stringify(sub_add_ons.length));
                    var optselect = "";
                    if (modifiers[i]['optional'] == 1) {
                        var optselect = "checked";
                    }
                    var optreqselect = "";
                    if (modifiers[i]['optional'] == 0) {
                        var optreqselect = "checked";
                    }
                    modifierdiv = '<div id="product_based_add_on_div_d">' +
                        '<div id="p_add_on_div_####" class="modifiactions-required  overlay">';
                    if(i>0) {
                        modifierdiv = modifierdiv + '<div class="overlay-close mouse-pointer" onclick="delete_product_edit_on_row("p_edit_on_div_",' + i + ');">' +
                            '<a class="hover-switch">' +
                            '<img src="./images/delete-svg.svg" alt="X">' +
                            '<img src="./images/delete.svg" alt="X">' +
                            '</a>' +
                            '</div>';
                    }
                    modifierdiv = modifierdiv +'<div class="col-12">' +
                        '<div class="row justify-content-end">' +
                        '<div class="col-4">' +
                        '<label class="radio mx-3">Optional' +
                        '<input type="radio" name="items['+i+'][optional]" value="' + modifiers[i].optional + '" onclick="changeDisplay("PMaxNoOfChanges_","+i+",true)" ' + optselect + '>' +
                        '<span class="checkround"></span>' +
                        '</label>' +
                        '</div>' +
                        '<div class="col-4">' +
                        '<label class="radio">Required' +
                        '<input type="radio" name="items['+i+'][optional]" value="' + modifiers[i].optional + '" onclick="changeDisplay("PMaxNoOfChanges_",'+i+',false)" ' + optreqselect + '>' +
                        '<span class="checkround"></span>' +
                        '</label>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group input-material mb-4">' +
                        '<input type="text" name="items['+i+'][ModifierName]" value="' + modifiers[i].name + '" class="form-control" id="ModifierName_'+i+'" required>' +
                        '<label for="ModifierName_'+i+'">Modifier Name</label>' +
                        '</div>' +
                        '<div class="form-group input-material mb-4">' +
                        '<textarea class="form-control" name="items['+i+'][ModifierDescription]" value="' + modifiers[i].description + '" id="ModifierDescription_'+i+'" rows="2" data-validation="required" required>' + modifiers[i].description + '</textarea>' +
                        '<label for="ModifierDescription_####">Modifier Description</label>' +
                        '</div>' +
                        '<div class="form-group input-material mb-4 hide" id="PMaxNoOfChanges_'+i+'">' +
                        '<input type="number" class="form-control" name="items['+i+'][MaxNoOfChanges]" id="MaxNoOfChanges_'+i+'" value="' + modifiers[i].max_count + '">' +
                        '<label for="MaxNoOfChanges_'+i+'">Max number of changes</label>' +
                        '</div>';

                    for (var j = 0; j < sub_add_ons.length; j++) {
                        modifierdiv = modifierdiv + '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
                            '<p>Options</p>' +
                            '<p class="mouse-pointer" onclick="add_p_a_sub_add_on("'+i+'")">+ Add row</p>' +
                            '</div>' +
                            '<div class="container-fluid p-0" id="product_sub_add_ons_'+i+'_'+j+'">' +
                            '<div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay border" id="p_a_sub_add_on_'+i+'_'+j+'">' ;
                        if(j>0) {
                            modifierdiv = modifierdiv + '<div class="overlay-close mouse-pointer" onclick="delete_product_add_on_row("p_a_sub_add_on_","' + i + '_' + j + '");">' +
                                '<a class="hover-switch">' +
                                '<img src="./images/delete-svg.svg" alt="X">' +
                                '<img src="./images/delete.svg" alt="X">' +
                                '</a>' +
                                '</div>';
                        }
                        modifierdiv = modifierdiv + '<div class="col-3 no-left-padding">' +
                            '<div class="avatar-upload">' +
                            '<div class="avatar-edit">' +
                            '<input type="file" id="PASAImageUpload_'+i+'_'+j+'" name="items['+i+'][sub_add_on]['+j+'][add_on_image]"' +
                            'accept=".png, .jpg, .jpeg" required onchange="p_a_sub_add_on_image_upload(this,'+i+',"'+j+'")"/>' +
                            '<label for="PASAImageUpload_'+i+'_'+j+'"></label>' +
                            '</div>' ;
                        if (+sub_add_ons[j].photo == "" || +sub_add_ons[j].photo == null) {
                            modifierdiv = modifierdiv + '<div class="avatar-preview' + j + '">';
                        } else {
                            modifierdiv = modifierdiv + '<div id="avatar-preview' + j + '"  style="background-image:url(' + sub_add_ons[j].photo + ');border-radius:10px;">';
                        }
                        modifierdiv = modifierdiv + '<div class="p_a_sub_add_on_img" id="PASAImagePreview_'+i+'_'+j+'"> + ICON </div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-9">' +
                            '<div class="form-group input-material mr-2">' +
                            '<input type="text" name="items['+i+'][sub_add_on]['+j+'][add_on_name]" class="form-control" value="'+sub_add_ons[j].name+'" id="PASAAddOnName_'+i+'_'+j+'" required>' +
                            '<label for="PASAAddOnName_'+i+'_!!!!">Item Name</label>' +
                            '</div>' +
                            '<div class="row">' +
                            '<div class="col-6">' +
                            '<div class="form-group input-material mr-2">' +
                            '<input type="text" name="items['+i+'][sub_add_on]['+j+'][add_on_price]" class="form-control" value="'+sub_add_ons[j].price+'" id="PASAAddOnPrice_'+i+'_'+j+'" required>' +
                            '<label for="PASAAddOnPrice_'+i+'_!!!!">Price</label>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-6">' +
                            '<div class="form-group input-material mr-2">' +
                            '<input type="text" name="items['+i+'][sub_add_on]['+j+'][add_on_discount]" class="form-control" value="'+sub_add_ons[j].discount+'" id="PASAAddOnDiscount_'+i+'_'+j+'" required>' +
                            '<label for="PASAAddOnDiscount_'+i+'_'+j+'">Discount</label>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }
                    modifierdiv = modifierdiv + '</div>' +
                        '</div>';
                    $('#product_based_add_on_div_for_item_edit').append(modifierdiv);
                }

                for(var k = 0; k < comboitems.length; k++){
                    $('#combo_for_item_edit').prop('checked', 'checked');
                    console.log("Combo_for_item_edit_div " + JSON.stringify(comboitems[k]));

                    comboitemsdiv = '<div id="combocard_for_edit' + k + '"><div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay category-add-on-delete' + p_combo_item_count + '">' +
                        '<div class="overlay-close mouse-pointer" onclick="remove_product_in_combo(' + k + ');">' +
                        '<a class="hover-switch" id="category-add-on-delete' + p_combo_item_count + '"><img src="./images/delete-svg.svg" alt="X"><img src="./images/delete.svg" alt="X"></a></div>' +
                        '<div class="col-12"><div class="row">' +
                        '<div class="col-12"><div class="form-group input-material mr-2">' +
                        '<span>' + comboitems[k].product_name + '</span>'+
                        '<input type="text" class="form-control hide" name="allowed_combo_add_ons[' + k + '][product_id]" value="' + comboitems[k].product_id + '">' +
                        '<br><span class="span-slash" style="color:#898989;" id="combo_item_description'+comboitems[k].product_id+'"></span>' +
                        '</div></div>' +
                        '</div></div>' +
                        '<div class="col-12"><div class="row">' +
                        '<div class="col-6"><div class="form-group input-material mr-2">' +
                        '<input type="text" name="allowed_combo_add_ons[' + k + '][offer]" class="form-control" id="price">' +
                        '<label for="price">Offer Percentage</label></div>' +
                        '</div><div class="col-6">' +
                        '<div class="form-group input-material mr-2" style="visibility: hidden;">' +
                        '</div></div></div></div>';
                    $('#combo_selected_for_item_edit').append(comboitemsdiv);
                }


            } else {

            }
        },error: function(res) {

        }
    });

}



function addmoreitmrows(a) {
    console.log('additemrows ' + a);
    m++;
    var additemrowsdelete = "block";
    if(i==0) {
        var additemrowsdelete = "none";
    }
    $('#additemrows' + a).append('<div class="row addonoptiosdelete' + a + m + i + '"><div id="' + a + '" class="col-6">' +
        '<div class="form-group input-material mr-2">' +
        '<input type="hidden" name="items[' + a + '][items][' + i + '][id]" value = "' + m + '" class="form-control" id="opt1">' +
        '<input type="text" name="items[' + a + '][items][' + i + '][name]" class="form-control" id="opt2">' +
        '<label for="opt2">Name the Option </label>' +
        '</div>' +
        '</div>' +
        '<div class="col-4">' +
        '<div class="form-group input-material mr-2">' +
        '<input type="number" name="items[' + a + '][items][' + i + '][price]" step="0.01" class="form-control" id="Price2">' +
        '<label for="Price2">Price</label>' +
        '</div>' +
        '</div>' +
        '<div class="col-2 addvarientststic">' +
        '<div class="form-group input-material mr-2">' +
        '<p class="f-10 mb-2" style="display: '+additemrowsdelete+'">Remove</p>' +
        '<a id="addonoptiosdelete' + a + m + i + '" onclick="deleterow(this.id, ' + a + m + i + ');" style="display: '+additemrowsdelete+'">' +
        '<img class="mx-3" src="./images/delete.svg" alt="" >' +
        '</a>' +
        '</div>' +
        '</div>' +
        '</div>');
    i++;
}

function newaddmoreitmrows1(a) {
    console.log('additemrows ' + a + ' ' + m);
    m++;
    $('#newadditemrows' + a).append('<div class="row addonoptiosdelete' + a + m + '"><div id="' + a + '" class="col-6">' +
        '<div class="form-group input-material mr-2">' +
        '<input type="hidden" name="items[' + a + '][items][' + i + '][id]" value = "' + a + m + '" class="form-control" id="opt1">' +
        '<input type="text" name="items[' + a + '][items][' + i + '][name]" class="form-control" id="opt2" required>' +
        '<label for="opt2">Name the Option </label>' +
        '</div>' +
        '</div>' +
        '<div class="col-4">' +
        '<div class="form-group input-material mr-2">' +
        '<input type="number" name="items[' + a + '][items][' + i + '][price]" step="0.01" class="form-control" id="Price2" required>' +
        '<label for="Price2">Price</label>' +
        '</div>' +
        '</div>' +
        '<div class="col-2 addvarientststic">' +
        '<div class="form-group input-material mr-2">' +
        '<p class="f-10 mb-2">Remove</p>' +
        '<a id="addonoptiosdelete' + a + m + '" onclick="deleterow(this.id, ' + a + m + ');">' +
        '<img class="mx-3" src="./images/delete.svg" alt="" >' +
        '</a>' +
        '</div>' +
        '</div>' +
        '</div>');
    i++;
}

function getproducts(data) {
    var cproduct = data.products;
    var products = [];
    for (var i = 0; i < cproduct.length; i++) {
        var itemnm = cproduct[i].name;
        console.log(itemnm);

        var appendprop = '<a class="nav-link text-truncate active-links" href="#">' +
            '<div>' +
            '<label class="check">' + itemnm + '<input type="checkbox" name="itemnameid" value="' + cproduct[i].id + '@@' + itemnm + '" onclick="additemstomenu(this.value);" id="' + cproduct[i].id + '">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</label>' +
            '</label>' +
            '</div>' +
            '</a>';
        products.push(appendprop);
    }
    $('#productitems').html(products);
}

function additemstomenu(data) {
    var item_data = data.split('@@');
    console.log("3434 3434 " + $('#' + item_data[0]).is(":checked"));
    var recommendFlag = false;
    for (var i = 0; i < recommended_product.length; i++) {
        if (parseInt(recommended_product[i]) == parseInt(item_data[0])) {
            //console.log("itemar "+parseInt(recommended_product[i])+" "+item_data[0]);
            recommendFlag = true;
        }
    }
    if (recommendFlag)
        return false;

    recommended_product.splice(0, 0, item_data[0]);

    $('#itemmenu').append('<div class="employee-options option-selected" id="card' + item_data[0] + '">' + item_data[1] + '' +
        '<input type="checkbox" name="allowed_products[]" value=' + item_data[0] + ' style="display:none;"><span class="ml-2">' +
        '<img src="./images/clearoption.svg" alt="" onclick="remove_rec(' + item_data[0] + ')"></div>');

    $("#recommendedproducts").val(recommended_product);

}

function confirm_modal(delete_url) {
    jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
    document.getElementById('delete_link').setAttribute('href', delete_url);
}

function remove_rec(data) {
    console.log(recommended_product.length);
    $("#recommendedproducts").val(data);
    $("#card" + data).remove();
    $("#" + data).prop("checked", false);
    // $("#recommendedproducts").val(item_data[0]);
    for (var i = 0; i < recommended_product.length; i++) {
        //if ($('#' + item_data[0]).is(":checked") == false) {
        console.log(data);
        if (parseInt(recommended_product[i]) == parseInt(data)) {
            console.log(recommended_product[i]);
            recommended_product.splice(i, 1);
        }
    }
    $("#recommendedproducts").val(recommended_product);
}


function openpopupmodel() {
    $('#add_item').modal({backdrop: 'static', keyboard: false});
    $('#add_item').modal('show');
    // $('#edit_item').modal('show');
    $('#id').val("");
    $('#itemSearch').val("");
    $('#description').val("");
    $('#unit_price').val("");
    $('#cuisine_id').val("");
    $('#set_max_order_number').val("");
    $('#service_line').val("");
    $('#available_days').val("");
    $("#featured").prop("checked", false);
    $("#healthpicks").prop("checked", false);
    $("#veg").prop("checked", false);
    $("#nonveg").prop("checked", false);
    $("#newModifiercheck").prop("checked", false);
    $('#varientaddon').hide();
    $('#image-preview').hide();
    $('#image-preview').attr('src', '');
    $('input[name="Modifier"]').attr('checked', false);
    $('#varientaddon').hide();
    $("#variants_div").hide();
    $("#addons_div").hide();
    $('#add_item_form')[0].reset();
    $("#newvarientaddon").hide();
    $("#newaddoninitialval").val(0);
    $("#recommended_products").val("");

}

$(document).ready(function () {

    var a = 0;
    var i = 0;
    var j = 1;
    $('#newaddmorevarients').on('click', function () {
        console.log("ABCD " + $('#newaddoninitialval').val());
        if ($('#newdynamicvariant').val() > 0) {
            i = parseInt($('#newdynamicvariant').val()) + parseInt(i);
        }
        var newaddvariantsdelete = "block";
        if(i==0) {
            var newaddvariantsdelete = "none";
        }
        $('#newaddvariants').append('<div id="' + i + '" class="col-6 variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="text" name="variants[' + i + '][variant]" class="form-control" id="opt2" required>' +
            '<label for="opt2">Name the Option </label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="variants[' + i + '][price]" step="0.01" class="form-control" id="Price2" required>' +
            '<label for="Price2">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic variantsdelete' + i + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p class="f-10 mb-2" style="display: '+newaddvariantsdelete+'">Remove</p>' +
            '<a id="variantsdelete' + i + '" onclick="deleterow(this.id, ' + i + ');" style="display: '+newaddvariantsdelete+'">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>');
        $('#newdynamicvariant').val(0);
        i++;
    });

    $('#newaddmoreaddons').on('click', function () {
        console.log(a);
        if ($('#newdynamicaddonincr').val() > 0) {
            a = parseInt($('#newdynamicaddonincr').val()) + parseInt(a);
        }
        /*if ($("#newaddoninitialval").val() == 0) {
         a= 0;
         $("#newaddoninitialval").val(1);
         }*/
        var newaddonssubdivdelete = "block";
        if(a==0) {
            var newaddonssubdivdelete = "none";
        }
        $('#newaddons_subdiv').append('<div id="' + a + '" class="modifiactions-required addonsdelete' + a + '">' +
            '<div class="d-flex align-items-end justify-content-end mb-0">' +
            '<label class="radio mx-3">Optional' +
            '<input type="radio" name="items[' + a + '][optional]" value="true" checked>' +
            '<span class="checkround"></span>' +
            '</label>' +
            '<label class="radio">Required' +
            '<input type="radio" name="items[' + a + '][optional]" value="false">' +
            '<span class="checkround"></span>' +
            '</label>' +
            '<button type="button" class="close closebtn" id="addonsdelete' + a + '" onclick="deleteaddonrow(this.id, ' + a + ');" style="display: '+newaddonssubdivdelete+'">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" ></button>' +
            '</div>' +
            '<div class="form-group input-material mb-5">' +
            '<input type="text" name="items[' + a + '][ModifierName]" class="form-control" id="ModifierName" required>' +
            '<label for="Modifier Name">Modifier Name</label>' +
            '</div>' +
            '<div class="my-5">' +
            '<p class="mb-3 text-color-grey f-14">Options for Selection</p>' +
            '<div class="container-fluid p-0">' +
            '<div class="row">' +
            '<div class="col-6">' +
            '<label class="radio">Select at least one' +
            '<input type="radio" name="items[' + a + '][selection]" value="true" checked>' +
            '<span class="checkround"></span>' +
            '</label>' +
            '</div>' +
            '<div class="col-6">' +
            '<label class="radio">Multiple Selection Required' +
            '<input type="radio" name="items[' + a + '][selection]" value="false">' +
            '<span class="checkround"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
            '<p >Options</p>' +
            '<p  onclick="newaddmoreitmrows(' + a + ')">+ Add row</p>' +
            '</div>' +
            '<div class="container-fluid p-0">' +
            '<div id="newadditemrows' + a + '">' +
            '<div class="row">' +
            '<div class="col-6 addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="hidden" name="items[' + a + '][items][0][id]" value = "' + (a + 1) + '" class="form-control" id="opt1">' +
            '<input type="text" name="items[' + a + '][items][0][name]" class="form-control" id="opt1" required>' +
            '<label for="opt1">Name the Option</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="items[' + a + '][items][0][price]" step="0.01" class="form-control" id="Price" required>' +
            '<label for="Price">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic addonoptiosdelete' + a + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p class="f-10 mb-2" style="display: none">Remove</p>' +
            '<a id="addonoptiosdelete' + a + '" onclick="deleterow(this.id, ' + a + ');" style="display: none">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');
        a++;
        $("#newaddoninitialval").val(0);
    });
    k = 1;
    $('#newaddmoreitems').on('click', function () {
        j++;
        $('#newadditemrows').append('<div id="' + j + '" class="col-8 addonoptiosdelete' + k + j + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="hidden" name="items[0][items][' + k + '][id]" value = "' + j + '" class="form-control" id="opt1">' +
            '<input type="text" name="items[0][items][' + k + '][name]" class="form-control" id="opt2" required>' +
            '<label for="opt2">Name the Option </label>' +
            '</div>' +
            '</div>' +
            '<div class="col-4 addonoptiosdelete' + k + j + '">' +
            '<div class="form-group input-material mr-2">' +
            '<input type="number" name="items[0][items][' + k + '][price]" step="0.01" class="form-control" id="Price2" required>' +
            '<label for="Price2">Price</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-2 addvarientststic addonoptiosdelete' + k + j + '">' +
            '<div class="form-group input-material mr-2">' +
            '<p class="f-10 mb-2">Remove</p>' +
            '<a id="addonoptiosdelete' + k + j + '" onclick="deleterow(this.id, ' + k + j + ');">' +
            '<img class="mx-3" src="./images/delete.svg" alt="" >' +
            '</a>' +
            '</div>' +
            '</div>');
        k++;
    });
});

$("#newModifiercheck").click(function () {
    if($('#newModifiercheck').is(":checked")) {
        $("#newaddons_div").show();
        add_p_add_on();

    }else{
        $("#newaddons_div").hide();
    }
});

$('#newimage-preview').hide();
function newreadURL(input) {
    var fileUpload = document.getElementById("newphotos");
    if (typeof (fileUpload.files) != "undefined") {
        var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
        if (size > 2000) {
            $('#newimagesizevaliderror').html("Image size should be below 2mb.");
            $('#newimage-preview').attr('src', '');
            return false;
        }
    }

    $('#newimage-preview').show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#newimage-preview').attr('src', e.target.result);
        }
        $('#newimagesizevaliderror').html("");
        reader.readAsDataURL(input.files[0]);
    }
}