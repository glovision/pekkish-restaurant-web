/**
 * Created by Venkat on 03-07-2021.
 */
function selectcatagory(id,name){
    $("#search").val(id);
    $('#search_category').val(name);
    $('#category_filter').submit();
}

function editCategory(url) {
    var meta_title_edit =  $('#meta_title_edit');
    var meta_description_edit =  $('#meta_description_edit');
    var category_name_edit =  $('#category_name_edit');
    var Start_edit =  $('#Start_edit');
    var end_edit =  $('#end_edit');
    var categoryEditImagePreview=$('#categoryEditImagePreview');
    categoryEditImagePreview.css('background-image', 'url("")');
    categoryEditImagePreview.hide();
    categoryEditImagePreview.fadeIn(650);
    categoryEditImagePreview.html('');
    $.ajax({
        cache:false,
        type: "GET",
        //url: "{{('editShopCategory')}}/" + id,
        url:url,
        success: function (res) {
            console.log("Shop_Category " +JSON.stringify(res));
            var  category = res.data[0];
            console.log(category['name']);
            $('#category_id').val(category.id);
            meta_title_edit.attr('value', category.meta_title);
            meta_title_edit.val(category.meta_title);
            meta_description_edit.attr('value', category.meta_description);
            meta_description_edit.val(category.meta_description);
            category_name_edit.attr('value', category.name);
            category_name_edit.val(category.name);
            $('#icon_edit').html(category.icon);                    
           // $('#icon_edit_img').attr('src', category.icon);
            Start_edit.attr('value', category.start_time);
            Start_edit.val(category.start_time);
            end_edit.attr('value', category.end_time);
            end_edit.val(category.end_time);
            categoryEditImagePreview.css('background-image', 'url('+category.icon +')');
            categoryEditImagePreview.hide();
            categoryEditImagePreview.fadeIn(650);
            categoryEditImagePreview.html('');
            $('#edit_section').modal({backdrop: 'static', keyboard: false});
        },error: function(res) {

        }
    });

}