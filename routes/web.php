<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\CouponController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */ 
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
header('Access-Control-Allow-Credentials: true');

Route::get('/home', 'HomeController@home');
Route::get('/', function () {
    return redirect('/index');
})->name('signin');


Route::get('/terms_conditions', 'AcountsController@terms_conditions');
Route::get('/faq', 'AcountsController@faq');
Route::get('/storyDetails/{id}', 'AuthController@storyDetails');
Route::get('/helpatlogin', 'AcountsController@helpatlogin');
//Route::post('/signin', 'AuthController@signIn');
Route::match(['GET', 'POST'], '/signin', 'AuthController@signin');
Route::get('/two-way-login/{id}', 'AuthController@twoWayLogin');
Route::get('/two_way_signin/{email}/{id}', 'AuthController@two_way_signin');
Route::match(['GET', 'POST'],'/signup', 'AuthController@signup');
Route::get('/index', 'AuthController@index');
Route::post('/ajaxEmailvarification', 'AuthController@ajaxEmailvarification');
Route::post('/ajaxmobilevarification', 'AuthController@ajaxmobilevarification');
Route::get('/verifyshopemail/{token}', 'AuthController@verifyshopemail');
Route::post('/forgot_password', 'AuthController@forgotPassword');
Route::post('/otp_validate', 'AuthController@OTPverification');
Route::post('/sendsignupotp','AuthController@sendSignUpotp');
Route::post('/verifysignupotp','AuthController@verifysignupotp');
Route::post('/resetpassword', 'AcountsController@resetpassword');
Route::post('/resendotp','AcountsController@resendotp');
Route::group(array('middleware'=>['token']),function() {

    Route::post('/profupd', 'AcountsController@profupd');
    Route::get('/storechange/{id}', 'DashboardController@storechange');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('/dayWiseSalesDetails', 'DashboardController@dayWiseSalesDetails');
    Route::post('/dashboardCountsDetails', 'DashboardController@dashboardCountsDetails');
  //  Route::get('/linechartDetails/{days}', 'DashboardController@linechartDetails');
    Route::post('/linechartDetails', 'DashboardController@linechartDetails');
//    Route::get('/menu', 'MenuController@index')->name('menu.index');
    Route::get('/menu', 'MenuController@indexNew')->name('menu.index');
    Route::get('/categorymenu/{id}', 'MenuController@categorymenu');
    Route::post('/ajaxupdateemailaddres', 'AcountsController@ajaxupdateemail');
    Route::post('/ajaxpasswordreset', 'AcountsController@ajaxpasswordreset');
    Route::post('/ajaxshopscheduleoff', 'AcountsController@ajaxshopscheduleoff');
    Route::post('/ajaxshopstatuschange', 'AcountsController@ajaxshopstatuschange');

    Route::post('/addItem', 'MenuController@addItem');
    Route::post('/item_edit', 'MenuController@update');
    Route::get('/productstatus/{id}', 'MenuController@productstatus');
    Route::get('/menu_destroy/{id}', 'MenuController@menu_destroy');
    Route::post('/addCategory', 'CategoryController@add_category');
    Route::post('/UpdateCategory', 'CategoryController@UpdateCategory');


    Route::get('/orders', 'OrderController@index')->name('orders.index');
    Route::get('/ajaxinvoicedtls/{id}', 'OrderController@getinvoicedtls');
    Route::get('/order_history', 'OrderController@history');
    Route::get('/orderhistory', 'OrderController@orderhistory');
    Route::get('/FoodReady/{id}', 'OrderController@FoodReady');
    Route::get('/FoodDispatched/{id}', 'OrderController@FoodDispatched');
    Route::get('/AssignDriver/{id}', 'OrderController@AssignDriver');
    Route::get('/AssignDriver/{orderid}/{id}', 'OrderController@AssignOrderDriver');
    Route::get('/get_drivers','OrderController@get_drivers');
    Route::post('/ajaxFoodReject', 'OrderController@FoodReject');
    Route::post('/ajaxFoodAccept', 'OrderController@FoodAccept');
    Route::post('/order_item_reject', 'OrderController@OrderItemReject');

    Route::get('/order_history_export', 'OrderController@order_history_export')->name('order_history_export');

    Route::get('/AjaxOrdersHistoryCounts/{days}', 'OrderController@AjaxOrdersHistoryCounts');
    Route::post('/AjaxOrdersHistoryCounts1', 'DashboardController@AjaxOrdersHistoryCounts1');
    Route::get('/AjaxTotalOrdersCounts/{days}', 'OrderController@AjaxTotalOrdersCounts');
    Route::get('/accounts', 'AcountsController@index');
    Route::get('/feedback_reviews', 'FeedbackReviewController@index');
    Route::get('/offer_list', 'OfferController@index')->name('offer_list');
    Route::post('/addOffer', 'OfferController@addOffer');
    Route::post('/editOffer', 'OfferController@editOffer');
    Route::get('/coupon_destroy/{id}', 'OfferController@destroy');
    Route::post('/categoryProduct', 'OfferController@categoryProduct');

    Route::get('/manage_roles', 'RoleController@index')->name('employees.roles');
    Route::post('/addrole', 'RoleController@addrole');
    Route::post('/editrole', 'RoleController@editrole');
    Route::get('/role_destroy/{id}', 'RoleController@role_destroy');

    Route::get('/manage_employees', 'EmployeeController@index')->name('employees.index');
    Route::post('/adduser', 'EmployeeController@store');
    Route::post('/user_edit', 'EmployeeController@edit');
    Route::get('/users_destroy/{id}', 'EmployeeController@users_destroy');
    Route::POST('/userEmailIdvarification', 'EmployeeController@userEmailIdvarification');
    Route::POST('/ajaxuserpasswordreset', 'EmployeeController@ajaxuserpasswordreset');

    Route::POST('/EmailIdvarification', 'RestaurantController@EmailIdvarification');
    Route::get('CouponVerify/{id}', 'OfferController@couponverify');

    Route::get('/manage_restaurant', 'RestaurantController@index')->name('restaurants.index');
    Route::get('/add_restaurant','RestaurantController@add');
    Route::post('/addShop', 'RestaurantController@store');
    Route::get('/editShop/{id}', 'RestaurantController@edit');
    Route::post('/UpdateShop', 'RestaurantController@update');
    Route::get('/shop_destroy/{id}', 'RestaurantController@shop_destroy');
    Route::get('/dutyHistoryStatus', 'RestaurantController@dutyHistoryStatus');

    Route::get('/business_hours', 'BusinesshoursController@index')->name('business_hours');
    Route::get('/ajaxBusinessTiming', 'BusinesshoursController@ajaxBusinessTiming');
    Route::post('/addWorkTiming', 'BusinesshoursController@store');
    Route::post('/shopAvailableStatusChange', 'BusinesshoursController@shopAvailableStatusChange');

    Route::post('/contact-us', 'AcountsController@ContactUs');

    Route::post('/my-orders', 'OrderController@index');
    Route::get('/new_orders', 'OrderController@new_orders')->name('my_orders');

    Route::get('/time_settings', 'SettingController@index');
    Route::post('/average_time', 'SettingController@average_time');

    Route::get('/term_conditions', 'AcountsController@term_conditions');
    Route::get('/help', 'AcountsController@help');
    Route::get('/faqs', 'AcountsController@faqs');
    Route::get('/edit_category/{id}','MenuController@edit_category')->name('edit_category');

    Route::get('/logout', 'AuthController@logout');
    Route::get('/cusines', 'CusineController@index')->name('cusine');
    Route::post('/addcusine', 'CusineController@store');
    Route::post('/updatecusine', 'CusineController@update');
    Route::get('/deletecusine/{id}','CusineController@deleteCusine');
    Route::get('/notifications','SettingController@notifications');
    Route::get('/shop_categories/{id}','OfferController@shop_categories');
    Route::get('/category_items/{id}','OfferController@category_items');
    Route::get('/category_destroy/{id}','MenuController@category_destroy');
    Route::get('/food_dispatch_delivey/{id}','OrderController@food_dispatch_delivey');
    Route::resource('coupons', CouponController::class)->except([
        'create', 'edit', 'show'
    ]);

    Route::get('/storeTest', 'RestaurantController@storeTest')->name('storeTest');
    Route::post('/addSellerItemsToStore', 'RestaurantController@addSellerItemsToStore');

    //New functions
    Route::get('/seller-menu', 'MenuController@indexNew')->name('seller-menu.indexNew');
    Route::get('/shop-menu', 'MenuController@indexNew')->name('shop-menu.indexNew');
    Route::post('/addSellerCategory', 'MenuController@addSellerCategory');
    Route::get('/edit_seller_category/{id}', 'MenuController@editSellerCategory');
    Route::post('/updateSellerCategory', 'MenuController@updateSellerCategory');
    Route::get('/sellerCategoryDestroy/{id}','MenuController@sellerCategoryDestroy');

    Route::post('/addShopCategory', 'MenuController@addShopCategory');
    Route::get('/editShopCategory/{id}', 'MenuController@editShopCategory');
    Route::post('/updateShopCategory', 'MenuController@updateShopCategory');
    Route::get('/deleteShopCategory/{id}','MenuController@deleteShopCategory');


    Route::get('/sellerCategoryAddon', 'MenuController@sellerCategoryAddon');
    Route::post('/add-category-addon', 'MenuController@addCategoryAddOn');
    Route::post('/edit-category-addon', 'MenuController@editCategoryAddOn');
    Route::get('/categoryAddonDestroy/{id}', 'MenuController@categoryAddonDestroy');

    Route::post('/addShopItem', 'MenuController@addShopItem');
    Route::post('/editShopItem', 'MenuController@updateShopItem');
    Route::get('/destroyShopItem/{id}', 'MenuController@destroyShopItem');

    Route::get('/shopCategoryAddon', 'MenuController@shopCategoryAddon');
    Route::post('/add-shop-category-addon', 'MenuController@addShopCategoryAddOn');
    Route::post('/edit-shop-category-addon', 'MenuController@editShopCategoryAddOn');
    Route::get('/shopCategoryAddonDestroy/{id}', 'MenuController@shopCategoryAddonDestroy');

    Route::get('/get_product_details/{id}', 'MenuController@getProductDetails');
});

//Route::fallback(function () {
//    return response()->json([
//        'data' => [],
//        'success' => false,
//        'status' => 404,
//        'message' => 'Invalid Route'
//    ]);
//});







