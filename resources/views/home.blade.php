@extends('layouts.app')
@section('content')
    <div class="home-bg" id="home-bg">

        <h1 class="centered">Discover something delicious</h1>

        <div class="bg-form  my-sm-0 my-md-0 my-lg-0  my-xl-0">
            <form class="form-inline">
                <div class="input-group">
                    <span class="location"><img src="images/location_small.png"></span> <input
                            class="form-control home-border search-bg" type="text" placeholder="Enter your address"
                            aria-label="Search" id="searchTextField" autocomplete="on" runat="server" value="{{$address}}">
                    <div class="input-group-addon "
                         style="margin-left: -50px; z-index: 3; border-radius: 40px; background-color: transparent; border:none;">
                        <span class="right-svg"> <i><img src="images/right_arrow1.svg" alt="" title=""></i></span>
                    </div>

                </div>
            </form>
        </div>


    </div>

    <!-- restaurant slider -->
    <div class="main-homebg main-padding">

        <div class="restaurant">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="restaurant-part text-center">

                            <img src="images/home_icon1.svg" alt="" title="">

                            <div class="title">
                                <h4 class="pt-5 f-24-b">Restaurant Partnerships</h4>
                            </div>

                            <div class="text">
                                <span>As a delivery driver, you’ll make reliable money - working anytime, anywhere</span>
                            </div>

                            <a href="#" class="f-16-g">Start earning</a>

                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="restaurant-part text-center">

                            <img src="images/home_icon2.svg" alt="" title="">

                            <div class="title">
                                <h4 class="pt-5  f-24-b">Pekkish Careers</h4>
                            </div>

                            <div class="text">
                                <span>Grow your business and reach new Customers by partnering with us.</span>
                            </div>

                            <a href="#" class="f-16-g">Sign up your store</a>

                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="restaurant-part text-center">

                            <img src="images/home_icon3.svg" alt="" title="">

                            <div class="title">
                                <h4 class="pt-5  f-24-b">Corporate Account</h4>
                            </div>

                            <div class="text">
                                <span>Get the best DoorDash experience With live order tracking</span>
                            </div>

                            <a href="#" class="f-16-g">Start earning</a>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- restaurant slider end -->
        <!-- Pekkish Picks start -->
        <div class="container-fluid">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Pekkish Picks</h3>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
        <span class="my-next-button" id="pekkishpicks-next-button">
          <i><img src="images/slider_left.svg" alt="" title=""></i>
        </span>
                        <span class="my-prev-button" id="pekkishpicks-prev-button">
          <i><img src="images/slider_right.svg" alt="" title=""></i>
        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="pekkishpicks">
                @foreach($pekkish_picks as $pekkish_pick)
                <div class="mr-3">
                    <div class="card" >
                        <img class="img-fluid img-radius" alt="" src="{{$pekkish_pick["logo"]}}" style="width:300px !important;height:140px !important;">
                        <a href="#"><img class=" img-display fav-slidertop" src="{{$pekkish_pick["favourite_icon"]}}" style="width:24px !important;height:24px !important;"></a>
                        @if($pekkish_pick["restaurant_tag"]!="")
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display"
                                         src="{{$pekkish_pick["restaurant_tag"]}}"></a>
                        @endif
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">{{$pekkish_pick["name"]}}</h4>
                            <p class="favourites-p favourites-text">{{$pekkish_pick["sub_label"]}}</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="{{$pekkish_pick["clock-icon"]}}" >{{$pekkish_pick["average_time"]}}</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">{{$pekkish_pick["ratings"]}}</span>
                            </button>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <!-- Pekkish Picks end -->
        <!-- Download our mobile app now!!! -->

        <div class="container-fluid download-appsbg mt-xl-3 mt-sm-0 mt-md-2 pb-sm-5">

            <div class="d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <div class="mr-auto p-2">
                    <h1 class="mt-xl-5 mt-sm-0 mt-md-1 pl-5"><img src="images/download_1.svg" alt="" title=""
                                                                  class="download-appimg"><span
                                class="download-apptext">Download our mobile app now!!!</span></h1>
                </div>
                <div class="p-2 mt-xl-5 mt-sm-0 mt-md-2">
                    <img src="images/playstore.png">
                </div>
                <div class="p-2 pr-xl-5 mt-xl-5 mt-sm-0 mt-md-1 ">
                    <img src="images/ios.png">
                </div>

            </div>
        </div>

        <!-- Download our mobile app now!!! end -->
        <!-- top Picks start -->
        <div class="container-fluid mt-xl-5 mt-sm-4 mt-md-5">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Top Picks</h3>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
                        <span class="my-next-button" id="toppicks-next-button">
                          <i><img src="images/slider_left.svg" alt="" title=""></i>
                        </span>
                                        <span class="my-prev-button" id="toppicks-prev-button">
                          <i><img src="images/slider_right.svg" alt="" title=""></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="toppicks">
                @foreach($pekkish_picks as $pekkish_pick)
                    <div class="mr-3">
                        <div class="card" >
                            <img class="img-fluid img-radius" alt="" src="{{$pekkish_pick["logo"]}}" style="width:300px !important;height:140px !important;">
                            <a href="#"><img class=" img-display fav-slidertop" src="{{$pekkish_pick["favourite_icon"]}}" style="width:24px !important;height:24px !important;"></a>
                            @if($pekkish_pick["restaurant_tag"]!="")
                                <a href="#"><img class=" img-fluid freedelivery-startop img-display"
                                                 src="{{$pekkish_pick["restaurant_tag"]}}"></a>
                            @endif
                            <div class="card-body">
                                <h4 class="order-delivery favourites-text">{{$pekkish_pick["name"]}}</h4>
                                <p class="favourites-p favourites-text">{{$pekkish_pick["sub_label"]}}</p>
                                <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                             src="{{$pekkish_pick["clock-icon"]}}" >{{$pekkish_pick["average_time"]}}</p>
                                <button type="button" class="btn rating-btn">
                                    <span class="fa fa-star pr-2"></span>
                                    <span class="star-text">{{$pekkish_pick["ratings"]}}</span>
                                </button>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="picks-bottom"></div>
        <!-- Pekkish Picks end -->
        <!-- Today’s Promos start -->
        <div class="container-fluid mt-xl-5 mt-sm-2 mt-md-3 mb-xl-5 mb-sm-2 mb-md-3">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Today’s Promos</h3>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">
                    <div class="my-owl-nav">
                        <span class="my-next-button" id="promo-next-button">
                          <i><img src="images/slider_left.svg" alt="" title=""></i>
                        </span>
                                        <span class="my-prev-button" id="promo-prev-button">
                          <i><img src="images/slider_right.svg" alt="" title=""></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="promo">
                @foreach($today_deals as $today_deal)
                    <div class="mr-3">
                        <img src="{{$today_deal["image"]}}" class="img-fluid img-radius" alt="" title="" style="height: 205px !important;width:205px !important;">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="picks-bottom"></div>
        <!-- Today’s Promos end -->
        <!-- Buy 1 Get 1 Free -->
        <div class="container-fluid mt-xl-5 mt-sm-2 mt-md-3">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Buy 1 Get 1 Free</h3>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
                        <span class="my-next-button" id="free-next-button">
                          <i><img src="images/slider_left.svg" alt="" title=""></i>
                        </span>
                                        <span class="my-prev-button" id="free-prev-button">
                          <i><img src="images/slider_right.svg" alt="" title=""></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="free">
                @foreach($pekkish_picks as $pekkish_pick)
                    <div class="mr-3">
                        <div class="card" >
                            <img class="img-fluid img-radius" alt="" src="{{$pekkish_pick["logo"]}}" style="width:300px !important;height:140px !important;">
                            <a href="#"><img class=" img-display fav-slidertop" src="{{$pekkish_pick["favourite_icon"]}}" style="width:24px !important;height:24px !important;"></a>
                            {{--@if($pekkish_pick["restaurant_tag"]!="")--}}
                                {{--<a href="#"><img class=" img-fluid freedelivery-startop img-display"--}}
                                                 {{--src="{{$pekkish_pick["restaurant_tag"]}}"></a>--}}
                            {{--@endif--}}
                            <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                            <div class="card-body">
                                <h4 class="order-delivery favourites-text">{{$pekkish_pick["name"]}}</h4>
                                <p class="favourites-p favourites-text">{{$pekkish_pick["sub_label"]}}</p>
                                <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                             src="{{$pekkish_pick["clock-icon"]}}" >{{$pekkish_pick["average_time"]}}</p>
                                <button type="button" class="btn rating-btn">
                                    <span class="fa fa-star pr-2"></span>
                                    <span class="star-text">{{$pekkish_pick["ratings"]}}</span>
                                </button>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- Buy 1 Get 1 Free end -->

        <!-- Download our mobile app now!!! -->

        <div class="container-fluid download-appsbg mt-xl-3 mt-sm-0 mt-md-2">

            <div class="d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <div class="mr-auto p-2">
                    <h1 class="mt-xl-5 mt-sm-0 mt-md-1 pl-5"><img src="images/download_1.svg" alt="" title=""
                                                                  class="download-appimg"><span
                                class="download-apptext">Download our mobile app now!!!</span></h1>
                </div>
                <div class="p-2 mt-xl-5 mt-sm-0 mt-md-2">
                    <img src="images/playstore.png">
                </div>
                <div class="p-2 pr-xl-5 mt-xl-5 mt-sm-0 mt-md-1 ">
                    <img src="images/ios.png">
                </div>

            </div>
        </div>

        <!-- Download our mobile app now!!! end -->

        <!-- From the pekkish start -->
        <div class="container-fluid">

            <div class=" p-5 mx-auto mt-3 mb-3">
                <div class="d-flex flex-row bd-highlight mb-3">
                    <div class="p-2 bd-highlight">
                        <h1 class="f-45">From the pekkish</h1>
                    </div>

                </div>
                <div class="d-flex flex-row-reverse bd-highlight">
                    <div class="p-2 bd-highlight">
                        <a href="#" class="view-text">View All</a>
                    </div>

                </div>
                <div id="carouselTestimonial" class="carousel carousel-testimonial slide" data-ride="carousel"
                     data-interval="false">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselTestimonial" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselTestimonial" data-slide-to="1"></li>
                        <li data-target="#carouselTestimonial" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="carousel-item text-center active">
                            <div class="carousel-testimonial-img p-1 m-auto">
                                <div class="row testmonials-bg">
                                    <div class="col-md-6">

                                        <img src="images/testmonials.png" alt="" class="w-100 testmonials-img"/>
                                    </div>
                                    <!-- /.col-md-6 -->
                                    <div class="col-md-6">

                                        <div class="row  h-100">
                                            <div class="col">
                                                <h1 class="f-32-bold pt-xl-5 pt-sm-3 pt-md-4">Visa brings it home
                                                    <br>with Pekkish</h1>
                                                <p class="f-14-textblack pt-xl-3 pt-sm-1 pt-md-3">Sed ut perspiciatis
                                                    unde omnis iste natus error sit voluptatem accusantium doloremque
                                                    laudantium s iste natus error sit voluptatem accusantium doloremque
                                                    laudantium accusantium doloremque laudantium s iste natus error sit
                                                    voluptatem accusantium doloremque laudantium
                                                </p>
                                                <div class="d-flex flex-row bd-highlight mb-3">
                                                    <div class="p-2 bd-highlight">
                                                        <a href="" class="lernmore-green pt-xl-3 pt-sm-2 pt-md-3">LEARN
                                                            MORE <img src="images/lernmore.svg" alt="" title=""></a>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                            </div>

                        </div>
                        <div class="carousel-item text-center">
                            <div class="carousel-testimonial-img p-1  m-auto">
                                <div class="row testmonials-bg">
                                    <div class="col-md-6">

                                        <img src="images/testmonials.png" alt="" class="w-100 testmonials-img"/>
                                    </div>
                                    <!-- /.col-md-6 -->
                                    <div class="col-md-6">

                                        <div class="row  h-100">
                                            <div class="col">
                                                <h1 class="f-32-bold pt-xl-5 pt-sm-3 pt-md-4">Visa brings it home
                                                    <br>with Pekkish</h1>
                                                <p class="f-14-textblack pt-xl-3 pt-sm-1 pt-md-3">Sed ut perspiciatis
                                                    unde omnis iste natus error sit voluptatem accusantium doloremque
                                                    laudantium s iste natus error sit voluptatem accusantium doloremque
                                                    laudantium accusantium doloremque laudantium s iste natus error sit
                                                    voluptatem accusantium doloremque laudantium
                                                </p>
                                                <div class="d-flex flex-row bd-highlight mb-3">
                                                    <div class="p-2 bd-highlight">
                                                        <a href="" class="lernmore-green pt-xl-3 pt-sm-2 pt-md-3">LEARN
                                                            MORE <img src="images/lernmore.svg" alt="" title=""></a>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                            </div>

                        </div>
                        <div class="carousel-item text-center">
                            <div class="carousel-testimonial-img p-1  m-auto">
                                <div class="row testmonials-bg">
                                    <div class="col-md-6">

                                        <img src="images/testmonials.png" alt="" class="w-100 testmonials-img"/>
                                    </div>
                                    <!-- /.col-md-6 -->
                                    <div class="col-md-6">

                                        <div class="row  h-100">
                                            <div class="col">
                                                <h1 class="f-32-bold pt-xl-5 pt-sm-3 pt-md-4">Visa brings it home
                                                    <br>with Pekkish</h1>
                                                <p class="f-14-textblack pt-xl-3 pt-sm-1 pt-md-3">Sed ut perspiciatis
                                                    unde omnis iste natus error sit voluptatem accusantium doloremque
                                                    laudantium s iste natus error sit voluptatem accusantium doloremque
                                                    laudantium accusantium doloremque laudantium s iste natus error sit
                                                    voluptatem accusantium doloremque laudantium
                                                </p>
                                                <div class="d-flex flex-row bd-highlight mb-3">
                                                    <div class="p-2 bd-highlight">
                                                        <a href="" class="lernmore-green pt-xl-3 pt-sm-2 pt-md-3">LEARN
                                                            MORE <img src="images/lernmore.svg" alt="" title=""></a>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                            </div>


                        </div>
                    </div>

                    <div>

                        <a class="carousel-control-prev" href="#carouselTestimonial" role="button" data-slide="prev">
                            <span><img src="images/slider_left.svg"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselTestimonial" role="button" data-slide="next">
                            <span><img src="images/slider_right.svg"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Info -->

        <!-- From the pekkish end -->


        <!-- Faqus -->
        <div class="container-fluid">

            <div class="row pt-5 pb-5">
                <h1 class="f-faq">You’ve got questions.
                    <br> &nbsp; We’ve got answers.
                    <br>
                    <a href="" class="faq-green pt-xl-3 pt-sm-2 pt-md-3">SEE FAQs <img src="images/lernmore.svg" alt=""
                                                                                       title=""></a>
                </h1>
                <img src="images/faq.svg" class="faq-img" alt="" title="">

            </div>
        </div>
        <!-- faqs end -->


    </div>

    <script>
        function Scrolldown() {
            var screen_height=$('#home-bg').height();
            window.scroll({top:screen_height,behavior: 'smooth'});
        }

        window.onload = Scrolldown;

        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('city2').value = place.name;
                document.getElementById('cityLat').value = place.geometry.location.lat();
                document.getElementById('cityLng').value = place.geometry.location.lng();
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection