@extends('layouts.app')
@push('css')
    <style>
        .stv-radio-tabs-wrapper {
            display: inline-block;
            width: 100%;
            border-bottom: 1px solid #428bca;
            padding: 0px;
        }
        input.stv-radio-tab {
            position: absolute;
            left: -99999em;
            top: -99999em;
        }
        input.stv-radio-tab + label {
            cursor: pointer;
            float: left;
            border: 1px solid #aaa;
            border-bottom: 0;
            background-color: #fff;
            margin-right: -1px;
            padding: 0.5em 1em;
            position: relative;
            margin-bottom: 0;
            width: 50%;
        }
        input.stv-radio-tab + label:hover {
            background-color: #eee;
        }
        input.stv-radio-tab:checked + label {
            box-shadow: 0 3px 0 -1px #fff, inset 0 5px 0 -1px #13cd4a;
            background-color: #fff;
            border-color: #428bca;
            z-index: 1;
        }
        .category-scroll {
            min-height: calc(100vh - 239px);
            max-height: calc(100vh - 239px);
            overflow-y: scroll;
            -ms-overflow-style: none;
            scrollbar-width: thin;
            mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
            mask-size: 100% 20000px;
            mask-position: left bottom;
            -webkit-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
            -webkit-mask-size: 100% 20000px;
            -webkit-mask-position: left bottom;
            -moz-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
            -moz-mask-size: 100% 20000px;
            -moz-mask-position: left bottom;
            transition: mask-position 0.3s, -webkit-mask-position 0.3s;
        }

        .category-scroll::-webkit-scrollbar {
             display: none;

        }

    </style>
@endpush
@section('content')

    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 bg-black pt-3 menu-list-fixed">
                 <nav class="navbar-options menu-list-items">
                        <ul class="nav flex-column flex-nowrap overflow-hidden">
                            <li class="nav-item">
                                <form class="category-scroll" id="category_filter" action="" method="GET">
                               @foreach($categories ?? '' as $category)
                                   <div class="d-flex align-itms-center justify-content-between">
                                            <a class="nav-link text-truncate" onclick="selectcatagory({{$category->id}},'{{$category->name}}')">
                                                <span class="menu-link ">{{$category->name}}</span></a>
                                            <a onclick="editcategory({{$category->id}})">
                                                <img src="./images/card-category-edit.svg" alt="" style="background-color: #fff">
                                            </a>
                                       <a onclick="confirm_modal('{{url('category_destroy', $category->id)}}');" >
                                           <img class="mr-2" src="./images/delete.svg" alt="" style="background-color: #fff">
                                       </a>
                                        </div>
                                @endforeach
                                   <input type="hidden" name="search"  id="search" >
                                   <input type="hidden" name="search_category"  id="search_category" >
                                </form>
                            </li>
                        </ul>
                    </nav>

                    <div class="menu-list-items d-flex justify-content-end">
                        <button class="add-section mt-3"  data-toggle="modal"
                                data-target="#add_section">
                            + Add Category
                        </button>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 main-section-padding">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('failed'))
                        <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
                    @endif
                    <div class="res-cards d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading"><span id="selectedcatg">@if(request()->get('search')) {{request()->get('search_category')}} @else All @endif</span>

                        </h4>
                        <button class="add-btn" onclick="openpopupmodel();">+ Add Item</button>
                    </div>

                    <div class="table-responsive">
                        <table class="table" id="products">
                            <thead class="thead-light">
                            <tr>
                                <th>Item Name</th>
                                <th>Available Days</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th>Availability</th>
                                <th>Actions</th>
                                {{--<th class="table-controls-button" id="table-button">--}}
                                    {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
                                    {{--<ul class="controls-dropdown-box" id="controls-dropdown-menu" style="display: none;"></ul>--}}
                                {{--</th>--}}

                            </tr>
                            </thead>
                            <tbody>
                            @if($itemlist =='')
                                <tr><td colspan="10">No data Available</td></tr>
                            @else
                                @foreach($itemlist as $items)
                                    <tr>
                                        <td>
                                            {{$items->name}}
                                        </td>
                                        <td>
                                            @if(($items->is_all_time_avaliable == 1))
                                                All Days
                                            @else
                                                @foreach($items->product_timing as $daysr)
                                                    {{$weekMap[$daysr->day_id]}}
                                                    @if((count($items->product_timing)-1) == $loop->index)  @else, @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            {{$items->category}}
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <span>$</span><span>{{$items->unit_price}}</span>
                                            </div>
                                        </td>
                                        <td>{{$items->description}}
                                            {{--@foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach--}}
                                        </td>
                                        <td>
                                            <img class="tbl-img" src="{{$items->thumbnail_img}}" alt="">
                                        </td>
                                        <td>
                                            <div class="checkbox checbox-switch switch-primary">
                                                <label>
                                                    <input type="checkbox" name="" {{($items->status=='1')?"checked":""}} onclick="product_available_sts('{{url('productstatus', $items->id.'@'.$items->status)}}');"/>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="d-flex">
                                            <a onclick="confirm_modal('{{url('menu_destroy', $items->id)}}');">
                                                <img class="mr-2" src="./images/delete.svg" alt="" >
                                            </a>
                                            <a onclick="update_modal({{json_encode($items)}});">
                                                <img src="./images/card-edit.svg" alt="">
                                            </a>
                                        </td>
                                        {{--<td></td>--}}
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                        <div class="row float-right">

                            <div class="col-12">
                                <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                                    <div class="d-flex ">
                                    </div>

                                    <div id="download_products">
                                        <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                                        <img src="./images/download.svg" alt="">
                                        <div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>



                </div>
            </div>
        </div>

    </div>






    @include('inc.side_nav')

    <div class="modal add-menu right fade " id="addmenuid" tabindex="-1" role="dialog" aria-labelledby="add-menu">
        <div class="modal-dialog" id="addnewmenu" role="add menu">
            <div class="modal-content model-row-colum ">
                <div class="modal-body pb-4 dialog-options-responsive">
                    <div class="bg-black text-color-white negative-margin" style="display:none;" id="showSearchDiv">
                        <nav class="navbar-options menu-list-items d-flex ">
                            <ul class="nav flex-column flex-nowrap ">
                                <li class="nav-item">
                                    @foreach($categories as $category)
                                        <a class="nav-link text-truncate" onclick="getproducts({{json_encode($category)}});">
                                            <span class="menu-link ">{{$category->name}} </span>
                                        </a>
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="productitems">
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="overflow-dialog">
                        <form id="manage-menu-formmodel" action="{{url('item_edit')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="f-20 f-medium">Edit Item</div>
                            <div class="form-group input-material">
                                <input type="hidden" class="form-control" name="id" id="id">
                                <input type="text" class="form-control" name="name" id="itemSearch" data-validation="required">
                                <label for="itemSearch">Item Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <textarea class="form-control" name="description" id="description" rows="2" data-validation="required"></textarea>
                                <label for="textarea-field">Item Description<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey my-2" >
                                0 / 40</div>
                            <div class="upload-image-section">
                                <img src="" alt="" id="image-preview" class="img-fluid" height="height:100px;">
                                <label class="btn btn-default btn-sm center-block btn-file">
                                    <span class="text-color-grey">+ ADD IMAGE (500px X 600px)<span style="color:red">*</span></span>
                                    <input type="file" name="photos" id="photos" style="display: none;" onchange="readURL(this);">
                                </label>
                                <div class="text-danger" id="imagesizevaliderror"></div>
                            </div>

                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" step="0.01" name="unit_price" class="form-control" id="unit_price" required>
                                                <label for="ItemPrice">Item Price<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('unit_price') }}</div>
                                            </div>
                                        </div>
                                        {{--<div class="col-12 col-sm-6">--}}
                                            {{--<div class="form-group input-material mr-2">--}}
                                                {{--<select name="service_line" id="service_line" class="form-control">--}}
                                                         {{--<option value="1">Delivery</option>--}}
                                                          {{--<option value="2">Pick Up</option>--}}
                                                          {{--<option value="3">Food @work</option>--}}
                                                {{--</select>--}}
                                                {{--<input type="number" name="service_line" class="form-control" id="service_line">--}}
                                                {{--<label for="Select Service Line">Select Service Line<span style="color:red">*</span></label>--}}
                                                {{--<div class="text-danger">{{ $errors->first('service_line') }}</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="available_days[]" id="available_days" multiple="multiple"  class="form-control demo-select2" data-validation="required">
                                                    <option value="0">Sun</option>
                                                    <option value="1">Mon</option>
                                                    <option value="2">Tue</option>
                                                    <option value="3">Wed</option>
                                                    <option value="4">Thu</option>
                                                    <option value="5">Fri</option>
                                                    <option value="6">Sat</option>
                                                </select>
                                                <label for="Available Days">Available Days<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                                <div class="text-danger">{{ $errors->first('available_days') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="menu_items_desc" id="menu_items_desc" class="form-control" data-validation="required" required>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" >{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="Menu For">Category<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" name="set_max_order_number" class="form-control" id="set_max_order_number"
                                                       required>
                                                <label for="Set Max Order Number">Set Max Order Number<span style="color:red">*</span></label>
                                            </div>
                                        </div>


                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select id="cuisine_id" name="cuisine_id" class="form-control">

                                                    @foreach ($cuisines as $cuisine)
                                                        <option value="{{$cuisine->id}}">{{$cuisine->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                    <div class="text-danger">{{ $errors->first('cuisines') }}</div>
                                                @endif
                                                <label for="Cusine Type">Cuisine Type</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Availability Status</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="1" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Not Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="0">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Type</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="check">Recommended
                                                <input type="checkbox" name="featured" id="featured" value="1" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="check">Healthy Picks
                                                <input type="checkbox" name="healthpicks" id="healthpicks" value="1" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Type<span style="color:red">*</span></p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Veg.
                                                <input type="radio" name="veg_nonveg" id="veg" value="1" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Non-Veg.
                                                <input type="radio" name="veg_nonveg" id="nonveg" value="2" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <label class="check">Modifier Required
                                    <input type="checkbox" name="Modifier" id="Modifiercheck" value="true">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div  id="varientaddon">
                                <div class="d-flex align-items-end justify-content-end mb-0">
                                    {{--<label class="radio mx-3">Variants
                                        <input type="radio" name="varientitem" id="varientsitem" value="1">
                                        <span class="checkround"></span>
                                    </label>
                                    <label class="radio">Add-ons
                                        <input type="radio" name="varientitem" id="addonitem" value="1">
                                        <span class="checkround"></span>
                                    </label>--}}
                                    <div class="stv-radio-tabs-wrapper">
                                        <input type="radio" class="stv-radio-tab" name="varientitem" id="varientsitem" value="1" checked />
                                        <label for="varientsitem">Variants</label>
                                        <input type="radio" class="stv-radio-tab" name="varientitem" id="addonitem" value="1" />
                                        <label for="addonitem">Add-ons</label>
                                    </div>
                                </div>
                            </div>



                            <input type="hidden" class="form-control" id="dynamicvariant">
                            <input type="hidden" class="form-control" id="dynamicaddonincr">
                            <div class="modifiactions-required"  id="variants_div">
                                <div class="d-flex align-items-end justify-content-end mb-0" style="display:none !important;">
                                    <label class="radio mx-3">Optional
                                        <input type="radio" name="variant_optional" value="true">
                                        <span class="checkround"></span>
                                    </label>
                                    <label class="radio">Required
                                        <input type="radio" name="variant_optional" value="false">
                                        <span class="checkround"></span>
                                    </label>
                                </div>
                                <div class="form-group input-material" style="display:none;">
                                    <input type="text" class="form-control" id="variant_ModifierName">
                                    <label for="Modifier Name">Modifier Name</label>
                                </div>
                                <div class="my-3" style="display:none;">
                                    <p class="mb-3 text-color-grey f-14">Options for Selection</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Select at least one
                                                    <input type="radio" name="variant_selection" value="single" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Multiple Selection Required
                                                    <input type="radio" name="variant_selection" value="multiple">
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-0 text-color-grey f-14 d-flex justify-content-between">
                                    <p >Options</p>
                                    <p  id="addmorevarients">+ Add row</p>
                                </div>
                                <div>
                                    <div class="container-fluid p-0">
                                        <div class="row" id="addvariants">
                                      <!--  <div class="col-8 addvarientststic">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" name="variants[0][variant]" class="form-control" id="opt1">
                                                    <label for="opt1">Name the Option </label>
                                                </div>
                                            </div>
                                            <div class="col-4 addvarientststic">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" name="variants[0][price]" step="0.01" class="form-control" id="Price">
                                                    <label for="Price">Price</label>
                                                </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="addons_div">
                                <div id="addons_subdiv">

                                    <div id="form-block" class="modifiactions-required addonsstaticsubdiv">
                                        <div class="d-flex align-items-end justify-content-end mb-0">
                                            <label class="radio mx-3">Optional
                                                <input type="radio" name="items[0][optional]" value="true" checked>
                                                <span class="checkround"></span>
                                            </label>
                                            <label class="radio">Required
                                                <input type="radio" name="items[0][optional]" value="false">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div>
                                            <div class="form-group input-material mb-5">
                                                <input type="text" name="items[0][ModifierName]" class="form-control" id="ModifierName">
                                                <label for="Modifier Name">Modifier Name</label>
                                            </div>
                                            <div class="my-5">
                                                <p class="mb-3 text-color-grey f-14">Options for Selection</p>
                                                <div class="container-fluid p-0">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <label class="radio">Select at least one
                                                                <input type="radio" name="items[0][selection]" value="true" checked>
                                                                <span class="checkround"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="radio">Multiple Selection Required
                                                                <input type="radio" name="items[0][selection]" value="false">
                                                                <span class="checkround"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="mt-2 mb-2">
                                                <div class="mb-0 text-color-grey f-14 d-flex justify-content-between">
                                                    <p >Options</p>
                                                    <p  id="addmoreitems">+ Add row</p>
                                                </div>

                                                <div class="container-fluid p-0">
                                                    <div class="row" id="additemrows">
                                                        <div class="col-8">
                                                            <div class="form-group input-material mr-2">
                                                                <input type="hidden" name="items[0][items][0][id]" value = "1" class="form-control" id="opt1">
                                                                <input type="text" name="items[0][items][0][name]" class="form-control" id="opt1">
                                                                <label for="opt1">Name the Option </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="form-group input-material mr-2">
                                                                <input type="number" name="items[0][items][0][price]" step="0.01" class="form-control" id="Price">
                                                                <label for="Price">Price</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="addmoreaddons" class="add_more d-flex align-items-center justify-content-center text-color-grey">
                                    + ADD MORE
                                </div>
                            </div>

                            <div>
                                <div class="mb-3">
                                    <label class="check">Recommended Pairing
                                        <input type="checkbox" name="recommended" id="recommended" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">

                                    <div>
                                        <div class="d-flex align-items-center flex-wrap">
                                            <div id="itemmenu"></div>
                                            <input type="hidden" name="recommendedproducts[]" id="recommendedproducts">
                                        </div>
                                    </div>

                                    <!---<button class="employee-options option-selected">
                                                     <span>Chicken Periperi</span><span class="ml-2"><img
                                                             src="./images/clearoption.svg" alt=""></span></button>
                                                 <button class="employee-options option-selected">
                                                     <span>Chicken Periperi</span><span class="ml-2"><img
                                                             src="./images/clearoption.svg" alt=""></span></button>-->

                                </div>

                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close">CANCEL</button>
                                <button class="addbtn" id="addbtn">UPDATE ITEM</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div><!-- modal-content -->

        </div><!-- modal-dialog -->
    </div><!-- modal -->


    <!-- Modal -->
    <div class="modal fade" id="archieve" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" id="categorymodel">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to archieve the item “Chicken Periperi"</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO</button>
                        <button class="addbtn w-100 mx-3 f-medium" data-dismiss="modal">YES</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{--@if(session()->has('error_code') == 5)--}}
    {{--        @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
            <script>
                $(function() {
                    $('#categorymodel').modal('show');
                });
            </script>
        @endif--}}
    <!-- Modal -->
    <div class="modal fade" id="add_section" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{url('addCategory')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="f-20 f-medium" style="text-align: center">Add Category</div>
                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <div class="panel-body">
                            <div class="form-group input-material mr-2">
                                <input type="text" id="category_name" name="name" class="form-control" data-validation="required" required>
                                <label for="name">Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <!--   <div id="inputStateselect">
                               <div class="form-group ">
                                   <select name="digital" id="digital" class="form-control">
                                       <option value="0">Physical</option>
                                       <option value="1">Digital</option>
                                   </select>
                                   <label for="digital">Type</label>
                               </div>
                               </div>

                               <div class="form-group input-material mr-2">
                                   <input type="file" id="banner" name="banner" class="form-control" required>
                                   <label for="banner">Banner</label>
                               </div>
                               <div class="form-group input-material mr-2">
                                   <input type="file" id="icon" name="icon" class="form-control" required>
                                   <label for="icon">Icon<small>(32x32)</small></label>
                               </div>-->

                            <div class="form-group input-material mr-2">
                                <input type="text" id="meta_title" name="meta_title" class="form-control" data-validation="required" required>
                                <label for="meta_title">Meta Title<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('meta_title') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <textarea name="meta_description" rows="3" id="meta_description" class="form-control" data-validation="required" required></textarea>
                                <label for="meta_description">Description<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('meta_description') }}</div>
                            </div>
                        </div>

                        <div class="mt-2 mb-0">
                            <p class="mb-0 text-color-grey f-14">Set Timings</p>
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group input-material mr-2">
                                            <input type="time" class="form-control"  name="start_time" id="Start" value="00:00" data-validation="required" required>
                                            <label for="Start" class="datetype" style="top:-10px;">Start Time<span style="color:red">*</span></label>
                                            <div class="text-danger">{{ $errors->first('start_time') }}</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group input-material mr-2">
                                            <input type="time" class="form-control" name="end_time" id="end" data-validation="required" value="23:59" required>
                                            <label for="end" class="datetype" style="top:-10px;">End Time<span style="color:red">*</span></label>
                                            <div class="text-danger">{{ $errors->first('end_time') }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <button type="submit" class="addbtn w-100 mx-3 f-medium">YES</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <!-- Edit Modal -->
    <div class="modal fade" id="edit_section" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{url('UpdateCategory')}}" method="POST" enctype="multipart/form-data">
                    @csrf
            <div class="f-20 f-medium" style="text-align: center">Edit Category</div>
            <div class="modal-body p-4">
                <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                    <img src="./images/dialogclose.svg" alt="">
                </div>
                <div class="panel-body">
                    <div class="form-group input-material mr-2">
                        <input type="hidden" id="category_id" name="id" class="form-control" data-validation="required" required>
                        <input type="text" id="category_name_edit" name="name" class="form-control" data-validation="required" required>
                        <label for="name">Name<span style="color:red">*</span></label>
                        <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <!--   <div id="inputStateselect">
                               <div class="form-group ">
                                   <select name="digital" id="digital" class="form-control">
                                       <option value="0">Physical</option>
                                       <option value="1">Digital</option>
                                   </select>
                                   <label for="digital">Type</label>
                               </div>
                               </div>

                               <div class="form-group input-material mr-2">
                                   <input type="file" id="banner" name="banner" class="form-control" required>
                                   <label for="banner">Banner</label>
                               </div>
                               <div class="form-group input-material mr-2">
                                   <input type="file" id="icon" name="icon" class="form-control" required>
                                   <label for="icon">Icon<small>(32x32)</small></label>
                               </div>-->

    <div class="form-group input-material mr-2">
        <input type="text" id="meta_title_edit" name="meta_title" class="form-control" data-validation="required" required>
        <label for="meta_title">Meta Title<span style="color:red">*</span></label>
        <div class="text-danger">{{ $errors->first('meta_title') }}</div>
    </div>

    <div class="form-group input-material">
        <textarea name="meta_description" rows="3" id="meta_description_edit" class="form-control" data-validation="required" required></textarea>
        <label for="meta_description">Description<span style="color:red">*</span></label>
        <div class="text-danger">{{ $errors->first('meta_description') }}</div>
    </div>
    </div>

    <div class="mt-2 mb-0">
        <p class="mb-0 text-color-grey f-14">Set Timings</p>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-6">
                    <div class="form-group input-material mr-2">
                        <input type="time" class="form-control" name="start_time" id="Start_edit" data-validation="required" required>
                        <label for="Start" class="datetype" style="top:-10px;">Start Time<span style="color:red">*</span></label>
                        <div class="text-danger">{{ $errors->first('start_time') }}</div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group input-material mr-2">
                        <input type="time" class="form-control" name="end_time" id="end_edit" data-validation="required" required>
                        <label for="end" class="datetype" style="top:-10px;">End Time<span style="color:red">*</span></label>
                        <div class="text-danger">{{ $errors->first('end_time') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <div class="mb-2">
        <div class=" mb-3 d-flex align-items-center justify-content-around">

            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                    aria-label="Close">NO</button>
            <button type="submit" class="addbtn w-100 mx-3 f-medium">YES</button>

        </div>
    </div>
    </form>
    </div>
    </div>
    </div>
    <div class="modal fade" id="product_sts" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to change item status ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">
                        <button class="cancelbtn w-100 mx-3 f-medium" onclick="itemAvaailableCancelModel();">NO</button>
                        <a id="productsts_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to delete it ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO</button>
                        <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    {{--pop up  model started--}}

    <div class="modal add-menu right fade " id="new_addmenuid" tabindex="-1" role="dialog" aria-labelledby="add-menu">
        <div class="modal-dialog" id="newaddnewmenu" role="add menu">
            <div class="modal-content model-row-colum ">
                <div class="modal-body pb-4 dialog-options-responsive">
                    <div class="bg-black text-color-white negative-margin" style="display:none;" id="newshowSearchDiv">
                        <nav class="navbar-options menu-list-items d-flex ">
                            <ul class="nav flex-column flex-nowrap ">
                                <li class="nav-item">

                                    @foreach($categories as $category)
                                        <a class="nav-link text-truncate" onclick="newgetproducts({{json_encode($category)}});">
                                            <span class="menu-link ">{{$category->name}} </span>
                                        </a>
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="newproductitems">
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="overflow-dialog">
                        <form id="new_manage-menu-formmodel" action="{{url('addItem')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="f-20 f-medium">Add Item</div>
                            <div class="form-group input-material">
                                <input type="hidden" class="form-control" name="id" id="id">
                                <input type="text" class="form-control" name="name" id="itemSearch" required data-validation="required">
                                <label for="itemSearch">Item Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <textarea class="form-control" name="description" id="description" rows="2" data-validation="required" required></textarea>
                                <label for="textarea-field">Item Description<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey my-2" >
                                0 / 40</div>
                            <div class="upload-image-section">
                                <img src="" alt="" id="newimage-preview" class="img-fluid" height="height:100px;">
                                <label class="btn btn-default btn-sm center-block btn-file">
                                    <span class="text-color-grey">+ ADD IMAGE (500px X 600px)<span style="color:red">*</span></span>
                                    <input type="file" name="photos" id="newphotos" style="display: none;" onchange="newreadURL(this);" required>
                                </label>
                                <div class="text-danger" id="newimagesizevaliderror"></div>
                            </div>

                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" step="0.01" name="unit_price" class="form-control" id="unit_price" data-validation="required" required>
                                                <label for="ItemPrice">Item Price<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('unit_price') }}</div>
                                            </div>
                                        </div>
                                        {{--<div class="col-12 col-sm-6">--}}
                                            {{--<div class="form-group input-material mr-2">--}}
                                                {{--<select name="service_line" id="service_line" class="form-control">--}}
                                                    {{--<option value="1">Delivery</option>--}}
                                                    {{--<option value="2">Pick Up</option>--}}
                                                    {{--<option value="3">Food @work</option>--}}
                                                {{--</select>--}}
                                                {{--@if ($errors->has('cuisines'))--}}
                                                    {{--<div class="text-danger">{{ $errors->first('cuisines') }}</div>--}}
                                                {{--@endif--}}
                                                {{--<label for="Cusine Type">Cuisine Type</label>--}}
                                                {{--<div class="linebottom"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}


                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="available_days[]" id="available_days" multiple="multiple" class="form-control demo-select2" data-validation="required" required>
                                                    <option value="0">Sun</option>
                                                    <option value="1">Mon</option>
                                                    <option value="2">Tue</option>
                                                    <option value="3">Wed</option>
                                                    <option value="4">Thu</option>
                                                    <option value="5">Fri</option>
                                                    <option value="6">Sat</option>
                                                </select>
                                                <label for="Available Days">Available Days<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="menu_items_desc" id="menu_items_desc" class="form-control" data-validation="required">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" @php if($category->id == request()->get('search')) echo "selected"; @endphp>{{$category->name}}</option>
                                                    @endforeach$category
                                                </select>
                                                <label for="Menu For">Category<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" name="set_max_order_number" class="form-control" id="set_max_order_number" data-validation="required"
                                                required>
                                                <label for="Set Max Order Number">Set Max Order Number</label>
                                            </div>
                                        </div>


                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select id="cuisine_id" name="cuisine_id" class="form-control" required>

                                                    @foreach ($cuisines as $cuisine)
                                                        <option value="{{$cuisine->id}}">{{$cuisine->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                    <div class="text-danger">{{ $errors->first('cuisines') }}</div>
                                                @endif
                                                <label for="Cusine Type">Cuisine Type</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Availability Status</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="1" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Not Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="0">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Type</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="check">Recommended
                                                <input type="checkbox" name="featured" id="featured" value="1" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="check">Healthy Picks
                                                <input type="checkbox" name="healthpicks" id="healthpicks" value="1" >
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Type<span style="color:red">*</span></p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Veg.
                                                <input type="radio" name="veg_nonveg" id="veg" value="1" data-validation="required">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Non-Veg.
                                                <input type="radio" name="veg_nonveg" id="nonveg" value="2" data-validation="required">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <label class="check">Modifier Required
                                    <input type="checkbox" name="Modifier" id="newModifiercheck" value="true">
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                            <div  id="newvarientaddon">
                                <div class="d-flex align-items-end justify-content-end mb-0">
                                    <div class="stv-radio-tabs-wrapper">
                                        <input type="radio" class="stv-radio-tab" name="varientitem" id="newvarientsitem" value="1" checked />
                                        <label for="newvarientsitem">Variants</label>
                                        <input type="radio" class="stv-radio-tab" name="varientitem" id="newaddonitem" value="1" />
                                        <label for="newaddonitem">Add-ons</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" id="newdynamicvariant">
                            <input type="hidden" class="form-control" id="newdynamicaddonincr">
                            <div class="modifiactions-required"  id="newvariants_div">
                                <div class="d-flex align-items-end justify-content-end mb-0" style="display:none !important;">
                                    <label class="radio mx-3">Optional
                                        <input type="radio" name="variant_optional" value="true">
                                        <span class="checkround"></span>
                                    </label>
                                    <label class="radio">Required
                                        <input type="radio" name="variant_optional" value="false">
                                        <span class="checkround"></span>
                                    </label>
                                </div>
                                <div class="form-group input-material" style="display:none;">
                                    <input type="text" class="form-control" id="variant_ModifierName">
                                    <label for="Modifier Name">Modifier Name</label>
                                </div>
                                <div class="my-3" style="display:none;">
                                    <p class="mb-3 text-color-grey f-14">Options for Selection</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Select at least one
                                                    <input type="radio" name="variant_selection" value="single" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Multiple Selection Required
                                                    <input type="radio" name="variant_selection" value="multiple">
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="newaddoninitialval">
                                <div class="mb-0 text-color-grey f-14 d-flex justify-content-between">
                                    <p >Options</p>
                                    <p  id="newaddmorevarients">+ Add row</p>
                                </div>
                                <div>
                                    <div class="container-fluid p-0">
                                        <div class="row" id="newaddvariants" >
                                    <!--    <div class="col-8 addvarientststic">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" name="variants[0][variant]" class="form-control" id="opt1">
                                                    <label for="opt1">Name the Option </label>
                                                </div>
                                            </div>
                                            <div class="col-4 addvarientststic">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" name="variants[0][price]" step="0.01" class="form-control" id="Price">
                                                    <label for="Price">Price</label>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="newaddons_div">
                                <div id="newaddons_subdiv">
                        <!--      <div id="form-block" class="modifiactions-required addonsstaticsubdiv">
                                        <div class="d-flex align-items-end justify-content-end mb-0">
                                            <label class="radio mx-3">Optional
                                                <input type="radio" name="items[0][optional]" value="true" checked>
                                                <span class="checkround"></span>
                                            </label>
                                            <label class="radio">Required
                                                <input type="radio" name="items[0][optional]" value="false">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div>
                                            <div class="form-group input-material mb-5">
                                                <input type="text" name="items[0][ModifierName]" class="form-control" id="ModifierName">
                                                <label for="Modifier Name">Modifier Name</label>
                                                <div class="text-danger" id="modifiernameerrormsg"></div>
                                            </div>
                                            <div class="my-5">
                                                <p class="mb-3 text-color-grey f-14">Options for Selection</p>
                                                <div class="container-fluid p-0">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <label class="radio">Select at least one
                                                                <input type="radio" name="items[0][selection]" value="true" checked>
                                                                <span class="checkround"></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-6">
                                                            <label class="radio">Multiple Selection Required
                                                                <input type="radio" name="items[0][selection]" value="false">
                                                                <span class="checkround"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="mt-2 mb-2">
                                                <div class="mb-0 text-color-grey f-14 d-flex justify-content-between">
                                                    <p >Options</p>
                                                    <p  id="newaddmoreitems">+ Add row</p>
                                                </div>

                                                <div class="container-fluid p-0">
                                                    <div class="row" id="newadditemrows">
                                                        <div class="col-8">
                                                            <div class="form-group input-material mr-2">
                                                                <input type="hidden" name="items[0][items][0][id]" value = "1" class="form-control" id="opt1">
                                                                <input type="text" name="items[0][items][0][name]" class="form-control" id="opt1">
                                                                <label for="opt1">Name the Option</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="form-group input-material mr-2">
                                                                <input type="number" name="items[0][items][0][price]" step="0.01" class="form-control" id="Price">
                                                                <label for="Price">Price</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->

                                </div>
                                <div id="newaddmoreaddons" class="add_more d-flex align-items-center justify-content-center text-color-grey">
                                    + ADD MORE
                                </div>
                            </div>

                            <div>
                                <div class="mb-3">
                                    <label class="check">Recommended Pairing
                                        <input type="checkbox" name="recommended" id="newrecommended" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">

                                    <div>
                                        <div class="d-flex align-items-center flex-wrap">
                                            <div id="newitemmenu"></div>
                                            <input type="hidden" name="recommendedproducts[]" id="newrecommendedproducts">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5 d-flex align-items-center justify-content-between">
                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close" onclick="close()">CANCEL</button>
                                <button class="addbtn" id="addbtn">ADD ITEM</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div><!-- modal-content -->

        </div><!-- modal-dialog -->
    </div><!-- modal -->

    {{--pop up  model ended--}}

    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea, date";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>
    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () { $inp.prop('checked', false) }, 0);
            } else {
                options.push(val);
                setTimeout(function () { $inp.prop('checked', true) }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>



    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#newrecommended").click(function() {
                var isChecked = $("#newrecommended").is(":checked");
                var x = document.getElementById('newshowSearchDiv');
                if(isChecked) {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';

                }
            });
        });
        $(document).ready(function() {
            $("#recommended").click(function() {
                var isChecked = $("#recommended").is(":checked");
                var x = document.getElementById('showSearchDiv');
                if(isChecked) {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
            });
        });
    </script>
    <script>
   function itemAvaailableCancelModel(input) { 
     $('#product_sts').hide();
     location.reload();
   }
        $('#image-preview').hide();
        $('#varientaddon').hide();
        $("#Modifiercheck").click(function() {
            if($('#Modifiercheck').is(":checked")) {
                $('#varientaddon').show();
            }else{
                $('#varientaddon').hide();
            }
        });

        function readURL(input) {
            var fileUpload = document.getElementById("photos");
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                console.log(size);
                if(size>2000){
                    $('#imagesizevaliderror').html("Image size should be below 2mb.");
                    $('#image-preview').attr('src', '');
                    return false;
                }
            }

            $('#image-preview').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                }
                $('#imagesizevaliderror').html("");
                reader.readAsDataURL(input.files[0]);
            }
        }





        $(document).ready(function () {
            var i = 0;
            var j = 1;
            var a = 0;
            $('#addmorevarients').on('click', function () {
                console.log($('#dynamicvariant').val());
                if($('#dynamicvariant').val() > 0) {
                    i++;
                    i = parseInt($('#dynamicvariant').val()) + parseInt(i);
                }
                $('#addvariants').append('<div id="'+i+'" class="col-6 variantsdelete'+i+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="text" name="variants['+i+'][variant]" class="form-control" id="opt2" required>' +
                    '<label for="opt2">Name the Option </label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 variantsdelete'+i+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="variants['+i+'][price]" step="0.01" class="form-control" id="Price2" required>' +
                    '<label for="Price2">Price</label>' +
                    '</div>' +
                    '</div>'+
                    '<div class="col-2 addvarientststic variantsdelete'+i+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="variantsdelete'+i+'" onclick="deleterow(this.id, '+i+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>');
                i++;
                $('#dynamicvariant').val(0);
            });
            $('#addmoreaddons').on('click', function () {
                console.log(a);
                if($('#dynamicaddonincr').val() > 0) {
                   a++;
                    a = parseInt($('#dynamicaddonincr').val()) + parseInt(a);
                 }
                //else{
                //     a =0;
                //     $('#dynamicaddonincr').val(0);
                // }
                $('#addons_subdiv').append('<div id="'+a+'" class="modifiactions-required addonsdelete'+a+'">' +
                    '<div class="d-flex align-items-end justify-content-end mb-0">' +
                    '<label class="radio mx-3">Optional' +
                    '<input type="radio" name="items['+a+'][optional]" value="true" checked>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<label class="radio">Required' +
                    '<input type="radio" name="items['+a+'][optional]" value="false">' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<button type="button" class="close closebtn" id="addonsdelete'+a+'" onclick="deleteaddonrow(this.id, '+a+');" style="background: #ff0000 !important;   border-radius: 50%;width: 18px;height: 18px;">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" ></button>'+
                    '</div>' +
                    '<div class="form-group input-material mb-5">' +
                    '<input type="text" name="items['+a+'][ModifierName]" class="form-control" id="ModifierName" required>' +
                    '<label for="Modifier Name">Modifier Name </label>' +
                    '</div>' +
                    '<div class="my-5">' +
                    '<p class="mb-3 text-color-grey f-14">Options for Selection</p>' +
                    '<div class="container-fluid p-0">' +
                    '<div class="row">' +
                    '<div class="col-6">' +
                    '<label class="radio">Select at least one' +
                    '<input type="radio" name="items['+a+'][selection]" value="true" checked>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<label class="radio">Multiple Selection Required' +
                    '<input type="radio" name="items['+a+'][selection]" value="false">' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
                    '<p >Options</p>' +
                    '<p  onclick="addmoreitmrows('+a+')">+ Add row</p>' +
                    '</div>' +
                    '<div class="container-fluid p-0">' +
                    '<div id="additemrows'+a+'">' +
                    '<div class="row">' +
                    '<div class="col-6 addonoptiosdelete'+a+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="hidden" name="items['+a+'][items][0][id]" value = "'+(a+1)+'" class="form-control" id="opt1">' +
                    '<input type="text" name="items['+a+'][items][0][name]" class="form-control" id="opt1">' +
                    '<label for="opt1">Name the Option</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 addonoptiosdelete'+a+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="items['+a+'][items][0][price]" step="0.01" class="form-control" id="Price">' +
                    '<label for="Price">Price</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-2 addvarientststic addonoptiosdelete'+a+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="addonoptiosdelete'+a+'" onclick="deleterow(this.id, '+a+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>'+
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
                a++;
				$('#dynamicaddonincr').val(0);
            });
            k=1;
            $('#addmoreitems').on('click', function () {
                j++;
                $('#additemrows').append('<div id="'+j+'" class="col-6 addonoptiosdelete'+j+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="hidden" name="items[0][items]['+k+'][id]" value = "'+j+'" class="form-control" id="opt1">' +
                    '<input type="text" name="items[0][items]['+k+'][name]" class="form-control" id="opt2" required>' +
                    '<label for="opt2">Name the Option </label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 addonoptiosdelete'+i+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="items[0][items]['+k+'][price]" step="0.01" class="form-control" id="Price2" required>' +
                    '<label for="Price2">Price</label>' +
                    '</div>' +
                    '</div>'+
                    '<div class="col-2 addvarientststic addonoptiosdelete'+j+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="addonoptiosdelete'+j+'" onclick="deleterow(this.id, '+j+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>');
                k++;
            });


            $("#variants_div").hide();
            $("#addons_div").hide();

            $('#varientsitem').on('click', function () {
                $("#variants_div").show();
                $("#addons_div").hide();
            });

            $('#addonitem').on('click', function () {
                $("#variants_div").hide();
                $("#addons_div").show();
                $("#addons_subdiv").show();
                $("#addmoreaddons").show();
            });

            $("#newvariants_div").hide();
            $("#newaddons_div").hide();
            $("#newaddmoreaddons").hide();
            $('#newvarientsitem').on('click', function () {
                $("#newvariants_div").show();
                $("#newaddons_div").hide();
            });
            $('#newaddonitem').on('click', function () {
                $("#newvariants_div").hide();
                $("#newaddons_div").show();
                $("#newaddons_subdiv").show();
                $("#newaddmoreaddons").show();
            });
        });
        var i = 1;
        var m = 1;
        function product_available_sts(product_url)
        {
            jQuery('#product_sts').modal('show', {backdrop: 'static'});
            document.getElementById('productsts_link').setAttribute('href' , product_url);
        }
        var availavleflag = [];
        function update_modal(param) {
            $('#addmenuid').modal({backdrop: 'static', keyboard: false});
            $("#variants_div").hide();
            $("#addons_div").hide();
            console.log("23243434 "+JSON.stringify(param));
            var APP_URL = {!! json_encode(url('/')) !!};
            //console.log(APP_URL + param.photos);
            var data = param;
            //var data = JSON.parse(param);
            $('#addons_subdiv').html("");
            $('#addvariants').html("");
            $('#additemrows').html("");
            console.log("APP_URL" + data.thumbnail_img);
            if (data.thumbnail_img){
                $('#image-preview').show();
                $('#photos').attr('src', data.thumbnail_img);
                $('#image-preview').attr('src', data.thumbnail_img);
            }
            //$('#image-preview').attr('src', source);
            $('#manage-menu-formmodel').attr('action', '{{url('item_edit')}}');
            $('#addmenuid').modal('show');
            $('#id').val(data.id);
            $('#itemSearch').val(data.name);
            $('#description').attr('value', data.description);
            $('#description').val(data.description);
            $('#unit_price').attr('value', data.unit_price);
            $('#unit_price').val(data.unit_price);
            $('#cuisine_id option[value='+data.cuisine_id+']').prop('selected','selected');
            //$('#cuisine_id').val(data.cuisine_id);
            $('#service_line').attr('value', data.service_line);
            $('#service_line').val(data.service_line);
            $('#set_max_order_number').attr('value', data.max_qty);
            $('#set_max_order_number').val(data.max_qty);
            /*if(data.featured == 1){
                $('input[name="featured"]').attr('checked', true);
            }*/
            $('input[name="featured"][value='+data.featured+']').prop('checked', 'checked');
            $('input[name="healthpicks"][value='+data.healthy_picks+']').prop('checked', 'checked');
            $('input[name="veg_nonveg"][value='+data.type+']').prop('checked', 'checked');
            //$('input[name="menu_items_desc"][value='+data.category_id+']').prop('selected', 'selected');
            $('#menu_items_desc option[value='+data.category_id+']').prop('selected','selected');
            $("#recommendedproducts").val(data.tags);
            console.log(JSON.stringify(data.choice_options));
            var varients = data.choice_options;


            if(data.Modifier == 1) {
                $('input[name="Modifier"]').prop('checked', 'checked');
                $('input[name="varientsitem"]').prop('checked', 'checked');
                $('#varientaddon').show();
                $("#variants_div").show();
            }else{
                $('input[name="Modifier"]').attr('checked', false);
                $('#varientaddon').hide();
                $("#variants_div").hide();
            }
            var availabedays = data.product_timing;
             
            for(var i=0; i<availabedays.length; i++){
                availavleflag.splice(0, 0, availabedays[i].day_id);
                console.log("23 "+availavleflag);
                $('#available_days').val(availavleflag).trigger('change');             
            }
            for(var i=0; i<varients.length; i++){
                console.log("23232323 "+varients[i]['variant']);
                $("#dynamicvariant").val(i);
                $(".addvarientststic").remove();
                if(varients[i]['variant']!= null)
                    $('#addvariants').append('<div id="'+i+'" class="col-6 variantsdelete'+i+'">' +
                        '<div class="form-group input-material mr-2">' +
                        '<input type="text" name="variants['+i+'][variant]" value="'+varients[i]['variant']+'" class="form-control" id="opt2" required>' +
                        '<label for="opt2">Name the Option </label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-4 variantsdelete'+i+'">' +
                        '<div class="form-group input-material mr-2">' +
                        '<input type="number" name="variants['+i+'][price]" value="'+varients[i]['price']+'" step="0.01" class="form-control" id="Price2" required>' +
                        '<label for="Price2">Price</label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-2 variantsdelete'+i+'">'+
                        '<div class="form-group input-material mr-2">'+
                        '<p class="f-10 mb-2">Remove</p>'+
                        '<a id="variantsdelete'+i+'" onclick="deleterow(this.id, '+i+');">'+
                        '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                        '</a>'+
                        '</div>'+
                        '</div>');
            }
            console.log("2323 "+JSON.stringify(data.addons));
            //console.log(JSON.stringify(data.addons[0].items));
            var addons = data.addons;
            console.log(JSON.stringify(addons));
            $(".addonsstaticsubdiv").remove();
            for(var j=0; j<addons.length; j++){
                $("#dynamicaddonincr").val(j);
                var optselect = "";
                if(addons[j]['optional']){
                    var optselect = "checked";
                }
                var optreqselect = "";
                if(addons[j]['optional'] == false){
                    var optreqselect = "checked";
                }
                var singleselect = "";
                if(addons[j]['selection'] == "single"){
                    var singleselect = "checked";
                }
                var selectionselect = "";
                if(addons[j]['selection'] == "multiple"){
                    var selectionselect = "checked";
                }
                $('#addons_subdiv').append('<div id="'+j+'" class="modifiactions-required addonsdelete'+j+'">' +
                    '<div class="d-flex align-items-end justify-content-end mb-0">' +
                    '<label class="radio mx-3">Optional' +
                    '<input type="radio" name="items['+j+'][optional]" value="true" '+optselect+'>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<label class="radio">Required' +
                    '<input type="radio" name="items['+j+'][optional]" value="false" '+optreqselect+'>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<button type="button" class="close closebtn mx-3" id="addonsdelete'+j+'" onclick="deleteaddonrow(this.id, '+j+');" style="background: #ff0000 !important;   border-radius: 50%;width: 18px;height: 18px;">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" ></button>'+
                    '</div>' +
                    '<div>' +
                    '<div class="form-group input-material mb-5">' +
                    '<input type="text" name="items['+j+'][ModifierName]" value="'+addons[j]['name']+'" class="form-control" id="ModifierName" required>' +
                    '<label for="Modifier Name">Modifier Name</label>' +
                    '</div>' +
                    '<div class="my-5">' +
                    '<p class="mb-3 text-color-grey f-14">Options for Selection</p>' +
                    '<div class="container-fluid p-0">' +
                    '<div class="row">' +
                    '<div class="col-6">' +
                    '<label class="radio">Select at least one' +
                    '<input type="radio" name="items['+j+'][selection]" value="single" '+singleselect+' checked>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<label class="radio">Multiple Selection Required' +
                    '<input type="radio" name="items['+j+'][selection]" value="multiple" '+selectionselect+'>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
                    '<p >Options</p>' +
                    '<p  onclick="addmoreitmrows('+j+')">+ Add row</p>' +
                    '</div>' +
                    '<div class="container-fluid p-0">' +
                    '<div id="additemrows'+j+'"> '+

                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
                var additemrow = addons[j]['items'];
                console.log("6565 "+JSON.stringify(addons[j]['items']));

                for (var i = 0; i < additemrow.length; i++) {
                    m++;
                    console.log("23232 "+addons[j]['items'][i]['name']);
                    $("#additemrows"+j).append('<div id="'+j+'" class="row">' +
                        '<div  class="col-6" addonoptiosdelete'+j+i+m+'>' +
                        '<div class="form-group input-material mr-2">' +
                        '<input type="hidden" name="items['+j+'][items]['+i+'][id]" value = "'+i+'" class="form-control" id="opt1">' +
                        '<input type="text" name="items['+j+'][items]['+i+'][name]" value= "'+addons[j]['items'][i]['name']+'" class="form-control" id="opt2">' +
                        '<label for="opt2">Name the Option</label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-4 addonoptiosdelete'+j+i+m+'">' +
                        '<div class="form-group input-material mr-2">' +
                        '<input type="number" name="items['+j+'][items]['+i+'][price]" value= "'+addons[j]['items'][i]['price']+'" step="0.01" class="form-control" id="Price2">' +
                        '<label for="Price2">Price</label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-2 addvarientststic addonoptiosdelete'+j+i+m+'">'+
                        '<div class="form-group input-material mr-2">'+
                        '<p class="f-10 mb-2">Remove</p>'+
                        '<a id="addonoptiosdelete'+j+i+m+'" onclick="deleterow(this.id, '+j+i+m+');">'+
                        '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                        '</a>'+
                        '</div>'+
                        '</div>'+		
                        '</div>');
                }
            }


        }

        function  addmoreitmrows(a){
            console.log('additemrows '+a);
            m++;
            $('#additemrows'+a).append('<div class="row"><div id="'+a+'" class="col-6 addonoptiosdelete'+a+m+i+'">' +
                '<div class="form-group input-material mr-2">' +
                '<input type="hidden" name="items['+a+'][items]['+i+'][id]" value = "'+m+'" class="form-control" id="opt1">' +
                '<input type="text" name="items['+a+'][items]['+i+'][name]" class="form-control" id="opt2">' +
                '<label for="opt2">Name the Option </label>' +
                '</div>' +
                '</div>' +
                '<div class="col-4 addonoptiosdelete'+a+m+i+'">' +
                '<div class="form-group input-material mr-2">' +
                '<input type="number" name="items['+a+'][items]['+i+'][price]" step="0.01" class="form-control" id="Price2">' +
                '<label for="Price2">Price</label>' +
                '</div>' +
                '</div>' +
                '<div class="col-2 addvarientststic addonoptiosdelete'+a+m+i+'">'+
                '<div class="form-group input-material mr-2">'+
                '<p class="f-10 mb-2">Remove</p>'+
                '<a id="addonoptiosdelete'+a+m+i+'" onclick="deleterow(this.id, '+a+m+i+');">'+
                '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                '</a>'+
                '</div>'+
                '</div>'+
                '</div>');
            i++;
        }

        function  newaddmoreitmrows(a){
            console.log('additemrows '+a+' '+m);
            m++;
            $('#newadditemrows'+a).append('<div class="row"><div id="'+a+'" class="col-6 addonoptiosdelete'+a+m+'">' +
                '<div class="form-group input-material mr-2">' +
                '<input type="hidden" name="items['+a+'][items]['+i+'][id]" value = "'+a+m+'" class="form-control" id="opt1">' +
                '<input type="text" name="items['+a+'][items]['+i+'][name]" class="form-control" id="opt2" required>' +
                '<label for="opt2">Name the Option </label>' +
                '</div>' +
                '</div>' +
                '<div class="col-4 addonoptiosdelete'+a+m+'">' +
                '<div class="form-group input-material mr-2">' +
                '<input type="number" name="items['+a+'][items]['+i+'][price]" step="0.01" class="form-control" id="Price2" required>' +
                '<label for="Price2">Price</label>' +
                '</div>' +
                '</div>' +
                '<div class="col-2 addvarientststic addonoptiosdelete'+a+m+'">'+
                '<div class="form-group input-material mr-2">'+
                '<p class="f-10 mb-2">Remove</p>'+
                '<a id="addonoptiosdelete'+a+m+'" onclick="deleterow(this.id, '+a+m+');">'+
                '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                '</a>'+
                '</div>'+
                '</div>'+
                '</div>');
            i++;
        }




        var recommproduct = [];
        function getproducts(data){
            var cproduct = data.products;
            var products = [];
            for (var i = 0; i < cproduct.length; i++) {
                var itemnm = cproduct[i].name;
                console.log(itemnm);

                var appendprop = '<a class="nav-link text-truncate active-links" href="#">' +
                    '<div>' +
                    '<label class="check">' +itemnm+'<input type="checkbox" name="itemnameid" value="'+cproduct[i].id+'@@'+itemnm+'" onclick="additemstomenu(this.value);" id="'+cproduct[i].id+'">' +
                    '<span class="checkmark"></span>' +
                    '</label>' +
                    '</label>' +
                    '</label>' +
                    '</div>' +
                    '</a>';
                    products.push(appendprop);
            }
            $('#productitems').html(products);
        }

        function additemstomenu(data) {
            var itemdtl = data.split('@@');
            console.log("3434 3434 "+$('#'+itemdtl[0]).is(":checked"));
            var recommendFlag = false;
            for(var i=0; i<recommproduct.length;i++){
                if(parseInt(recommproduct[i]) == parseInt(itemdtl[0])){
                    //console.log("itemar "+parseInt(recommproduct[i])+" "+itemdtl[0]);
                    recommendFlag = true;
                }
            }
            if(recommendFlag)
                return false;

            recommproduct.splice(0, 0, itemdtl[0]);

            $('#itemmenu').append('<div class="employee-options option-selected" id="card'+itemdtl[0]+'">' + itemdtl[1] + '' +
                '<input type="checkbox" name="allowed_products[]" value=' + itemdtl[0] + ' style="display:none;"><span class="ml-2">' +
                '<img src="./images/clearoption.svg" alt="" onclick="remove_rec(' + itemdtl[0] + ')"></div>');

            $("#recommendedproducts").val(recommproduct);

        }
        function confirm_modal(delete_url)
        {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
        }
        function remove_rec(data){
            $("#recommendedproducts").val(data);
            $("#card"+data).remove();
            $("#"+data).prop("checked",false);
            // $("#recommendedproducts").val(itemdtl[0]);
            for(var i=0; i<recommproduct.length;i++) {
                //if ($('#' + itemdtl[0]).is(":checked") == false) {
                console.log(data);
                if(parseInt(recommproduct[i]) == parseInt(data)){
                    console.log(recommproduct[i]);
                    recommproduct.splice(i, 1);
                }
            }
            $("#recommendedproducts").val(recommproduct);
        }
        function selectcatagory(id,name){
            $("#search").val(id);
            $('#search_category').val(name);
            $('#category_filter').submit();
        }

   function editcategory(id) {
       $('#edit_section').modal({backdrop: 'static', keyboard: false});

       $.ajax({
           cache:false,
           type: "GET",
           url: "{{('edit_category')}}/" + id,
           success: function (res) {
               console.log(res);
              var  category = res.data;
               $('#category_id').val(category.id);
               $('#meta_title_edit').attr('value', category.meta_title);
               $('#meta_title_edit').val(category.meta_title);
               $('#meta_description_edit').attr('value', category.meta_description);
              $('#meta_description_edit').val(category.meta_description);
               $('#category_name').attr('value', category.name);
               $('#category_name_edit').val(category.name);
               $('#Start_edit').attr('value', category.start_time);
               $('#Start_edit').val(category.start_time);
               $('#end_edit').attr('value', category.end_time);
               $('#end_edit').val(category.end_time);

              //$('#end').val(category.);
           },error: function(res) {

           }
       });

   }
   function  deletecategory(id) {

   }

        function openpopupmodel(){
            $('#new_addmenuid').modal({backdrop: 'static', keyboard: false});
            $('#new_addmenuid').modal('show');
            // $('#addmenuid').modal('show');
            $('#id').val("");
            $('#itemSearch').val("");
            $('#description').val("");
            $('#unit_price').val("");
            $('#cuisine_id').val("");
            $('#set_max_order_number').val("");
            $('#service_line').val("");
            $('#available_days').val("");
            $("#featured").prop( "checked", false );
            $("#healthpicks").prop( "checked", false );
            $("#veg").prop( "checked", false );
            $("#nonveg").prop( "checked", false );
            $("#newModifiercheck").prop( "checked", false );
            $('#varientaddon').hide();
            $('#image-preview').hide();
            $('#image-preview').attr('src', '');
            $('input[name="Modifier"]').attr('checked', false);
            $('#varientaddon').hide();
            $("#variants_div").hide();
            $("#addons_div").hide();
            $('#new_manage-menu-formmodel')[0].reset();
            $("#newvarientaddon").hide();
            $("#newaddoninitialval").val(0);
            $("#newrecommendedproducts").val("");

        }

        $( document ).ready(function() {

            var a = 0;
            var i = 0;
            var j = 1;
            $('#newaddmorevarients').on('click', function () {
                console.log("ABCD "+$('#newaddoninitialval').val());
                if($('#newdynamicvariant').val() > 0) {
                    i = parseInt($('#newdynamicvariant').val()) + parseInt(i);
                }
                $('#newaddvariants').append('<div id="' + i + '" class="col-6 variantsdelete'+i+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="text" name="variants[' + i + '][variant]" class="form-control" id="opt2" required>' +
                    '<label for="opt2">Name the Option </label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 variantsdelete'+i+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="variants[' + i + '][price]" step="0.01" class="form-control" id="Price2" required>' +
                    '<label for="Price2">Price</label>' +
                    '</div>' +
                    '</div>'+
                    '<div class="col-2 addvarientststic variantsdelete'+i+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="variantsdelete'+i+'" onclick="deleterow(this.id, '+i+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>');
                $('#newdynamicvariant').val(0);
                i++;
            });

            $('#newaddmoreaddons').on('click', function () {
                console.log(a);
                if($('#newdynamicaddonincr').val() > 0) {
                    a = parseInt($('#newdynamicaddonincr').val()) + parseInt(a);
                }
                /*if ($("#newaddoninitialval").val() == 0) {
                    a= 0;
                    $("#newaddoninitialval").val(1);
                }*/
                $('#newaddons_subdiv').append('<div id="'+a+'" class="modifiactions-required addonsdelete'+a+'">' +
                    '<div class="d-flex align-items-end justify-content-end mb-0">' +
                    '<label class="radio mx-3">Optional' +
                    '<input type="radio" name="items['+a+'][optional]" value="true" checked>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<label class="radio">Required' +
                    '<input type="radio" name="items['+a+'][optional]" value="false">' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '<button type="button" class="close closebtn" id="addonsdelete'+a+'" onclick="deleteaddonrow(this.id, '+a+');" style="background: #ff0000 !important;   border-radius: 50%;width: 18px;height: 18px;">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" ></button>'+
                    '</div>' +
                    '<div class="form-group input-material mb-5">' +
                    '<input type="text" name="items['+a+'][ModifierName]" class="form-control" id="ModifierName" required>' +
                    '<label for="Modifier Name">Modifier Name</label>' +
                    '</div>' +
                    '<div class="my-5">' +
                    '<p class="mb-3 text-color-grey f-14">Options for Selection</p>' +
                    '<div class="container-fluid p-0">' +
                    '<div class="row">' +
                    '<div class="col-6">' +
                    '<label class="radio">Select at least one' +
                    '<input type="radio" name="items['+a+'][selection]" value="true" checked>' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<label class="radio">Multiple Selection Required' +
                    '<input type="radio" name="items['+a+'][selection]" value="false">' +
                    '<span class="checkround"></span>' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="mb-0 text-color-grey f-14 d-flex justify-content-between">' +
                    '<p >Options</p>' +
                    '<p  onclick="newaddmoreitmrows('+a+')">+ Add row</p>' +
                    '</div>' +
                    '<div class="container-fluid p-0">' +
                    '<div id="newadditemrows'+a+'">' +
                    '<div class="row">' +
                    '<div class="col-6 addonoptiosdelete'+a+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="hidden" name="items['+a+'][items][0][id]" value = "'+(a+1)+'" class="form-control" id="opt1">' +
                    '<input type="text" name="items['+a+'][items][0][name]" class="form-control" id="opt1" required>' +
                    '<label for="opt1">Name the Option</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 addonoptiosdelete'+a+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="items['+a+'][items][0][price]" step="0.01" class="form-control" id="Price" required>' +
                    '<label for="Price">Price</label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-2 addvarientststic addonoptiosdelete'+a+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="addonoptiosdelete'+a+'" onclick="deleterow(this.id, '+a+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>'+
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
                a++;
				$("#newaddoninitialval").val(0);
            });
            k = 1;
            $('#newaddmoreitems').on('click', function () {
                j++;
                $('#newadditemrows').append('<div id="'+j+'" class="col-8 addonoptiosdelete'+k+j+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="hidden" name="items[0][items]['+k+'][id]" value = "'+j+'" class="form-control" id="opt1">' +
                    '<input type="text" name="items[0][items]['+k+'][name]" class="form-control" id="opt2" required>' +
                    '<label for="opt2">Name the Option </label>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-4 addonoptiosdelete'+k+j+'">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="number" name="items[0][items]['+k+'][price]" step="0.01" class="form-control" id="Price2" required>' +
                    '<label for="Price2">Price</label>' +
                    '</div>' +
                    '</div>'+
                    '<div class="col-2 addvarientststic addonoptiosdelete'+k+j+'">'+
                    '<div class="form-group input-material mr-2">'+
                    '<p class="f-10 mb-2">Remove</p>'+
                    '<a id="addonoptiosdelete'+k+j+'" onclick="deleterow(this.id, '+k+j+');">'+
                    '<img class="mx-3" src="./images/delete.svg" alt="" >'+
                    '</a>'+
                    '</div>'+
                    '</div>');
                k++;
            });
        });

        $("#newModifiercheck").click(function() {
            if($('#newModifiercheck').is(":checked")) {
                $('#newvarientaddon').show();
                $('#modifiernameerrormsg').html("Please Enter Modifier Name.");
            }else{
                $('#newvarientaddon').hide();
            }
        });

        $('#newimage-preview').hide();
        function newreadURL(input) {
            var fileUpload = document.getElementById("newphotos");
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                if(size>2000){
                    $('#newimagesizevaliderror').html("Image size should be below 2mb.");
                    $('#newimage-preview').attr('src', '');
                    return false;
                }
            }

            $('#newimage-preview').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#newimage-preview').attr('src', e.target.result);
                }
                $('#newimagesizevaliderror').html("");
                reader.readAsDataURL(input.files[0]);
            }
        }


        var newrecommproduct = [];
        function newgetproducts(data){
            var cproduct = data.products;
            var itemnm = '';
            var newproducts = [];
            for (var i = 0; i < cproduct.length; i++) {
                var itemnm = cproduct[i].name;
                console.log(itemnm);
                var appendprod = '<a class="nav-link text-truncate active-links" href="#">' +
                    '<div>' +
                    '<label class="check">' +cproduct[i].name+'<input type="checkbox" name="itemnameid" value="'+cproduct[i].id+'@@'+cproduct[i].name+'" onclick="newadditemstomenu(this.value);" id="'+cproduct[i].id+'">' +
                    '<span class="checkmark"></span>' +
                    '</label>' +
                    '</label>' +
                    '</label>' +
                    '</div>' +
                    '</a>';
                    newproducts.push(appendprod);
            }
            $('#newproductitems').html(newproducts);
        }

        function newadditemstomenu(data) {
            var itemdtl = data.split('@@');
            console.log("3434 3434 "+$('#'+itemdtl[0]).is(":checked"));
            var recommendFlag = false;
            for(var i=0; i<newrecommproduct.length;i++){
                if(parseInt(newrecommproduct[i]) == parseInt(itemdtl[0])){
                    //console.log("itemar "+parseInt(recommproduct[i])+" "+itemdtl[0]);
                    recommendFlag = true;
                }
            }
            if(recommendFlag)
                return false;

            newrecommproduct.splice(0, 0, itemdtl[0]);

            $('#newitemmenu').append('<div class="employee-options option-selected" id="card'+itemdtl[0]+'">' + itemdtl[1] + '' +
                '<input type="checkbox" name="allowed_products[]" value=' + itemdtl[0] + ' style="display:none;"><span class="ml-2">' +
                '<img src="./images/clearoption.svg" alt="" onclick="remove_rec(' + itemdtl[0] + ')"></div>');

            $("#newrecommendedproducts").val(newrecommproduct);

        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#download_products").click(function(){
                var date = new Date();
                TableToExcel.convert(document.getElementById("products"), {

                    name: "Products_" +date+ ".xlsx",
                    sheet: {
                        name: "Products"
                    }
                });
            });
            $("#close").click(function(){
                $('#newimage-preview').attr('src','');

            });
            $("#addbtn").click(function(){
                $('#newimage-preview').attr('src','');

            });
        });




    function deleterow(val, param) {
        console.log(val);
        $("."+val).remove();        
    }
    function deleteaddonrow(val, param) {
        console.log(val);
        $("."+val).remove();        
    }
    
    </script>
    <link rel="stylesheet" href="js/select/msfmultiselect.css">
<script type="text/javascript" src="{{asset('js/select/msfmultiselect.js')}}"></script>
  <script>

  var select = new MSFmultiSelect(
    document.querySelector('.multiselect'),
    {
      theme: 'theme1',
      selectAll: true,
      searchBox: true,
      width: 250,
      height: 30,
      onChange:function(checked, value, instance) {
        //console.log(checked, value, instance);
      },
      //appendTo: '#myselect',
      //readOnly:true,
     // placeholder: 'languages',
      //autoHide: false,
      afterSelectAll:function(checked, values, instance) {
        //console.log(checked, values, instance);
      }
    }
  );
  var select = new MSFmultiSelect(
    document.querySelector('.multiselects'),
    {
      theme: 'theme1',
      selectAll: true,
      searchBox: true,
      width: 250,
      height: 30,
      onChange:function(checked, value, instance) {
        //console.log(checked, value, instance);
      },
      //appendTo: '#myselect',
      //readOnly:true,
     // placeholder: 'languages',
      //autoHide: false,
      afterSelectAll:function(checked, values, instance) {
        //console.log(checked, values, instance);
      }
    }
  );
  </script>
@endsection

