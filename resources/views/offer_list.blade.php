@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        @if(session()->has('success'))
            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
        @endif
        @if(session()->has('failed'))
            <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
        @endif
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">OFFERS LIST</span>
                <button class="add-btn" onclick="openpopupmodel();">+ Add New Offer</button>
            </div>
            <div class="container">
               <div class="row">
                    @foreach($offers ?? [] as $offer)
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                            <div class="offer-cards">
                                <div class="d-flex align-items-center justify-content-between mb-5">
                                    <span class="status-view f-14 f-medium">{{($offer->status==1)?'ACTIVE':'IN ACTIVE'}}</span>
                                    <span>
                                        <a onclick="update_modal({{json_encode($offer)}});">
                                            <img class="mx-3" src="./images/card-edit.svg" alt="">
                                        </a>

                                        <a onclick="confirm_modal('{{url('coupon_destroy', $offer->id)}}');">
                                            <img class="mx-3" src="./images/delete.svg" alt="">
                                        </a>
                                    </span>
                                </div>
                                <div class="text-center f-16 f-bold">{{$offer->code}}</div>
                                <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center max-height-45">
                                    <div>{{$offer->description}}</div>
                                    <div class="text-danger" id=""></div>
                                </div>
                                <div class="text-center f-14 f-medium">{{$offer->min_spending_amount}} of {{$offer->max_spending_amount}} Coupon Users</div>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>

        </div>

    </div>


    <div>
        @include('inc.side_nav')


        <form id="manage-menu-form" method="POST" action="{{url('addOffer')}}">
            @csrf
            <div class="modal add-menu right fade " id="addmenuid" tabindex="-1" role="dialog"
                 aria-labelledby="add-menu">
                <div class="modal-dialog" id="addnewmenu" role="add menu">
                    <div class="modal-content model-row-colum ">
                        <div class="modal-body pb-4 dialog-options-responsive">
                            <div class="bg-black text-color-white negative-margin" style="display:none;" id="categoryshowSearchDiv">

                            </div>
                            <div class="bg-black text-color-white negative-margin" style="display:none;" id="showSearchDiv">
                            {{--<nav class="navbar-options menu-list-items d-flex ">--}}
                            {{--<ul class="nav flex-column flex-nowrap ">--}}
                            {{--<li class="nav-item">--}}
                            {{--@foreach($categories as $category)--}}
                            {{--<a class="nav-link text-truncate" onclick="getproducts({{json_encode($category)}});">--}}
                            {{--<span class="menu-link ">{{$category->name}} </span>--}}
                            {{--</a>--}}
                            {{--@endforeach--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--<ul class="nav flex-column flex-nowrap menu-list-items-options">--}}
                            {{--<li class="nav-item" id="productitems">--}}
                            <!--  <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 1
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 2
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 3
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>
                                            <a class="nav-link text-truncate active-links" href="#">
                                                <div>
                                                    <label class="check">Item 4
                                                        <input type="checkbox" name="is_name">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </a>-->

                                {{--</li>--}}
                                {{--</ul>--}}
                                {{--</nav>--}}
                            </div>
                            <div class="overflow-dialog">
                                <div class="f-20 f-medium"+ Add New Offer><span id="addedit"></span> Offer</div>

                                <div class="form-group input-material">
                                    <input type="hidden" class="form-control" name="id" id="id">
                                    <textarea class="form-control" name="Promocode" id="Promocode" rows="1" data-validation="required" required onblur="couponveriy(this.value)"></textarea>
                                    <label for="Promocode">Promocode<span style="color:red">*</span></label>
                                    <div class="text-danger">{{ $errors->first('Promocode') }}</div>
                                    <div class="text-danger" id="couponverfiy"></div>
                                </div>

                                <div class="form-group input-material">
                                    <textarea class="form-control" name="description" id="description" rows="1" data-validation="required" required></textarea>
                                    <label for="description">Description<span style="color:red">*</span></label>
                                    <div class="text-danger">{{ $errors->first('description') }}</div>
                                </div>

                                <div class="d-flex justify-content-end f-10 text-color-grey" style="margin-top: -20px;">
                                    0 / 6</div>
                                <div class="mt-2 mb-0">
                                    <p class="mb-0 text-color-grey f-14">Offer Start & End Dates</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" name="start_date" id="StartDate" data-validation="required" required>
                                                    <label for="StartDate" class="datetype" style="top: -12px;">Start Date<span style="color:red">*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="text" class="form-control" name="end_date" id="ExpiryDate" data-validation="required" required>
                                                    <label for="ExpiryDate" class="datetype" style="top: -12px;">Expiry Date<span style="color:red">*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="flex radio-toolbar">
                                      @foreach ($percentages as $percentage)
                                          <div type="input" class="employee-options" id="radio-percent{{$loop->index}}" onclick="addremovestyle({{$loop->index}})">
                                              {{$percentage->percent}}%
                                              <input type="radio" name="percent" id="radiobtnpercent{{$loop->index}}" value="{{$percentage->percent}}" style="display: none;">
                                          </div>

                                      @endforeach
                                  </div>--}}


                                <div class="mt-3 mb-0">
                                    <p class="mb-2 text-color-grey f-14">Offer Value<span style="color:red">*</span></p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="radio">Dollar
                                                    <input type="radio" name="discount_type" value="amount" id="Dollar" >
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-6">
                                                <label class="radio">Percentage
                                                    <input type="radio" name="discount_type" value="percentage" id="percentage" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            {{--<div class="col-4">--}}
                                            {{--<label class="radio">Free Delivery--}}
                                            {{--<input type="radio" name="discount_type" value="free_delivery" id="free_delivery">--}}
                                            {{--<span class="checkround"></span>--}}
                                            {{--</label>--}}
                                            {{--</div>--}}

                                        </div>
                                    </div>
                                </div>

                                <div class="mt-3 mb-0">

                                    <div class="row">
                                        <div class="col-6" id="discount">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" class="form-control" name="discount" id="discount_amount">
                                                <label for="min" class="datetype">Discount</label>
                                            </div>
                                        </div>
                                        <div class="col-6" id="discountmax">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" class="form-control" name="discount_max_amount" id="discount_max_amount">
                                                <label for="max" class="datetype">Maximum Amount</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!--   <button class="employee-options option-selected">
                                       <span>$200</span><span class="ml-2"></span>

                                     </button>-->

                                </div>


                                <div>
                                    <p class="mb-2 mt-4 text-color-grey f-14">Days to Run Offer<span style="color:red">*</span></p>
                                    <div class="d-flex align-items-center flex-wrap">
                                        <label class="flex chk-toolbar">
                                            @foreach ($days as $day)
                                                <label class="employee-options" id="chk-role{{$loop->index}}" onclick="addremovestyleoffer({{$loop->index}})">
                                                    {{$day['name']}}
                                                    <input type="checkbox" name="days_to_run[]" id="chkbtnoffer{{$loop->index}}" value="{{$day['id']}}" style="display:none;">
                                                </label>
                                            @endforeach
                                            {{--<button class="working-hours-selected">S</button>
                                            <button class="working-hours-selection">M</button>
                                            <button class="working-hours-selection">T</button>
                                            <button class="working-hours-selection">W</button>
                                            <button class="working-hours-selection">T</button>
                                            <button class="working-hours-selection">F</button>
                                            <button class="working-hours-selection">S</button>--}}
                                        </label>
                                    </div>
                                </div>
                                <div class="mt-4 mb-0" id="inputStateselect">
                                    <p class="mb-2 mt-2 text-color-grey f-14">Restaurants</p>

                                    <select   name="restaurant[]" id="restaurant" class="form-control demo-select2" data-validation="required" multiple="multiple" required onchange="category_items(this.value);category_products(this.value);" style="min-width:100%!important;">
                                        <option value="">Select One Restaurant</option>

                                        @foreach($restaurants as $restaurant)
                                            <option id="restaurant{{$restaurant->id}}"  value="{{$restaurant->id}}">
                                                {{$restaurant->name}}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>

                                <div class="mt-4 mb-0">
                                    <p class="mb-2 mt-2 text-color-grey f-14">Specify Items for this Offer</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="radio">Entire Menu
                                                    <input type="radio" name="all_categories" class="itemcatg" id="entiremenu" value="1" checked>
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>
                                            <div class="col-4">
                                                <label class="radio">Specific Category
                                                    <input type="radio" name="all_categories" class="specificcatg" id="catgmenu" value="0">
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                            <div class="col-4">
                                                <label class="radio">Specific Item
                                                    <input type="radio" name="all_categories" id="specificmenu" value="0" >
                                                    <span class="checkround"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group input-material categoryhide">
                                    <textarea class="form-control" name="categorysearch"  id="categorysearch" rows="1"></textarea>
                                    <label for="description">Categories</label>
                                </div>

                                <div class="form-group input-material producthide">
                                    <textarea class="form-control" name="productsearch"  id="productsearch" rows="1"></textarea>
                                    <label for="description">Items</label>
                                </div>

                                <div>
                                    <div class="d-flex align-items-center flex-wrap">
                                        <div id="itemmenu"></div>
                                        <input type="hidden" name="recommendedproducts[]" id="recommendedproducts">
                                    </div>
                                </div>
                                <div>
                                    <div class="d-flex align-items-center flex-wrap">
                                        <div id="categorymenu"></div>
                                        <input type="hidden" name="recommendedcategory[]" id="recommendedcategory">
                                    </div>
                                </div>



                                <!--   <div class="mt-3 mb-0">
                                       <p class="mb-2 text-color-grey f-14">Set Maximum Usage</p>
                                       <div class="container-fluid p-0">
                                           <div class="row">
                                               <div class="col-6">
                                                   <label class="radio">Dollar
                                                       <input type="radio" name="discount_type"  id="Dollar" >
                                                       <span class="checkround"></span>
                                                   </label>
                                               </div>
                                               <div class="col-6">
                                                   <label class="radio">Percentage
                                                       <input type="radio" name="discount_type" id="percentage">
                                                       <span class="checkround"></span>
                                                   </label>
                                               </div>

                                           </div>
                                       </div>
                                   </div>
                                   <div class="mt-3 mb-0">
                                       <input type="number" name="discount_max_amount" id="discount_max_amount" style="width: 15%;">
                                   </div>-->

                                <div class="mt-4 mb-0">
                                    <p class="mb-2 text-color-grey f-14">Set Minimum Usage</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" class="form-control" name="max_users" id="max_users" min="0" data-validation="required" required>
                                                    <label for="min" class="datetype">Per Person</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" class="form-control" name="max_usage_per_user" id="max_usage_per_user" min="0" data-validation="required" required>
                                                    <label for="max" class="datetype">Unlimited</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="mt-2 mb-0">
                                    <p class="mb-0 text-color-grey f-14">Set Timings<span style="color:red">*</span></p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input name="start_time" id="Start" value="00:00" class="form-control" data-clocklet data-validation="required" required>
                                                    <label for="Start" class="datetype" style="top:-10px;">Start Time</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input name="end_time" id="end" value="23:59" class="form-control" data-clocklet data-validation="required" required>
                                                    <label for="end" class="datetype" style="top:-10px;">End Time</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="mt-2 mb-0">
                                    <p class="mb-1 text-color-grey f-14">Set Maximum and Minimum Purchase Limit</p>
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" class="form-control" name="min_spending_amount" id="min_spending_amount" required>
                                                    <label for="min" class="datetype">Minimum Limit</label>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group input-material mr-2">
                                                    <input type="number" class="form-control" name="max_spending_amount" id="max_spending_amount" required>
                                                    <label for="max" class="datetype">Maximum Limit</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <div class="mt-5 d-flex align-items-center justify-content-between">

                                    <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                    <button class="addbtn"> <span id="saveoffer"></span> OFFER</button>
                                </div>
                            </div>




                        </div>

                    </div><!-- modal-content -->

                </div><!-- modal-dialog -->
            </div><!-- modal -->
        </form>


        <!-- Modal -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to delete it ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')

    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () { $inp.prop('checked', false) }, 0);
            } else {
                options.push(val);
                setTimeout(function () { $inp.prop('checked', true) }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">

        function openpopupmodel()
        {
            $('#addrestaurantid').modal({backdrop: 'static', keyboard: false});
            $('#addedit').html('Add');
            $('#addmenuid').modal({backdrop: 'static', keyboard: false});
            $('#id').val("");
            $('#Promocode').val("");
            $('#description').val("");
            $('#StartDate').val("");
            $('#ExpiryDate').val("");
            $('#discount_type').val("");
            $('#discount_amount').val("");
            $('#discount_max_amount').val("");
            $('#allowed_products').val("");

            $('#max_users').val("");
            $('#max_usage_per_user').val("");
            $('#Start').val("");
            $('#end').val("");
            $('#min_spending_amount').val("");
            $('#max_spending_amount').val("");
            $('#saveoffer').html('ADD');
            for (var i = 0; i < 7; i++) {
                $('#chk-role'+i).removeClass('option-selected');
                $('#chkbtnoffer'+i).prop("checked", false);
            }
        }

        function confirm_modal(delete_url)
        {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
        }
        var offershopflag = [];
        function update_modal(param)
        {
            console.log(param.shops);
            console.log(JSON.stringify(param));
            //var data = JSON.parse(param);
            $('#manage-menu-form').attr('action', '{{url('editOffer')}}');
            $('#addmenuid').modal('show');
            $('#addedit').html('Edit');
            $('#id').val(param.id);
            $('#Promocode').val(param.code);
            $('#description').attr('value', param.description);
            $('#description').val(param.description);
            $('#StartDate').attr('value',param.start_date);
            $('#StartDate').val(param.start_date);
            $('#ExpiryDate').attr('value',param.end_date);
            $('#ExpiryDate').val(param.end_date);
            $('#discount_type').attr('value',param.discount_type);
            $('#discount_type').val(param.discount_type);
            $('#discount_amount').attr('value',param.discount);
            $('#discount_amount').val(param.discount);
            $('#discount_max_amount').attr('value',param.discount_max_amount);
            $('#discount_max_amount').val(param.discount_max_amount);
            $('#allowed_products').val(param.allowed_products);
            $('#max_users').attr('value',param.max_users);
            $('#max_users').val(param.max_users);
            $('#max_usage_per_user').attr('value',param.max_usage_per_user);
            $('#max_usage_per_user').val(param.max_usage_per_user);
            $('#Start').val(param.start_time);
            $('#end').val(param.end_time);
            $('#min_spending_amount').attr('value',param.min_spending_amount);
            $('#min_spending_amount').val(param.min_spending_amount);
            $('#max_spending_amount').attr('value',param.max_spending_amount);
            $('#max_spending_amount').val(param.max_spending_amount);
            $('#saveoffer').html('UPDATE');
             var oshopflag = JSON.parse(param.shops);         
             for (var i = 0; i < oshopflag.length; i++) {
                 //console.log(oshopflag[i]);
                 offershopflag.splice(0, 0, oshopflag[i]);
                 $('#restaurant').val(offershopflag).trigger('change');
             }            
            for (var i = 0; i < 7; i++) {
                $('#chk-role'+i).removeClass('option-selected');
                $('#chkbtnoffer'+i).prop("checked", false);
            }
            if(param.discount_type =='amount') {
                $("#Dollar").prop("checked", true);
                $('#discountmax').hide();
            }else if(param.discount_type =='free_delivey'){
                $("#free_delivery").prop("checked", true);
                $('#discount_amount').hide();
                $('#discountmax').hide();
            }else{
                $("#percentage").prop("checked", true);
                //$('#discount').show();
                $('#discountmax').show();
            }

            var rundays = param.days_to_run;

            rundays =  JSON.stringify(rundays);
            rundays =  JSON.parse(rundays);
            // console.log(rundays);
            rundays =  rundays.replace('[', '');
            rundays =  rundays.replace(']', '');
            rundays = rundays.split(",");
            //console.log(rundays);
            for (var i = 0; i < rundays.length; i++) {
                rundays[i] =  rundays[i].replace(/"/g, '');
                //console.log(rundays[i]);
                $('#chk-role'+parseInt(rundays[i])).addClass('option-selected');
                $('#chkbtnoffer'+parseInt(rundays[i])).prop("checked", true);
            }
        }

    </script>
    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>

    <script>
        $(document).ready(function () {



            $('.categoryhide').hide();
            $('.producthide').hide();

            $("#categorysearch").keyup(function () {
                var x = document.getElementById('categoryshowSearchDiv');
                var y = document.getElementById('showSearchDiv');
                if ($(this).val() == "") {
                    x.style.display = 'none';
                    y.style.display = 'block';
                } else {
                    x.style.display = 'block';
                    y.style.display = 'none';
                }
            });

            $("#productsearch").keyup(function () {
                var x = document.getElementById('categoryshowSearchDiv');
                var y = document.getElementById('showSearchDiv');
                if ($(this).val() == "") {
                    x.style.display = 'block';
                    y.style.display = 'none';
                } else {
                    x.style.display = 'none';
                    y.style.display = 'block';
                }
            });
        });


        function addremovestyle(param){
            console.log(param);
            $('.chk-toolbar div').removeClass('option-selected');
            $('#chk-role'+param).addClass('option-selected');
            $('#chkbtnoffer'+param).prop('checked', true);
        }
        function addremovestyleoffer(param){
            console.log(param);

            if($('#chkbtnoffer'+param).is(":checked")){
                //$('.chk-toolbar div').removeClass('option-selected');
                $('#chk-role'+param).addClass('option-selected');
                $('#chkbtnoffer'+param).prop('checked', true);
            }else{
                // $('.chk-toolbar div').addClass('option-selected');
                $('#chk-role'+param).removeClass('option-selected');
                $('#chkbtnoffer'+param).prop('checked', false);
            }
        }

        $('.itemcatg').click(function() {
            $('.categoryhide').hide();
            $('.producthide').hide();
            $('#itemmenu').hide();
            $('#categorymenu').hide();
            $('input[name=allowed_products]').removeAttr('checked');
            $('#itemmenu').html("");
            $('#categorymenu').html("");
            $('#productsearch').val("");
            $('#categorysearch').val("");
            allowedcategory = [];
            recommproduct = [];
            recommendedproducts = [];
            recommendedcategory = [];
            $('#recommendedcategory').val("");
            $('#recommendedproducts').val("");
            $("#categoryshowSearchDiv").css("display", "none");
            $("#showSearchDiv").css("display", "none");
        });
        $('.specificcatg').click(function() {
            $('.categoryhide').show();
            $('.producthide').hide();
            $('#itemmenu').hide();
            $('#categorymenu').show();
            $('#recommendedproducts').val("");
            $('input[name=allowed_products]').removeAttr('checked');
            $('#categorymenu').html("");
            $('#itemmenu').html("");
            $('#productsearch').val("");
            $('#categorysearch').val("");
            allowedcategory = [];
            recommproduct = [];
            recommendedproducts = [];
            recommendedcategory = [];
            $('#recommendedcategory').val("");
            $('#recommendedproducts').val("");
            $("#categoryshowSearchDiv").css("display", "none");
            $("#showSearchDiv").css("display", "none");
        });
        $('#specificmenu').click(function() {
            $('.categoryhide').hide();
            $('.producthide').show();
            $('#itemmenu').show();
            $('#categorymenu').hide();
            $('#recommendedcategory').val("");
            $('#productsearch').val("");
            $('#categorysearch').val("");
            allowedcategory = [];
            recommproduct = [];
            recommendedproducts = [];
            recommendedcategory = [];
            $('#recommendedcategory').val("");
            $('#recommendedproducts').val("");
            $("#categoryshowSearchDiv").css("display", "none");
            $("#showSearchDiv").css("display", "none");
            $('#productitems').html("");
        });


        var checkedvalues = [];
        function category_items(id) {
            var shop_id = id;
            var url = "{{url('shop_categories')}}/" + shop_id;
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log(res);
                    if (res.success) {
                        var category =res.data;
                        console.log(category.length);
                        var categories = '<nav class="navbar-options menu-list-items d-flex ">'+
                            '<ul class="nav flex-column flex-nowrap ">'+
                            '<li class="nav-item">'
                        for(var i=0;i<category.length;i++) {
                            console.log(category[i]);
                            categories +='<a class="nav-link text-truncate" onclick="categoryadd('+category[i].id+',\'' + category[i].name + '\')">' +
                                '<span class="menu-link ">'+category[i].name+'</span>' +
                                '</a>'
                        }
                        categories+='</li>'+
                            '</ul>'+
                            '</nav>';

                        $('#categoryshowSearchDiv').html(categories);
                    } else {

                    }
                },error: function(res) {

                }
            });

        }

        function category_products(id) {
            var shop_id = id;
            var url = "{{url('shop_categories')}}/" + shop_id;
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log(res);
                    if (res.success) {
                        var category =res.data;
                        console.log(category.length);
                        var categories = '<nav class="navbar-options menu-list-items d-flex ">'+
                            '<ul class="nav flex-column flex-nowrap ">'+
                            '<li class="nav-item">'
                        for(var i=0;i<category.length;i++) {
                            console.log(category[i]);
                            categories +='<a class="nav-link text-truncate" onclick="getproducts('+category[i].id+')">' +
                                '<span class="menu-link ">'+category[i].name+'</span>' +
                                '</a>'
                        }
                        categories+='</li>'+
                            '</ul>'+
                            '<ul class="nav flex-column flex-nowrap menu-list-items-options">'+
                            '<li class="nav-item" id="productitems">'+
                            '</li>'+
                            '</ul>'+
                            '</nav>';

                        $('#showSearchDiv').append(categories);
                    } else {

                    }
                },error: function(res) {

                }
            });

        }
        function getproducts(data){

            var id = data.id;
            var url = "{{url('category_items')}}/" + data;
            $.ajax({
                cache: false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log(res.data);
                    var cproduct = res.data;//data.products;
                    for (var i = 0; i < cproduct.length; i++) {
                        var itemnm = cproduct[i].name;
                        var checked = '';

//                if (checkedvalues.includes("141")) {
//                    //  $('#string').prop("checked", true);
//                } else {
//                    checked = '';
//                }

                        var appendprod = '<a class="nav-link text-truncate active-links" href="#">' +
                            '<div>' +
                            '<label class="check">' + itemnm + '<input type="checkbox" name="itemnameid"   value=' + cproduct[i].id + '@@' + itemnm + ' onclick="additemstomenu(this.value);" id="string">' +
                            '<span class="checkmark"></span>' +
                            '</label>' +
                            '</label>' +
                            '</label>' +
                            '</div>' +
                            '</a>';

                        //$('#string').prop("checked", true);
                    }
                    $('#productitems').html(appendprod);
                }
            });


        }

        var allowedcategory = [];
        function categoryadd(id,name) {
            var itemdtl = id;

            var itemdtlname = name;
            console.log("3434 3434 "+$('#'+itemdtl).is(":checked"));
            var recommendFlag = false;
            for(var i=0; i<allowedcategory.length;i++){
                if(parseInt(allowedcategory[i]) == parseInt(itemdtl)){
                    recommendFlag = true;
                    checkedvalues.push(itemdtl[0]);
                }

            }
            if(recommendFlag)
                return false;
            console.log("sdfsf "+allowedcategory);
            allowedcategory.splice(0, 0, itemdtl);

            $('#categorymenu').append('<div class="employee-options option-selected" id="card'+itemdtl+'">' + itemdtlname + '' +
                '<input type="checkbox" name="allowed_category[]" value=' + itemdtl + ' style="display:none;"><span class="ml-2">' +
                '<img src="./images/clearoption.svg" alt="" onclick="remove_categoryrec(' + itemdtl + ')"></div>');

            $("#recommendedcategory").val(allowedcategory);

        }

        var recommproduct = [];
        function additemstomenu(data) {

            var itemdtl = data.split('@@');

            console.log("343443434 "+$('#'+itemdtl[0]).is(":checked"));
            var recommendFlag = false;
            for(var i=0; i<recommproduct.length;i++){
                if(parseInt(recommproduct[i]) == parseInt(itemdtl[0])){
                    //console.log("itemar "+parseInt(recommproduct[i])+" "+itemdtl[0]);
                    recommendFlag = true;
                }
            }
            if(recommendFlag)
                return false;

            recommproduct.splice(0, 0, itemdtl[0]);
            checkedvalues.push(itemdtl[0]);
            console.log("sdfsf "+recommproduct);
            $('#itemmenu').append('<div class="employee-options option-selected" id="card'+itemdtl[0]+'">' + itemdtl[1] + '' +
                '<input type="checkbox" name="allowed_products[]" value=' + itemdtl[0] + ' style="display:none;"><span class="ml-2">' +
                '<img src="./images/clearoption.svg" alt="" onclick="remove_rec(' + itemdtl[0] + ')"></div>');

            $("#recommendedproducts").val(recommproduct);

        }


        function removeitem(data){
            $('#'+data).remove();
        }

        function remove_categoryrec(data){
            $("#recommendedcategory").val(data);
            $("#card"+data).remove();
            $("#"+data).prop("checked",false);
            for(var i=0; i<allowedcategory.length;i++) {
                console.log(data);
                if(parseInt(allowedcategory[i]) == parseInt(data)){
                    console.log(allowedcategory[i]);
                    allowedcategory.splice(i, 1);
                }
            }
            $("#recommendedcategory").val(allowedcategory);
        }
        function remove_rec(data){
            $("#recommendedproducts").val(data);
            $("#card"+data).remove();
            $("#"+data).prop("checked",false);
            // $("#recommendedproducts").val(itemdtl[0]);
            for(var i=0; i<recommproduct.length;i++) {
                //if ($('#' + itemdtl[0]).is(":checked") == false) {
                console.log(data);
                if(parseInt(recommproduct[i]) == parseInt(data)){
                    console.log(recommproduct[i]);
                    recommproduct.splice(i, 1);
                }
            }
            $("#recommendedproducts").val(recommproduct);
        }


        $("#Dollar").click(function() {
            if($('#Dollar').is(":checked")) {
                $('#discountmax').hide();
                $('#discount').show();
            }else{
                $('#discountmax').show();
                $('#discount').show();
            }
        });
        $("#percentage").click(function() {
            if($('#percentage').is(":checked")) {
                $('#discountmax').show();
                $('#discount').show();
            }
        });
        $("#free_delivery").click(function() {
            if($('#free_delivery').is(":checked")) {
                $('#discountmax').hide();
                $('#discount').hide();
            }
        });
        function couponveriy(id){
            $("#saveoffer").attr('disabled','disabled');
            var url = "{{url('CouponVerify')}}/" + id;
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    if(res.success){
                        $('#couponverfiy').html("Coupon Code already exists. Please Enter another Coupon Code.");
                        //$('#').focus();
                    }else{
                        $('#couponverfiy').html("");
                        $("#saveoffer").removeAttr('disabled');
                        return false;
                    }
                },error: function(res) {

                }
            });
        }


    </script>
@endsection
