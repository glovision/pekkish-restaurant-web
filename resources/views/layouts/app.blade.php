
<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$titlePage ?? 'Pekkish'}}</title>
	<link rel="icon" href="{{url('/')}}/images/favicon/icon.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
            href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
            rel="stylesheet">
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/sweetalert.css">
    <link rel="stylesheet" href="css/res-cards.css">
    <link rel="stylesheet" href="css/custom-radio-check.css">
    <link rel="stylesheet" href="css/table-toggle.css">
    <link rel="stylesheet" href="css/custom-dialog-input.css">
    <link rel="stylesheet" href="css/tablestyles.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel='stylesheet' href="{{asset('css/vanilla-script1.css')}}">
    {{--<link rel='stylesheet' href='css/dist/vanilla-dataTables.min.css'>--}}
    <link rel='stylesheet' href="{{asset('css/dist/vanilla-dataTables.min.css')}}">
    <link href="{{ asset('plugins/select2/css/select2.min.css')}}" rel="stylesheet">
  
    @stack('css')
</head>
<body>
@include('inc.nav')
<div class="main-section max-content-width">
    @yield('content')
</div>
@include('inc.side_nav')
@include('inc.footer')
@yield('modal')
@yield('script')
</body>


<script type="text/javascript" src="{{asset('js/sweetalert.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/JqueryFormValidation.js')}}"></script>

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
<script type="text/javascript" src="{{asset('js/duplicate.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-dataTables1.6.16.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-script1.js')}}"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>

<style>
    .dataTable-bottom>div:last-child, .dataTable-top>div:last-child {float:left}
    dataTable-dropdown
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(".demo-select2").select2();
    $( function() {
        $( "#StartDate" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $( "#ExpiryDate" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $( "#joiningdate" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<script>
    $(document).ready(function () {
        if (window.IsDuplicate()) {
            // alert user the tab is duplicate
            alert("Window Already Opened in Another Tab\nPlease Click Ok");
        }
    });
</script>
<script>
    $.fn.materializeInputs = function (selectors) {
        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea";
        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }
        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });
        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };
    /**
     * Material Inputs
     */
    $('body').materializeInputs();
</script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
</html>