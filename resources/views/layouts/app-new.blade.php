<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$titlePage ?? 'Pekkish Restaurant'}}</title>
    <link rel="icon" href="{{url('/')}}/images/pekkish-icon.svg" sizes="any" type="image/svg+xml">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel='stylesheet' href="{{asset('css/bootstrap.min.css')}}">
    <link rel='stylesheet' href="{{asset('css/sweetalert.css')}}">
    <link rel='stylesheet' href="{{asset('css/res-cards.css')}}">
    <link rel='stylesheet' href="{{asset('css/custom-radio-check.css')}}">
    <link rel='stylesheet' href="{{asset('css/table-toggle.css')}}">
    <link rel='stylesheet' href="{{asset('css/custom-dialog-input.css')}}">
    <link rel='stylesheet' href="{{asset('css/tablestyles.css')}}">
    <link rel='stylesheet' href="{{asset('css/style.css')}}">
    <link rel='stylesheet' href="{{asset('css/vanilla-script1.css')}}">

    {{--<link rel='stylesheet' href='css/dist/vanilla-dataTables.min.css'>--}}
    <link rel='stylesheet' href="{{asset('css/dist/vanilla-dataTables.min.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    <style>
        .dataTable-bottom > div:last-child, .dataTable-top > div:last-child {
            float: left
        }

        /*dataTable-dropdown*/
    </style>
    <style>

        /*CUSTOM PRELOADER*/
        .loader-bg{
            position: fixed;
            z-index: 999999;
            background: #fff;
            width: 100%;
            height: 100%;
        }
        .loader-p{
            border: 0 solid transparent;
            border-radius: 50%;
            width: 150px;
            height: 150px;
            position: absolute;
            top: calc(50vh - 75px);
            left: calc(50vw - 75px);
        }

        .loader-p:before, .loader-p:after{
            content: '';
            border: 1em solid #15e38a;
            border-radius: 50%;
            width: inherit;
            height: inherit;
            position: absolute;
            top: 0;
            left: 0;
            animation: loader 2s linear infinite;
            opacity: 0;
        }

        .loader-p:before{
            animation-delay: 0.5s;
        }

        @keyframes loader{
            0%{
                transform: scale(0);
                opacity: 0;
            }
            50%{
                opacity: 1;
            }
            100%{
                transform: scale(1);
                opacity: 0;
            }
        }
        /*end of custom preloader*/
    </style>
    @stack('css')
</head>
<body>
<div class="loader-bg">
    <div class="loader-p">

    </div>
</div>
@include('inc.left_side_nav')
@include('inc.right_side_nav')
@include('inc.nav')
<div class="main-section max-content-width">
    @yield('content')
</div>

@include('inc.footer')

@yield('modal')


<script type="text/javascript" src="{{asset('js/sweetalert.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/JqueryFormValidation.js')}}"></script>

<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
<script type="text/javascript" src="{{asset('js/duplicate.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-dataTables1.6.16.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/vanilla-script1.js')}}"></script>
<script src="{{ asset('plugins/select2/js/select2.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
@yield('script')
<script>
    //$('.loader-bg').show();
    setTimeout(function(){
        $('.loader-bg').fadeToggle();
    },1500);
    $(".demo-select2").select2();
</script>
<script>
    $(function () {
        $("#StartDate").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $("#ExpiryDate").datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $("#joiningdate").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
<script>
    $(document).ready(function () {
        if (window.IsDuplicate()) {
            // alert user the tab is duplicate
            alert("Window Already Opened in Another Tab\nPlease Click Ok");
        }
    });
</script>
<script>
    $.fn.materializeInputs = function (selectors) {
        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea";
        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }

        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });
        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };
    /**
     * Material Inputs
     */
    $('body').materializeInputs();
</script>
{{--right sidebar script--}}
<script>
    function currentschedule() {
        if ($('#current').is(":checked")) {
            $('#scheduleoffline').hide();
            $('#manuallyonoff').show();
        }
    }

    function featureschedule() {
        if ($('#schedule').is(":checked")) {
            $('#scheduleoffline').show();
            $('#manuallyonoff').hide();
        }
    }

    function shopoffline(status) {
        $('#scheduleoffline').hide();
        $('#manuallyonoff').hide();
        $("#shopcurrsts").val(status);
        jQuery('#shopschedule_model').modal('show', {backdrop: 'static'});
        if (status == 1) {
            $('#onoffstsnow').html("Offline");
            console.log("Offline " + status);
        } else {
            $('#onoffstsnow').html("Online");
            console.log("Online " + status);
        }

        $('#current').prop("checked", true);
        $('#scheduleoffline').hide();
        $('#manuallyonoff').show();

    }

    function password_reset() {
        $("#oldpassword").val("");
        $("#newpassword").val("");
        $("#confirmpassword").val("");
        $('#passwordreset_model').modal({backdrop: 'static', keyboard: false});
    }
    function update_email() {
        $("#newemail").val("");
        jQuery('#updateemail_model').modal('show', {backdrop: 'static'});
    }

    function checknewpassword(data) {
        console.log(data);
        if (data.length < 8) {
            $('#newpasswordvaliderror').html("Password must be morethan 8 characters.");
            return false;
        }
        if (data.match("^[a-zA-Z0-9]*$")) {
            $('#newpasswordvaliderror').html("Password must be aplanumeric characters.");
            return false;
        }
        $('#newpasswordvaliderror').html("");
    }
    function checkconfirmpassword(data) {
        console.log(data);
        var newpwd = $("#newpassword").val();
        if (data != newpwd) {
            $('#confirmpasswordvaliderror').html("Confirm Password Mismatch.");
            $('#resetbutton').attr('disabled', 'disabled');
            return false;
        }
        $('#confirmpasswordvaliderror').html("");
        $("#resetbutton").removeAttr('disabled');
    }


    //$('#passwordreset_model').modal({backdrop: 'static', keyboard: false});
    //    $(document).ready(function() {
    //        duty_HistoryStatus();
    //    });
    function emailverification(id) {
        $("#emailupdatesave").prop("disabled", true);
        var userid = "null";
        var send = {"user_id": userid, "email": id, _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('ajaxEmailvarification')}}"
        console.log("3333 " + url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res) {
                    //console.log("email success");
                    $('#emailerrormsg').html("");
                    $("#emailupdatesave").prop("disabled", false);
                } else {
                    //console.log("email failed");
                    $('#emailerrormsg').html("EmailId alredy exists. Please Enter another EmailId");
                    $('#Email').focus();
                    return false;
                }
            }, error: function (res) {

            }
        });
    }
    function manuallychange() {
        var status = $("#shopcurrsts").val();
        var send = {"status": status, _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('ajaxshopstatuschange')}}"
        console.log("3333 " + url + " data " + send);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    console.log("success " + JSON.stringify(res));
                    var status_msg = res.data.status == 0 ? 'Offline' : 'Online';
                    swal({title: "success", text: 'Restaurant status updated to ' + status_msg});
                    $('#shopschedule_model').modal('hide');
                    $('input[name=shopoffon]').removeAttr('checked');
                    $('#onoffstsnow').html("");
                    // location.reload();session('user_details')->type == 'shop'

                } else {
                    swal({title: "error", text: res.message});
                }
            }, error: function (res) {

            }
        });
    }

    function duty_HistoryStatus() {
        var url = "{{url('dutyHistoryStatus')}}";
        console.log("URL " + url);
        $.ajax({
            cache: false,
            type: "GET",
            url: url,
            success: function (res) {
                console.log("duty " + JSON.stringify(res));
                if (res.success) {
                    var data = res.data;
                    if (data.STATUS == 0) {
                        $('#switch').prop('checked', false);
                    }
                    var avail = data.Avail;
                    if (avail.length > 0) {
                        $('#switch').prop('checked', false);
                    }
                }
            }, error: function (res) {

            }
        });
    }


    function profupdmodel() {
        $('#profupd_model').modal({backdrop: 'static', keyboard: false});
    }

    function readavatarURL(input) {
        var fileUpload = document.getElementById("avatar");
        if (typeof (fileUpload.files) != "undefined") {
            var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
            if (size > 2000) {
                $('#imagesizevaliderror').html("Image size should be below 2mb.");
                $('#image-profile').attr('src', '');
                return false;
            }
        }

        $('#image-profile').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-profile').attr('src', e.target.result);
            }
            $('#imagesizevaliderror').html("");
            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
</body>
</html>