@extends('layouts.app-new',["titlePage" => "Manage Restaurants"])
@push('css')
    <style>
        .pac-container {
            z-index: 10000 !important;
        }
    </style>
@endpush
@section('content')
    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">MANAGE RESTAURANTS</span>
                {{--<button class="add-btn" onclick="openpopupmodel();">+ Add Restaurant</button>--}}
                <a href="{{url('/add_restaurant')}}" class="add-btn">+ Add Restaurant</a>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="container-fluid p-0">
                <div class="row">
                    @foreach($shops as $shop)
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                            <div class="manage-restaurant-card">
                                <div class="d-flex f-14 f-medium res-title mb-2">
                                    {{--<img class="resturant-logo-img" src="http://localhost/pekkish-dev/public/{{$shop['logo']}}" alt="">--}}
                                    <img class="resturant-logo-img" src="{{$shop['logo']}}" alt="">
                                    <div>
                                        <p class="mb-0">{{$shop['name']}}</p>
                                        <p class="mb-0 f-12 text-color-grey">{{$shop['bank_acc_no']}}</p>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <div class="mb-1 f-14 f-medium">Address</div>
                                    <div class="f-14 ">{{mb_strimwidth($shop['address']??'' ,0,30,"...")}}</div>
                                    <div class="f-14 text-color-grey "></div>
                                </div>

                                <div>
                                    <div class="mb-1 f-14 f-medium">Bank Account</div>
                                    <div class="f-14 ">{{$shop['bank_acc_no']}}</div>
                                    <div class="f-14 text-color-grey ">{{$shop['bank_name']}}</div>
                                    <div class="f-14 text-color-grey ">{{$shop['bank_acc_name']}}</div>
                                </div>

                                <div class="d-flex justify-content-end mt-3">
				{{--<a id="{{$shop['user']['email']??''}}" onclick="loginAsShop(this.id);" style="cursor:pointer;">
                                        <i class="fa fa-power-off" aria-hidden="true"></i>--}}
                                    </a>
                                    <a  href="{{url('editShop', $shop['id'])}}">
                                    <img class="mx-3" src="./images/card-edit.svg" alt=""/>
                                    </a>
                                    <a onclick="confirm_modal('{{url('shop_destroy', $shop['id'])}}');">
                                        <img class="mx-3" src="./images/delete.svg" alt="" >
                                    </a>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

    <div>
        @include('inc.side_nav')

        <div class="modal add-restaurant right fade " id="addrestaurantid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewrestaurant" role="add restaurant">
                <div class="modal-content">

                    <div class="modal-body pb-4">
                        <div class="f-20 f-medium"><span id="addedit"></span> Restaurant</div>

                        <form id="manage-restaurant-form" method="POST" action="{{url('addShop')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group input-material">
                                <input type="hidden" name="id" class="form-control" id="id">                                
                                <input type="text" name="name" class="form-control" id="name" data-validation="required" required>
                                <label for="store-field">Restaurant Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="shortname" class="form-control" id="short-name"  data-validation="required" required>
                                <label for="short-name-field">Short Name of Restaurant<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('shortname') }}</div>
                            </div>
                            <div class="form-group input-material">
                                <input type="email" name="email" class="form-control" id="email" value="{{session()->get('user_details')->email}}"  data-validation="required" required>
                                <label for="email-field">Email<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('email') }}</div>
                                <div class="text-danger" id="mobileerrormsg1"></div>
                            </div>
                      <div class="row">
                        <div class="col-12 col-sm-6">
                        <div class="form-group input-material mr-2">
                            <select name="country_code" id="countrycode" class="form-control" data-validation="required" required>
                               
                            @foreach($countries as $country)
                                    <option value="{{$country->country_code}}">{{$country->code}} ({{$country->country_code}})</option>
                            @endforeach
                            </select>
                            <label for="Country Code" style="top:-14px;">Country Code</label>
                            <div class="text-danger">{{ $errors->first('countrycode') }}</div>
                        </div>
                        </div>
                        <div class="col-12 col-sm-6">
                        <div class="form-group input-material mr-2">
                                <input type="text" class="form-control" name="phone" id="phone" minlength="10" value="{{session()->get('user_details')->phone}}" maxlength="10" pattern="[0-9]*"  data-validation="required" required>
                                <label for="Last Name">Mobile Number<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('phone') }}</div>
                                <div class="text-danger" id="mobileerrormsg"></div>
                            </div>
                            </div>
                            </div>
                            <div class="form-group input-material">
                            {{--<input type="text" name="address" class="form-control" id="address" required>--}}
                            {{--<label for="address">Address</label>--}}
                                <input id="searchInput" class="controls form-control" type="text" placeholder="Enter a location*" required>
                                <!-- Google map -->
                                <div id="map" style="width: 100%;height: 400px;"></div>
                                <!-- Display geolocation data -->
                                <input type="hidden" id="location" name="address" />
                                <input type="hidden" id="postal_code" name="postal_code"/>
                                <input type="hidden" id="country" name="country"/>
                                <input type="hidden" id="lat" name="cityLat" required/>
                                <input type="hidden" id="lon" name="cityLng" required/>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="landmark" class="form-control" id="landmark" data-validation="required" required>
                                <label for="landmark">Landmark<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('landmark') }}</div>
                            </div>
                            <div class="form-group input-material">
                                <select name="city" class="form-control" id="city" data-validation="required">
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" name="city" class="form-control" id="city" data-validation="required" required>--}}
                                <label for="city">Select City<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('city') }}</div>
                            </div>
                            


<div class="d-flex align-items-center flex-wrap">
        <label class="flex chk-toolbar">
            <label class="employee-options" id="chk-role0" onclick="add_remove_style(0)">
                Pick Up
                <input type="checkbox" name="pickup" id="chkbtnshop0" value="1" style="display:none;">
            </label>
            <label class="employee-options" id="chk-role1" onclick="add_remove_style(1)">
                Delivery
                <input type="checkbox" name="delivery" id="chkbtnshop1" value="1" style="display:none;">
            </label>
            <label class="employee-options" id="chk-role2" onclick="add_remove_style(2)">
                Food@work
                <input type="checkbox" name="food_work" id="chkbtnshop2" value="1" style="display:none;">
            </label>
        </label>
</div>                           

                            <!-- <div class="form-group input-material">
                                   <div type="input" class="employee-options"  >Pick Up
                                    <input type="checkbox" id="pickup" name="pickup" value="1"/>
                                </div>
                                <div type="input" class="employee-options"  >Delivery
                                    <input type="checkbox" id="delivery" name="delivery" value="1"/>
                                </div>
                                <div type="input" class="employee-options"  >Food@work
                                    <input type="checkbox" id="food_work" name="food_work" value="1"/>
                                </div>

                            </div>-->
                           <div class="form-group input-material">
                                <input type="text" name="radius" class="form-control" id="radius" data-validation="required" required>
                                <label for="city">Radius<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('radius') }}</div>
                            </div>
                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">GCT<span style="color:red">*</span></p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-4">
                                            <label class="radio">Yes
                                                <input type="radio" name="gct" id="available_ststus" value="Y" >
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-4">
                                            <label class="radio">No
                                                <input type="radio" name="gct" id="available_ststus" value="N" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-group input-material">--}}
                                {{--<input type="number" name="gct" class="form-control" id="gct" data-validation="required" required>--}}
                                {{--<label for="city">GCT<span style="color:red">*</span></label>--}}
                                {{--<div class="text-danger">{{ $errors->first('gct') }}</div>--}}
                            {{--</div>--}}

                            <div class="form-group input-material">
                                <input type="text" name="bank_acno" class="form-control" id="bankaccountnumber" data-validation="required" required> 
                                <label for="bankaccountnumber">Bank Account Number<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('bank_acno') }}</div>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="bank_code" class="form-control" id="bankcode" data-validation="required" required>
                                <label for="bankcode">Bank Code<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('bank_code') }}</div>
                            </div>
                            <div class="form-group input-material">
                                <input type="text" name="bank_name" class="form-control" id="bankname" data-validation="required" required>
                                <label for="banknames">Bank Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('bank_name') }}</div>
                            </div>
                            <div class="form-group input-material mr-2">
                                <img src="" class="resturant-add-logo-img" alt="" id="image-preview" class="img-fluid">
                                <input type="file" id="logo" name="logo" class="form-control" onchange="readURL(this);">
                                <label for="logo" style="top:-14px;">logo<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('logo') }}</div>
                                <div class="text-danger" id="imagesizevaliderror"></div>
                            </div>
                            <div class="mt-5 d-flex align-items-center justify-content-between">
                             <input type="button" class="cancelbtn resetform" value="CANCEL" />
                                <button class="addbtn"><span id="savebtn"></span> RESTAURANT</button>
                            </div>
                        </form>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <!-- Modal -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to delete it ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" onclick="modelclose()">NO</button>
                            <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $('#manage-restaurant-form').on('hidden.bs.modal', function () {
            $('#manage-restaurant-form form')[0].reset();
        });
        $(document).ready(function(){
            $('#shopeditbtn').hide();
	});
        $(".resetform").click(function (e) {
            $('#addrestaurantid').modal('hide');
            $('#manage-restaurant-form').find('.text-danger').empty();
        });
	function modelshopclose(){
            $('#addrestaurantid').modal('hide');
            //jQuery('#addrestaurantid').modal('hide', {backdrop: 'static'});
        }
        function modelclose(){
		
	   $('#confirm_delete').modal('hide');
           // jQuery('#confirm_delete').modal('hide', {backdrop: 'static'});
           // $('#addrestaurantid').find('textarea,input').val('');
        }
        function confirm_modal(delete_url)
        {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
        }

        function openpopupmodel()
        {

            $('#addedit').html('Add');
            $('#savebtn').html('ADD');
            $('#addrestaurantid').modal({backdrop: 'static', keyboard: false});
            $('#manage-restaurant-form').attr('action', '{{url('addShop')}}');
            $('#addrestaurantid').modal('show');
            $('#id').val("");
            $('#name').val("");
            $('#short-name').val("");
           // $('#email').val("");
            //$('#phone').val("");
            $('#countrycode').val("");
            $('#searchInput').val("");
            $('#location').val("");
            $('#lat').val("");
            $('#lon').val("");
            $('#landmark').val("");
            $('#city').val("");
            $('#bankaccountnumber').val("");
            $('#bankcode').val("");
            $('#bankname').val("");
            $('#radius').val("");
            $('#logo').val("");
            $('#image-preview').hide();
            $('input[name="pickup"]').prop('checked', '');
            $('input[name="delivery"]').prop('checked', '');
            $('input[name="food_work"]').prop('checked', '');
        }

        function update_modal(param)
        {
            $('input[name="pickup"]').prop('checked', '');
            $('input[name="delivery"]').prop('checked', '');
            $('input[name="food_work"]').prop('checked', '');
            $('#addedit').html('Edit');
            $('#savebtn').html('UPDATE');
            $('#addrestaurantid').modal({backdrop: 'static', keyboard: false});
            console.log(param);
            var data = JSON.parse(param);
            //console.log(data.user.id);
            //map.setCenter(new GLatLng(data.latitude,data.longitude));
            var latlng = new google.maps.LatLng(parseFloat(data.latitude), parseFloat(data.longitude));
            var marker = new google.maps.Marker({
            "map": map,
            "position": latlng,
                draggable: true
        });
            map.setCenter(latlng);

            var infowindow = new google.maps.InfoWindow({
                "content": data.address,
                "position": latlng,

            }).open(map, marker);
                 map.setZoom(13);
            console.log("Latitude "+data.latitude);
            console.log("Longitude "+data.longitude);
            google.maps.event.addListener(marker,'dragend',function() {
                console.log("lat: "+marker.position.lat())
                console.log("lng: "+marker.position.lng())
                $('#lat').val(marker.position.lat());
                $('#lon').val(marker.position.lng());
            });


            $('#manage-restaurant-form').attr('action', '{{url('shop_edit')}}');
            $('#addrestaurantid').modal('show');
            $('#addedit').html('Edit');
            $('#id').val(data.id);
           // $('#userid').attr('value', data.user.id);
            $('#name').attr('value', data.name);
            $('#name').val(data.name);
            $('#short-name').attr('value', data.meta_title);
            $('#short-name').val(data.meta_title);
            $('#email').attr('value', data.user.email);
            $('#email').val(data.user.email);
            $('#phone').attr('value', data.user.phone);
            $('#phone').val(data.user.phone);
            //console.log("country "+data.user.country_id);            
            $('#countrycode option[value="'+data.user.country_id+'"]').prop('selected', 'selected');
            $('#radius').attr('value', data.radius);
            $('#radius').val(data.radius);
            $('#searchInput').attr('value', data.address);
            $('#searchInput').val(data.address);
            $('#location').attr('value', data.address);
            $('#location').val(data.address);
            $('#lat').attr('value', data.latitude);
            $('#lat').val(data.latitude);
            $('#lon').attr('value', data.longitude);
            $('#lon').val(data.longitude);
            $('#landmark').attr('value', data.user.landmark);
            $('#landmark').val(data.user.landmark);
            $('#city').attr('value', data.user.city);
            $('#city').val(data.user.city);
            $('#bankaccountnumber').attr('value', data.bank_acc_no);
            $('#bankaccountnumber').val(data.bank_acc_no);
            $('#bankcode').attr('value', data.bank_acc_name);
            $('#bankcode').val(data.bank_acc_name);
            $('#bankname').attr('value', data.bank_name);
            $('#bankname').val(data.bank_name);
            //$('#logo').val(data.logo);
            $('#image-preview').show();
            $('#image-preview').attr('src', data.logo);
            $('input[name="gct"][value='+data.gct+']').prop('checked', 'checked');
            $('input[name="pickup"][value='+data.pickup+']').prop('checked', 'checked');
            $('input[name="delivery"][value='+data.delivery+']').prop('checked', 'checked');
            $('input[name="food_work"][value='+data.foodatwork+']').prop('checked', 'checked');
            if(data.pickup){
                $('#chk-role0').addClass('option-selected');
                $('#chkbtnshop0').prop('checked', true);
            }
            if(data.delivery){
                $('#chk-role1').addClass('option-selected');
                $('#chkbtnshop1').prop('checked', true);
            }
            if(data.foodatwork){
                $('#chk-role2').addClass('option-selected');
                $('#chkbtnshop2').prop('checked', true);
            }      
            //$('#gct'+data.gct).addClass('option-selected');
        }

        function emailverification(id){
            $("#savebtn").attr('disabled','disabled');
            var userid =  $('#userid').val();
            var send = {"user_id":userid,"email":id, _token: '{{csrf_token()}}'};
            send=$(this).serialize()+" & "+$.param(send);
            var url = "{{url('EmailIdvarification/')}}"
            console.log("3333 "+url);
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data:send,
                success: function (res) {
                    if(res.success){
                        $('#mobileerrormsg1').html("");
                        $("#savebtn").removeAttr('disabled');
                    }else{
                        $('#mobileerrormsg1').html("Email Id. already exists. Please Enter another Email Id.");
                        $('#email').focus();
                        return false;
                    }
                },error: function(res) {

                }
            });
        }
        function mobileverification(id){
            $("#savebtn").attr('disabled','disabled');
            var mob_length = id.length;
            if(mob_length != 10){
                $('#mobileerrormsg').html("Please Enter 10 Digit Mobile No.");
                $('#phone').focus();
                return false;
            }

            var userid =  $('#userid').val();
            var send = {"user_id":userid,"mobile":id, _token: '{{csrf_token()}}'};
            send=$(this).serialize()+" & "+$.param(send);
            var url = "{{url('ajaxmobilevarification')}}"
            console.log("3333 "+url);
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data:send,
                success: function (res) {
                    if(res){
                        $('#mobileerrormsg').html("");
                        $("#savebtn").removeAttr('disabled');
                    }else{
                        //console.log("email failed");
                        $('#mobileerrormsg').html("Phone No. already exists. Please Enter another Phone No.");
                        $('#phone').focus();
                        return false;
                    }
                },error: function(res) {

                }
            });
        }

        function loginAsShop(email)
        {
           var url = "{{url('two_way_signin')}}/" + email;
            console.log(url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    if(res.success){
                        //console.log("login Success "+JSON.stringify(res));
                        window.open("{{url('two-way-login')}}/"+res.access_token, "_self");
                    }else{
                        console.log("Failed to Login "+JSON.stringify(res));
                    }
                },error: function(res) {

                }
            });
        }
        function readURL(input) {
            var fileUpload = document.getElementById("logo");
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                if(size>2000){
                    $('#imagesizevaliderror').html("Image size should be below 2mb.");
                    $('#image-preview').attr('src', '');
                    //return false;
                }
            }

            $('#image-preview').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                }
                $('#imagesizevaliderror').html("");
                reader.readAsDataURL(input.files[0]);
            }
        }

        function addremovestyle(param){
            $('.radio-toolbar div').removeClass('option-selected');
            $('#radio-role'+param).addClass('option-selected');
            $('#radiobtnrole'+param).prop('checked', true);
        }
    </script>
    <script>
        var map=null;
        function initMap() {

             map = new google.maps.Map(document.getElementById('map'), {
	      center: {lat: 18.1096, lng: -77.2975},
                zoom: 13,
                streetViewControl:false,
                mapTypeId:google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                navigationControl: false,
                zoomControl: false,
                fullscreenControl: false,
            });
            var input = document.getElementById('searchInput');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
console.log("testing "+input);
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                marker.setIcon(({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);

                // Location details
                for (var i = 0; i < place.address_components.length; i++) {
                    if(place.address_components[i].types[0] == 'postal_code'){
                        document.getElementById('postal_code').value = place.address_components[i].long_name;
                    }
                    if(place.address_components[i].types[0] == 'country'){
                        document.getElementById('country').value = place.address_components[i].long_name;
                    }
                }
                document.getElementById('location').value = place.formatted_address;
                document.getElementById('lat').value = place.geometry.location.lat();
                document.getElementById('lon').value = place.geometry.location.lng();
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById('lat').value = this.getPosition().lat();
                document.getElementById('lon').value = this.getPosition().lng();
                geocodePosition(marker.getPosition());
            });
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {

                if (responses && responses.length > 0) {
                    marker.formatted_address = responses[0].formatted_address;
                } else {
                    marker.formatted_address = 'Cannot determine address at this location.';
                }
                infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
                infowindow.open(map, marker);
            });
        }

        function add_remove_style(param){
            console.log(param);

            if($('#chkbtnshop'+param).is(":checked")){
                //$('.chk-toolbar div').removeClass('option-selected');
                $('#chk-role'+param).addClass('option-selected');
                $('#chkbtnshop'+param).prop('checked', true);
            }else{
                // $('.chk-toolbar div').addClass('option-selected');
                $('#chk-role'+param).removeClass('option-selected');
                $('#chkbtnshop'+param).prop('checked', false);
            }
        }

    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFwT4Fajz4alzQiYm2fwgW0ZmxEa-fjEk&libraries=places&callback=initMap" async defer></script>
    {{--<script type="text/javascript" src="{{$googleapi->api}}" async defer></script>--}}
@endsection
