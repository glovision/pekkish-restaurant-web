@extends('layouts.app-new',["titlePage" => "Edit Restaurant"])
@push('css')
<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>

<style>

    h1 {
        font-size: 20px;
        text-align: center;
        margin: 20px 0 20px;
    }
    h1 small {
        display: block;
        font-size: 15px;
        padding-top: 8px;
        color: gray;
    }
    .avatar-upload {
        position: relative;
        max-width: 100%;
        margin: 5px;
    }
    .avatar-upload .avatar-edit {
        position: absolute;
        right: -15px;
        z-index: 1;
        top: -15px;
    }
    .avatar-upload .avatar-edit input {
        display: none;
    }
    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #fff;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
        content: "\f040";
        font-family: 'FontAwesome';
        color: #757575;
        position: absolute;
        top: 6px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }
    .avatar-upload .avatar-preview {
        width: 100%;
        height: 200px;
        position: relative;
        border-radius: 0%;
        border: 6px solid #f8f8f8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 0%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

</style>
@endpush
@section('content')
    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">Edit Restaurant</span>
            </div>
            <form id="manage-restaurant-form" method="POST" action="{{url('UpdateShop')}}" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-sm-6">
                        <label for="city">Basic Details</label><br>

                        <label for="city">Shop Logo<span style="color:red">*</span></label><br>
                        <div class="">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' id="imageUpload" name="logo" accept=".png, .jpg, .jpeg" />
                                    <label for="imageUpload"></label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview"  style="background-image: @if(!is_null($shop[0]['logo'] ?? null)) url({{$shop[0]['logo']}}) @else url({{url('/')}}/images/robet.png)@endif ;" value="{{$shop[0]['logo']}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group input-material">
                            <input type="hidden" name="id" class="form-control" id="id" value="{{$shop[0]['id']}}" >
                            <input type="text" name="name" class="form-control" id="name" data-validation="required" value="{{$shop[0]['name']}}" required>
                            <label for="store-field">Restaurant Name<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('name') }}</div>
                        </div>
                        <div class="form-group input-material">
                            <input type="text" name="shortname" class="form-control" id="short-name"
                            value="{{$shop[0]['meta_title']}}" data-validation="required" required>
                            <label for="short-name-field">Short Name of Restaurant<span
                                        style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('shortname') }}</div>
                        </div>
                        <label for="email">Contact Details</label><br>
                        <div class="form-group input-material">
                            <input type="email" name="email" class="form-control" id="email" value="{{$shop[0]['user']['email']}}" data-validation="required" required>
                            <label for="email-field">Email<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('email') }}</div>
                            <div class="text-danger" id="mobileerrormsg1"></div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group input-material mr-2">
                                    <select name="country_code" id="countrycode" class="form-control"
                                     data-validation="required"  required>                                        
                                        </option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->country_code}}" @if($country->country_code==$shop[0]['user']['country_id']) selected
                                        @endif>{{$country->code}}
                                                ({{$country->country_code}})
                                            </option>
                                        @endforeach
                                    </select>
                                    <label for="Country Code" style="top:-14px;">Country Code</label>
                                    <div class="text-danger">{{ $errors->first('countrycode') }}</div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group input-material mr-2">
                                    <input type="text" class="form-control" name="phone" id="phone" minlength="10" value="{{$shop[0]['user']['phone']}}" maxlength="10"
                                           pattern="[0-9]*" data-validation="required" required>
                                    <label for="Last Name">Mobile Number<span style="color:red">*</span></label>
                                    <div class="text-danger">{{ $errors->first('phone') }}</div>
                                    <div class="text-danger" id="mobileerrormsg"></div>
                                </div>
                            </div>
                        </div>



                        <label for="bankaccountnumber">Bank Details</label><br>
                        <div class="form-group input-material">
                            <input type="text" name="bank_acno" class="form-control" id="bankaccountnumber"
                            value="{{$shop[0]['bank_acc_no']}}" data-validation="required" required>
                            <label for="bankaccountnumber">Bank Account Number<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('bank_acno') }}</div>
                        </div>
                        <div class="form-group input-material">
                            <input type="text" name="bank_code" class="form-control" id="bankcode"
                            value="{{$shop[0]['bank_acc_name']}}" data-validation="required" required>
                            <label for="bankcode">Bank Code<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('bank_code') }}</div>
                        </div>
                        <div class="form-group input-material">
                            <input type="text" name="bank_name" class="form-control" id="bankname"
                            value="{{$shop[0]['bank_name']}}" data-validation="required" required>
                            <label for="banknames">Bank Name<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('bank_name') }}</div>
                        </div>
                        <label for="radius">Distance to deliver</label><br>
                        <div class="form-group input-material">
                            <input type="text" name="radius" class="form-control" id="radius" data-validation="required" value="{{$shop[0]['radius']}}"
                                   required>
                            <label for="radius">Radius<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('radius') }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label for="city">Address Details</label><br>
                        <div class="form-group input-material">
                            <select name="city" class="form-control" id="city" data-validation="required" required>
                                <option value=""></option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}" @if($city->id==$shop[0]['user']['city_id']) selected @endif>{{$city->name}}</option>
                                @endforeach
                            </select>
                            <label for="city" style="top:-10px;">Select City<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('city') }}</div>
                        </div>
                        <div class="form-group input-material">
                            <input type="text" name="address" class="form-control" id="search"
                            value="{{$shop[0]['address']}}" data-validation="required" required>
                            <label for="landmark">Address<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('location') }}</div>
                        </div>
                        <div class="form-group input-material">
                            <!-- Google map -->
                            <div id="map" style="width: 100%;height: 400px;"></div>
                            <!-- Display geolocation data -->                            
                            <input type="hidden" id="postal_code" name="postal_code"/>
                            <input type="hidden" id="country" name="country"/>
                            <input type="hidden" id="lat" name="cityLat" value="{{$shop[0]['latitude']}}" required/>
                            <input type="hidden" id="lon" name="cityLng" value="{{$shop[0]['longitude']}}" required/>
                        </div>
                        <div class="form-group input-material">
                            <input type="text" name="landmark" class="form-control" id="landmark"
                            value="{{$shop[0]['user']['landmark']}}" data-validation="required" required>
                            <label for="landmark">Landmark<span style="color:red">*</span></label>
                            <div class="text-danger">{{ $errors->first('landmark') }}</div>
                        </div>



                        <label for="city">Service line</label><br>
                        <div class="d-flex align-items-center justify-contetnt-between">
                            <div class="col-12 col-sm-4">
                                <label>
                                <input type="checkbox" name="pickup" id="chkbtnshop0" value="1" <?php if($shop[0]['pickup'] == "1") echo "checked";?>> <span>Pick Up</span>
                                </label>
                            </div>
                            <div class="col-12 col-sm-4">
                                <label>
                                <input type="checkbox" name="delivery" id="chkbtnshop1" value="1" <?php if($shop[0]['delivery'] == "1") echo "checked";?>><span>Delivery</span>
                                </label>
                            </div>
                            <div class="col-12 col-sm-4">
                                <label>
                                <input type="checkbox" name="food_work" id="chkbtnshop2" value="1" <?php if($shop[0]['foodatwork'] == "1") echo "checked";?>><span>Food@work</span>
                                </label>
                            </div>
                        </div>

                        <label for="city">Accessibility from seller</label><br>
                        <div class="container-fluid p-0">
                            <div class="row">
                                <div class="col-4">
                                    <label class="radio">Yes
                                       <input type="radio" name="seller_access" id="seller_access" value="1" <?php if($shop[0]['seller_products'] == "1") echo "checked";?>>
                                        <span class="checkround"></span>
                                    </label>
                                </div>
                                <div class="col-4">
                                    <label class="radio">No
                                        <input type="radio" name="seller_access" id="seller_access" value="0" <?php if($shop[0]['seller_products'] == "0") echo "checked";?>>
                                        <span class="checkround"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p class="mb-1 text-color-grey f-14">GCT Applicable<span style="color:red">*</span></p>
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-4">
                                        <label class="radio">Yes
                                            <input type="radio" name="gct" id="available_ststus" value="Y" <?php if($shop[0]['gct'] == "Y") echo "checked";?>>
                                            <span class="checkround"></span>
                                        </label>
                                    </div>
                                    <div class="col-4">
                                        <label class="radio">No
                                            <input type="radio" name="gct" id="available_ststus" value="N" <?php if($shop[0]['gct'] == "N") echo "checked";?>>
                                            <span class="checkround"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="mt-5 d-flex align-items-center justify-content-between">
                        <a type="button" class="cancelbtn resetform" href="{{ url('manage_restaurant/')}}" >Cancel</a>
                        <button class="addbtn"><span id="savebtn"></span> Update</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

    <div>
        @include('inc.side_nav')

    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $('#manage-restaurant-form').on('hidden.bs.modal', function () {
            $('#manage-restaurant-form form')[0].reset();
        });
        $(document).ready(function () {
            $('#shopeditbtn').hide();
        });
        $(".resetform").click(function (e) {
            $('#addrestaurantid').modal('hide');
            $('#manage-restaurant-form').find('.text-danger').empty();
        });

        function emailverification(id) {
            $("#savebtn").attr('disabled', 'disabled');
            var userid = $('#userid').val();
            var send = {"user_id": userid, "email": id, _token: '{{csrf_token()}}'};
            send = $(this).serialize() + " & " + $.param(send);
            var url = "{{url('EmailIdvarification/')}}"
            console.log("3333 " + url);
            $.ajax({
                cache: false,
                type: "POST",
                url: url,
                data: send,
                success: function (res) {
                    if (res.success) {
                        $('#mobileerrormsg1').html("");
                        $("#savebtn").removeAttr('disabled');
                    } else {
                        $('#mobileerrormsg1').html("Email Id. already exists. Please Enter another Email Id.");
                        $('#email').focus();
                        return false;
                    }
                }, error: function (res) {

                }
            });
        }
        function mobileverification(id) {
            $("#savebtn").attr('disabled', 'disabled');
            var mob_length = id.length;
            if (mob_length != 10) {
                $('#mobileerrormsg').html("Please Enter 10 Digit Mobile No.");
                $('#phone').focus();
                return false;
            }

            var userid = $('#userid').val();
            var send = {"user_id": userid, "mobile": id, _token: '{{csrf_token()}}'};
            send = $(this).serialize() + " & " + $.param(send);
            var url = "{{url('ajaxmobilevarification')}}"
            console.log("3333 " + url);
            $.ajax({
                cache: false,
                type: "POST",
                url: url,
                data: send,
                success: function (res) {
                    if (res) {
                        $('#mobileerrormsg').html("");
                        $("#savebtn").removeAttr('disabled');
                    } else {
                        //console.log("email failed");
                        $('#mobileerrormsg').html("Phone No. already exists. Please Enter another Phone No.");
                        $('#phone').focus();
                        return false;
                    }
                }, error: function (res) {

                }
            });
        }

        function loginAsShop(email) {
            var url = "{{url('two_way_signin')}}/" + email;
            console.log(url);
            $.ajax({
                cache: false,
                type: "GET",
                url: url,
                success: function (res) {
                    if (res.success) {
                        //console.log("login Success "+JSON.stringify(res));
                        window.open("{{url('two-way-login')}}/" + res.access_token, "_self");
                    } else {
                        console.log("Failed to Login " + JSON.stringify(res));
                    }
                }, error: function (res) {

                }
            });
        }
        function readURL(input) {
            var fileUpload = document.getElementById("logo");
            if (typeof (fileUpload.files) != "undefined") {
                var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
                if (size > 2000) {
                    $('#imagesizevaliderror').html("Image size should be below 2mb.");
                    $('#image-preview').attr('src', '');
                    //return false;
                }
            }

            $('#image-preview').show();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                }
                $('#imagesizevaliderror').html("");
                reader.readAsDataURL(input.files[0]);
            }
        }

        function addremovestyle(param) {
            $('.radio-toolbar div').removeClass('option-selected');
            $('#radio-role' + param).addClass('option-selected');
            $('#radiobtnrole' + param).prop('checked', true);
        }
    </script>
    <script>
        var map = null;
        function initMap() {

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 18.1096, lng: -77.2975},
                zoom: 13,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                navigationControl: false,
                zoomControl: false,
                fullscreenControl: false,
            });
            var input = document.getElementById('search');
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            //console.log("testing " + input);
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                marker.setIcon(({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);

                // Location details
                for (var i = 0; i < place.address_components.length; i++) {
                    if (place.address_components[i].types[0] == 'postal_code') {
                        document.getElementById('postal_code').value = place.address_components[i].long_name;
                    }
                    if (place.address_components[i].types[0] == 'country') {
                        document.getElementById('country').value = place.address_components[i].long_name;
                    }
                }
                document.getElementById('location').value = place.formatted_address;
                document.getElementById('lat').value = place.geometry.location.lat();
                document.getElementById('lon').value = place.geometry.location.lng();
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById('lat').value = this.getPosition().lat();
                document.getElementById('lon').value = this.getPosition().lng();
                geocodePosition(marker.getPosition());
            });
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {

                if (responses && responses.length > 0) {
                    marker.formatted_address = responses[0].formatted_address;
                } else {
                    marker.formatted_address = 'Cannot determine address at this location.';
                }
                infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
                infowindow.open(map, marker);
            });
        }

        function add_remove_style(param) {
            console.log(param);

            if ($('#chkbtnshop' + param).is(":checked")) {
                //$('.chk-toolbar div').removeClass('option-selected');
                $('#chk-role' + param).addClass('option-selected');
                $('#chkbtnshop' + param).prop('checked', true);
            } else {
                // $('.chk-toolbar div').addClass('option-selected');
                $('#chk-role' + param).removeClass('option-selected');
                $('#chkbtnshop' + param).prop('checked', false);
            }
        }

    </script>
    {{--<script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFwT4Fajz4alzQiYm2fwgW0ZmxEa-fjEk&libraries=places&callback=initMap"
            async defer></script>--}}
    <script type="text/javascript" src="{{$googleapi->api}}" async defer></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
        });
    </script>
@endsection
