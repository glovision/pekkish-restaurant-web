<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pekkish</title>
	<link rel="icon" href="{{url('/')}}/images/favicon/icon.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
            href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
            rel="stylesheet">
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./js/slick/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="./js/slick/slick/slick-theme.css">

    <link rel="stylesheet" href="css/landing-page.css">
    <link rel="stylesheet" href="css/custom-dialog-input.css">

    <link rel="stylesheet" href="css/style.css">
    <style>
        .landing-page-banner {
            background: url(./images/landing-page-banner.png) no-repeat ;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>

<body>
<div>
    <div class="max-content-width main-section p-0">

        <div class="landing-page-banner">


            <img src="./images/pekkish-logo.svg" alt="" class="mainpage-logo">
            <button class="signin" data-toggle="modal" data-target="#signinid" onclick="opensignupmodal()">
                Sign Up
            </button>
            <div class="bussiness-login">
                        <div class="bussiness-login-box">
                    <div class="login-heading">Business Login</div>

                    <form method="POST" action="{{url('signin')}}">
                        @csrf
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>
                        @endif
                        @if(session()->has('failed'))
                            <div class="alert alert-danger alert-dismissable">{{session()->get('failed')}}</div>
                        @endif

                        @if(session()->has('failed'))
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        @endif
                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="email" id="email" data-validation="required">
                            <label for="User Name">User Name</label>
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="password" class="form-control" name="password" id="password"
                                   data-validation="required">
                            <label for="password">Password</label>
                        </div>
                        <div class="form-group input-material mr-2 mb-1 mt-1">
                            <select name="user_type" class="form-control" id="user_type" data-validation="required"
                                    required>
                                {{--<option value="">Select User Type</option>--}}
                                <option value="seller">Seller</option>
                                <option value="shop">Store</option>
                                <option value="staff">Staff</option>
                            </select>
                            <label for="user_type">Role</label>
                        </div>


                        <div class="forgot-password"><a onclick="openforgotpwdmodel();">Forgot Password</a></div>
                        <button class="sign-in-btn">SIGN IN</button>
                        <div class="contact-us">
                            Interested in partnering with Pekkish?
                            <a>CONTACT US</a>
                        </div>

                    </form>
                    <div>

                        <div style="float:left;">
                            <a href="{{ url('faq/') }}">
                                <span class=" menu-link f-12 ml-3 mt-4">FAQ</span></a>
                        </div>
                        <div style="float:right;">
                            <a href="{{ url('terms_conditions/') }}">
                                <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                        </div>

                    </div>


                </div>
                <div class="download-the-app">
                    <div class="text-color-white">Download our business app:</div>
                    @foreach($apklinks as $apk)
                        {{--@if($apk->apptype==2)--}}
                        <a href="{{$apk->url}}" target="_blank"><img src="{{$apk->logo}}" alt=""></a>
                        {{--@endif--}}
                    @endforeach
                    {{--<img src="./images/ios.png" alt="">--}}


                </div>
            </div>
        </div>

        <div class="sticky-top w-100 bg-white px-5 py-4 nav-cu">
            <img src="./images/pekkish-logo.svg" alt="" style="width:129px">
        </div>

        <div class="container-fluid clients">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 p-0">
                    <div class="clients-section">


                        <div class="clients-heading">
                            You cook. We deliver.
                        </div>
                        <div class="clients-sub-heading">
                            Thousands of locals may be searching for food in your area. Sed ut perspiciatis unde
                            omnis iste natus error sit voluptatem accusantium doloremque laudantium s iste natus
                            error sit voluptatem accusantium doloremque laudantium accusantium doloremque laudantium
                            s iste natus error sit voluptatem accusantium doloremque laudantium
                        </div>

                        <div class="f-18 f-medium">
                            BRANDS THAT WE WORK WITH
                        </div>
                        <div class="clients-images">
                            @foreach($brands as $brand)
                                <div class="mx-2">
                                    <img src="{{$brand->logo}}" alt="">
                                </div>
                            @endforeach

                            {{--    <div class="mx-2">
                                        <img src="./images/mc.png" alt="">
                                    </div>

                                    <div class="mx-2">
                                        <img src="./images/pizza.png" alt="">
                                    </div>

                                    <div class="mx-2">
                                        <img src="./images/subway.png" alt="">
                                    </div>--}}


                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 p-0">
                    <img class="banner-img" src="./images/bannerimg.png" alt="">
                </div>
            </div>
        </div>

        <div class="user-stories">
            <div class="user-stories-heading">
                Success Stories
            </div>
            <div class="user-stories-sub-title">
                Show your restaurant owners like you are making waves with Pekkish.
            </div>

            <div class=" container-fluid stories">
                <div class="row">
                    @foreach($success_stories as $story)
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class="stories-list">

                                <img class="story-img" src="{{$story->image}}" alt="">
                                <div class="story-details">
                                    <div class=success-stories-heading>{{$story->name}}</div>
                                    <div class="success-stories-description">{{$story->description}}</div>
                                    <a class="read-more" href="{{url('storyDetails', $story->id)}}">READ MORE <img
                                                src="./images/rightangle.svg" alt=""> </a>
                                </div>
                            </div>
                        </div>                        
                        @if($loop->index == 2)
                            @break
                        @endif
                    @endforeach

                    <div class="col-12 col-sm-12 col-md-6">
                        <div class="see-all-stories">
                            <div class="text-center">
                                <div class="see-all-stories-list">See all stories</div>
                                <a class="read-more f-14" href="{{url('storyDetails', 'all')}}">READ MORE <img
                                            src="./images/rightangle.svg" alt=""> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="cta-img" class="carousel slide cta" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#cta-img" data-slide-to="0" class="active"></li>
                <li data-target="#cta-img" data-slide-to="1"></li>
                <li data-target="#cta-img" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
            <span style="display:none;">{{$i=0}}</span>           
                @foreach($itworks as $itwork)
                    @if($loop->index)
                        <div class="carousel-item  @if($i++==0) active @endif">
                            @else
                                <div class="carousel-item">
                                    @endif
                                    <div class="container-fluid p-0">
                                        <div class="row">
                                            <div class="cta-main-section mt-3">
                                                <img class="banner-img" src="{{$itwork->image}}">
                                                <div class="d-flex">
                                                    <div class="cta-section">
                                                        <div class="cta-heading">
                                                            {{$itwork->name}}
                                                        </div>
                                                        <div class="cta-description">
                                                            {{$itwork->description}}
                                                        </div>
                                                        <!--        <div data-toggle="modal" data-target="#howdoesitwork_model"  onclick="openhowdoesmodel();">-->
                                                        <div>
                                                            <a class="text-color-green f-14 f-medium">HOW DOES IT WORK?
                                                                <img src="./images/rightangle.svg" alt=""></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>

            </div>
            <div class="d-flex justify-content-center align-items-center callback">
                <span>Still have questions?</span> <a href="#">Request a callback</a>
            </div>
        </div>
    </div>
</div>


<footer>
    <div class="max-content-width footer-section">
        <img src="./images/pekkish-logo.svg" alt="">
        <div class="copyright">
            Copyright © 2021-2021 pekkish.com
        </div>
    </div>
</footer>


<div class="modal fade" id="signinid" tabindex="-1" role="dialog" aria-labelledby="signinidTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="signin">
        <div class="modal-content">
            <div class="modal-body p-4 registration-form ">
                <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" id="signupclear"
                     aria-label="Close">
                    <img src="./images/dialogclose.svg" alt="">
                </div>


                <div id="getstarted">
                    <p class="f-24 f-medium">Get Started</p>
                    <div class="d-flex justify-content-between bussiness-login-box">
                        <div class="form-group input-material mr-2 mb-1 mt-1">
                            <select name="country_code" id="countrycode" class="form-control"
                                    data-validation="required">
                                {{--<option value=""> Select Country Code</option>--}}
                                @foreach($countries as $country)
                                    <option value="{{$country->country_code}}">{{$country->code}}
                                        ({{$country->country_code}})
                                    </option>
                                @endforeach
                            </select>
                            @if ($errors->has('country_code'))
                                <div class="text-danger">{{ $errors->first('country_code') }}</div>
                            @endif
                            <div class="text-danger" id="country_codevalidate"></div>
                        </div>


                        <div class="form-group input-material mr-2 mb-1 mt-1">
                            <input type="text" class="form-control" name="phone" id="mobile" placeholder="Mobile Number"
                                   minlength="10" maxlength="10" pattern="[0-9]*" data-validation="required number">
                            @if ($errors->has('phone'))
                                <div class="text-danger">{{ $errors->first('phone') }}</div>
                            @endif
                            <div class="text-danger" id="mobileerrormsg"></div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-end">
                        <button class="addbtn mt-5 mb-3" id="verifybutton" onclick="mobile_form()">SEND OTP</button>
                    </div>
                        </div>
                            <div id="otpverificationdiv1">
                                <h6 class="mb-3 acount-text" style="text-align: center;">OTP Verification</h6>
                            <!--<div class="d-flex justify-content-center">
                                <p class="otp-text text-center" >OTP has been sent to<br> your mobile number<br>
                                <span id="phonenumberdisplay"></span></p>
                                </div>-->

                                <div class="modal-body p-4">
                                <div class="alert alert-danger alert-dismissable" id="successemobilemessage"></div>
                                    
                                <div class="form-group input-material mr-2 d-flex justify-content-center">
                                
                                <input type="text" class="form-control" name="otp" id="validotpverify" placeholder="xxxxxx" maxlength="6" style="letter-spacing:1.2em;margin-right:-1.2em;text-align:right;">
                                <label for="otp">OTP</label>
                                </div> <div class="text-danger" id="validotpverifymessage"></div>

                        <input type="hidden" class="form-input input" name="phone" id="phoneid">
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" onclick="resendotp()">RESEND OTP</button>
                            <button type="submit" class="addbtn w-100 mx-3 f-medium" onclick="SignupOTPvalidate()">
                                SUBMIT
                            </button>
                        </div>
                    </div>
                </div>
                <div id="signupformdiv">

                    <form id="signupform" method="POST" action="{{url('signup')}}"
                          onsubmit="return emailverification(this.value)">
                        @csrf
                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="store_name" id="store_name"
                                   placeholder="Store Name" data-validation="required">
                            <label for="Store Name">Store Name</label>
                            @if ($errors->has('store_name'))
                                <div class="text-danger">{{ $errors->first('store_name') }}</div>
                            @endif
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="store_address" id="store_address"
                                   placeholder="Store Address" data-validation="required">
                            <label for="Store Address">Store Address</label>
                            <div class="text-danger">{{ $errors->first('store_address') }}</div>
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="flowSuite" id="optional"
                                   placeholder="Flow / Suite (Optional)">
                            <label for="Flow / Suite (Optional)">Flow / Suite (Optional)</label>
                        </div>

                        <div class="d-flex justify-content-between">

                            <div class="form-group input-material mr-2 mb-1 mt-1">
                                <input type="text" class="form-control" name="firstname" id="firstname"
                                       placeholder="First Name" data-validation="required">
                                <label for="First Name">First Name</label>
                                <div class="text-danger">{{ $errors->first('firstname') }}</div>
                            </div>

                            <div class="form-group input-material mr-2 mb-1 mt-1">
                                <input type="text" class="form-control" name="lastname" id="lastname"
                                       placeholder="Last Name" data-validation="required">
                                <label for="Last Name">Last Name</label>
                                <div class="text-danger">{{ $errors->first('lastname') }}</div>
                            </div>

                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="Email" class="form-control" name="email" id="Email" placeholder="Email"
                                   data-validation="required email" onblur="emailverification(this.value)">
                            <label for="Email">Email</label>
                            @if ($errors->has('email'))
                                <div class="text-danger">{{ $errors->first('email') }}</div>
                            @endif
                            <div class="text-danger" id="emailerrormsg"></div>
                        </div>

                        <div class="d-flex justify-content-between">
                            {{--<div class="form-group input-material mr-2 mb-1 mt-1">--}}
                            {{--<select name="country_code" id="countrycode" class="form-control" data-validation="required">--}}
                            {{--<option value=""> Select Country </option>--}}
                            {{--@foreach($countries as $country)--}}
                            {{--<option value="{{$country->country_code}}">{{$country->code}} ({{$country->country_code}})</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<label for="Last Name">Country Code</label>--}}
                            {{--<div class="text-danger">{{ $errors->first('countrycode') }}</div>--}}
                            {{--</div>--}}
                            {{--<div class="text-danger">{{ $errors->first('countrycode') }}</div>--}}
                            <div class="form-group input-material mr-2 mb-1 mt-1">
                                <input type="hidden" class="form-control" name="mobile_id" id="mobileid"
                                       placeholder="Mobile Number" minlength="10" maxlength="10" pattern="[0-9]*"
                                       data-validation="required number">
                                {{--<label for="Last Name">Mobile Number</label>--}}
                                {{--<div class="text-danger">{{ $errors->first('phone') }}</div>--}}
                                {{--<div class="text-danger" id="mobileerrormsg"></div>--}}
                            </div>
                        </div>

                        <div class="d-flex justify-content-end">
                            <input type="submit" class="submit mt-5 mb-3" id="signupbtn" value="SUBMIT">
                        </div>


                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="forgotpassword_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" id="signupclear"
                     aria-label="Close">
                    <img src="./images/dialogclose.svg" alt="">
                </div>
                <div id="forgotpwddiv">
                    <h6 class="mb-3 acount-text" style="text-align: center;">Forgot Password</h6>


                    <!--   <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                           <img src="./images/dialogclose.svg" alt="">
                       </div>-->

                    <div class="form-group input-material mr-2">
                        <input type="email" id="forgotpwdemail" name="email" class="form-control"
                               data-validation="required">
                        <label for="name">Email Address</label>
                        <div class="text-danger" id="forgotpwdvaliderror"></div>
                    </div>
                    <div class="form-group input-material mr-2 mb-1 mt-1">
                        <select name="user_type" class="form-control" id="forgotuser_type" data-validation="required"
                                required>
                            {{--<option value="">Select User Type</option>--}}
                            <option value="seller">Seller</option>
                            <option value="shop">Shop</option>
                            <option value="staff">Staff</option>
                        </select>
                        <label for="user_type">Role</label>
                    </div>


                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO
                            </button>
                            <button type="submit" class="addbtn w-100 mx-3 f-medium" onclick="forgot_password()">NEXT
                            </button>
                        </div>
                    </div>
                </div>


                <div id="otpverificationdiv">
                    <h6 class="mb-3 acount-text" style="text-align: center;">OTP Verification</h6>
                    <div class="d-flex justify-content-center">
                        <p class="otp-text text-center">OTP has been sent to<br> your mobile number<br>
                            <span id="phonenumberforgotpwd"></span></p>
                    </div>

                    <div class="modal-body p-4">
                        <div class="alert alert-danger alert-dismissable" id="forgotsuccessmeg"></div>

                        <div class="form-group input-material mr-2 d-flex justify-content-center">
                            <input type="text" class="form-control" name="otp" id="validotp" placeholder="xxxxxx"
                                   maxlength="6" style="letter-spacing:1.2em;margin-right:-1.2em;text-align:right;">
                            <label for="otp">OTP</label><br>
                        </div>
                        <div class="text-danger" id="verifyotpvaliderror"></div>

                        <input type="hidden" class="form-input input" name="email" id="otpgetemail">
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" onclick="resendotpforgot()">RESEND OTP
                            </button>
                            <button type="submit" class="addbtn w-100 mx-3 f-medium" onclick="OTPvalidate()">SUBMIT
                            </button>
                        </div>
                    </div>
                </div>
                <div id="reset_password">
                    <h6 class="mb-3 acount-text" style="text-align: center;">Reset Password</h6>
                    <form action="{{url('resetpassword')}}" method="POST">
                        @csrf
                        <input type="hidden" id="email_hide" name="id" class="form-control" required>
                        <div class="form-group input-material mr-2">
                            <input type="text" id="newpassword" name="password" class="form-control"
                                   onblur="checknewpassword(this.value);" required>
                            <label for="name">New Password</label>
                            <div class="text-danger">{{ $errors->first('newpassword') }}
                                <div class="text-danger" id="newpasswordvaliderror"></div>
                            </div>
                        </div>
                        <div class="form-group input-material mr-2">
                            <input type="text" id="confirmpassword" name="confirmpassword" class="form-control"
                                   onblur="checkconfirmpassword(this.value)" required>
                            <label for="name">Confirm Password</label>
                            <div class="text-danger">{{ $errors->first('confirmpassword') }}</div>
                            <div class="text-danger" id="confirmpasswordvaliderror"></div>
                        </div>
                        <div class="mb-2">
                            <div class=" mb-3 d-flex align-items-center justify-content-around">

                                {{--<button class="cancelbtn w-100 mx-3 f-medium" onclick="resendotp()">RESEND OTP</button>--}}
                                <input type="submit" class="addbtn w-100 mx-3 f-medium" id="resetbutton" value="SUBMIT">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="howdoesitwork_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-4">
                <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" id="signupclear"
                     aria-label="Close">
                    <img src="./images/dialogclose.svg" alt="">
                </div>
                <form method="POST" action="{{url('howdoesitwork')}}">
                    @csrf
                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="yourname" id="yourname"
                               placeholder="" data-validation="required">
                        <label for="YourName">YourName</label>
                        @if ($errors->has('yourname'))
                            <div class="text-danger">{{ $errors->first('yourname') }}</div>
                        @endif
                    </div>

                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="storename" id="storename"
                               placeholder="" data-validation="required">
                        <label for="Store Name">Store Name</label>
                        @if ($errors->has('storename'))
                            <div class="text-danger">{{ $errors->first('storename') }}</div>
                        @endif
                    </div>

                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="storeaddress" id="storeaddress"
                               placeholder="" data-validation="required">
                        <label for="Store Address">Store Address</label>
                        @if ($errors->has('storeaddress'))
                            <div class="text-danger">{{ $errors->first('storeaddress') }}</div>
                        @endif
                    </div>


                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="youremail" id="youremail"
                               placeholder="" data-validation="required">
                        <label for="Your Email">Your Email</label>
                        @if ($errors->has('youremail'))
                            <div class="text-danger">{{ $errors->first('youremail') }}</div>
                        @endif
                    </div>


                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="yourmobile" id="yourmobile"
                               placeholder="" data-validation="required">
                        <label for="Your Mobile">Your Mobile</label>
                        @if ($errors->has('yourmobile'))
                            <div class="text-danger">{{ $errors->first('yourmobile') }}</div>
                        @endif
                    </div>

                    <div class=" mb-3 d-flex align-items-center justify-content-around">
                        {{--<button class="cancelbtn w-100 mx-3 f-medium">SUBMIT</button>--}}
                        <input type="submit" class="addbtn w-100 mx-3 f-medium" id="howdoes" value="SUBMIT">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</body>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/JqueryFormValidation.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/slick/slick/slick.min.js"></script>

<script type="text/javascript">
    $.validate();
    $(document).ready(function () {
        $('.clients-images').slick({
            infinite: true,
            speed: 300,
            centerMode: false,
            variableWidth: true,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,

        });
    });
    function openforgotpwdmodel() {
        $("#Email Address").val("");
        //jQuery('#forgotpassword_model').modal('show', {backdrop: 'static'});
        $('#forgotpassword_model').modal({backdrop: 'static', keyboard: false});
        $('#forgotpwddiv').show();
        $('#otpverificationdiv').hide();
        $('#reset_password').hide();
    }
    function opensignupmodal() {
        $("#phone").attr("value", '');
        $("#phone").val("");
        $("#country_code").attr("value", '');
        $("#country_code").val("");
        $("#validotpverify").attr("value", '');
        $("#validotpverify").val("");
        $('#signinid').modal({backdrop: 'static', keyboard: false});
        $('#getstarted').show();
        $('#signupformdiv').hide();
        $('#otpverificationdiv1').hide();
    }
    function resendotp() {

        var phone = $('#phoneid').val();
        console.log(phone);
        if (phone == "") {
            $('#validotpverifymessage').html("Invalid Mobile Number..!!");
            return false;
        }
        var send = {"id": phone, _token: '{{csrf_token()}}'};
        console.log(send);
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('resendotp')}}"
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    $('#successemobilemessage').html(res.data.message);
                    $('#phonenumberdisplay').html(res.data.phone_number);
                    $('#otpverificationdiv1').show();

                } else {
                    $('#otpverificationdiv1').show();
                    $('#phonenumberdisplay').html('');
                    $('#validotpverifymessage').html('Something went wrong please try again!!');


                }
            }, error: function (res) {

            }
        });
    }
    function resendotpforgot() {
        var email = $('#email_hide').val(res.id);
        if (email == "") {
            $('#verifyotpvaliderror').html("Please Enter valid Email Address..");
            return false;
        }
        var send = {"id": email, _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('resendotp')}}"
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    $('#forgotsuccessmeg').html(res.data.message);
                    $('#phonenumberforgotpwd').html(res.data.phone_number);
                    $('#otpverificationdiv').show();


                } else {
                    $('#phonenumberforgotpwd').html('');
                    $('#otpverificationdiv').show();
                    $('#verifyotpvaliderror').html('Something went wrong please try again!!');


                }
            }, error: function (res) {

            }
        });
    }
    function forgot_password() {
        var email = $('#forgotpwdemail').val();
        var user_type = $('#forgotuser_type').val();
        if (email == "") {
            $('#forgotpwdvaliderror').html("Please Enter valid Email Address..");
            return false;
        }
        var send = {"email": email, 'user_type': user_type, _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('forgot_password')}}"
        console.log("3333 " + url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    //console.log("email success"+JSON.stringify(res));
                    var phone = res.data.phone_number;
                    var hidephone = phone[0] + "*".repeat(phone.length - 4) + phone.slice(-3);
                    //console.log(hidephone);
                    $('#phoneid').val(res.data.id);
                    $('#phonenumberforgotpwd').html(hidephone);
                    $('#forgotsuccessmeg').html(res.message);
                    $('#forgotpwddiv').hide();
                    $('#otpverificationdiv').show();
                } else {
                    $('#forgotpwdvaliderror').html(res.message);
                    $('#forgotpwddiv').show();
                    $('#otpverificationdiv').hide();
                }
            }, error: function (res) {

            }
        });
    }
    function mobile_form() {

        var mobile = $('#mobile').val();
        var country_code = $('#countrycode').val();
        var mob_length = mobile.length;
        if (mob_length != 10) {
            $('#mobileerrormsg').html("Please Enter 10 Digit Mobile No.");
            return false;
        }
//        if(country_code==""){
//            $('#country_codevalidate').html("Please Enter Valid Country Code..!!");
//            return false;
//        }
        var send = {"phone": mobile, "country_code": country_code, _token: '{{csrf_token()}}'};
        //send=$(this).serialize()+" & "+$.param(send);
        var url = "{{url('sendsignupotp')}}"
        console.log(send);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    $('#mobileerrormsg').html("");
                    $('#country_codevalidate').html("");
                    console.log(res);
                    $('#phoneid').val(res.data.id);
                    $('#successemobilemessage').html(res.data.message);
                    $('#phonenumberdisplay').html(res.data.phone_number);
                    $('#otpverificationdiv1').show();
                    $('#getstarted').hide();
                } else {
                    console.log(res);
                    $('#phoneid').val('');
                    $('#phonenumberdisplay').html('');
                    $('#otpverificationdiv1').hide();
                    $('#mobileerrormsg').html(res.message);
                    $('#getstarted').show();
                }
//
            }, error: function (res) {
//                $('#otpverificationdiv1').hide();

            }
        });
    }
    function OTPvalidate() {
        var otp = $('#validotp').val();
        if (otp == "") {
            $('#verifyotpvaliderror').html("Please Enter Valid OTP..!!");
            return false;
        }
        var email = $('#forgotpwdemail').val();
        if (email == "") {
            $('#forgotpwdvaliderror').html("Something went wrong please try again!!");
            return false;
        }
        var send = {"otp": otp, "email": email, _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('otp_validate')}}"
        console.log("3333 " + url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {

                    console.log(res);
                    //jQuery('#reset_password').modal('false', {backdrop: 'static'});
                    $('#email_hide').val(res.id);
                    $('#forgotpwdemail').val("");
                    $('#validotp').val("");
                    $('#phonenumberforgotpwd').html("");
                    $('#forgotsuccessmeg').html(res.message);
                    $('#verifyotpvaliderror').html("OTP Verified ");
                    $('#forgotpwddiv').hide();
                    $('#otpverificationdiv').hide();
                    $('#reset_password').show();
                } else {
                    $('#forgotpwddiv').hide();
                    $('#otpverificationdiv').show();
                    $('#verifyotpvaliderror').html("Please Enter Valid OTP..!!");
                }
            }, error: function (res) {

            }
        });
    }
    function SignupOTPvalidate() {
        var otp = $('#validotpverify').val();
        var phone = $('#phoneid').val();
        console.log(phone);
        console.log(otp);
        if (otp == "") {
            $('#validotpverifymessage').html("Please Enter Valid OTP..!!");
            return false;
        }

        if (phone == "") {
            $('#validotpverifymessage').html("Something went wrong please try again!!");
            return false;
        }
        var send = {"otp": otp, "id": phone, _token: '{{csrf_token()}}'};
        var url = "{{url('verifysignupotp')}}"
        console.log("3333 " + url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {

                    console.log(res);
                    $('#mobileid').val(res.data.id);
                    //jQuery('#signinid').modal('false', {backdrop: 'static'});
                    $('#otpverificationdiv1').hide();
                    $('#getstarted').hide();
                    $('#signupformdiv').show();

                } else {
                    $('#otpverificationdiv1').show();
                    $('#validotpverifymessage').html("Please Enter Valid OTP..!!");
                }
            }, error: function (res) {

            }
        });
    }

    function emailverification(id) {
        $("#signupbtn").attr('disabled', 'disabled');
        var userid = "null";
        var send = {"user_id": userid, "email": id, "user_type": 'seller', _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('ajaxEmailvarification')}}"
        console.log("3333 " + url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res) {
                    //console.log("email success");
                    $('#emailerrormsg').html("");
                    $("#signupbtn").removeAttr('disabled');
                } else {
                    //console.log("email failed");
                    $('#emailerrormsg').html("EmailId alredy exists. Please Enter another EmailId");
                    // $('#Email').focus();
                    return false;
                }
            }, error: function (res) {

            }
        });
    }
    function mobileverification(id) {
        $("#verifybutton").attr('disabled', 'disabled');
        var mob_length = id.length;
        if (mob_length != 10) {
            $('#mobileerrormsg').html("Please Enter 10 Digit Mobile No.");
            return false;
        }
        var userid = "null";
        var send = {"user_id": userid, "mobile": id, "user_type": 'seller', _token: '{{csrf_token()}}'};
        send = $(this).serialize() + " & " + $.param(send);
        var url = "{{url('ajaxmobilevarification')}}"
        //console.log("3333 "+url);
        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: send,
            success: function (res) {
                if (res.success) {
                    $('#mobileerrormsg').html("");
                    $("#verifybutton").removeAttr('disabled');
                } else {
                    console.log("email failed");
                    $('#mobileerrormsg').html("Phone No. already exists. Please Enter another Phone No.");
                    // $('#phone').focus();
                    return false;
                }
            }, error: function (res) {

            }
        });
    }


    function checknewpassword(data) {
        console.log(data);
        if (data.length < 8) {
            $('#newpasswordvaliderror').html("Password must be morethan 8 characters.");
            return false;
        }
        if (data.match("^[a-zA-Z0-9]*$")) {
            $('#newpasswordvaliderror').html("Password must be aplanumeric characters.");
            return false;
        }
        $('#newpasswordvaliderror').html("");
    }
    function checkconfirmpassword(data) {
        console.log(data);
        var newpwd = $("#newpassword").val();
        if (data != newpwd) {
            $('#confirmpasswordvaliderror').html("Confirm Password Mismatch.");
            $('#resetbutton').attr('disabled', 'disabled');
            return false;
        }
        $('#confirmpasswordvaliderror').html("");
        $("#resetbutton").removeAttr('disabled');
    }
</script>
<script>
    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 800) {
            $('.nav-cu').fadeIn();
        } else {
            $('.nav-cu').fadeOut();
        }
    });

    $.fn.materializeInputs = function (selectors) {

        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea,select";

        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }

        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });

        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };

    /**
     * Material Inputs
     */
    $('body').materializeInputs();


</script>

@if($errors->count() > 0)
    <script>
        $(document).ready(function () {
            $('#signinid').modal('show');

        });
    </script>
@endif
<script>
    $(document).ready(function () {
        $("#signupclear").on('click', function () {
            //$('#signupform').$.validate().resetForm();
            //$('#signupform').trigger("reset");
            var $t = $(this),
                target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

            $(target)
                .find("input,textarea,select")
                .val('')
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end()
                .find("input[type=submit]")
                .val('SUBMIT')
                .end();
            $('.form-error').css('display', 'none');
            $('#emailerrormsg').html("");
            $('#mobileerrormsg').html("");

        });
    });

    function openhowdoesmodel() {
        $("#yourname").attr("value", '');
        $("#yourname").val("");
        $("#storename").attr("value", '');
        $("#storename").val("");
        $("#storeaddress").attr("value", '');
        $("#storeaddress").val("");
        $("#youremail").attr("value", '');
        $("#youremail").val("");
        $("#yourmobile").attr("value", '');
        $("#yourmobile").val("");
        $('#howdoesitwork_model').modal({backdrop: 'static', keyboard: false});

    }
</script>


</html>
