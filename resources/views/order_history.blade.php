@extends('layouts.app')
@section('content')

        <div class="main-section-padding">

            <div class="container-fluid p-0 sales">

                <div class="row">
                    <div class="col-12 mt-4 mb-2">
                        <div class="d-flex align-items-center justify-content-between pb-2">
                            <h4 class="heading">ORDERS HISTORY</h4>
                            <!-- <form>
                              <select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>

                              </select>
                            </form> -->
                        </div>
                    </div>
                </div>
                <div class="row itemized-report">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Selling Item</div>
                            <div class="itemized-report-percent-up">
                <span>
                        <img src="./images/uparrow-green.svg" alt="" id="topsellingimg"></span>
                        <span id="topsellingitempercent"></span>
                                <span id="topsellingpercent">%</span> 
                        </div>
                            <div class="itemized-report-value" id="topsellingitemthisweek"></div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black" id="topsellingitemlastweek"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Total Orders</div>
                            <div class="itemized-report-percent-down">
                         <span>
                            <img src="./images/down-arrow-red.svg" alt="" id="orderimg"></span>
                            <span id="orderspercent"></span>
                                <span id="orderpercent">%</span> </div>
                            <div class="itemized-report-value" id="ordersval"></div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black" id="lastwekordersval"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Item Earning</div>
                            <div class="itemized-report-percent-up">
                            <span ><img src="./images/uparrow-green.svg" alt="" id="topitemearnimg"></span>
                            <span id="topitemearnpercent"></span>
                            <span id="topitemearnpercentimg">%</span> </div>
                            <div class="itemized-report-value text-uppercase" id="topitemearnThisweek"></div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black text-uppercase" id="topitemearnLastweek"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                        <div>
                            <div class="itemized-report-title">Top Section</div>
                            <div class="itemized-report-percent-up">
                            <span><img src="./images/uparrow-green.svg" alt="" id="topsectionimg"></span>
                            <span id="topsectionpercent"></span>
                            <span id="topsectionpercentimg">%</span> </div>
                            <div class="itemized-report-value d-flex" id="thisweektopsection"></div>
                            <div class="itemized-report-compare">
                                <span>Compared to previous 7 days</span> -
                                <span class="font-weight-bold text-color-black d-flex" id="lastweektopsection"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="table-responsive table-container">
                    <table class="table"  id="order_history">
                        <thead class="thead-light">
                        <tr>
                            <th>Order Id</th>
                            <th>Date</th>
                            <th>Order Type</th>
                            <th>Price</th>
                            {{--<th>Payment Mode</th>--}}
                            <th>Payment Status</th>
                            <th>Order Status</th>
                            <th>Avg.time Accept</th>
						    <th>Avg.time Prepare</th>
						    <th>Avg.time Deliver</th>
                            {{--<th class="table-controls-button" id="table-button">                            --}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
				            {{--<ul class="controls-dropdown-box" id="controls-dropdown-menu" style="display: none;"></ul>--}}
                            {{--</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                        <tr>
                            <td>{{$order->order_id}}</td>
                            <td>{{$order->ordered_time}}</td>
                            <td>{{ucfirst($order->order_type)}}</td>
                            <td class="d-flex"><span>$</span> <span>{{number_format($order->total_amount)}}</span></td>
                            {{--<td></td>--}}
                            <td class="text-color-green">{{ucfirst($order->payment_status??'')}}</td>
                            <td class="text-color-green ">{{ucfirst($order->order_status??'')}}</td>
                            <td>
                            @if($order->r_accepted_at != null)
							    {{gmdate('H:i:s',(new Carbon\Carbon($order->r_accepted_at))->diffInSeconds(new Carbon\Carbon($order->created_at)))}} 
                            @endif
                            </td>
							<td>
                            @if($order->r_prepared_at != null)
							{{gmdate('H:i:s',(new Carbon\Carbon($order->r_prepared_at))->diffInSeconds(new Carbon\Carbon($order->r_accepted_at)))}} 
                            @endif
                            </td>
							<td>
                            @if($order->d_delivered_at != null)
							{{gmdate('H:i:s',(new Carbon\Carbon($order->d_delivered_at))->diffInSeconds(new Carbon\Carbon($order->r_prepared_at)))}}
                            @endif
							</td>
                            {{--<td></td>--}}
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                @if(session()->has('message'))
                    <div class="alert alert-danger alert-dismissable">{{ session()->get('message') }}</div>
                @endif
                <div class="row float-right">

                    <div class="col-12">
                        <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                            <div class="d-flex ">
                            </div>

                            <div id="exportBtn1" onclick="getorderDetails();">
                                <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                                <img src="./images/download.svg" alt="">
                                <div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        </div>
@endsection
@section('model')
    <div>
    @include('inc.side_nav')

        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">



                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>
@endsection
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script> 
   function TotalOrdersCounts(days)
    {
        var url = "{{url('AjaxTotalOrdersCounts')}}/" + days;
            //console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Total orders "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        $('.saledays').html(days);
                        $('#orderspercent').html(res.data.PERCENT_ORDER);
                        $('#orderspercent').css("color", res.data.ORDERINCRDECR);
                        $('#ordersval').html((res.data.THIS_WEEK));
                        $('#lastwekordersval').html((res.data.LAST_WEEK));
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#orderimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#orderimg').attr('src', './images/uparrow-green.svg');
                        }

                    }
                },error: function(res) {

                }
            });

    }
    function OrdersHistoryCounts(days)
    {
        var url = "{{url('AjaxOrdersHistoryCounts')}}/" + days;
            //console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Counts "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        $('.saledays').html(days);
                        $('#topsellingitempercent').html(res.data.PERCENT_ORDER);                        
                        $('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                        $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsellingimg').attr('src', './images/uparrow-green.svg');
                        }

                        $('#topitemearnpercent').html(res.data.PERCENT_TOP_ITEM_EARN);                        
                        $('#topitemearnpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topitemearnpercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#topitemearnThisweek').html((res.data.THIS_WEEK_TOP_ITEM_EARN).toFixed(2));
                        $('#topitemearnLastweek').html((res.data.LAST_WEEK_TOP_ITEM_EARN).toFixed(2));
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topitemearnimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topitemearnimg').attr('src', './images/uparrow-green.svg');
                        }

                        $('#topsectionpercent').html(res.data.PERCENT_ORDER);                       
                        $('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                        $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsectionimg').attr('src', './images/uparrow-green.svg');
                        }

                    }
                },error: function(res) {

                }
            });

    }
    OrdersHistoryCounts(7);
    TotalOrdersCounts(7);
</script>
            <script>
                $(document).ready(function () {
                    $("#exportBtn1").click(function () {
                        var date = new Date();
                        TableToExcel.convert(document.getElementById("order_history"), {

                            name: "Order history " + date + ".xlsx",
                            sheet: {
                                name: "Order History"
                            }
                        });
                    });
                });

    function getorderDetails()
    {
        var url = "{{url('/orderhistory')}}";
        console.log("URL "+url);
        $.ajax({
            cache:false,
            type: "GET",
            url: url,
            success: function (res) {
               // console.log("Order Hist  "+JSON.stringify(res));
                if(res.success){
					orderhistExport(res.data);
                }
            },error: function(res) {

            }
        });
    }


function orderhistExport(data) {
        //var currdate = new Date();
        console.log("Order Hist EXL  "+JSON.stringify(data));
		var currdate = new Date().toISOString().slice(0, 10);
        
        var Heading = ['', 'Total Orders', 'Order Deliverd', 'Order Cancelled', 'Order Inprogress'];
        var results = data;
        DayWiseOrdersExport = function() {
            var CsvString = "";
            CsvString += "Name of the Report:, Order Details";
			CsvString += "\r\n";
            CsvString += "Report Date:, "+currdate;
			CsvString += "\r\n";
            CsvString += "Report Period:, "+currdate;
            CsvString += "\r\n";
            results.forEach(function(RowItem, RowIndex) {
                CsvString += JSON.stringify(Heading[RowIndex]) + ',';
                RowItem.forEach(function(ColItem, ColIndex) {
                    CsvString += ColItem + ',';
                });
                CsvString += "\r\n";
            });
            CsvString = "data:application/csv," + encodeURIComponent(CsvString);
            var x = document.createElement("A");
            x.setAttribute("href", CsvString);
            x.setAttribute("download", "Day Wise Order Details " + currdate + ".csv");
            document.body.appendChild(x);
            x.click();
        }
        

    }
</script>