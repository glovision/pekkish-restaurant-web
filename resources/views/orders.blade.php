@extends('layouts.app')
@section('content')


    <div class="main-section-padding">

        <div class="container-fluid p-0 orders">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4 border-right-table">
                    <div>
                        <div class="align-items-center justify-content-between">
                            <div class="text-uppercase  new-order-title f-16 f-medium">
                                <span>new orders</span>
                                <span id="count_orders">({{count($orders->orders)}})</span>
                                <img class="notify" src="./images/alert-bell.svg" alt="">
                            </div>
                            @if(session()->has('message'))
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('message') }}</div>
                            @endif
                             <div id="new-order">
                             </div>
                        </div>

                    </div>
                </div>
                <section class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 border-left-table">
                    <section class="on-going-orders">
                        <header class="text-uppercase  new-order-title f-16 f-medium d-flex justify-content-between">
                            <div>
                                <span>ONGOING ORDERS</span>
                                {{-- <span>({{count($allorders->all_orders)}})</span>--}}
                            </div>
                            <div>
                                <div class="menu-list-items d-flex justify-content-end align-items-center">
                                    <!--  <button class="add-section mx-4">
                                        Create New Order
                                      </button>-->
                                    <form id="filter_orders"  action="" method="GET">
                                        <select name="key" id="key" class="select-drop-down" onchange="selectorder(this.value)">
                                            <option value="">All Orders</option>
                                            <option value="3" @if(request()->get('key') == '3') selected @endif >Preparing</option>
                                            <option value="5" @if(request()->get('key') == '5') selected @endif>Ready</option>
                                            <option value="6" @if(request()->get('key') == '6') selected @endif>Picked Up</option>
                                        </select>
                                    </form>

                                </div>
                            </div>
                        </header>
                    </section>
                    <div class="container-fluid">
                        @if(session()->has('success'))
                            <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                        @endif
                        @if(session()->has('failed'))
                            <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
                        @endif
                        <div class="row res-cards">

                            @foreach($allorders->all_orders as $allorder)
                                @if($allorder->status_id != 1)
                                    @foreach($allorder->orders as $order)
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                            <div class="orders-card text-color-grey">
                                                <div class="d-flex justify-content-between">
                                                    <div>
                                                        <div class="d-flex align-items-end label-status">
                                                            <img src="{{$order->order_tag}}" alt="" style="height: 30px;cursor: pointer;"  onclick="getorderdetails({{$order->id}});">
                                                        </div>
                                                        <div class="f-14 f-medium mt-2 text-color-black">{{$order->order_name}}</div>
                                                        <div class=" f-12">
                                                            <img src="{{$order->ordered_time_icon}}" alt="" class="w-18">
                                                            <span>{{$order->ordered_time}}</span><span>|</span> <span></span><span>{{$order->total_amount}}</span>

                                                        </div>

                                                    </div>
                                                    <div class="text-right">
                                                        <div>
                                                            <span class="text-color-red f-12">{{$order->quick_status->first_line->text}}</span>
                                                            <img src="{{$order->quick_status->first_line->image}}" alt="" class="h-30">
                                                        </div>
                                                        <div class="f-9 f-italic mt-1 ">
                                                            {{$order->quick_status->second_line->text}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <div class="f-12 mt-2">{{$order->order_id}}</div>
                                                        <div class="f-12">
                                                            <span>{{$order->items->key}}</span> <span> - </span> <span>{{$order->items->value}}</span>
                                                        </div>
                                                    </div>
                                                    @if($order->quick_status->fourth_line->button!=null && $order->quick_status->fourth_line->button!='')
                                                    <div>
                                                        <img src="{{$order->quick_status->fourth_line->image}}" alt="" class="w-24">
                                                        <button class="food-ready" onclick="food_ready('{{url('FoodReady', $order->id)}}');">{{$order->quick_status->fourth_line->button}} <i class="mx-2 fa fa-angle-right"
                                                                                                                                                                                              aria-hidden="true"></i></button>
                                                    </div>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                {{--@if($allorder->status_id == 5)--}}
                                    {{--@foreach($allorder->orders as $order)--}}
                                        {{--<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">--}}
                                            {{--<div class="orders-card text-color-grey">--}}
                                                {{--<div class="d-flex justify-content-between">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="d-flex align-items-end label-status">--}}
                                                            {{--<img src="{{$order->order_tag}}" alt="" class="h-30" onclick="getorderdetails({{$order->id}});">--}}

                                                        {{--</div>--}}
                                                        {{--<div class="f-14 f-medium mt-2 text-color-black">{{$order->order_name}}</div>--}}
                                                        {{--<div class=" f-12">--}}
                                                            {{--<img src="{{$order->ordered_time_icon}}" alt="" class="w-18">--}}
                                                            {{--<span>{{$order->ordered_time}}</span> <span>|</span> <span></span><span>{{$order->total_amount}}</span>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                    {{--<div class="text-right">--}}
                                                        {{--<div>--}}
                                                            {{--<span class="text-color-green f-12">{{$order->quick_status->first_line->text}}</span>--}}
                                                            {{--<img src="{{$order->quick_status->first_line->image}}" alt="" class="h-30">--}}
                                                        {{--</div>--}}
                                                        {{--<div class="f-9 f-italic mt-1 ">--}}
                                                            {{--{{$order->quick_status->second_line->text}}--}}
                                                            {{--<img class="driverimg" src="./images/userprofile.svg" alt="">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="d-flex justify-content-between align-items-center">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="f-12 mt-2">{{$order->order_id}}</div>--}}
                                                        {{--<div class="f-12">--}}
                                                            {{--<span>{{$order->items->key}}</span> <span> - </span> <span>{{$order->items->value}}</span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--@if($order->quick_status->fourth_line->button!=null && $order->quick_status->fourth_line->button!='')--}}
                                                    {{--<div>--}}
                                                        {{--<img src="{{$order->quick_status->fourth_line->image}}" alt="" class="w-24">--}}
                                                        {{--<button class="food-ready" onclick="food_dispatched('{{url('FoodDispatched', $order->id)}}');">{{$order->quick_status->fourth_line->button}}<i class="mx-2 fa fa-angle-right"--}}
                                                                                                                                                                                                       {{--aria-hidden="true"></i></button>--}}
                                                    {{--</div>--}}
                                                        {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}
                                {{--@if($allorder->status_id == 6)--}}
                                    {{--@foreach($allorder->orders as $order)--}}
                                        {{--<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">--}}
                                            {{--<div class="orders-card text-color-grey">--}}
                                                {{--<div class="d-flex justify-content-between">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="d-flex align-items-end label-status">--}}
                                                            {{--<img src="{{$order->order_tag}}" alt="" style="height: 30px;" onclick="getorderdetails({{$order->id}});">--}}

                                                        {{--</div>--}}
                                                        {{--<div class="f-14 f-medium mt-2 text-color-black">{{$order->order_name}}</div>--}}
                                                        {{--<div class=" f-12">--}}
                                                            {{--<img src="{{$order->ordered_time_icon}}" alt="" class="w-18">--}}
                                                            {{--<span>{{$order->ordered_time}}</span> <span>|</span> <span></span><span>{{$order->total_amount}}</span>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                    {{--<div class="text-right">--}}
                                                        {{--<div>--}}
                                                            {{--<span class="text-color-green f-12">{{$order->quick_status->first_line->text}}</span>--}}
                                                            {{--<img src="{{$order->quick_status->first_line->image}}" alt="" class="h-30">--}}
                                                        {{--</div>--}}
                                                        {{--<div class="f-9 f-italic mt-1 ">--}}
                                                            {{--{{$order->quick_status->second_line->text}}--}}
                                                            {{--<img class="driverimg" src="./images/userprofile.svg" alt="">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="d-flex justify-content-between align-items-center">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="f-12 mt-2">{{$order->order_id}}</div>--}}
                                                        {{--<div class="f-12">--}}
                                                            {{--<span>{{$order->items->key}}</span> <span> - </span> <span>{{$order->items->value}}</span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--@if($order->quick_status->fourth_line->button!=null && $order->quick_status->fourth_line->button!='')--}}
                                                    {{--<div>--}}
                                                        {{--<img src="{{$order->quick_status->fourth_line->image}}" alt="" class="w-24">--}}

                                                    {{--</div>--}}
                                                        {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}
                                {{--@if($allorder->status_id == 8)--}}
                                    {{--@foreach($allorder->orders as $order)--}}
                                        {{--<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">--}}
                                            {{--<div class="orders-card text-color-grey">--}}
                                                {{--<div class="d-flex justify-content-between">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="d-flex align-items-end label-status">--}}
                                                            {{--<img src="{{$order->order_tag}}" alt="" style="height: 30px;" onclick="getorderdetails({{$order->id}});">--}}

                                                        {{--</div>--}}
                                                        {{--<div class="f-14 f-medium mt-2 text-color-black">{{$order->order_name}}</div>--}}
                                                        {{--<div class=" f-12">--}}
                                                            {{--<img src="{{$order->ordered_time_icon}}" alt="" class="w-18">--}}
                                                            {{--<span>{{$order->ordered_time}}</span> <span>|</span> <span></span><span>{{$order->total_amount}}</span>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                    {{--<div class="text-right">--}}
                                                        {{--<div>--}}
                                                            {{--<span class="text-color-green f-12">{{$order->quick_status->first_line->text}}</span>--}}
                                                            {{--<img src="{{$order->quick_status->first_line->image}}" alt="" class="h-30">--}}
                                                        {{--</div>--}}
                                                        {{--<div class="f-9 f-italic mt-1 ">--}}
                                                            {{--{{$order->quick_status->second_line->text}}--}}
                                                            {{--<img class="driverimg" src="./images/userprofile.svg" alt="">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="d-flex justify-content-between align-items-center">--}}
                                                    {{--<div>--}}
                                                        {{--<div class="f-12 mt-2">{{$order->order_id}}</div>--}}
                                                        {{--<div class="f-12">--}}
                                                            {{--<span>items</span> <span> - </span> <span>20</span>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--@if($order->quick_status->fourth_line->button!=null && $order->quick_status->fourth_line->button!='')--}}
                                                    {{--<div>--}}
                                                        {{--<img src="{{$order->quick_status->fourth_line->image}}" alt="" class="w-30">--}}
                                                    {{--</div>--}}
                                                        {{--@endif--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            @endforeach




                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>



@endsection
@section('modal')

    <div>
        @include('inc.side_nav')



        <div id="ordermodeldialog"></div>



        <!-- Modal -->
        <div class="modal fade" id="foodready_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">Food is prepared ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="foodready_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="manual_assign_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Request for driver!</p>
                        <p class="f-12 mb-0">Do you want request for driver (or) Manual Assign  ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal" id="assign"
                                    aria-label="" onclick="driver_request(this.value)">Manual Assign</button>
                            <a id="manual_assign_link" class="addbtn w-100 mx-3 f-medium">Request Driver</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="fooddispatch_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to dispatch Food ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="fooddispatch_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--- Driver Request Model ---->

        <div class="modal fade" id="driver_request_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to Request New Driver ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="driver_request_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Order item reject Modal -->
        <div class="modal fade" id="orderitem_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <h6 class="mb-3 acount-text" style="text-align: center;">Item Reject</h6>
                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want Reject Item?</p>
                    </div>
                    <div class="form-group input-material mr-2">
                        <input type="hidden" id="ordproductid" name="ordproductid" class="form-control" required>
                        <input type="hidden" id="orderid" name="orderid" class="form-control" required>
                        <input type="text" id="ordreason" name="reason" class="form-control" required>
                        <label for="name">Reason</label>
                        <div class="text-danger" id="itemrejectvaliderror"></div>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a onclick="orderitemreject();" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $("ul.nav-tabs a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    </script>
    <script>
        $(document).ready(function() {
//            setTimeout(function(){
//                location.reload();
//            },30000);

            fetchallorders();

    setInterval(function() {
    var randomnumber = Math.floor(Math.random() * 100);
           fetchallorders();
    }, 10000);
    });

    function fetchallorders(){
        var i = 0;
        var url = "{{route('my_orders')}}";
        var new_orders =[];
        var new_order_info = '';
        $('#new-order').empty('');
        $.ajax({
            cache: false,
            type: "get",
            url: url,
            success: function (res) {
                if (res.success == true) {

                    new_orders = res.data.orders;
                    $('span#count_orders').text('(' + new_orders.length + ')');
                    for (i = 0; i < new_orders.length; i++) {

                      var order_object =   JSON.stringify(new_orders[i]);
                        new_order_info = '';

                        if (new_orders[i]['order_type'] == 1) {
                            new_order_info += '<div class="new-orders">'
                        }
                        else if(new_orders[i]['order_type'] == 2){
                        new_order_info += '<div class="group-orders">'
                        }else{
                        new_order_info += '<div class="workperks-orders">'
                        }

                        new_order_info += '<div class="d-flex justify-content-between">' +
                                '<section class="d-flex align-items-center w-100">' +
                                '<section class="d-flex ">' +
                                '<img src="'+new_orders[i]['order_image']+'" alt="" class="w-36 h-36">' +
                                '<div class="mx-2">' +
                                '<p class="f-14 d-flex mb-0">'+new_orders[i]['description']+'</p>' +
                                '<p class="d-flex f-10 mb-0">' +
                                '<span class="mx-1">Total</span>' +
                                '<span></span>' +
                                '<span class="mx-1">' + new_orders[i]['total_items'] + '</span>' +
                                '<span class="mx-1">|</span>' +
                                '<span></span>' +
                                '<span>' + new_orders[i]['total_amount'] + '</span>' +
                                '</p>' +
                                '</div>' +
                                '</section>'
                                 if (new_orders[i]['order_type'] == 1) {
                                   new_order_info += '<span class="individual-more-details"  style="cursor: pointer;"  onclick="getorderdetails(' + new_orders[i]['order_id'] + ') " />'
                                    }
                                else if (new_orders[i]['order_type'] == 2) {
                                    new_order_info += '<span class="group-more-details"  style="cursor: pointer;" onclick="getorderdetails('+new_orders[i]['order_id'] +') "/>'
                                }
                                else{
                                    new_order_info += '<span class="workperks-more-details"  style="cursor: pointer;" onclick="getorderdetails('+new_orders[i]['order_id']+') "/>'
                                }
                                    new_order_info+= '<i class="fa fa-angle-right" aria-hidden="true"></i>' +
                                                '</span>' +
                                                '</section>' +
                                                '</div>' +
                                                '</div>';

                            $('#new-order').append(new_order_info);
                        }
                    }
                },
                error: function (res) {
                }
        });
    };
    </script>
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("activeorder");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }



        function getorderdetails(param){
            console.log((param));
            //alert(JSON.stringify(param));
            $('#orderdialog').modal({backdrop: 'static', keyboard: false});
            $('#resonreject').hide();
            var orderinvoice = '';
            $('#orderrejectshowhide').html("");
                $('#ordermodeldialog').append('<div class="modal orders-list right fade " id="orderdialog" tabindex="-1" role="dialog" aria-labelledby="orders-list">' +
                    '<div class="modal-dialog" id="addnewrestaurant" role="orders list">' +
                    '<div class="modal-content">' +
                    '<ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">' +
                    '<li class="nav-item" role="OrdersList">' +
                    '<a class="nav-link" id="PickUp" data-toggle="tab" href="#DELIVERYDETAILS" role="tab" aria-controls="PICKUPDETAILS" aria-selected="false">PICKUP</a>' +
                    '</li>' +
                    '<li class="nav-item" role="OrdersList">' +
                    '<a class="nav-link active" id="DELIVERY" data-toggle="tab" href="#DELIVERYDETAILS" role="tab" aria-controls="DELIVERYDETAILS" aria-selected="true">DELIVERY</a>' +
                    '</li>' +
                    '<li class="nav-item" role="OrdersList">' +
                    '<a class="nav-link" id="FOOD@WORK" data-toggle="tab" href="#FOODWORKDETAILS" role="tab" aria-controls="FOODWORKDETAILS" aria-selected="false">FOOD@WORK</a>' +
                    '</li>' +
                    '</ul>' +
                    '<div class="tab-content" id="myTabContent">' +
                    '<div class="tab-pane fade" id="PICKUPDETAILS" role="tabpanel" aria-labelledby="PickUp">' +
                    'PICKUPDETAILS' +
                    '</div>' +
                    '<div class="tab-pane fade show active" id="DELIVERYDETAILS" role="tabpanel" aria-labelledby="DELIVERY">' +
                    '<div class="d-flex align-items-center justify-content-between p-2 px-3">' +
                    '<div>' +
                    '<p class="f-14 mb-1"><span id="inv_ordername"></span>  |  <span id="inv_orderid"></span></p>' +
                    '<p class="d-flex f-12 mb-0"><img src="./images/time.svg" alt="">' +
                    '<span id="inv_ordertime"></span> | Items : <span id="inv_orderitems"></span> | Amount : <span id="inv_orderAmount"></span>' +
                    '</p>' +
                    '</div>' +
                    '<div class="d-flex">' +
                    '<span class="ordertypechange sactiveicon">' +
                    '<i class="fa fa-user f-12" aria-hidden="true"></i>' +
                    '</span>' +
                    '<span class="ordertypechange gactiveicon">' +
                    '<i class="fa fa-group f-12" aria-hidden="true"></i>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="line mb-0"></div>' +
                    '<div class="preparing-time px-3" id="inv_order_status">' +
                    '<span id="inv_order_statustext"></span>' +
                    '<img src="" id="image-preview" alt="">' +
                    '</div>' +
                    '<div class="container-fluid">' +
                    '<div class="row pt-3 pb-4">' +
                    '<div class="col-5 f-12 text-color-greytext-uppercase ">ITEMS</div>' +
                    '<div class="col-3 f-12 text-color-grey text-uppercase text-right">AVAILABILITY</div>' +
                    '<div class="col-2 f-12 text-color-grey text-uppercase text-right">QUANTITY</div>' +
                    '<div class="col-2 f-12 text-color-grey text-uppercase text-right">PRICE</div>' +
                    '</div>' +

                    ' <!-- single order for loop start-->' +
                    '<div id="orderinvoicedialog"></div>'+
                    '<div class="row mt-3">'+
                    '<div class="col-12 driver-status" id="driver_status"></div>'+
                    '</div>'+
                    '<div class="row" id="driver_details"></div>'+
                    '<div class="row"  id="drivers_list"></div>'+
                    ' <!-- single order for loop end-->' +

                    '<!-- Order timestamps start-->' +
                    '<!--<div class="line mb-0"></div>' +
                    '<div class="d-flex align-items-center justify-content-between px-3" style="background:#e1e1e1;" id="order_timestamps">' +
                    '<span id="ordered">Food ordered</span>' +
                    '<span id="time">2021-06-16</span>' +
                    '</div>-->' +
                    '<!-- Order timestamps end-->' +

                    '<div class="col-12 my-3" id="resonsingleitemreject">' +
                    '<div class="mt-5 d-flex align-items-center justify-content-between" id="orderbutton">' +
                  '</div>' +

                    '<div class="pt-3" id="orderrejectshowhide">'+
                    '</div>' +

                    '</div>' +
                    '</div><!-- modal-content -->' +
                    '</div><!-- modal-dialog -->' +
                    '</div><!-- modal addddddddd-->'
                );
                var id = param;

                var url = "{{url('ajaxinvoicedtls')}}/" + id;
                console.log(url);
                $.ajax({
                    cache:false,
                    type: "GET",
                    url: url,
                    success: function (res) {
                        console.log(res);
                        //return false;
                        $(".sactiveicon").addClass("bg-light-grey");
                        $(".gactiveicon").addClass("bg-light-grey");
                        if(res.order_type == 1){
                            $(".sactiveicon").removeClass("bg-light-grey");
                            $(".sactiveicon").addClass("bg-black");
                        }
                        if(res.order_type == 2){
                            $(".gactiveicon").removeClass("bg-light-grey");
                            $(".gactiveicon").addClass("bg-black");
                        }

                        $('#inv_orderid').html(res.data.header.order_id);
                        $('#inv_ordername').html(res.data.header.order_name);
                        $('#inv_ordertime').html(res.data.header.time.text);
                        $('#inv_orderAmount').html(res.data.header.total_amount);
                        $('#inv_order_statustext').html('');
                        $('#inv_orderitems').html(res.data.header.items_count);
                        $('#inv_foodaccept').val(res.id);
                        $('#inv_foodreject').val(res.id);
                        $('#inv_foodreject1').val(res.id);
                        $("#orderinvoicedialog").html("");
                        $("#orderbutton").html("");
                        if(res.order_status_code == 1){
                            $("#inv_foodaccept").show();
                        }else{
                            $("#inv_foodaccept").hide();
                        }


                        var orderinvoice = res['data'];
                        var order_id = orderinvoice.id;
                        var orderinvrpt = '';
                        var orderbutton = '';
                        var driver_info = '';
                        var driver_status ='';
                        var inv_order_statustext = '';
                        var single_order = orderinvoice['order_details'][0]['products'];
                        for (var i = 0; i < single_order.length; i++) {

                            if(single_order[i]['order_status'] == 4){
                                var ischeckval = 'unchecked';
                            }else{
                                var ischeckval = 'checked';
                            }

                            orderinvrpt += '<div class="row align-items-center">' +
                                '<div class="col-5 f-12">' +
                                '<div class="f-14 f-medium text-color-black" id="singleOrderProductName'+i+'">'+single_order[i]['product_name']+'</div>' +
                                '</div>' +
                                '<div class="col-3  d-flex justify-content-end">' +
                                '<div class="checkbox checbox-switch switch-primary">' +
                                '<label>' +
                                '<input type="checkbox" name="invorder_item_reject'+i+'" id="invorder_item_reject'+i+'" onclick="item_reject_reason('+single_order[i]['order_detailed_id']+', '+single_order[i]['order_status']+','+order_id+');" '+ischeckval+'/>' +
                                '<span></span>' +
                                '</label>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-2 f-14 f-medium text-color-black text-right"><span>'+single_order[i]['quantity']+'</span></div>' +
                                '<div class="col-2 f-14 f-medium text-color-green text-right">'+single_order[i]['price']+'</div>' +
                                '<div class="col-6 f-12 text-color-grey">Main Course</div>' +
                                '<div class="col-6 f-12 text-color-red text-right" id="inv_order_item_rejct_sts">'+single_order[i]['reason']+'</div>' +
//                                '<div class="col-12 f-14 text-color-grey py-3">Item has to be bit less spicier as its going to be served for kids</div>' +
                                '<div class="mx-2 line"></div>' +
                                '</div>';

                            $('#singleOrderProductName'+i).html(single_order[i]['product_name']);
                           }
                        orderinvrpt +='<div class="row pt-3">' +
                        '<div class="col-12"><img class="mx-3" src="./images/utencils.svg" alt=""><b style="font-size: 12px;">Include Utensils</b></div>' +
                        '<div class="col-12 pt-3">Bill Details</div>' +
                        '</div>';
                        var bill = orderinvoice['bill'];
                        for(var j = 0;j < bill.length ; j++){
                            orderinvrpt+='<div class="mt-3 d-flex align-items-center justify-content-between">' +
                                '<div class="col-6 f-12" ><span id="billAmountkey">'+bill[j]["key"] +'</span></div>'+
                                '<div class="col-6 f-12" ><span id="billAmountval">'+bill[j]["value"]+'</span></div>'+
                                '</div>'
                        };

//                        '<div class="mt-3 d-flex align-items-center justify-content-between">' +
//                        '<div class="col-6 f-12" ><span id="billAmountkey">Sub Total</span></div>' +
//                        '<div class="col-6 f-12" ><span id="billAmountval">'+orderinvoice['bill']['sub_total']['value']+'</div>' +
//                        '</div>'+
//                        '<div class="mt-3 d-flex align-items-center justify-content-between">' +
//                        '<div class="col-6 f-12" ><span id="billAmountkey">Shipping Cost</span></div>' +
//                        '<div class="col-6 f-12" ><span id="billAmountval">'+orderinvoice['bill']['shipping_cost']['value']+'</div>' +
//                        '</div>'+
//                        '<div class="mt-3 d-flex align-items-center justify-content-between">' +
//                        '<div class="col-6 f-12" ><span id="billAmountkey">Service Cost</span></div>' +
//                        '<div class="col-6 f-12" ><span id="billAmountval">'+orderinvoice['bill']['service_fee']['value']+'</div>' +
//                        '</div>'+
//                        '<div class="mt-3 d-flex align-items-center justify-content-between">' +
//                        '<div class="col-6 f-12" ><span id="billAmountkey">Tax</span></div>' +
//                        '<div class="col-6 f-12" ><span id="billAmountval">'+orderinvoice['bill']['tax']['value']+'</div>' +
//                        '</div>'+
//                        '<div class="mt-3 d-flex align-items-center justify-content-between">' +
//                        '<div class="col-6 f-12" ><span id="billAmountkey"><b>'+orderinvoice['bill']['total_amount']['key']+'</b></span></div>' +
//                        '<div class="col-6 f-12" ><span id="billAmountval"><b>'+orderinvoice['bill']['total_amount']['value']+'</b></div>' +
//                        '</div>'+
                        orderinvrpt+= '<div class="mt-3 d-flex align-items-center justify-content-between">' +
                        '<div class="col-6 f-12" ><span id="billSubTotalkey"></span></div>' +
                        '<div class="col-6 f-12" ><span id="billSubTotalval"></div>' +
                        '</div>';

                        $("#orderinvoicedialog").html(orderinvrpt);

                        var  foodstatus = orderinvoice['buttons']['button_two']['action'];
                        var bgcolor = orderinvoice['buttons']['button_two']['background_color'];
                        var order_id = orderinvoice.id;

                        orderbutton = '<button class="cancelbtn"  onclick="modelclose()">Close</button>'
                                if(orderinvoice['buttons']['visible']==true) {
                                    if (orderinvoice['buttons']['button_one']['clickable'] == true){
                                        if(orderinvoice['buttons']['button_one']['action']!='cancel') {
                                            orderbutton += '<a href="#reject_dialog"><button class="cancelbtn text-center"    id="inv_foodreject1"  onclick="food_reject(' + order_id + ')">' + orderinvoice['buttons']['button_one']['text'] + '</button></a>'
                                        }
                                     }
                                    if (orderinvoice['buttons']['button_two']['clickable'] == true && foodstatus != 'reject') {
                                    orderbutton += '<button class="addbtn text-center" id="inv_foodreject" onclick="food_status(\''+order_id+'\',\''+foodstatus+'\')">' + orderinvoice['buttons']['button_two']['text'] + '</button>'
                                    }if(orderinvoice['buttons']['button_two']['clickable'] == true && foodstatus == 'reject'){
                                        orderbutton += '<a href="#reject_dialog"><button class="cancelbtn text-center"    id="inv_foodreject1"  onclick="food_reject(' + order_id + ')">' + orderinvoice['buttons']['button_two']['text'] + '</button></a>'
                                    }
                                }
                            $("#orderbutton").html(orderbutton);


                        inv_order_statustext+=orderinvoice['quick_status']['text'];
                        var driver_action = orderinvoice['driver_info']['line_two']['driver']['button']['action'];
                            if(orderinvoice['driver_status']!=2) {
                                if (orderinvoice['driver_info']['visible'] == true) {

                                    driver_info += '<div class="col-12 driver-staus-call">' +
                                        '<div class="d-flex align-items-center justify-content-center">'
                                    if (orderinvoice['driver_info']['line_two']['driver']['image'] != '') {
                                        driver_info += '<img src="' + orderinvoice['driver_info']['line_two']['driver']['image'] + '" class="driver-img" alt="">'

                                    }
                                    driver_info += '<div class="f-12 f-medium ml-2">' + orderinvoice['driver_info']['line_two']['driver']['name'] + '</div>' +
                                        '</div>' +
                                        '<div class="call-to-driver">'
                                    if (orderinvoice['driver_info']['line_two']['driver']['button']['icon'] != '') {
                                        driver_info += '<span><img src="./images/drivercall.svg" alt=""></span>'
                                    }
                                    if (orderinvoice['driver_info']['line_two']['driver']['button']['action'] == 'driver') {
                                        driver_info += '<span class="f-14 f-medium ml-1" style="cursor: pointer;" onclick="food_status(\'' + order_id + '\',\'' + driver_action + '\')">' + orderinvoice['driver_info']['line_two']['driver']['button']['text'] + '</span>'
                                    } else {
                                        driver_info += '<span class="f-14 f-medium ml-1">' + orderinvoice['driver_info']['line_two']['driver']['button']['text'] + '</span>'
                                    }
                                    '</div>' +
                                    '</div>'
                                }
                            }
                        $("#driver_details").html(driver_info);
                        $("#inv_order_statustext").html(inv_order_statustext);
                        if(orderinvoice['quick_status']['visible']==true) {
                            $("#inv_order_status").css('background-color', '#' + orderinvoice['quick_status']['background_color']);
                            $("#inv_order_status").css('color', '#' + orderinvoice['quick_status']['color']);
                        }else{
                            $("#inv_order_status").css('background-color','');
                            $("#inv_order_status").css('color', '');
                        }
                        if(orderinvoice['driver_info']['visible']==true){
                            driver_status=orderinvoice['driver_info']['line_one']['text'];
                        };
                        $("#driver_status").html(driver_status);
                        if(orderinvoice['driver_info']['visible'] == true) {
                            $("#driver_status").css("background-color", '#' + orderinvoice['driver_info']['line_one']['background_color']);
                            $("#driver_status").css("color", '#' + orderinvoice['driver_info']['line_one']['color']);
                            $("#driver_details").css("background-color", '#' + orderinvoice['driver_info']['line_two']['background_color']);
                            $("#driver_details").css("color", '#' + orderinvoice['driver_info']['line_two']['color']);
                        }else{
                            $("#driver_status").css("background", '');
                            $("#driver_status").css("color", '');

                        }

                        $('#image-preview').attr('src', orderinvoice['quick_status']['icon']);
                        },error: function (res) {

                    }
                });



        }



        function food_ready(foodreadyurl)
        {

            jQuery('#foodready_model').modal('show', {backdrop: 'static'});
            document.getElementById('foodready_link').setAttribute('href' , foodreadyurl);
        }
        function manual_assing(manual_assign_url,id)
        {


            jQuery('#manual_assign_model').modal('show', {backdrop: 'static'});
            document.getElementById('manual_assign_link').setAttribute('href' , manual_assign_url);
            $('#assign').val(id);
        }

        function food_dispatched(fooddispatchurl)
        {
            jQuery('#fooddispatch_model').modal('show', {backdrop: 'static'});
            document.getElementById('fooddispatch_link').setAttribute('href' , fooddispatchurl);
        }

        function assign_drivers(driver_request_url)
        {
            jQuery('#driver_request_model').modal('show', {backdrop: 'static'});
            document.getElementById('driver_request_link').setAttribute('href' , driver_request_url);
        }
        function modelclose(){
            $('#orderdialog').modal('hide');
            $('#orderdialog').find('textarea,input').val('');
            $('#drivers_list').empty();
        }

        $( document ).ready(function() {
            $('#orderrejectshowhide').hide();
        });

        function food_reject(data)
        {

            $('#orderrejectshowhide').html('<div class="p-3  bg-light-grey border-radius-8" id="reject_dialog" >' +
                '<p class="f-14 f-medium">Select Reasons</p>' +
                '<div class="d-flex">' +
                '<label class="radio mr-4">Out of Stock' +
                '<input type="radio" name="reasonreject" id="reasonreject" value="Out of Stock">' +
                '<span class="checkround"></span>' +
                '</label>' +
                '<label class="radio">Restaurant Busy' +
                '<input type="radio" name="reasonreject" id="reasonreject" value="Restaurant Busy">' +
                '<span class="checkround"></span>' +
                '</label>' +
                '</div>' +
                '</div>' +
                '<div class="pt-3" style="float: right;">' +
                '<button class="cancelbtn text-right" style="padding: 4px 50px;" id="inv_foodreject" onclick="confirmfood_reject('+data+')">Submit</button>'+
                '<div>'
            );
        }

        function confirmfood_reject(id)
        {
           var reason ="";
            reason = $("input[name='reasonreject']:checked").val();
            if(reason == '' || reason == undefined){
                swal({title: "Failed", text: "Please select reason!"});
                return false;
            }
            var orderreject = {"orderid":id, "rejectstatus":reason, _token: '{{csrf_token()}}'};
            //orderreject=$(this).serialize()+" & "+$.param(orderreject);
            console.log(orderreject);

            $.ajax({
                cache:false,
                type: "POST",
                url: "{{url('ajaxFoodReject')}}",
                data:orderreject,
                success: function (res) {
                    console.log(res);
                    if (res.success) {
                        swal({title: "success", text: "Food has been rejected successfully."});
                        window.location.href = "{{url('orders/')}}";
                    } else {
                        swal({title: "error", text: res.message});
                    }
                },error: function(res) {

                }
            });
        }
        function food_status(id,type){
            if(type=='reject'){
                food_reject(id);
            }
            if(type=='confirm'){
                   food_accept(id);
               }
            if(type=='ready'){
                food_ready("{{url('FoodReady')}}/"+id);
            }
            if(type=='DISPATCH FOOD'){
                food_dispatched("{{url('FoodDispatched')}}/"+id);
            }if(type=='driver'){
                manual_assing("{{url('AssignDriver')}}/"+id,id);
                //driver_request(id);
            }
            if(type=='dispatch-deliver'){
                food_dispatch_delivey(id);
            }

        }
        function food_accept(id)
        {
            console.log(id);
            var addtime ="30";
            var orderacpt = {"orderid":id, "add_time":addtime, _token: '{{csrf_token()}}'};
            orderacpt=$(this).serialize()+" & "+$.param(orderacpt);
            console.log(orderacpt);
            $.ajax({
                cache:false,
                type: "POST",
                url: "{{url('ajaxFoodAccept')}}",
                data:orderacpt,
                success: function (res) {
                    console.log(res);
                    if (res.success) {
                        swal({title: "success", text: "Food has been accepted successfully."});
                        window.location.href = "{{url('orders/')}}";
                    } else {
                        swal({title: "error", text: res.message});
                    }
                },error: function(res) {

                }
            });
        }

        function driver_request(id)
        {
            var id = id;
            var driver_info = '';
            $.ajax({
                cache:false,
                type: "GET",
                url: "{{url('get_drivers')}}/",
                success: function (res) {
                    console.log(res.success);
                    var i=0;
                    if (res.success) {

                        var drivers = res.data;
                        if(drivers.length==0){
                            driver_info = '<div class="col-12 driver-staus-call">'+ 'No Drivers Found Please Contact Admin !!'
                                            '</div>'
                            $("#drivers_list").html(driver_info);
                        }else {

                            for(i=0;i<drivers.length;i++){
                            driver_info += '<div class="col-12 driver-staus-call">' +
                                '<div class="d-flex align-items-center justify-content-center">' +
                                '<div class="f-12 f-medium ml-2">'+drivers[i]['driver_first_name']+'</div>' +
                                '</div>' +
                                '<div class="call-to-driver" onclick=assign_driver('+id+','+drivers[i]['driver_id']+')>' +
                                '<span><img src="./images/drivercall.svg" alt=""></span>' +
                                '<span class="f-14 f-medium ml-1" >'+drivers[i]['driver_phone']+'</span>' +
                                '</div>' +
                                '</div>'
                            }
                            $("#drivers_list").html(driver_info);

                        }

                    } else {
                        //swal({title: "error", text: res.message});
                    }
                },error: function(res) {

                }
            });
        }
            function assign_driver(order_id,id){
                assign_drivers("{{url('AssignDriver')}}/"+order_id+'/'+id);

            }

        function reject_reason(){
            $("#resonreject").show();
        }
        function single_reject_reason(param){
            console.log("#resonsingleitemreject"+param);
            $("#resonsingleitemreject"+param).show();
        }
        function group_reject_reason(param){
            console.log("#resongroupitemreject"+param);
            $("#resongroupitemreject"+param).show();
        }

        function item_reject_reason(param, ordersts,id){
            if(ordersts == 4){
                return false;
            }
            $('#ordproductid').val(param);
            $('#orderid').val(id);
            $("#orderitem_model").show();
            jQuery('#orderitem_model').modal('show', {backdrop: 'static'});
           // document.getElementById('itemreject_link').setAttribute('href' , fooddispatchurl);
        }
        function food_dispatch_delivey(id)
        {
            var url = "{{url('food_dispatch_delivey')}}"+'/'+id
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log(res);
                    if (res.success) {
                        swal({title: res.data.body.heading, text: res.data.body.description});
                        window.location.href = "{{url('orders/')}}";
                    } else {
                        swal({title: res.data.body.heading, text: res.data.body.description});
                    }
                },error: function(res) {

                }
            });
        }
        function orderitemreject(data, param)
        {

         var productid = $('#ordproductid').val();
            var id = $('#orderid').val();
         var reason = $('#ordreason').val();
            if(reason==""){
                $('#itemrejectvaliderror').html("Please Enter reject reason");
                return false;
            }
            var send = {"reason":reason, "order_id":productid, _token: '{{csrf_token()}}'};
            send=$(this).serialize()+" & "+$.param(send);
            console.log("data "+send);
            $.ajax({
                cache:false,
                type: "POST",
                url: "{{url('order_item_reject/')}}",
                data:send,
                success: function (res) {
                    console.log(res);
                    if (res.success==true) {
                        swal({title: "success", text: res.message});
                        window.location.href = "{{url('orders/')}}";
                    } else {
                        swal({title: res.message, text:res.data.description,showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, I am sure!',
                            cancelButtonText: "No, cancel it!",
                            closeOnConfirm: false,
                            closeOnCancel: false },
                            function(isConfirm) {

                                if (isConfirm) {
                                    var orderreject = {
                                        "orderid":id ,
                                        "rejectstatus": reason,
                                        _token: '{{csrf_token()}}'
                                    };
                                    $.ajax({
                                        cache: false,
                                        type: "POST",
                                        url: "{{url('ajaxFoodReject')}}",
                                        data: orderreject,
                                        success: function (res) {
                                            console.log(res);
                                            //return false;
                                            if (res.success) {
                                                swal({
                                                    title: "success",
                                                    text: "Order Items  has been rejected successfully."
                                                });
                                                window.location.href = "{{url('orders/')}}";
                                            } else {
                                                swal({title: "error", text: res.message});
                                            }
                                        }, error: function (res) {

                                        }
                                    });
                                }else{
                                    swal({
                                        title: "success",
                                        text: "Your order item not rejected ."
                                    });
                                    window.location.href = "{{url('orders/')}}";
                                }



                            });

                    }
                },error: function(res) {
                    console.log(res);
                    swal({title: "error", text: res.message});
                }
            });
        }
        function selectorder(key){
           $('#filter_orders').submit();
        }
</script>
    {{--<script>--}}

        {{--$(document).ready(function () {--}}
            {{--$("#inv_foodreject1").click(function () {--}}
                {{--alert(1);--}}
{{--//            $('html,body').animate({--}}
{{--//                    scrollTop: $(".orderrejectshowhide").offset().top},--}}
{{--//                'slow');--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

@endsection
