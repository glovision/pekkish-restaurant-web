@extends('layouts.app',['titlePage' => __('Offers')])

@push('css')

@endpush
@section('content')

    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">OFFERS LIST</span>
                <button class="add-btn" data-toggle="modal" data-target="#addmenuid">+ Add New Offer</button>

            </div>
            <div class="container">
                @foreach($coupons as $key=>$value)
                    @if($key%4==0)
                        <div class="row">
                    @endif
                            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 ">
                                <div class="offer-cards">
                                    <div class="d-flex align-items-center justify-content-between mb-5">
                                        <span class='@if($value["status"]=="expired")status-view-expired @else status-view @endif f-14 f-medium'>
                                            @if($value["status"]=="active")
                                                ACTIVE
                                            @else
                                                EXPIRED
                                            @endif
                                        </span>
                                        <span>
                                        <img class="mx-2" src="{{url('/')}}/images/card-edit.svg" alt="">
                                        <img src="{{url('/')}}/images/delete.svg" alt="">
                                    </span>
                                    </div>
                                    <div class="text-center f-16 f-bold">{{$value["name"]}}</div>
                                    <div class="mb-0 pt-1 mb-4 f-14 text-color-grey text-center">
                                        <div>{{$value["line-one"]}}</div>
                                        <div>{{$value["line-two"]}}</div>
                                    </div>
                                    <div class="text-center f-14 f-medium">{{$value["details"] .$key}}</div>
                                </div>
                            </div>
                            @if($key%3==0 && $key!=0)
                                </div>
                            @endif
                @endforeach

            </div>

        </div>
    </div>
@endsection

@section('modal')
    <form id="manage-menu-form">
        <div class="modal add-menu right fade " id="addmenuid" tabindex="-1" role="dialog"
             aria-labelledby="add-menu">
            <div class="modal-dialog" id="addnewmenu" role="add menu">
                <div class="modal-content model-row-colum ">
                    <div class="modal-body pb-4 dialog-options-responsive">
                        <div class="bg-black text-color-white negative-margin" style="display:none;"
                             id="showSearchDiv">
                            <nav class="navbar-options menu-list-items d-flex ">
                                <ul class="nav flex-column flex-nowrap ">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <span class="menu-link ">Recommended</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Soups & Subs</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Starters</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Sandwiches</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Burgers</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Pizzas</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Pastas</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Breakfast</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Combo Meals</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Rice Bowls</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Greens & Healthy</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Desserts</span>
                                        </a>
                                        <a class="nav-link text-truncate " href="#">
                                            <span class="menu-link ">Wine</span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 1
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 2
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 3
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>
                                        <a class="nav-link text-truncate active-links" href="#">
                                            <div>
                                                <label class="check">Item 4
                                                    <input type="checkbox" name="is_name">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </a>

                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="overflow-dialog">
                            <div class="f-20 f-medium">Add New Offer</div>
                            <!-- <div class="form-group input-material">
                                <input type="text" class="form-control" id="itemSearch" required>
                                <label for="itemSearch">Item Name</label>
                            </div> -->

                            <div class="form-group input-material">
                                <textarea class="form-control" id="Promocode" rows="1" required></textarea>
                                <label for="Promocode">Promocode</label>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey" style="margin-top: -20px;">
                                0 / 6
                            </div>
                            <div class="mt-2 mb-0">
                                <p class="mb-0 text-color-grey f-14">Offer Start & End Dates</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="StartDate" required
                                                       onfocus="(this.type='date')" onblur="(this.type='text')">
                                                <label for="StartDate" class="datetype">Start Date</label>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="ExpiryDate" required
                                                       onfocus="(this.type='date')" onblur="(this.type='text')">
                                                <label for="ExpiryDate" class="datetype">Expiry Date</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-2 mb-0">
                                <p class="mb-2 text-color-grey f-14">Offer Value</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Dollar
                                                <input type="radio" name="Dollar" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Percentage
                                                <input type="radio" name="Dollar" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="flex">
                                <button class="employee-options option-selected">
                                    <span>5%</span><span class="ml-2"></span>
                                </button>

                                <button class="employee-options ">
                                    <span>10%</span><span class="ml-2"></span>
                                </button>

                                <button class="employee-options ">
                                    <span>15%</span><span class="ml-2"></span>
                                </button>
                                <!--
                                <button class="employee-options ">
                                    <span>20%</span><span class="ml-2"></span>
                                </button>

                                <button class="employee-options ">
                                    <span>Buy 1 Get 1 (50%)%</span><span class="ml-2"></span>
                                </button> -->


                            </div>

                            <div>
                                <p class="mb-2 mt-4 text-color-grey f-14">Days to Run Offer</p>
                                <div class="d-flex align-items-center flex-wrap">
                                    <button class="working-hours-selected">S</button>
                                    <button class="working-hours-selection">M</button>
                                    <button class="working-hours-selection">T</button>
                                    <button class="working-hours-selection">W</button>
                                    <button class="working-hours-selection">T</button>
                                    <button class="working-hours-selection">F</button>
                                    <button class="working-hours-selection">S</button>

                                </div>
                            </div>

                            <div class="mt-4 mb-0">
                                <p class="mb-2 mt-2 text-color-grey f-14">Specify Items for this Offer</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-4">
                                            <label class="radio">Entire Menu
                                                <input type="radio" name="menu" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-4">
                                            <label class="radio">Specific Category
                                                <input type="radio" name="menu">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>

                                        <div class="col-4">
                                            <label class="radio">Specific Item
                                                <input type="radio" name="menu">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="mt-3 mb-0">
                                <p class="mb-2 text-color-grey f-14">Offer Value</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Dollar
                                                <input type="radio" name="Dollar" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Percentage
                                                <input type="radio" name="Dollar" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="flex">
                                <button class="employee-options option-selected">
                                    <span>$200</span><span class="ml-2"></span>
                                </button>


                            </div>

                            <div class="mt-4 mb-0">
                                <p class="mb-2 text-color-grey f-14">Set Minimum Usage</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Per Person
                                                <input type="radio" name="Usage" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Unlimited
                                                <input type="radio" name="Usage">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="mt-2 mb-0">
                                <p class="mb-0 text-color-grey f-14">Set Timings</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="time" class="form-control" id="Start" required
                                                >
                                                <label for="Start" class="datetype">Start Date</label>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="time" class="form-control" id="end" required
                                                >
                                                <label for="end" class="datetype">End Date</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="mt-2 mb-0">
                                <p class="mb-1 text-color-grey f-14">Set Maximum and Minimum Purchase Limit</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="min" required
                                                >
                                                <label for="min" class="datetype">Minimum Limit</label>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" id="max" required
                                                >
                                                <label for="max" class="datetype">Maximum Limit</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn" data-dismiss="modal">ADD OFFER</button>
                            </div>
                        </div>


                    </div>

                </div><!-- modal-content -->

            </div><!-- modal-dialog -->
        </div><!-- modal -->
    </form>
@endsection

@section('scripts')
    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>

    <script>
        $.fn.materializeInputs = function (selectors) {

            // default param with backwards compatibility
            if (typeof (selectors) === 'undefined') selectors = "input, textarea";

            // attribute function
            function setInputValueAttr(element) {
                element.setAttribute('value', element.value);
            }

            // set value attribute at load
            this.find(selectors).each(function () {
                setInputValueAttr(this);
            });

            // on keyup
            this.on("keyup", selectors, function () {
                setInputValueAttr(this);
            });
        };

        /**
         * Material Inputs
         */
        $('body').materializeInputs();


    </script>

    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>

    <script>
        $(document).ready(function () {
            $("#itemSearch").keyup(function () {
                var x = document.getElementById('showSearchDiv');
                if ($(this).val() == "") {
                    x.style.display = 'none';
                } else {
                    x.style.display = 'block';
                }
            });
        });
    </script>

@endsection