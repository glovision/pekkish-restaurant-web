<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>index</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <!-- Font Awesome Icon Library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
       <link rel="stylesheet" href="../css/stories.css">
    <link rel="stylesheet" href="../css/custom-dialog-input.css">

    <link rel="stylesheet" href="../css/style.css">
    <style>

    </style>
</head>

<body>

    <div>
        <div class="max-content-width main-section p-0">

            <div class="stories__header sticky-top">


                <img src="../images/pekkish-logo.svg" alt="" class="stories__logo">
                <button class="signin" data-toggle="modal" data-target="#signinid">
                    Sign In
                </button>
             
            </div>

            <!-- <div class="sticky-top w-100 bg-white px-5 py-4 nav-cu">
                <img src="./images/pekkish-logo.svg" alt="" style="width:129px">
            </div> -->

      

            <div class="user-stories">
                <div class="user-stories-heading">
                    Success Stories
                </div>
                <div class="user-stories-sub-title">
                    Show your restaurant owners like you are making waves with Pekkish.
                </div>

                <div class=" container-fluid stories">
                    <div class="row">

                    @foreach ($story as $storie)
                        <div class="col-12 col-sm-12 col-md-6">
                            <div class="stories-list">

                                <img class="story-img" src="{{$storie->image}}" alt="">
                                <div class="story-details">
                                    <div class=success-stories-heading>{{$storie->name}}</div>
                                    <div class="success-stories-description">{{$storie->description}}</div>
                                    <a class="read-more" href="{{url('storyDetails', $storie->id)}}">READ MORE <img src="./images/rightangle.svg" alt=""> </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                                           
                    </div>
                </div>
            </div>

          
        </div>
    </div>
    </div>


    <footer>
        <div class="max-content-width footer-section">
            <img src="../images/pekkish-logo.svg" alt="">
            <div class="copyright">
                Copyright © 2021-2021 pekkish.com
            </div>
        </div>
    </footer>

  

    <div class="modal fade" id="signinid" tabindex="-1" role="dialog" aria-labelledby="signinidTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="signin">
            <div class="modal-content">

                <div class="modal-body p-4 registration-form ">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-24 f-medium">Get Started</p>
                    <form action="">
                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="Store Name" id="Store Name"
                                placeholder="Store Name" required>
                            <label for="Store Name">Store Name</label>
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="Store Address" id="Store Address"
                                placeholder="Store Address" required>
                            <label for="Store Address">Store Address</label>
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="Flow / Suite (Optional)" id="optional"
                                placeholder="Flow / Suite (Optional)" required>
                            <label for="Flow / Suite (Optional)">Flow / Suite (Optional)</label>
                        </div>

                        <div class="d-flex justify-content-between">

                            <div class="form-group input-material mr-2 mb-1 mt-1">
                                <input type="text" class="form-control" name="First Name" id="First Name"
                                    placeholder="First Name" required>
                                <label for="First Name">First Name</label>
                            </div>

                            <div class="form-group input-material mr-2 mb-1 mt-1">
                                <input type="text" class="form-control" name="Last Name" id="Last Name"
                                    placeholder="Last Name" required>
                                <label for="Last Name">Last Name</label>
                            </div>

                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="Email" class="form-control" name="Email" id="Email" placeholder="Email" required>
                            <label for="Email">Email</label>
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" class="form-control" name="Mobile Number" id="Mobile Number" placeholder="Mobile Number"
                                required>
                            <label for="fullname">Mobile Number</label>
                        </div>


                        <div class="d-flex justify-content-end">
                            <button class="submit mt-5 mb-3"> SUBMIT</button>
                        </div>


                    </form>

                </div>


            </div>
        </div>
    </div>



</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 800) {
            $('.nav-cu').fadeIn();
        } else {
            $('.nav-cu').fadeOut();
        }
    });
</script>

</html>