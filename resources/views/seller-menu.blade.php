@extends('layouts.app-new',["titlePage" => "Menu"])
@push('css')
<style>
    .stv-radio-tabs-wrapper {
        display: inline-block;
        width: 100%;
        border-bottom: 1px solid #428bca;
        padding: 0px;
    }

    input.stv-radio-tab {
        position: absolute;
        left: -99999em;
        top: -99999em;
    }

    input.stv-radio-tab + label {
        cursor: pointer;
        float: left;
        border: 1px solid #aaa;
        border-bottom: 0;
        background-color: #fff;
        margin-right: -1px;
        padding: 0.5em 1em;
        position: relative;
        margin-bottom: 0;
        width: 50%;
    }

    input.stv-radio-tab + label:hover {
        background-color: #eee;
    }

    input.stv-radio-tab:checked + label {
        box-shadow: 0 3px 0 -1px #fff, inset 0 5px 0 -1px #13cd4a;
        background-color: #fff;
        border-color: #428bca;
        z-index: 1;
    }

    .category-scroll {
        min-height: calc(100vh - 239px);
        max-height: calc(100vh - 239px);
        overflow-y: scroll;
        -ms-overflow-style: none;
        scrollbar-width: thin;
        mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        mask-size: 100% 20000px;
        mask-position: left bottom;
        -webkit-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -webkit-mask-size: 100% 20000px;
        -webkit-mask-position: left bottom;
        -moz-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -moz-mask-size: 100% 20000px;
        -moz-mask-position: left bottom;
        transition: mask-position 0.3s, -webkit-mask-position 0.3s;
    }

    .category-scroll::-webkit-scrollbar {
        display: none;

    }

    .svg-invert { /* svg on an img tag */
        -webkit-filter: invert(.75); /* safari 6.0 - 9.0 */
        filter: invert(.75);
    }

    .dataTable-wrapper {
        position: relative;
    }

    .dataTable-top {
        position: absolute;
        bottom: 0;
    }

    .dataTable-bottom {
        margin: 0px 18% !important;
    }

    .dataTable-info {
        margin: 12px 0;
    }

    .dataTable-bottom, .dataTable-top {
        padding: 0px 20px;
        font-size: 12px;
        margin-top: 2rem;
    }

    .dataTable-wrapper.no-footer .dataTable-container {
        border-bottom: none;
    }
</style>
<style>
    .avatar-upload {
        position: relative;
        max-width: 100%;
        height:50%;
        margin: 5px;
    }
    .avatar-upload .avatar-edit {
        position: absolute;
        right: 0px;
        z-index: 1;
        top: 0px;
        width: 100%;
        height: 100%;
    }
    .avatar-upload .avatar-edit input {
        display: none;
    }
    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 100%;
        height: 100%;
        margin-bottom: 0;
        background: transparent;
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
        background: transparent;
    }
    .avatar-upload .avatar-edit input + label:after {
        position: absolute;
        top: 6px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }
    .avatar-upload .avatar-preview {
        width: 100%;
        height: 100%;
        position: relative;
        font-size: 14px;
    }
    .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 0%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

</style>
<style>
    .modal-header {
        padding: 2rem 2rem 1rem 2rem;
    }
    .modal-body {
        padding: 0rem 2rem !important;
    }
    .modal-footer {
        background-color: transparent;
        border-top: 1px solid transparent;
    }
    .modal-header .close {
        padding: 0rem 0rem;
        margin: -1.5rem -1rem -1rem auto;
    }
    .form-group.input-material {
        position: relative;
        margin-top: 0px;
        margin-bottom: 26px;
    }
    .add-on-div{
        padding-top: 20px;
        margin: 20px 0;
    }
    .slim-scroll{
        height:100% !important;
    }
    .slimScrollDiv{
        height:100% !important;
    }
    .max-length_word {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 39px;
        display: inline-block;
        top: 2px;
        position: relative;
        right: 4px;
    }
    .vcenter {
        display: inline-block;
        vertical-align: middle;
        float: none;
    }

    .p_a_sub_add_on_img{
        background-color:white;
        color:#a7a796;
        border-radius:10px !important;
        vertical-align:center;
        text-align: center;
        padding-top: 30%;
    }
    .select2-container .select2-selection--multiple {
        border-top: 0px;
        border-right: 0px;
        border-bottom: 1px solid black;
        border-left: 0px;
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border-top: 0px;
        border-right: 0px;
        border-bottom: 1px solid black;
        border-left: 0px;
    }
    .span-slash span:before{
        content: ' / ';
    }

    .span-slash span:nth-child(1):before{
        content: '';
    }

    .nav-link.text-truncate.active ::after {
        content: url("http://127.0.0.1:8000/images/right-selected.svg");
        top: 1px;
        position: relative;
        margin-left: 10px;
    }

    .nav-link.active ::after {
        content: url("http://127.0.0.1:8000/images/right-selected.svg");
        top: 1px;
        position: relative;
        margin-left: 10px;
    }
</style>
<link rel="stylesheet" href="{{asset('css/landing-page.css')}}">
@endpush
@section('content')

    <div class="container-fluid">
            <div class="row" style="height: calc(100vh - 60px);">
                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 bg-black pt-3 menu-list-fixed" style="max-height: 95vh;overflow: auto">
                    <div class="slim-scroll">
                        <nav class="navbar-options menu-list-items" style="min-height: 83vh;">

                             {{--<ul class="nav md-pills pills-secondary flex-column">
                                 <span style="display:none;">{{$i=0}}</span>
                                 @foreach($items["menu"] ?? [] as $category)
                                     <li class="nav-item d-flex">
                                         <a class="mr-auto nav-link @if($i++==0) active @endif" data-toggle="tab"
                                            href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}"
                                            role="tab">{{$category['name']}}</a>

                                         <a onclick="edit_category('{{url('edit_seller_category',$category['id'])}}')">
                                             <img class="svg-invert" src="./images/card-category-edit.svg" alt="">
                                         </a>

                                         <a onclick="confirm_modal('{{url('sellerCategoryDestroy', $category['id'])}}');">
                                             <img class="svg-invert" src="./images/delete.svg" alt="">
                                         </a>

                                     </li>
                                 @endforeach
                             </ul>--}}
                            <div class="accordion" id="accordionExample">
                            <ul class="nav md-pills pills-secondary flex-column">
                             <span style="display:none;">{{$i=0}}</span>
                             @foreach($items["menu"] ?? [] as $key=>$category)
                                 @if($category["add_ons_count"] > 0)
                                        <li class="nav-item">
                                            <span class="d-flex">
                                                <a class="mr-auto nav-link collapsed text-truncate" href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu"
                                                   data-toggle="collapse"
                                                   data-target="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu">
                                                    <span class="">{{$category['name']}}</span></a>
                                                <a onclick="edit_category('{{url('edit_seller_category',$category['id'])}}')">
                                                    <img class="svg-invert" src="./images/card-category-edit.svg" alt="">
                                                </a>

                                                <a onclick="confirm_modal('{{url('sellerCategoryDestroy', $category['id'])}}');">
                                                    <img class="svg-invert" src="./images/delete.svg" alt="">
                                                </a>
                                            </span>
                                            <div class="collapse" id="{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu" data-parent="#accordionExample">
                                                <ul class="flex-column pl-2 nav" aria-expanded="false">
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($i++==0) active @endif" data-toggle="tab"
                                                           href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}"
                                                           role="tab">All Items</a>

                                                        <a class="nav-link @if($i++==0) active @endif" data-toggle="tab"
                                                           href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))."-add-on"}}"
                                                           role="tab">All Subcategories</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                     @else
                                        <li class="nav-item d-flex">
                                            <a class="mr-auto nav-link @if($i++==0) active @endif" data-toggle="tab"
                                               href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}"
                                               role="tab">{{$category['name']}}</a>
                                            <a onclick="edit_category('{{url('edit_seller_category',$category['id'])}}')">
                                                <img class="svg-invert" src="./images/card-category-edit.svg" alt="">
                                            </a>

                                            <a onclick="confirm_modal('{{url('sellerCategoryDestroy', $category['id'])}}');">
                                                <img class="svg-invert" src="./images/delete.svg" alt="">
                                            </a>
                                        </li>
                                     @endif
                             @endforeach
                         </ul>
                            </div>
                    </nav>
                        <div class="menu-list-items d-flex justify-content-end">
                        <button class="add-section mt-3" data-toggle="modal"
                                data-target="#add_section" style="position: relative;bottom: 20px;right: 0;">
                            + Add Category
                        </button>
                    </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 main-section-padding">
                @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                    @endif
                    @if(session()->has('failed'))
                        <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
                    @endif                
                    <div class="tab-content pt-0">
                        <span style="display:none;">{{$i=0}}</span>
                        @foreach($items["products"] ?? [] as $product)
                            <div class="tab-pane fade @if($i++==0)in show active @endif"
                                 id="{{strtolower(preg_replace('/\PL/u', '',$product["category"]))}}" role="tabpanel">
                                <div class="res-cards d-flex align-items-center justify-content-between pb-2">
                                    <h4 class="heading"><span id="selectedcatg">{{$product["category"]}}</span></h4>
                                    <div class="justify-content-end">
                                    <button class="add-btn" onclick="openpopupmodel();">+ Add Item</button>
                                    <button class="add-btn" onclick="open_add_on_popup_model();">+ Add Subcategory</button>
                                    {{--<a class="add-btn" href="{{url('sellerCategoryAddon')}}">+ get Subcategory</a>--}}
                                    </div>
                                </div>

                                <div class="tab-content pt-0">
                                    <div class="table-responsive">
                                        <table class="table" id="products">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>Item Name</th>
                                                <th>Available Days</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Description</th>
                                                <th>Photo</th>
                                                <th>Availability</th>
                                                <th>Actions</th>
                                                <th class="table-controls-button" id="table-button">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <ul class="controls-dropdown-box" id="controls-dropdown-menu"
                                                        style="display: none;"></ul>
                                                </th>

                                            </tr>
                                            </thead>
                                            @if(count($product["items"] ?? []) ==0 )
                                                <tbody>
                                                    <tr>
                                                        <td colspan="9">
                                                            No data available!
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            @else
                                                <tbody>
                                                @foreach($product["items"] ?? [] as $item)
                                                    <tr>
                                                        <td>
                                                            {{$item['name']}}
                                                        </td>
                                                        <td>
                                                            {{$item['name']}}
                                                        </td>
                                                        <td>
                                                            {{$product["category"]}}
                                                        </td>
                                                        <td>
                                                            <div class="d-flex">
                                                                <span>{{$item['price_symbol']}}</span><span>{{$item['base_price']}}</span>
                                                            </div>
                                                        </td>
                                                        <td>{{$item['description'] ?? ""}}
                                                        </td>
                                                        <td>
                                                            <img class="tbl-img" src="{{$item['thumbnail_img']}}"
                                                                 alt="">
                                                        </td>
                                                        <td>
                                                            <div class="checkbox checbox-switch switch-primary">
                                                                <label>
                                                                    <input type="checkbox" name=""
                                                                           {{($item['status']=='1')?"checked":""}} onclick="product_available_sts('{{url('productstatus', $item['id'].'@'.$item['status'])}}');"/>
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <a onclick="update_modal('{{json_encode($item)}}','{{url('item_edit')}}', '{{url('get_product_details', $item['id'])}}');">
                                                                <img src="./images/card-edit.svg" alt="">
                                                            </a>
                                                            <a onclick="confirm_modal('{{url('menu_destroy', $item['id'])}}');">
                                                                <img class="mr-2" src="./images/delete.svg" alt="">
                                                            </a>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                                <div class="row float-right">

                                    <div class="col-12">
                                        <div class="d-flex justify-content-between download f-12">
                                            <div class="d-flex ">
                                            </div>

                                            <div id="download_products" class="mouse-pointer">
                                                <span class="text-color-green mx-2 f-14 f-medium ">Download </span>
                                                <img src="./images/download.svg" alt="" style="width: 14px;height: 17px;">
                                                <div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endforeach
                        @foreach($items["add_ons"] ?? [] as $add_on)
                            <div class="tab-pane fade"
                                 id="{{strtolower(preg_replace('/\PL/u', '',$add_on["category"]))."-add-on"}}" role="tabpanel">
                                <div class="res-cards d-flex align-items-center justify-content-between pb-2">
                                    <h4 class="heading"><span id="selectedcatg">{{$add_on["category"]}}</span></h4>
                                    <div class="justify-content-end">
                                        <button class="add-btn" onclick="openpopupmodel();">+ Add Item</button>
                                        <button class="add-btn" onclick="open_add_on_popup_model();">+ Add Subcategory</button>
                                    </div>
                                </div>


                                    <div class="table-responsive">
                                        <table class="table" id="add_ons">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>Add On Name</th>
                                                <th>Category</th>
                                                <th>Add On Description</th>
                                                <th>Availability</th>
                                                <th>Actions</th>
                                       {{--     <th class="table-controls-button" id="table-button">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <ul class="controls-dropdown-box" id="controls-dropdown-menu"
                                                        style="display: none;"></ul>
                                                </th>--}}

                                            </tr>
                                            </thead>
                                            @if(count($add_on["add_ons_data"] ?? []) ==0 )
                                                <tbody>
                                                <tr>
                                                    <td colspan="6">
                                                        No data available!
                                                    </td>
                                                </tr>
                                                </tbody>
                                            @else
                                                <tbody>
                                                @foreach($add_on["add_ons_data"] ?? [] as $add_on_data)
                                                    <tr>
                                                        <td>
                                                            {{$add_on_data['name']}}
                                                        </td>
                                                        <td>
                                                            {{$add_on["category"]}}
                                                        </td>
                                                        <td>
                                                            {{$add_on_data['description'] ?? ""}}
                                                        </td>
                                                        <td>
                                                            <div class="checkbox checbox-switch switch-primary">
                                                                <label>
                                                                    <input type="checkbox" name=""
                                                                           {{($add_on_data['status']=='1')?"checked":""}} onclick="add_on_available_statuss('{{url('productstatus', $add_on_data['id'].'@'.$add_on_data['status'])}}');"/>
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <a id="{{json_encode($add_on_data)}}" onclick="edit_category_add_on_model(this.id);">
                                                                <img src="./images/card-edit.svg" alt="">
                                                            </a>
                                                            <a onclick="confirm_modal('{{url('categoryAddonDestroy', $add_on_data['id'])}}');">
                                                                <img class="mr-2" src="./images/delete.svg" alt="">
                                                            </a>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            @endif
                                        </table>
                                    </div>

                                <div class="row float-right">

                                    <div class="col-12">
                                        <div class="d-flex justify-content-between download f-12">
                                            <div class="d-flex ">
                                            </div>

                                            <div id="download_add_ons" class="mouse-pointer">
                                                <span class="text-color-green mx-2 f-14 f-medium ">Download </span>
                                                <img src="./images/download.svg" alt="" style="width: 14px;height: 17px;">
                                                <div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    <div id="product_based_add_on_div_d" class="hide">
        <div id="p_add_on_div_####" class="modifiactions-required  overlay">
            <div class="overlay-close mouse-pointer"
                 onclick="delete_product_add_on_row('p_add_on_div_','####');">
                <a class="hover-switch">
                    <img src="./images/delete-svg.svg" alt="X">
                    <img src="./images/delete.svg" alt="X">
                </a>
            </div>
            <div class="col-12">
                <div class="row justify-content-end">
                    <div class="col-4">
                        <label class="radio mx-3">Optional
                            <input type="radio" name="items[####][optional]" value="1" onclick="changeDisplay('PMaxNoOfChanges_','####',true)">
                            <span class="checkround"></span>
                        </label>
                    </div>
                    <div class="col-4">
                        <label class="radio">Required
                            <input type="radio" name="items[####][optional]" value="0" onclick="changeDisplay('PMaxNoOfChanges_','####',false)">
                            <span class="checkround"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group input-material mb-4">
                <input type="text" name="items[####][ModifierName]" class="form-control"
                       id="ModifierName_####" required>
                <label for="ModifierName_####">Modifier Name</label>
            </div>
            <div class="form-group input-material mb-4">
                <textarea class="form-control" name="items[####][ModifierDescription]"
                          id="ModifierDescription_####" rows="2"
                          data-validation="required" required></textarea>
                <label for="ModifierDescription_####">Modifier Description</label>
            </div>
            <div class="form-group input-material mb-4 hide" id="PMaxNoOfChanges_####">
                <input type="number" class="form-control" name="items[####][MaxNoOfChanges]"
                       id="MaxNoOfChanges_####">
                <label for="MaxNoOfChanges_####">Max number of changes</label>
            </div>
            <div class="mb-0 text-color-grey f-14 d-flex justify-content-between">
                <p>Options</p>
                <p class="mouse-pointer" onclick="add_p_a_sub_add_on('####')">+ Add row</p>
            </div>
            <div class="container-fluid p-0" id="product_sub_add_ons_####_!!!!">
                <div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay border"
                     id="p_a_sub_add_on_####_!!!!">
                    <div class="overlay-close mouse-pointer"
                         onclick="delete_product_add_on_row('p_a_sub_add_on_','####_!!!!');">
                        <a class="hover-switch">
                            <img src="./images/delete-svg.svg" alt="X">
                            <img src="./images/delete.svg" alt="X">
                        </a>
                    </div>
                    <div class="col-3 no-left-padding">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' id="PASAImageUpload_####_!!!!"
                                       name="items[####][sub_add_on][!!!!][add_on_image]"
                                       accept=".png, .jpg, .jpeg" required onchange="p_a_sub_add_on_image_upload(this,'####','!!!!')"/>
                                <label for="PASAImageUpload_####_!!!!"></label>
                            </div>
                            <div class="avatar-preview">
                                <div class="p_a_sub_add_on_img" id="PASAImagePreview_####_!!!!">
                                    + ICON
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="form-group input-material mr-2">
                            <input type="text" name="items[####][sub_add_on][!!!!][add_on_name]" class="form-control"
                                   id="PASAAddOnName_####_!!!!" required>
                            <label for="PASAAddOnName_####_!!!!">Item Name</label>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group input-material mr-2">
                                    <input type="text" name="items[####][sub_add_on][!!!!][add_on_price]"
                                           class="form-control"
                                           id="PASAAddOnPrice_####_!!!!" required>
                                    <label for="PASAAddOnPrice_####_!!!!">Price</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group input-material mr-2">
                                    <input type="text" name="items[####][sub_add_on][!!!!][add_on_discount]"
                                           class="form-control" id="PASAAddOnDiscount_####_!!!!" required>
                                    <label for="PASAAddOnDiscount_####_!!!!">Discount</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hide" id="add_on_div_d">
        <div class="modifiactions-required  row add-on-div overlay"
             id="a_sub_add_on_::::">
            <div class="overlay-close mouse-pointer"
                 onclick="delete_product_add_on_row('a_sub_add_on_','::::');">
                <a class="hover-switch">
                    <img src="./images/delete-svg.svg" alt="X">
                    <img src="./images/delete.svg" alt="X">
                </a>
            </div>
            <div class="col-3 no-left-padding">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload_::::"
                               name="items[::::][items_image]"
                               accept=".png, .jpg, .jpeg" required onchange="a_sub_add_on_image_upload(this,'::::')"/>
                        <label for="imageUpload_::::"></label>
                    </div>
                    <div class="avatar-preview">
                        <div class="p_a_sub_add_on_img" id="imagePreview_::::">
                            + ICON
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="form-group input-material mr-2">
                    <input type="text" name="items[::::][items_name]" class="form-control"
                           id="ASAAddOnName_::::" required>
                    <label for="ASAAddOnName_::::">Item Name</label>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group input-material mr-2">
                            <input type="text" name="items[::::][price]"
                                   class="form-control"
                                   id="ASAAddOnPrice_::::" required>
                            <label for="ASAAddOnPrice_::::">Price</label>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group input-material mr-2">
                            <input type="text" name="items[::::][discount]"
                                   class="form-control" id="ASAAddOnDiscount_::::" required>
                            <label for="ASAAddOnDiscount_::::">Discount</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hide" id="edit_add_on_div_d">
        <div class="modifiactions-required  row add-on-div overlay"
             id="edit_a_sub_add_on_::::">
            <div class="overlay-close mouse-pointer"
                 onclick="delete_product_add_on_row('edit_a_sub_add_on_','::::');">
                <a class="hover-switch">
                    <img src="./images/delete-svg.svg" alt="X">
                    <img src="./images/delete.svg" alt="X">
                </a>
            </div>
            <div class="col-3 no-left-padding">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="editImageUpload_::::"
                               name="items[::::][items_image]"
                               accept=".png, .jpg, .jpeg" required onchange="edit_a_sub_add_on_image_upload(this,'::::')"/>
                        <label for="editImageUpload_::::"></label>
                    </div>
                    <div class="avatar-preview">
                        <div class="p_a_sub_add_on_img" id="editImagePreview_::::">
                            + ICON
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="form-group input-material mr-2">
                    <input type="hidden" name="items[::::][items_id]" class="form-control"
                           id="EASAAddOnId_::::" value="0">
                    <input type="text" name="items[::::][items_name]" class="form-control"
                           id="EASAAddOnName_::::" required>
                    <label for="EASAAddOnName_::::">Item Name</label>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group input-material mr-2">
                            <input type="text" name="items[::::][price]"
                                   class="form-control"
                                   id="EASAAddOnPrice_::::" required>
                            <label for="EASAAddOnPrice_::::">Price</label>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group input-material mr-2">
                            <input type="text" name="items[::::][discount]"
                                   class="form-control" id="EASAAddOnDiscount_::::" required>
                            <label for="EASAAddOnDiscount_::::">Discount</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

    <!--Add Category Modal -->
    <div class="modal fade" id="add_section" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="panel-body">
                        <div class="bussiness-login">
                            <div class="bussiness-login-box">
                                <form action="{{url('addSellerCategory')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group input-material">
                                                <input type="text" id="category_name" name="name" class="form-control"
                                                       data-validation="required" required>
                                                <label for="name"><span style="color:red">*</span>Name</label>
                                                <div class="text-danger">{{ $errors->first('name') }}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group input-material">
                                                <input type="text" id="meta_title" name="meta_title"
                                                       class="form-control"
                                                       data-validation="required" required>
                                                <label for="meta_title"><span style="color:red">*</span>Meta
                                                    Title</label>
                                                <div class="text-danger">{{ $errors->first('meta_title') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-3">
                                            <div class="avatar-upload" style="height:100%;margin:0;">
                                                <div class="avatar-edit">
                                                    <input type='file' id="categoryImageUpload" name="icon"
                                                           accept=".png, .jpg, .jpeg"/>
                                                    <label for="categoryImageUpload"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="categoryImagePreview"
                                                         style="background-color:#eceff1;color:#a7a796;border-radius:10px;vertical-align:center;text-align: center;padding-top: 10px;font-size: 14px;font-weight: normal;">
                                                        + ICON
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="form-group input-material mt-0 mb-0">
                                                <textarea name="meta_description" rows="3" id="meta_description" class="form-control"
                                                          data-validation="required" required style="height:100px;"></textarea>
                                                <label for="meta_description"><span style="color:red">*</span>Description</label>
                                                <div class="text-danger">{{ $errors->first('meta_description') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-12">
                                            <div class="mt-3 mb-5 d-flex pull-right">

                                                <button class="model-cancelbtn w-100 ml-3 f-medium" data-dismiss="modal"
                                                        aria-label="Close">CANCEL
                                                </button>
                                                <button type="submit" class="model-addbtn w-100 ml-3 f-medium">SUBMIT
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Edit Category Modal -->
    <div class="modal fade" id="edit_section" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="panel-body">
                        <div class="bussiness-login">
                            <div class="bussiness-login-box">
                                <form action="{{url('updateSellerCategory')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group input-material">
                                                <input type="hidden" class="form-control" name="id" id="category_id">
                                                <input type="text" id="category_name_edit" name="name" class="form-control"
                                                       data-validation="required" required>
                                                <label for="name"><span style="color:red">*</span>Name</label>
                                                <div class="text-danger">{{ $errors->first('name') }}</div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group input-material">
                                                <input type="text" id="meta_title_edit" name="meta_title"
                                                       class="form-control"
                                                       data-validation="required" required>
                                                <label for="meta_title"><span style="color:red">*</span>Meta
                                                    Title</label>
                                                <div class="text-danger">{{ $errors->first('meta_title') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-3">
                                            <div class="avatar-upload" style="height:100%;margin:0;">
                                                <div class="avatar-edit">
                                                    <input type='file' id="categoryEditImageUpload" name="icon"
                                                           accept=".png, .jpg, .jpeg"/>
                                                    <label for="categoryEditImageUpload"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div id="categoryEditImagePreview"
                                                         style="background-color:#eceff1;color:#a7a796;border-radius:10px;vertical-align:center;text-align: center;padding-top: 10px;font-size: 14px;font-weight: normal;">
                                                        + ICON
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="form-group input-material mt-0 mb-0">
                                                <textarea name="meta_description" rows="3" id="meta_description_edit" class="form-control"
                                                          data-validation="required" required style="height:100px;"></textarea>
                                                <label for="meta_description"><span style="color:red">*</span>Description</label>
                                                <div class="text-danger">{{ $errors->first('meta_description') }}</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-12">
                                            <div class="mt-3 mb-5 d-flex pull-right">

                                                <button class="model-cancelbtn w-100 ml-3 f-medium" data-dismiss="modal"
                                                        aria-label="Close">CANCEL
                                                </button>
                                                <button type="submit" class="model-addbtn w-100 ml-3 f-medium">UPDATE
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Add Category Add Ons Modal -->
    <div class="modal add-menu right fade " id="add_category_add-ons" tabindex="-1" role="dialog"
         aria-labelledby="add_category_add-ons" aria-hidden="true">
        <div class="modal-dialog" id="add_new_category_add_ons" role="add menu">
            <div class="modal-content model-width slim-scroll">
                <div class="overflow-dialog" style="padding:0 0.25rem !important;">
                    <div class="slim-scroll" style="padding:1rem;">
                <form id="category-add-on-model" action="{{url('add-category-addon')}}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                <div class="modal-header">
                    <div class="f-20 f-medium">Add Subcategory</div>
                </div>

                <div class="modal-body" style="min-height:78vh;">
                    <div>

                            <div class="form-group input-material">
                                <select name="section_name" id="section_name" class="form-control"
                                        data-validation="required" required>
                                    @foreach($categories as $category)
                                        <option value="{{$category['id']}}" @php if($category['id'] == request()->get('search')) echo "selected"; @endphp>{{$category['name']}}</option>
                                    @endforeach$category
                                </select>
                                <label for="itemSearch" style="margin-top: -15px;"><span style="color:red;">*</span>Section
                                    Name</label>
                                <div class="text-danger" style="top: -12px;">{{ $errors->first('section_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <input type="text" class="form-control" name="subcategory_name" id="subcategory-name"
                                       data-validation="required" required>
                                <label for="textarea-field"><span style="color:red">*</span>Subcategory Name</label>
                                <div class="text-danger">{{ $errors->first('subcategory_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                    <textarea class="form-control" name="subcategory_description"
                                              id="subcategory-description" rows="2"
                                              data-validation="required" required></textarea>
                                <label for="textarea-field"><span style="color:red">*</span>
                                    Subcategory Description</label>
                                <div class="text-danger">{{ $errors->first('subcategory_description') }}</div>
                            </div>
                            <div>
                                <lable>Items</lable>
                            </div>

                            <div>
                                <div id="category_add_ons_sub_add_ons">

                                    <div class="modifiactions-required addonsstaticsubdiv row add-on-div overlay"
                                         id="a_sub_add_on_0">
                                        {{--<div class="overlay-close mouse-pointer"
                                             onclick="delete_product_add_on_row('a_sub_add_on_','0');">
                                            <a class="hover-switch">
                                                <img src="./images/delete-svg.svg" alt="X">
                                                <img src="./images/delete.svg" alt="X">
                                            </a>
                                        </div>--}}
                                        <div class="col-3 no-left-padding">
                                            <div class="avatar-upload">
                                                <div class="avatar-edit">
                                                    <input type='file' id="imageUpload_0"
                                                           name="items[0][items_image]"
                                                           accept=".png, .jpg, .jpeg" onchange="a_sub_add_on_image_upload(this,0)"/>
                                                    <label for="imageUpload_0"></label>
                                                </div>
                                                <div class="avatar-preview">
                                                    <div class="p_a_sub_add_on_img" id="imagePreview_0">
                                                        + ICON
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" name="items[0][items_name]" class="form-control"
                                                       id="ASAAddOnName_0" required>
                                                <label for="ASAAddOnName_0">Item Name</label>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="text" name="items[0][price]"
                                                               class="form-control"
                                                               id="ASAAddOnPrice_0" required>
                                                        <label for="ASAAddOnPrice_0">Price</label>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group input-material mr-2">
                                                        <input type="text" name="items[0][discount]"
                                                               class="form-control" id="ASAAddOnDiscount_0" required>
                                                        <label for="ASAAddOnDiscount_0">Discount</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="add_more d-flex align-items-center justify-content-center text-color-grey" onclick="add_a_sub_add_on()">
                                    + ADD MORE
                                </div>
                            </div>
                        </div>
                </div>

                <div class="modal-footer row mb-4 mt-4" style="display:block ruby;padding: 0rem 2rem !important;">
                    <div class="col-6" style="margin:0;">
                        <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close"
                                style="width:100%;">
                            CANCEL
                        </button>
                    </div>
                    <div class="col-6" style="margin:0;">
                        <button type="submit" class="addbtn" id="addbtn" style="width:100%;">
                            ADD SUBCATEGORY
                        </button>
                    </div>
                </div>
                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Edit Category Add-ons Modal -->
    <div class="modal add-menu right fade " id="edit_category_add-ons" tabindex="-1" role="dialog"
         aria-labelledby="edit_category_add-ons" aria-hidden="true">
        <div class="modal-dialog" id="edit_category_add_on" role="add menu">
            <div class="modal-content model-width slim-scroll">
                <div class="overflow-dialog" style="padding:0 0.25rem !important;">
                    <div class="slim-scroll" style="padding:1rem;">
                <form id="category-add-on-model" action="{{url('edit-category-addon')}}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <div class="f-20 f-medium">Edit Subcategory</div>
                    </div>

                    <div class="modal-body" style="min-height:78vh;">
                        <div>
                            <div class="form-group input-material">
                                <input type="hidden" class="form-control" name="id" id="editcategoryaddonid"
                                       data-validation="required">
                                <select name="section_name" id="edit_section_name" class="form-control"
                                        data-validation="required" required>
                                    @foreach($categories ?? [] as $category)
                                        <option value="{{$category['id']}}" @php if($category['id'] == request()->get('search')) echo "selected"; @endphp>{{$category['name']}}</option>
                                    @endforeach$category
                                </select>
                                <label for="itemSearch"><span style="color:red">*</span>Section Name</label>
                                <div class="text-danger">{{ $errors->first('section_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <input type="text" class="form-control" name="subcategory_name" id="edit_subcategory-name"
                                       data-validation="required" required>
                                <label for="textarea-field"><span style="color:red">*</span>Subcategory Name</label>
                                <div class="text-danger">{{ $errors->first('subcategory_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                    <textarea class="form-control" name="subcategory_description"
                                              id="edit_subcategory-description" rows="2"
                                              data-validation="required" required></textarea>
                                <label for="textarea-field"><span style="color:red">*</span>Subcategory
                                    Description</label>
                                <div class="text-danger">{{ $errors->first('subcategory_description') }}</div>
                            </div>
                            <div>
                                <lable>Items</lable>
                            </div>

                            <div>
                                <div id="edit_category_add_ons_sub_add_ons">


                                </div>
                                <div class="add_more d-flex align-items-center justify-content-center text-color-grey" onclick="edit_add_a_sub_add_on()">
                                    + ADD MORE
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer row mb-4 mt-4" style="display:block ruby;padding: 0rem 2rem !important;">
                        <div class="col-6" style="margin:0;">
                            <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close"
                                    style="width:100%;">
                                CANCEL
                            </button>
                        </div>
                        <div class="col-6" style="margin:0;">
                            <button class="addbtn" type="submit" id="addbtn" style="width:100%;">
                                <span class="addonbtn"></span> UPDATE {{--SUBCATEGORY--}}
                            </button>
                        </div>
                    </div>
                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Archive Modal -->
    <div class="modal fade" id="archieve" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" id="categorymodel">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to archieve the item “Chicken Periperi"</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO
                        </button>
                        <button class="addbtn w-100 mx-3 f-medium" data-dismiss="modal">YES</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--toggle button modal for products -->
    <div class="modal fade" id="product_sts" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to change item status ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">
                        <button class="cancelbtn w-100 mx-3 f-medium" onclick="itemAvaailableCancelModel();">NO</button>
                        <a id="productsts_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Delete Modal -->
    <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to delete it ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO
                        </button>
                        <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Add Product Modal -->
    <div class="modal add-menu right fade " id="add_item" tabindex="-1" role="dialog" aria-labelledby="add-menu">
        <div class="modal-dialog" id="add_new_item" role="document">
            <div class="modal-content model-row-column">
                <div class="modal-body pb-4 dialog-options-responsive" style="padding:0 !important;overflow: hidden;">

                    <div class="bg-black text-color-white negative-margin overlay" style="display:none;" id="categories_data">
                            <div class="overlay-close mouse-pointer"
                                 onclick="close_recommended_div(false)">
                                <a class="hover-switch">
                                    <img src="./images/right-selected.svg" alt=">">
                                    <img class="svg-invert" src="./images/right-selected.svg" alt=">">
                                </a>
                            </div>
                            <nav class="navbar-options menu-list-items d-flex slim-scroll">
                                <ul class="nav flex-column flex-nowrap ">
                                    <li class="nav-item">
                                        @foreach($items["products"] ?? [] as $key=>$category)
                                            @if(count($category["items"]) >0)
                                            <a class="nav-link text-truncate" onclick="get_category_products({{json_encode($category)}});">
                                                <span class="menu-link ">{{$category['category']}} </span>
                                            </a>
                                            @endif
                                        @endforeach
                                    </li>
                                </ul>
                                <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                    <li class="nav-item" id="category_product_list">
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    <div class="bg-black text-color-white negative-margin overlay" style="display:none;" id="category_add_ons">
                            <div class="overlay-close mouse-pointer"
                                 onclick="close_category_add_on_div(false)">
                                <a class="hover-switch">
                                    <img src="./images/right-selected.svg" alt=">">
                                    <img class="svg-invert" src="./images/right-selected.svg" alt=">">
                                </a>
                            </div>
                            <nav class="navbar-options menu-list-items d-flex slim-scroll">
                                <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                    <li class="nav-item" id="category_based_add_ons">

                                        @foreach($categories as $category)
                                            <div class="nav-link text-truncate active-links">
                                                <label class="check">{{$category['name']}}
                                                    <input type="checkbox" name="addonnameid" onclick="add_add_on_to_product('{{$category['id']."@@".$category['name']}}', 'ADD');" id="cat_add_on{{$category['id']}}">
                                                    <span class="checkmark"></span>
                                                </label>
                                                {{--<span class="menu-link "> <label for="{{$category['name']}}"></label></span>--}}
                                            </div>
                                        @endforeach
                                    </li>
                                </ul>
                                <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                    <li class="nav-item" id="category_add_on_sub_div">
                                    </li>
                                </ul>
                            </nav>
                        </div>

                    <div class="bg-black text-color-white negative-margin overlay" style="display:none;" id="combo_menu">
                            <div class="overlay-close mouse-pointer"
                                 onclick="close_combo_div(false)">
                                <a class="hover-switch">
                                    <img src="./images/right-selected.svg" alt=">">
                                    <img class="svg-invert" src="./images/right-selected.svg" alt=">">
                                </a>
                            </div>
                            <nav class="navbar-options menu-list-items d-flex slim-scroll">
                            <ul class="nav flex-column flex-nowrap ">
                                <li class="nav-item">

                                    @foreach($items["products"] ?? [] as $key=>$category)
                                        @if(count($category["items"]) >0)
                                            <a class="nav-link text-truncate" onclick="get_combo_category_products({{json_encode($category)}});">
                                                <span class="menu-link ">{{$category['category']}} </span>
                                            </a>
                                        @endif
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="combo_category_products">
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="combo_category_product_add_ons">
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="combo_category_product_a_sub_add_ons">
                                </li>
                            </ul>
                        </nav>
                        </div>

                    <div class="overflow-dialog slim-scroll" style="padding:0 0.25rem !important;">
                        <div class="slim-scroll" style="padding:1rem;">
                        <form id="add_item_form" action="{{url('addItem')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="f-20 f-medium" style="padding-top: 0.5rem;">Add Item</div>
                            <div class="form-group input-material mt-3">
                                <input type="hidden" class="form-control" name="id" id="id">
                                <input type="text" class="form-control" name="name" id="AdditemName" required data-validation="required">
                                <label for="itemName">Item Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <textarea class="form-control" name="description" id="description" rows="2" data-validation="required" required></textarea>
                                <label for="textarea-field">Item Description<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey my-2" >
                                0 / 40</div>
                            <div class="upload-image-section">
                                <img src="" alt="" id="newimage-preview" class="img-fluid" height="height:100px;">
                                <label class="btn btn-default btn-sm center-block btn-file">
                                    <span class="text-color-grey">+ ADD IMAGE (500px X 600px)<span style="color:red">*</span></span>
                                    <input type="file" name="photos" id="newphotos" style="display: none;" onchange="newreadURL(this);" accept=".png, .jpg, .jpeg" required>
                                </label>
                                <div class="text-danger" id="newimagesizevaliderror"></div>
                            </div>

                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-12" id="inputStateselect">
                                            <div class="form-group ">
                                                <label class="control-label" for="Available Days" style="padding:0px;margin-bottom:0px;">Available Days<span style="color:red">*</span></label>
                                                <select name="available_days[]" id="available_days" multiple="multiple"
                                                        class="form-control demo-select2" data-validation="required" required>
                                                    <option value="0">Sun</option>
                                                    <option value="1">Mon</option>
                                                    <option value="2">Tue</option>
                                                    <option value="3">Wed</option>
                                                    <option value="4">Thu</option>
                                                    <option value="5">Fri</option>
                                                    <option value="6">Sat</option>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="menu_items_desc" id="menu_items_desc" class="form-control"
                                                        data-validation="required" required onchange="change_category_add_on_div(this.value,'{{getUrl('SELLERCATEGORYADDONS')}}');">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category['id']}}" @php if($category['id'] == request()->get('search')) echo "selected"; @endphp>{{$category['name']}}</option>
                                                    @endforeach$category
                                                </select>
                                                <label for="Menu For">Category<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select id="cuisine_id" name="cuisine_id" class="form-control" required>

                                                    @foreach ($cuisines ?? [] as $cuisine)
                                                        <option value="{{$cuisine["id"]}}">{{$cuisine["name"]}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                    <div class="text-danger">{{ $errors->first('cuisines') }}</div>
                                                @endif
                                                <label for="Cusine Type">Cuisine Type</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" step="0.01" name="unit_price" class="form-control"
                                                       id="unit_price" data-validation="required" required>
                                                <label for="ItemPrice">Item Price<span
                                                            style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('unit_price') }}</div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" name="set_max_order_number" class="form-control"
                                                       id="set_max_order_number" data-validation="required"
                                                       required>
                                                <label for="Set Max Order Number">Set Max Order Number</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Availability Status</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="1" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Not Available
                                                <input type="radio" name="available_ststus" id="available_ststus" value="0">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Item Type<span style="color:red">*</span></p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Vegetarian
                                                <input type="radio" name="veg_nonveg" id="veg" value="1"
                                                       data-validation="required" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Non Vegetarian
                                                <input type="radio" name="veg_nonveg" id="nonveg" value="2"
                                                       data-validation="required" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="mb-3">
                                    <label class="check">Healthy Picks
                                        <input type="checkbox" name="healthpicks" id="healthpicks" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">
                                </div>
                            </div>
                            <div>
                                <div class="mb-3">
                                    <label class="check">Recommended
                                        <input type="checkbox" name="featured" id="featured" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">
                                </div>
                            </div>
                            <div>
                                <div class="mb-3">
                                    <div class="check">
                                        <label class="mouse-pointer no-padding"  onclick="show_combo_div(false)">Combo Items</label>
                                        <label class="mouse-pointer">
                                            <input type="checkbox" name="combo" id="combo" value="1">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="flex">

                                    <div>
                                        <div class="d-flex align-items-center flex-wrap">
                                            <div id="combo_selected"></div>
                                            <input type="hidden" name="products_for_combo[]" id="products_for_combo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="categoryAddOnDiv">
                                <div>
                                    <div class="mb-3">
                                        <div class="check">
                                            <label class="mouse-pointer no-padding" onclick="show_category_add_on_div()">Add Category Add-Ons</label>
                                            <label class="mouse-pointer"></label>
                                            <label class="mouse-pointer">
                                                <input class="hide" type="checkbox" name="category_addon" id="category_addon" value="1">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="flex">

                                        <div>
                                            <div class="d-flex align-items-center flex-wrap">
                                                <div id="category_add_on_selected"></div>
                                                <input type="hidden" name="category_add_on_for_product[]" id="category_add_on_for_product">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="mb-3">
                                        <label class="check">Modifier Required
                                            <input type="checkbox" name="Modifier" id="newModifiercheck" value="true">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="flex">
                                    </div>
                                </div>

                                <div id="newaddons_div">
                                    <div id="product_based_add_on_div">
                                    </div>
                                    <div class="add_more d-flex align-items-center justify-content-center text-color-grey" onclick="add_p_add_on()">
                                        + ADD MORE
                                    </div>
                                </div>

                                <div>
                                    <div class="mb-3">
                                        <div class="check">
                                            <label class="mouse-pointer no-padding" onclick="show_recommended_div()">Recommended Pairing</label>
                                            <label class="mouse-pointer">
                                                <input class="hide"  type="checkbox" name="recommended" id="newrecommended" value="1">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="flex">
                                        <div>
                                            <div class="d-flex align-items-center flex-wrap">
                                                <div id="selected_recommended_items"></div>
                                                <input type="hidden" name="recommendedproducts[]" id="recommended_products">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-5 d-flex align-items-center justify-content-between">
                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close" onclick="close()" style="width:45%;">CANCEL</button>
                                <button class="addbtn" id="addbtn" style="width:45%;">ADD ITEM</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Product Modal -->
    <div class="modal add-menu right fade " id="edit_item" tabindex="-1" role="dialog" aria-labelledby="add-menu">
        <div class="modal-dialog" id="edit_old_item" role="document">
            <div class="modal-content model-row-column">
                <div class="modal-body pb-4 dialog-options-responsive" style="padding:0 !important;overflow: hidden;">

                    <div class="bg-black text-color-white negative-margin" style="display:none;" id="categories_data_for_item_edit">
                        <nav class="navbar-options menu-list-items d-flex slim-scroll">
                            <ul class="nav flex-column flex-nowrap ">
                                <li class="nav-item">

                                    @foreach($items["products"] ?? [] as $key=>$category)
                                        @if(count($category["items"]) >0)
                                            <a class="nav-link text-truncate" onclick="get_category_products_for_item_edit({{json_encode($category)}});">
                                                <span class="menu-link ">{{$category['category']}} </span>
                                            </a>
                                        @endif
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="category_product_list_for_item_edit">
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="bg-black text-color-white negative-margin" style="display:none;" id="category_add_ons_for_item_edit">
                        <nav class="navbar-options menu-list-items d-flex slim-scroll">
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="category_based_add_ons">

                                    @foreach($categories as $category)
                                        <div class="nav-link text-truncate active-links">
                                            <label class="check">{{$category['name']}}
                                                <input type="checkbox" name="addonnameid" onclick="add_add_on_to_product('{{$category['id']."@@".$category['name']}}', 'EDIT');" id="cat_add_on_for_item_edit{{$category['id']}}">
                                                <span class="checkmark"></span>
                                            </label>
                                            {{--<span class="menu-link "> <label for="{{$category['name']}}"></label></span>--}}
                                        </div>
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="category_add_on_sub_div_for_item_edit">
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="bg-black text-color-white negative-margin" style="display:none;" id="combo_menu_for_item_edit">
                        <nav class="navbar-options menu-list-items d-flex slim-scroll">
                            <ul class="nav flex-column flex-nowrap ">
                                <li class="nav-item">

                                    @foreach($items["products"] ?? [] as $key=>$category)
                                        @if(count($category["items"]) >0)
                                            <a class="nav-link text-truncate" onclick="get_combo_category_products_for_item_edit({{json_encode($category)}});">
                                                <span class="menu-link ">{{$category['category']}} </span>
                                            </a>
                                        @endif
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="combo_category_products_for_item_edit">
                                </li>
                            </ul>
                            <ul class="nav flex-column flex-nowrap menu-list-items-options">
                                <li class="nav-item" id="combo_category_product_add_ons_for_item_edit">
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="overflow-dialog" style="padding:0 0.5rem !important;">
                        <div class="slim-scroll" style="padding:1rem;">
                        <form id="edit_item_form" action="{{url('item_edit')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="f-20 f-medium" style="padding-top: 0.5rem;">Edit Item</div>
                            <div class="form-group input-material mt-3">
                                <input type="hidden" class="form-control" name="id" id="editItemid" required>
                                <input type="text" class="form-control" name="name" id="editItemName"
                                       data-validation="required" required>
                                <label for="editItemName">Item Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <textarea class="form-control" name="description" id="editItemDescription" rows="2" data-validation="required" required></textarea>
                                <label for="textarea-field">Item Description<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            </div>

                            <div class="d-flex justify-content-end f-10 text-color-grey my-2">
                                0 / 40
                            </div>

                            <div class="upload-image-section">
                                <img src="" alt="" id="image-preview" class="img-fluid" height="height:100px;">
                                <label class="btn btn-default btn-sm center-block btn-file">
                                    <span class="text-color-grey">+ ADD IMAGE (500px X 600px)<span
                                                style="color:red">*</span></span>
                                    <input type="file" name="photos" id="photos" style="display: none;"
                                           onchange="readURL(this);" accept=".png, .jpg, .jpeg">
                                </label>
                                <div class="text-danger" id="imagesizevaliderror"></div>
                            </div>

                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-12" id="inputStateselect">
                                            <div class="form-group ">
                                                <label for="Available Days">Available Days<span style="color:red">*</span></label>
                                                <select name="available_days[]" id="editItemavailable_days" multiple="multiple"
                                                        class="form-control demo-select2" data-validation="required" required>
                                                    <option value="0">Sun</option>
                                                    <option value="1">Mon</option>
                                                    <option value="2">Tue</option>
                                                    <option value="3">Wed</option>
                                                    <option value="4">Thu</option>
                                                    <option value="5">Fri</option>
                                                    <option value="6">Sat</option>
                                                </select>
                                                <div class="linebottom"></div>
                                                <div class="text-danger">{{ $errors->first('available_days') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" step="0.01" name="unit_price" class="form-control"
                                                       id="editItemunit_price" required>
                                                <label for="ItemPrice">Item Price<span
                                                            style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('unit_price') }}</div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="number" name="set_max_order_number" class="form-control"
                                                       id="editItemmSet_max_order_number"
                                                       required>
                                                <label for="Set Max Order Number">Set Max Order Number<span
                                                            style="color:red">*</span></label>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select name="menu_items_desc" id="edit_menu_items_desc" class="form-control"
                                                        data-validation="required" required onchange="change_category_add_on_div(this.value,'{{getUrl('SELLERCATEGORYADDONS')}}');">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category['id']}}" >{{$category['name']}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="Menu For">Category<span style="color:red">*</span></label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>


                                        <div class="col-12 col-sm-6" id="inputStateselect">
                                            <div class="form-group ">
                                                <select id="editItemmCuisine_id" name="cuisine_id" class="form-control">
                                                    @foreach ($cuisines ?? [] as $cuisine)
                                                        <option value="{{$cuisine["id"]}}">{{$cuisine["name"]}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('cuisines'))
                                                    <div class="text-danger">{{ $errors->first('cuisines') }}</div>
                                                @endif
                                                <label for="Cusine Type">Cuisine Type</label>
                                                <div class="linebottom"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Availability Status</p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Available
                                                <input type="radio" name="available_ststus" id="available_ststus"
                                                       value="1" checked>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Not Available
                                                <input type="radio" name="available_ststus" id="available_ststus"
                                                       value="0">
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 mb-3">
                                <p class="mb-1 text-color-grey f-14">Type<span style="color:red">*</span></p>
                                <div class="container-fluid p-0">
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="radio">Veg.
                                                <input type="radio" name="veg_nonveg" id="veg" value="1" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                        <div class="col-6">
                                            <label class="radio">Non-Veg.
                                                <input type="radio" name="veg_nonveg" id="nonveg" value="2" required>
                                                <span class="checkround"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="mb-3">
                                    <label class="check">Healthy Picks
                                        <input type="checkbox" name="healthpicks" id="healthpicks" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">
                                </div>
                            </div>
                            <div>
                                <div class="mb-3">
                                    <label class="check">Recommended
                                        <input type="checkbox" name="featured" id="featured" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">
                                </div>
                            </div>
                            <div>
                                <div class="mb-3">
                                    <label class="check">Combo Items
                                        <input type="checkbox" name="combo" id="combo_for_item_edit" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="flex">

                                    <div>
                                        <div class="d-flex align-items-center flex-wrap">
                                            <div id="combo_selected_for_item_edit"></div>
                                            <input type="hidden" name="products_for_combo[]" id="products_for_combo_for_item_edit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="categoryAddOnDiv">
                                <div>
                                    <div class="check">
                                        <label class="mouse-pointer no-padding" onclick="show_category_add_on_div_for_item_edit()">Add Category Add-Ons</label>
                                        <label class="mouse-pointer"></label>
                                        <label class="mouse-pointer">
                                            <input class="hide" type="checkbox" name="category_addon" id="category_add_on_for_item_edit" value="1">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>

                                    <div class="flex">

                                        <div>
                                            <div class="d-flex align-items-center flex-wrap">
                                                <div id="category_add_on_selected_for_item_edit"></div>
                                                <input type="hidden" name="category_add_on_for_product[]" id="category_add_on_for_product_for_item_edit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="mb-3">
                                        <label class="check">Modifier Required
                                            <input type="checkbox" name="Modifier" id="modifier_check_for_item_edit" value="1">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="flex">
                                    </div>
                                </div>

                                <div id="add_ons_div_for_item_edit">
                                    <div id="product_based_add_on_div_for_item_edit">

                                    </div>
                                    <div class="add_more d-flex align-items-center justify-content-center text-color-grey" onclick="add_p_add_on_for_item_edit()">
                                        + ADD MORE
                                    </div>
                                </div>

                                <div>
                                    <div class="mb-3">
                                        <div class="check">
                                            <label class="mouse-pointer no-padding" onclick="show_recommended_div_for_item_edit()">Recommended Pairing</label>
                                            <label class="mouse-pointer">
                                                <input class="hide"  type="checkbox" name="recommended" id="recommended_for_item_edit" value="1">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="flex">

                                        <div>
                                            <div class="d-flex align-items-center flex-wrap">
                                                <div id="selected_recommended_items_edit"></div>
                                                <input type="hidden" name="recommendedproducts[]" id="recommended_products_for_item_edit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close">CANCEL
                                </button>
                                <button class="addbtn" id="addbtn">UPDATE ITEM</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')

    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>

    <script>
        function checkValue(element) {
            // check if the input has any value (if we've typed into it)
            if ($(element).val())
                $(element).addClass('has-value');
            else
                $(element).removeClass('has-value');
        }

        $(document).ready(function () {
            // Run on page load
            $('.form-control').each(function () {
                checkValue(this);
            })
            // Run on input exit
            $('.form-control').blur(function () {
                checkValue(this);
            });

        });
    </script>
    {{--Item div script--}}
    <script>
        var x = document.getElementById('combo_menu');
        var y = document.getElementById('category_add_ons');
        var z = document.getElementById('categories_data');

        $(document).ready(function () {
            $("#combo").click(function () {
                var isChecked = $("#combo").is(":checked");
                if (isChecked) {
                    show_combo_div(true);
                } else {
                    close_combo_div(true);
                }
            });

            $("#category_addon").click(function () {
                var isChecked = $("#category_addon").is(":checked");
                if (isChecked) {
                    show_category_add_on_div();
                } else {
                    close_category_add_on_div(true);
                }
            });

            $("#newrecommended").click(function () {
                var isChecked = $("#newrecommended").is(":checked");
                if (isChecked) {
                    show_recommended_div();
                } else {
                    close_recommended_div(true);
                }
            });
            $("#edit_category_addon").click(function () {
                var isChecked = $("#edit_category_addon").is(":checked");
                var x = document.getElementById('combo_menu');
                var y = document.getElementById('category_add_ons');
                var z = document.getElementById('showSearchDiv');
                console.log(JSON.stringify(isChecked));
                if (isChecked) {
                    change_category_add_on_div(document.getElementById('edit_menu_items_desc').value,"{{getUrl('SELLERCATEGORYADDONS')}}");
                    x.style.display = 'none';
                    y.style.display = 'block';
                    z.style.display = 'none';
                } else {
                    x.style.display = 'none';
                    y.style.display = 'none';
                    z.style.display = 'none';
                    $('#category_edit_on_selected').html('');
                    $('#category_edit_on_for_product').val('');
                    $('#category_edit_on_for_product').attr('value', '');
                    product_category_add_on = [];
                }
            });

            $("#newrecommended").click(function () {
                var isChecked = $("#newrecommended").is(":checked");
                if (isChecked) {
                    show_recommended_div();
                } else {
                    close_recommended_div(true);
                }
            });
        });

        function show_recommended_div(){
            x.style.display = 'none';
            y.style.display = 'none';
            z.style.display = 'block';
            $('#category_product_list').html('');
        }

        function close_recommended_div(close){
            if(close) {
                $('#selected_recommended_items').html('');
                new_recommended_product = [];
            }
            var recommended_item = $("#newrecommended").is(":checked");
            var category_add_on = $("#category_addon").is(":checked");
            if(recommended_item && category_add_on){
                x.style.display = 'none';
                y.style.display = 'none';
                z.style.display = 'none';
            }else {
                if (category_add_on) {
                    show_category_add_on_div();
                } else {
                    x.style.display = 'none';
                    y.style.display = 'none';
                    z.style.display = 'none';

                }
            }
        }

        function show_category_add_on_div(){
            change_category_add_on_div(document.getElementById('menu_items_desc').value,"{{getUrl('SELLERCATEGORYADDONS')}}");
            x.style.display = 'none';
            y.style.display = 'block';
            z.style.display = 'none';
        }

        function close_category_add_on_div(close){
            if(close) {
                $('#category_add_on_selected').html('');
                product_category_add_on = [];
                product_category_add_on_length = 0;
            }
            var recommended_item = $("#newrecommended").is(":checked");
            var category_add_on = $("#category_addon").is(":checked");
            if(recommended_item && category_add_on){
                x.style.display = 'none';
                y.style.display = 'none';
                z.style.display = 'none';
            }else {
                if (recommended_item) {
                    show_recommended_div();
                } else {
                    x.style.display = 'none';
                    y.style.display = 'none';
                    z.style.display = 'none';
                }
            }
        }

        function show_combo_div(condition){
            x.style.display = 'block';
            y.style.display = 'none';
            z.style.display = 'none';
            $('#combo_category_products').html('');
            $('#combo_category_product_add_ons').html('');
            if(condition){
                document.getElementById('categoryAddOnDiv').style.display = 'none';
                document.getElementById('category_addon').checked = false;
            }
        }

        function close_combo_div(close) {
            if(close) {
                $('#combo_selected').html('');
                combo_item_product = [];
                p_combo_item_count = 0;
                combo_product_category_add_on = [];
                combo_product_category_add_on_length = 0;
                document.getElementById('category_addon').checked = false;
                document.getElementById('categoryAddOnDiv').style.display = 'block';
                close_category_add_on_div(true);
                close_recommended_div(true);
            }
            x.style.display = 'none';
            y.style.display = 'none';
            z.style.display = 'none';

        }

    </script>

    <script>

        $(document).ready(function () {
            $("#recommended").click(function () {
                var isChecked = $("#recommended").is(":checked");
                var x = document.getElementById('showSearchDiv');
                if (isChecked) {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#download_products").click(function () {
                var date = new Date();
                TableToExcel.convert(document.getElementById("products"), {

                    name: "Products_" + date + ".xlsx",
                    sheet: {
                        name: "Products"
                    }
                });
            });
            $("#close").click(function () {
                $('#newimage-preview').attr('src', '');

            });
            $("#addbtn").click(function () {
                $('#newimage-preview').attr('src', '');

            });
            $('.slim-scroll').slimScroll();
            /*$('#products').dataTable( {
                "language": {
                    "emptyTable": "No data available in table",
                    "infoEmpty": "No entries to show"
                }
            } );*/
        });


        function deleterow(val, param) {
            console.log(val);
            $("." + val).remove();
        }
        function deleteaddonrow(val, param) {
            console.log(val);
            $("." + val).remove();
        }

        function delete_product_add_on_row(id,val) {
            $("#"+id+ val).remove();
        }
    </script>

    <script src="{{asset('js/custom/categories.js')}}"></script>
    <script src="{{asset('js/custom/category-add-ons.js')}}"></script>
    <script src="{{asset('js/custom/items.js')}}"></script>

    {{--category image preview--}}
    <script>
        function readCategoryImageUpload(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#categoryImagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#categoryImagePreview').hide();
                    $('#categoryImagePreview').fadeIn(650);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#categoryImageUpload").change(function() {
            readCategoryImageUpload(this);
            $("#categoryImagePreview").html('');
        });
    </script>
    {{--category edit image preview--}}
    <script>
        function readEditCategoryImageUpload(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#categoryEditImagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#categoryEditImagePreview').hide();
                    $('#categoryEditImagePreview').fadeIn(650);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#categoryEditImageUpload").change(function() {
            readEditCategoryImageUpload(this);
            $("#categoryEditImagePreview").html('');
        });
    </script>
    {{--category addon image preview--}}
    <script>
        function readCategoryAddOnImageUpload(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            //alert(0);
            readCategoryAddOnImageUpload(this);
            $("#imagePreview").html('');
        });


    </script>

    {{--product category addon image preview--}}
    {{--PASA product addon subaddon--}}
    <script>
        function readPASAImageUpload(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#PASAImagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#PASAImagePreview').hide();
                    $('#PASAImagePreview').fadeIn(650);
                    $("#PASAImagePreview").html('');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#PASAImageUpload").change(function() {
            readPASAImageUpload(this);
        });


    </script>

    <script>
        function readItemImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#itemImagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#itemImagePreview').hide();
                    $('#itemImagePreview').fadeIn(650);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#itemImageUpload").change(function() {
            readItemImagePreview(this);
            $("#itemImagePreview").html('');
        });
    </script>
    {{-- Product Edit script--}}
    <script>
        var p = document.getElementById('combo_for_item_edit');
        var q = document.getElementById('category_add_ons_for_item_edit');
        var r = document.getElementById('categories_data_for_item_edit');

        $("#category_add_on_for_item_edit").click(function () {
            var isChecked = $("#category_add_on_for_item_edit").is(":checked");
            if (isChecked) {
                show_category_add_on_div_for_item_edit();
            } else {
                close_category_add_on_div_for_item_edit(true);
            }
            $("#category_add_on_selected_for_item_edit").html('');
        });

        function show_category_add_on_div_for_item_edit(){
            change_category_add_on_div(document.getElementById('edit_menu_items_desc').value,"{{getUrl('SELLERCATEGORYADDONS')}}");
            p.style.display = 'none';
            q.style.display = 'block';
            r.style.display = 'none';
        }


        $("#recommended_for_item_edit").click(function () {
            var isChecked = $("#recommended_for_item_edit").is(":checked");
            if (isChecked) {
                show_recommended_div_for_item_edit();
            } else {
                close_recommended_div_for_item_edit(true);
            }
        });
        function show_recommended_div_for_item_edit(){
            p.style.display = 'none';
            q.style.display = 'none';
            r.style.display = 'block';
            $('#category_product_list_for_item_edit').html('');
        }

        function close_recommended_div_for_item_edit(close){
            if(close) {
                $('#selected_recommended_items').html('');
                new_recommended_product = [];
            }
            var recommended_item = $("#recommended_for_item_edit").is(":checked");
            var category_add_on = $("#category_add_on_for_item_edit").is(":checked");
            if(recommended_item && category_add_on){
                p.style.display = 'none';
                q.style.display = 'none';
                r.style.display = 'none';
            }else {
                if (category_add_on) {
                    show_category_add_on_div_for_item_edit();
                } else {
                    p.style.display = 'none';
                    q.style.display = 'none';
                    r.style.display = 'none';

                }
            }
        }

    </script>



@endsection