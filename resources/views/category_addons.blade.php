@extends('layouts.app-new',["titlePage" => "Subcategory"])
@push('css')
<style>
    .stv-radio-tabs-wrapper {
        display: inline-block;
        width: 100%;
        border-bottom: 1px solid #428bca;
        padding: 0px;
    }

    input.stv-radio-tab {
        position: absolute;
        left: -99999em;
        top: -99999em;
    }

    input.stv-radio-tab + label {
        cursor: pointer;
        float: left;
        border: 1px solid #aaa;
        border-bottom: 0;
        background-color: #fff;
        margin-right: -1px;
        padding: 0.5em 1em;
        position: relative;
        margin-bottom: 0;
        width: 50%;
    }

    input.stv-radio-tab + label:hover {
        background-color: #eee;
    }

    input.stv-radio-tab:checked + label {
        box-shadow: 0 3px 0 -1px #fff, inset 0 5px 0 -1px #13cd4a;
        background-color: #fff;
        border-color: #428bca;
        z-index: 1;
    }

    .category-scroll {
        min-height: calc(100vh - 239px);
        max-height: calc(100vh - 239px);
        overflow-y: scroll;
        -ms-overflow-style: none;
        scrollbar-width: thin;
        mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        mask-size: 100% 20000px;
        mask-position: left bottom;
        -webkit-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -webkit-mask-size: 100% 20000px;
        -webkit-mask-position: left bottom;
        -moz-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -moz-mask-size: 100% 20000px;
        -moz-mask-position: left bottom;
        transition: mask-position 0.3s, -webkit-mask-position 0.3s;
    }

    .category-scroll::-webkit-scrollbar {
        display: none;

    }

    .svg-invert { /* svg on an img tag */
        -webkit-filter: invert(.75); /* safari 6.0 - 9.0 */
        filter: invert(.75);
    }

    .dataTable-wrapper {
        position: relative;
    }

    .dataTable-top {
        position: absolute;
        bottom: 0;
    }

    .dataTable-bottom {
        margin: 0px 18% !important;
    }

    .dataTable-info {
        margin: 12px 0;
    }

    .dataTable-bottom, .dataTable-top {
        padding: 0px 20px;
        font-size: 12px;
        margin-top: 2rem;
    }

    .dataTable-wrapper.no-footer .dataTable-container {
        border-bottom: none;
    }
</style>
<style>
    .avatar-upload {
        position: relative;
        max-width: 100%;
        height:50%;
        margin: 5px;
    }
    .avatar-upload .avatar-edit {
        position: absolute;
        right: 0px;
        z-index: 1;
        top: 0px;
        width: 100%;
        height: 100%;
    }
    .avatar-upload .avatar-edit input {
        display: none;
    }
    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 100%;
        height: 100%;
        margin-bottom: 0;
        background: transparent;
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
        background: transparent;
    }
    .avatar-upload .avatar-edit input + label:after {
        position: absolute;
        top: 6px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }
    .avatar-upload .avatar-preview {
        width: 100%;
        height: 100%;
        position: relative;
        font-size: 14px;
    }
    .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 0%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

</style>
<style>
    .modal-header {
        padding: 2rem 2rem 1rem 2rem;
    }
    .modal-body {
        padding: 0rem 2rem !important;
    }
    .modal-footer {
        background-color: transparent;
        border-top: 1px solid transparent;
    }
    .modal-header .close {
        padding: 0rem 0rem;
        margin: -1.5rem -1rem -1rem auto;
    }
    .form-group.input-material {
        position: relative;
        margin-top: 0px;
        margin-bottom: 26px;
    }
    .add-on-div{
        padding-top: 20px;
        margin: 20px 0;
    }
    .slim-scroll{
        height:100% !important;
    }
    .slimScrollDiv{
        height:100% !important;
    }
</style>
<link rel="stylesheet" href="{{asset('css/landing-page.css')}}">
@endpush
@section('content')

    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">Subcategory List</span>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="table-responsive" style="text-align: -webkit-center;">
                <table class="table" style="max-width: 50%;">
                    <thead class="thead-light">
                    <tr>
                        <th> # </th>
                        <th> Name</th>
                        <th> Description</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categoryaddons ?? [] as $categoryaddon)
                        <tr>

                            <td>{{ $loop->iteration }}</td>
                            <td>{{$categoryaddon['name']}}</td>
                            <td>{{$categoryaddon['description']}}</td>
                            <td>
                                <div class="d-flex">
                                    <a id="{{json_encode($categoryaddon)}}" onclick="update_modal(this.id);">
                                        <img class="mx-3" src="./images/card-edit.svg" alt="">
                                    </a>
                                    <a onclick="confirm_modal('{{url('categoryAddonDestroy', $categoryaddon['id'])}}');">
                                        <img class="mx-3" src="./images/delete.svg" alt="" >
                                    </a>
                                </div>
                            </td>

                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>


        </div>

    </div>

@endsection

@section('modal')

    <!--Delete Modal -->
    <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to delete it ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO
                        </button>
                        <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--Edit Category Add-ons Modal -->
    <div class="modal add-menu right fade " id="edit_category_add-ons" tabindex="-1" role="dialog"
         aria-labelledby="edit_category_add-ons" aria-hidden="true">
        <div class="modal-dialog" id="addnewmenu" role="add menu">
            <div class="modal-content model-width slim-scroll">
                <div class="modal-header">
                    <div class="f-20 f-medium">Edit Category Add-ons</div>
                </div>

                <div class="modal-body">
                    <form id="category-add-on-model" action="{{url('edit-category-addon')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div>
                            <div class="form-group input-material">
                                <input type="hidden" class="form-control" name="id" id="categoryaddonid" data-validation="required">
                                <select name="section_name" id="section_name" class="form-control" data-validation="required" required>
                                    @foreach($categories ?? [] as $category)
                                        <option value="{{$category['id']}}" @php if($category['id'] == request()->get('search')) echo "selected"; @endphp>{{$category['name']}}</option>
                                    @endforeach$category
                                </select>
                                <label for="itemSearch"><span style="color:red">*</span>Section Name</label>
                                <div class="text-danger">{{ $errors->first('section_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                <input type="text" class="form-control" name="subcategory_name" id="subcategory-name" data-validation="required" required>
                                <label for="textarea-field"><span style="color:red">*</span>Subcategory Name</label>
                                <div class="text-danger">{{ $errors->first('subcategory_name') }}</div>
                            </div>

                            <div class="form-group input-material">
                                    <textarea class="form-control" name="subcategory_description"
                                              id="subcategory-description" rows="2"
                                              data-validation="required" required></textarea>
                                <label for="textarea-field"><span style="color:red">*</span>Subcategory
                                    Description</label>
                                <div class="text-danger">{{ $errors->first('subcategory_description') }}</div>
                            </div>
                            <div>
                                <lable>Items</lable>
                            </div>

                            <div>
                                <div id="categoryaddons_subdiv">

                                    {{--<div id="form-block"
                                         class="modifiactions-required addonsstaticsubdiv row add-on-div">
                                        <a class="add-on-delete hide"><img class="mr-2" src="{{url('/images/delete.svg')}}" alt=""></a>

                                          <div class="col-3 no-left-padding">
                                              <div class="avatar-upload">
                                                  <div class="avatar-edit">
                                                      <input type='file' id="imageUpload" name="items[0][items_image]"
                                                             accept=".png, .jpg, .jpeg"/>
                                                      <label for="imageUpload"></label>
                                                  </div>
                                                  <div class="avatar-preview">
                                                      <div id="imagePreview"
                                                           style="background-color:white;color:#a7a796;border-radius:10px;vertical-align:center;text-align: center;padding-top: 30%;">
                                                          + ICON
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-9">
                                              <div class="form-group input-material mr-2">
                                                  <input type="text" name="items[0][items_name]" class="form-control"
                                                         id="items_name">
                                                  <label for="items_name">Item Name</label>
                                              </div>
                                              <div class="row">
                                                  <div class="col-6">
                                                      <div class="form-group input-material mr-2">
                                                          <input type="text" name="items[0][price]" class="form-control"
                                                                 id="price">
                                                          <label for="price">Price</label>
                                                      </div>
                                                  </div>
                                                  <div class="col-6">
                                                      <div class="form-group input-material mr-2">
                                                          <input type="text" name="items[0][discount]"
                                                                 class="form-control" id="discount">
                                                          <label for="discount">Discount</label>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>--}}

                                </div>
                                <div id="addmorecategoryaddons"
                                     class="add_more d-flex align-items-center justify-content-center text-color-grey">
                                    + ADD MORE
                                </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer row mb-4 mt-4" style="display:block ruby;padding: 0rem 2rem !important;">
                <div class="col-6" style="margin:0;">
                    <button class="cancelbtn" data-dismiss="modal" aria-label="Close" id="close" style="width:100%;">
                        CANCEL
                    </button>
                </div>
                <div class="col-6" style="margin:0;">
                    <button class="addbtn" id="addbtn" style="width:100%;"
                            onclick="document.getElementById('category-add-on-model').submit();"><span class="addonbtn"></span> CATEGORY
                        Add-on
                    </button>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('script')
    <script src="{{asset('js/custom/category-add-ons.js')}}"></script>
    <script>
        function confirm_modal(delete_url) {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href', delete_url);
        }

        function deleterow(val, param) {
            console.log(val);
            $("." + val).remove();
        }

        function update_modal(data) {

            $('#categoryaddons_subdiv').html("");
            var foodsubaddon = [];
            var param = JSON.parse(data);
            console.log("23243434 " + JSON.stringify(param));
            $('#edit_category_add-ons').modal({backdrop: 'static', keyboard: false});
            $('#edit_category_add-ons').modal('show');
            $('#category-add-on-model').trigger("reset");
            console.log(param);
            //var param = param[0];
            //var param = JSON.parse(data[0]);
            console.log(param.category_id);
            var section_name = $('#section_name');
            var subcategory_name = $('#subcategory-name');
            var subcategory_description = $('#subcategory-description');
            $('#categoryaddonid').val(param.id);
            section_name.attr('value', param.category_id);
            section_name.val(param.category_id);
            subcategory_name.attr('value', param.name);
            subcategory_name.val(param.name);
            subcategory_description.attr('value', param.description);
            subcategory_description.val(param.description);
            foodsubaddon = param.foodsubaddon;
            for (var a = 0; a < foodsubaddon.length; a++) {
                console.log(foodsubaddon[a].name);
                var categoryaddondelete = "block";
                if (a == 0) {
                    var categoryaddondelete = "none";
                }
                var content='<div id="form-block" class="modifiactions-required addonsstaticsubdiv row add-on-div category-add-on-delete' + a + '">' +
                    '<div class="add-on-delete"><a class="mouse-pointer" style="display: ' + categoryaddondelete + '" id="category-add-on-delete' + a + '" onclick="deleterow(this.id,' + a + ')"><img src="./images/delete.svg" alt=""></a></div>' +
                    '<div class="col-3 no-left-padding"> <div class="avatar-upload"><div class="avatar-edit">' +
                    '<input type="file" id="imageUpload' + a + '" name="items[' + a + '][items_image]" accept=".png, .jpg, .jpeg" required/>' +
                    '<label for="imageUpload' + a + '"></label>' +
                    '</div> <div class="avatar-preview">';
                    if(foodsubaddon[a].photo == "" || foodsubaddon[a].photo ==null ){
                        content=content+'<div id="imagePreview' + a + '"  style="background-color:white;color:#a7a796;border-radius:10px;vertical-align:center;text-align: center;padding-top: 30%;">+ ICON';
                    }else {
                        content=content+'<div id="imagePreview' + a + '"  style="background-image:url(' + foodsubaddon[a].photo + ');border-radius:10px;">';
                    }
                    content=content+'</div></div></div></div>' +
                    '<div class="col-9"><div class="form-group input-material mr-2">' +
                    '<input type="text" name="items[' + a + '][items_name]" class="form-control"id="items_name" value="' + foodsubaddon[a].name + '" required>' +
                    '<label for="items_name">Item Name</label></div>' +
                    '<div class="row">' +
                    '<div class="col-6"><div class="form-group input-material mr-2">' +
                    '<input type="text" name="items[' + a + '][price]" class="form-control" id="price"  value="' + foodsubaddon[a].price + '" required>' +
                    '<label for="price">Price</label></div>' +
                    '</div><div class="col-6">' +
                    '<div class="form-group input-material mr-2">' +
                    '<input type="text" name="items[' + a + '][discount]" class="form-control" id="discount" value="' + foodsubaddon[a].discount + '" required>' +
                    '<label for="discount">Discount</label>' +
                    '</div></div></div></div></div>';
        $('#categoryaddons_subdiv').append(content);
            }
        }
    </script>

@endsection
