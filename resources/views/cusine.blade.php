@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">LIST OF Cuisines</span>
                <button class="add-btn" data-toggle="modal" onclick="add_cusine_modal()">+ Add Cuisine</button>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="table-responsive">
                <table class="table" id="employees">
                    <thead class="thead-light">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if($cusines =='')
                        <tr><td colspan="7">No data Available</td></tr>
                    @else
                        @foreach($cusines as $cusine)
                            <tr>
                                <td>{{$cusine['name']}}</td>
                                <td>{{$cusine['meta_description']}}</td>
                                <td>
                                    <div class="d-flex">
                                        <a id="{{json_encode($cusine)}}" onclick="update_cusine(this.id);">
                                            <img class="mx-3" src="./images/card-edit.svg" alt="">
                                        </a>
                                        <a onclick="confirm_cusine('{{url('deletecusine', $cusine['id'])}}');">
                                            <img class="mx-3" src="./images/delete.svg" alt="" >
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>


        </div>

    </div>

    <div>
        @include('inc.side_nav')


        <div class="modal add-restaurant right fade " id="addemployeesid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewemployees" role="add employees">
                <div class="modal-content">

                    <div class="modal-body pb-4">
                        <div class="f-20 f-medium"><span id="addedit"></span> Cuisine</div>
                        <form id="manage-employees-form" method="POST" action="{{url('addcusine')}}">
                            @csrf
                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="hidden" class="form-control" name="id" id="cusineid">
                                                <input type="text" class="form-control" name="name" id="name" required data-validation="required">
                                                <label for="name">Name<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('name') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" name="meta_description" id="meta_description" required data-validation="required">
                                                <label for="meta_description">Description<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('meta_description') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                    <div class="mt-5 d-flex align-items-center justify-content-between">

                        <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                        <button class="addbtn" id="addbtn"> </button>
                    </div>

                    </form>

                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- Modal -->
    <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body p-4">
                    <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                        <img src="./images/dialogclose.svg" alt="">
                    </div>
                    <p class="f-20 f-medium">Are you sure!</p>
                    <p class="f-12 mb-0">You want to delete it ?</p>
                </div>
                <div class="mb-2">
                    <div class=" mb-3 d-flex align-items-center justify-content-around">

                        <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                aria-label="Close">NO</button>
                        <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                    </div>
                </div>

            </div>
        </div>
    </div>





    </div>

   @endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    function confirm_cusine(delete_url)
    {
        jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('href' , delete_url);
    }

    function add_cusine_modal()
    {
        $('#addedit').html('Add');
        $('#addbtn').html('ADD CUISINE');
        $('#manage-employees-form').attr('action', '{{url('addcusine')}}');
        $('#addemployeesid').modal({backdrop: 'static', keyboard: false});
        $('#cusineid').val('');
        $('#name').attr('');
        $('#name').val('');
        $('#meta_description').attr('');
        $('#meta_description').val('');

    }
    function update_cusine(param)
    {
        console.log(param);
        var data = JSON.parse(param);
        $('#addedit').html('Edit');
        $('#addbtn').html('UPDATE CUISINE');
        $('#manage-employees-form').attr('action', '{{url('updatecusine')}}');
        $('#addemployeesid').modal('show');
        $('#cusineid').val(data.id);
        $('#name').attr('value', data.name);
        $('#name').val(data.name);
        $('#meta_description').attr('value', data.meta_description);
        $('#meta_description').val(data.meta_description);
//        $('#phone').attr('value', data.phone);
//        $('#phone').val(data.phone);
//        $('#emailid').attr('value', data.email);
//        $('#emailid').val(data.email);
//        $('input[name="role_id"][value='+data.role_id+']').prop('checked', 'checked');
//        $('#radio-role'+data.role_id).addClass('option-selected');
//        //console.log($('#radiobtnrole'+data.role_id).prop('checked', true));
//        $('#joiningdate').attr('value', data.joining_date);
//        $('#joiningdate').val(data.joining_date);
//        addremovestyle(data.role_id);
    }
</script>