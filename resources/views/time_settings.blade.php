@extends('layouts.app')
@section('content')
    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">DEFAULT FOOD PREPARATION TIME</span>
            </div>
            <div class="container-fluid p-0">
                <div class="row">
                    @foreach($timings['body'] as $timing)
                        @if(isset($timing['text']))
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="time-setting-card">
                            <div class="d-flex f-14 f-medium res-title">
                                {{$timing['text']}}
                            </div>
                            <div class="mb-0 mb-3 f-14 text-color-grey ">{{$timing['full_text']}}</div>
                            <div class="d-flex justify-content-between">

                                <button class="timings-button{{($timing['time'] == '15')?"-active":""}} f-12 f-medium" onclick="confirm_modal(15, '{{$timing['text']}}');">15 min</button>
                                <button class="timings-button{{($timing['time'] == '20')?"-active":""}} f-12 f-medium" onclick="confirm_modal(20, '{{$timing['text']}}');">20 min</button>
                                <button class="timings-button{{($timing['time'] == '30')?"-active":""}} f-12 f-medium" onclick="confirm_modal(30, '{{$timing['text']}}');">30 min</button>
                                <button class="timings-button{{($timing['time'] == '40')?"-active":""}} f-12 f-medium" onclick="confirm_modal(40, '{{$timing['text']}}');">40 min</button>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
               </div>
            </div>
        </div>
    </div>
@endsection
@section('model')

        @include('inc.side_nav')
        <!-- Modal -->
        <div class="modal fade" id="confirm_setting" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to set Time  ?</p>
                        <input type="text" name="go_average_time" id="go_average_time">
                        <input type="text" name="ordertype" id="ordertype">
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="setting_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection
@section('script')
    <script>
    function confirm_modal(data, param)
    {
      // var go_average_time = $('#go_average_time').val(data);
       // var ordertype = $('#ordertype').val(param);
       // jQuery('#confirm_setting').modal('show', {backdrop: 'static'});
       //document.getElementById('setting_link').setAttribute('href' , seting_url);

        swal({
                title: "Are you sure?",
                text: "You want to update it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    var send = {"go_average_time":data, "ordertype":param, _token: '{{csrf_token()}}'};
                    send=$(this).serialize()+" & "+$.param(send);
                    console.log("data "+send);
                    $.ajax({
                        cache:false,
                        type: "POST",
                        url: "{{url('average_time/')}}",
                        data:send,
                        success: function (res) {
                            console.log(res);
                            if (res =='Success') {
                                swal({title: "success", text: "Successfully updated."});
                                window.location.href = "{{url('time_settings/')}}";
                            } else {
                                swal({title: "error", text: "Failed to update."});
                            }
                        },error: function(res) {
                            console.log(res);
                            swal({title: "error", text: "Error to update."});
                        }
                    });
                //    swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                } else {
                    swal("Cancelled", "Failed", "error");
                }
            });

    }
    </script>
@endsection
