@extends('layouts.app')
@section('content')


    <div class="orders-notifications">
        <!-- <div class="bg-black">
          <div class="searching-for-orders max-content-width">
            Searching for new orders
          </div>
        </div> -->


        <!-- <div class="bg-green">
          <div class="received-orders max-content-width">
            <div class="received-orders-content">
              <img src="./images/usericon.svg" alt="">
              <div class="d-flex flex-wrap justify-content-center align-items-center">
                <div class="mx-3 new-order-text">You have new order!</div>

                <div class="mx-3 d-flex"><span>Total 4 Items </span> <span class="mx-1">|</span><span>$</span>
                  <span>2500.25</span>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-center">
              <img class="mr-2" src="./images/alret.svg" alt="">
              <button class="see-details-btn">SEE THE DETAILS</button>
            </div>
          </div>
        </div> -->
    </div>

    <div class="main-section-padding">
 <!--       <div class="d-flex align-items-center justify-content-between pb-2">
            <h4 class="heading">SALES INSIGHTS</h4>
            <form>
                <select name="days" id="days" class="select-drop-down" onchange="fetchdaysdata(this.value);">>
                    <option value="7">last 7 days</option>
                    <option value="30">last 30 days</option>
                </select>
            </form>
        </div>-->
        <div class="container-fluid p-0 sales">
            {{--<div class="row">--}}
                {{--<div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">--}}
                    {{--<div class="sales-card">--}}
                        {{--<div class="d-flex align-items-center justify-content-between">--}}
                            {{--<div class="d-grid">--}}
                                {{--<div class="sale-type">New Sales</div>--}}
                                {{--<img class="mt-2" src="./images/netsales.svg" alt="">--}}
                            {{--</div>--}}

                            {{--<div class="text-to-end">--}}
                                {{--<div class="sale-percent-up">--}}
                                {{--<span id="salespercent"></span>--}}
                                {{--<span id="salepercent">%</span>--}}
                                {{--<img src="./images/uparrow-green.svg"  alt=""  id="salesimg"></div>--}}
                                {{--<div class="sale-value"><span>$</span><span id="salesval"></span></div>--}}
                                {{--<div class="sale-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sales-card">--}}
                        {{--<div class="d-flex align-items-center justify-content-between">--}}
                            {{--<div class="d-grid">--}}
                                {{--<div class="sale-type">Total Orders</div>--}}
                                {{--<img class="mt-2" src="./images/totalorders.svg" alt="">--}}
                            {{--</div>--}}

                            {{--<div class="text-to-end">--}}
                                {{--<div class="sale-percent-down">--}}
                                {{--<span id="toporderpercentl"></span>--}}
                                {{--<span id="orderpercent">%</span>--}}
                                 {{--<img src="./images/down-arrow-red.svg" alt=""  id="orderimg"></div>--}}
                                {{--<div class="sale-value"><span>$</span><span id="totordersvalue"></span></div>--}}
                                {{--<div class="sale-compare">Compared to previous <span class="saledays"></span>  days</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="sales-card">--}}

                        {{--<div class="d-flex align-items-center justify-content-between">--}}
                            {{--<div class="d-grid">--}}
                                {{--<div class="sale-type">New Items</div>--}}
                                {{--<img class="mt-2" src="./images/items.svg" alt="">--}}
                            {{--</div>--}}

                            {{--<div class="text-to-end" >--}}
                                {{--<div class="sale-percent-down">--}}
                                    {{--<span id="orderspercent">100</span>--}}
                                    {{--<span id="orderpercent">%</span>--}}
                                    {{--<img src="./images/down-arrow-red.svg" alt=""  id="orderimg"></div>--}}
                                {{--<div class="sale-value"><span id="ordersval">10</span></div>--}}
                                {{--<div class="sale-compare">Compared to previous <span class="saledays"></span>  days</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div></a>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">--}}

                {{--<div style="box-shadow: rgb(235 235 235) 0px 0px 10px; border-radius: 8px;">--}}
                    {{--<div class="d-flex align-items-center justify-content-between pl-2 pr-3 pt-3 pb-4">--}}
                        {{--<form>--}}
                            {{--<select name="days" id="days" class="select-drop-down">--}}
                                {{--<option value="7">last 7 days</option>--}}

                            {{--</select>--}}
                        {{--</form>--}}
                        {{--<div class="d-flex align-items-center justify-content-between">--}}
                            {{--<span class="d-flex align-items-center">--}}
                                {{--<span class="present-7-days"></span>--}}
                                {{--<span class="f-10 text-color-grey">Present <span class="saledays"></span> Days</span>--}}
                            {{--</span>--}}

                            {{--<span class="d-flex align-items-center">--}}
                                {{--<span class="previous-7-days"></span>--}}
                                {{--<span class="f-10 text-color-grey">Previous <span class="saledays"></span> Days</span>--}}
                            {{--</span>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<canvas id="line-chart" style="height:319px"></canvas>--}}
                {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}



            <!--Sales history-->

            <div class="row">
                <div class="col-12 mt-4 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">SALES HISTORY</h4>

                    </div>
                </div>
            </div>
            <div class="row itemized-report">

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Sales</div>
                        <div class="itemized-report-percent-up">
                <span class="mx-1">
                  <img src="./images/uparrow-green.svg" alt=""></span><span>2.25%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3122900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Food@Work</div>
                        <div class="itemized-report-percent-up">
                <span>
                  <img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">2.25%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Delivery</div>
                        <div class="itemized-report-percent-up"> <span><img src="./images/uparrow-green.svg" alt=""></span>
                            <span class="mx-1">16.50%</span>
                        </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>13900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>1900</span></span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Rejected/Cancelled Orders</div>
                        <div class="itemized-report-percent-down"><span ><img
                                    src="./images/down-arrow-red.svg" alt=""></span>
                            <span class="mx-1">2.25%</span> </div>
                        <div class="itemized-report-value d-flex"><span>$</span><span>3900</span></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="table-responsive">
                <table class="table" id="sales_history">
                    <thead class="thead-light">
                    <tr>
                        <th>Order Id</th>
                        <th>Date</th>
                        <th>Order Type</th>
                        <th>Subtotal</th>
                        <th>GCT</th>
                        <th>Total</th>
                        <th>Commision</th>
                        <th>Discount</th>
                        <th>Net Income</th>
                        <th>Status</th>
                        <th>Payment</th>
                        {{--<th class="table-controls-button" id="table-button">--}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
				            {{--<ul class="controls-dropdown-box" id="controls-dropdown-menu" style="display: none;"></ul>--}}
                            {{--</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @if($salehist =='')
                        <tr><td colspan="11">No data Available</td></tr>
                    @else
                    @foreach($salehist as $sale)
                        <tr>
                            <td>{{$sale->order_id}}</td>
                            <td>{{$sale->order_date}}</td>
                            <td>{{$sale->order_type}}</td>
                            <td>
                                <div class="d-flex"><span>$</span> <span>{{number_format($sale->sub_total)}}</span></div>
                            </td>
                            <td>
                                <div class="d-flex"><span>$</span> <span>{{$sale->gct}}</span></div>
                            </td>
                            <td class="d-flex"><span>$</span> <span>{{number_format($sale->total_amount)}}</span></td>
                            <td class="text-color-green">{{$sale->payment_status??''}}</td>
                            <td class="d-flex"><span>$</span> <span>{{number_format($sale->total_amount)}}</span></td>

                            <td>
                                <div class="d-flex"><span>$</span> <span>{{$sale->gct}}</span></div>
                            </td>
                            <td class="text-color-green ">{{$sale->order_status??''}}</td>
                            <td>Online</td>
                            {{--<td></td>--}}
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            <div class="row float-right">

                <div class="col-12">
                    <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                        <div class="d-flex "></div>
                         <div id="download_sales_history">
                            <a class="text-color-green mx-2 f-14 f-medium" href="">Download Report</a>
                            <img src="./images/download.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>

            <!--ITEMIZED SALES HISTORY-->

            <div class="row">
                <div class="col-12 mt-4 mb-2">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">ITEMIZED SALES HISTORY</h4>

                    </div>
                </div>
            </div>
            <div class="row itemized-report">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Top Item</div>
                        <div class="itemized-report-percent-up"> <span ><img
                                        src="./images/uparrow-green.svg" alt="" id="topsellingimg"></span>
                            <span id="topsellingitempercent"></span>
                            <span id="topsellingpercent">%</span> </div>
                        <div class="itemized-report-value" id="topsellingitemthisweek"></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black" id="topsellingitemlastweek"> </span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Total Orders</div>
                        <div class="itemized-report-percent-down"> <span ><img
                                        src="./images/down-arrow-red.svg" alt="" id="totorderimg"></span>
                            <span id="totorderspercent"></span>
                            <span id="totorderspercentimg">%</span></div>
                        <div class="itemized-report-value" id="totordersvalorder"></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black" id="totlastwekordersval"> 218</span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Top Item Earning</div>
                        <div class="itemized-report-percent-up"> <span ><img
                                        src="./images/uparrow-green.svg" alt="" id="topitemearnimg"></span>
                            <span id="topitemearnpercent"></span>
                            <span id="topitemearnpercentimg">%</span> </div>
                        <div class="itemized-report-value d-flex" id="topitemearnThisweek"></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black d-flex" id="topitemearnLastweek"> </span>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                    <div>
                        <div class="itemized-report-title">Top Section</div>
                        <div class="itemized-report-percent-up"><span ><img
                                        src="./images/uparrow-green.svg" alt="" id="topsectionimg"></span>
                            <span id="topsectionpercent"></span>
                            <span id="topsectionpercentimg">%</span> </div>
                        <div class="itemized-report-value" id="thisweektopsection"></div>
                        <div class="itemized-report-compare">
                            <span>Compared to previous 7 days</span> -
                            <span class="font-weight-bold text-color-black text-uppercase" id="lastweektopsection"> </span>
                        </div>
                    </div>
                </div>



            </div>
            <div class="table-responsive">
                <table class="table" id="itemizedsalehistory">
                    <thead class="thead-light">
                    <tr>
                        <th>Item Name</th>
                        <th>Section</th>
                        <th>Total Orders</th>
                        <th>Net Sales</th>
                        <th>Discounted</th>
                        <th>Total Earnings</th>
                        {{--<th class="table-controls-button" id="table-button1">--}}
                            {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
                            {{--<ul class="controls-dropdown-box" id="controls-dropdown-menu1" style="display: none;"></ul>--}}
                        {{--</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @if($itemizedsalehist =='')
                        <tr><td colspan="6">No data Available</td></tr>
                    @else
                        @foreach($itemizedsalehist as $salehist)
                            <tr>
                                <td>{{$salehist->itemname}}</td>
                                <td>{{$salehist->category}}</td>
                                <td>{{$salehist->count}}</td>
                                <td>
                                    <div class="d-flex"><span>$</span> <span>{{number_format($salehist->NET_Sales)}}</span></div>
                                </td>
                                <td>
                                    <div class="d-flex"><span>$</span> <span>{{number_format($salehist->coupon_discount)}}</span></div>
                                </td>
                                <td>
                                    <div class="d-flex"><span>$</span> <span>{{number_format($salehist->Total_Earnings)}}</span></div>
                                </td>
                                {{--<td></td>--}}
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>

            <div class="row float-right">
                <div class="col-12">
                    <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                        <div class="d-flex "></div>
                        <div id="download_itemized_sales_history">
                            <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!--ACCOUNT SETTLEMENT-->

        <div class="row">
            <div class="col-12 mt-4 mb-2">
                <div class="d-flex align-items-center justify-content-between pb-2">
                    <h4 class="heading">ACCOUNT SETTLEMENT</h4>

                </div>
            </div>
        </div>
        <div class="row itemized-report">


            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Total Disburement</div>
                    <div class="itemized-report-percent-up"> <span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                        <span class="mx-1">4.13%</span></div>
                    <div class="itemized-report-value d-flex"><span>$</span><span>433900</span></div>
                    <div class="itemized-report-compare">
                        <span>Compared to previous 7 days</span> -
                        <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>32900</span></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Toral Refund Amount</div>
                    <div class="itemized-report-percent-down"><span ><img
                                    src="./images/down-arrow-red.svg" alt=""></span>
                        <span class="mx-1">3.33%</span> </div>
                    <div class="itemized-report-value d-flex"><span>$</span><span>33900</span></div>
                    <div class="itemized-report-compare">
                        <span>Compared to previous 7 days</span> -
                        <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>13900</span></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Total Earnings</div>
                    <div class="itemized-report-percent-up"><span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                        <span class="mx-1">8.25%</span> </div>
                    <div class="itemized-report-value d-flex"><span>$</span><span>8993900</span></div>
                    <div class="itemized-report-compare">
                        <span>Compared to previous 7 days</span> -
                        <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>883900</span></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Cancelled Orders</div>
                    <div class="itemized-report-percent-up"><span ><img
                                    src="./images/uparrow-green.svg" alt=""></span>
                        <span class="mx-1">1.25%</span> </div>
                    <div class="itemized-report-value d-flex"><span>$</span><span>900</span></div>
                    <div class="itemized-report-compare">
                        <span>Compared to previous 7 days</span> -
                        <span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>100</span></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="table-responsive">
            <table class="table" id="account_setteled">
                <thead class="thead-light">
                <tr>
                    <th>Date</th>
                    <th>Transaction Id</th>
                    <th>Total Disbursment</th>
                    <th>Refund Amount</th>
                    <th>Total Earnings</th>
                    <th>Transaction Status</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>01-06-2021</td>
                    <td>ABC000012345677</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1329.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1329.00</span>
                        </div>
                    </td>
                    <td class="text-color-green ">Credited</td>

                </tr>


                <tr>
                    <td>01-06-2021</td>
                    <td>ABC000012345678</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1000.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1000.00</span>
                        </div>
                    </td>
                    <td class="text-color-green ">Credited</td>

                </tr>

                <tr>
                    <td>01-06-2021</td>
                    <td>ABC000012345679</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>200.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>500.00</span>
                        </div>
                    </td>
                    <td class="text-color-orange ">Invoiced</td>

                </tr>

                <tr>
                    <td>02-08-2021</td>
                    <td>ABC000012345998</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1200.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1500.00</span>
                        </div>
                    </td>
                    <td class="text-color-orange ">Invoiced</td>

                </tr>

                <tr>
                    <td>02-24-2021</td>
                    <td>ABC000012345999</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1329.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1329.00</span>
                        </div>
                    </td>
                    <td class="text-color-green ">Credited</td>

                </tr>


                <tr>
                    <td>02-16-2021</td>
                    <td>ABC000012346999</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1000.00</span>
                        </div>
                    </td>
                    <td>0</td>
                    <td>
                        <div class="d-flex">
                            <span>$</span> <span>1000.00</span>
                        </div>
                    </td>
                    <td class="text-color-green ">Credited</td>

                </tr>



                </tbody>
            </table>

        </div>
        <div class="row float-right">
            <div class="col-12">
                <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                    <div class="d-flex ">
                    </div>

                    <div id="exportBtn1">
                        <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                        <img src="./images/download.svg" alt="">
                        <div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        </div>



    </div>

@endsection

@section('model')
<div>
    <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
         aria-labelledby="sidebar-option-lable">
        <div class="modal-dialog" role="sidebar options">
            <div class="modal-content bg-black">


                <div class="modal-body sidebar-options">
                    <header>
                        <img class="logo" src="./images/pekkish-logo.svg" alt="">
                        <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                      aria-hidden="true">&times;</span></button> -->
                    </header>
                    <nav class="navbar-options">
                        <ul class="nav flex-column flex-nowrap overflow-hidden">
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./dashboard.html">
                                    <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                    <span class=" menu-link">Dashboard</span></a>
                            </li>

                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                   data-target="#orders-menu">
                                    <img class="nav-icons" src="./images/orders.svg" alt="">
                                    <span class="">Orders</span></a>
                                <div class="collapse" id="orders-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1" href="./orders.html"><span>Orders</span></a>
                                            <a class="nav-link collapsed py-1" href="./order_history.html"><span>History</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./menu.html">
                                    <img class="nav-icons" src="./images/menu.svg" alt="">
                                    <span class="menu-link">Menu</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                    <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                    <span class=" menu-link">Bussiness Hours</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate active-links" href="./accounts.html">
                                    <img class="nav-icons" src="./images/accounts.svg" alt="">
                                    <span class=" menu-link">Accounts</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./feedback_reviews.html">
                                    <img class="nav-icons" src="./images/feeback.svg" alt="">
                                    <span class=" menu-link">Feedbacks
                      & Reviews</span></a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="./offer_list.html">
                                    <img class="nav-icons" src="./images/offers.svg" alt="">
                                    <span class=" menu-link">Offers</span></a>
                            </li>


                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                   data-target="#settings-menu">
                                    <img class="nav-icons" src="./images/settings.svg" alt="">
                                    <span class="">Settings</span></a>
                                <div class="collapse" id="settings-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1" href="./manage_employees.html"><span>Manage
                            Employees</span></a>
                                            <a class="nav-link collapsed py-1" href="./manage_restaurant.html"><span>Manage
                            Restaurants</span></a>
                                            <a class="nav-link collapsed py-1" href="./time_settings.html"><span>Time Settings</span></a>

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <div class="mt-5">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/logout.svg" alt="">
                                        <span class=" menu-link">Logout</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <img class="nav-icons" src="./images/help.svg" alt="">
                                        <span class=" menu-link">Help</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">

                                        <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                </li>
                            </div>


                            <!-- <li class="nav-item">
                              <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                              <div class="collapse" id="orders-menu" aria-expanded="false">
                                  <ul class="flex-column pl-2 nav">
                                      <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                      <li class="nav-item">
                                          <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                          <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                              <ul class="flex-column nav pl-4">
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                  </li>
                                              </ul>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                          </li> -->
                        </ul>

                    </nav>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
         aria-labelledby="user-profile">
        <div class="modal-dialog" role="profile dialog">
            <div class="modal-content bg-black">

                <!-- <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                              aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                  </div> -->

                <div class="modal-body">

                    <div class="outerDivFull">
                        <div class="switchToggle">
                            <input type="checkbox" id="switch">
                            <label for="switch">Toggle</label>
                        </div>
                    </div>
                    <div class="text-center">
                        <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                        <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                        <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                    </div>
                    <div>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Update Email Address</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link">Reset Password</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="#">
                                        <span class=" menu-link">Update <Address></Address></span></a>
                                </li>
                            </ul>
                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

</div>

@endsection

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
<script>
$(document).ready(function() {
   // dashboardCounts(7);
   // linechartdata(7);
    OrdersHistoryCounts(7);
    TotalOrdersCounts(7);


//    $("#download_item_sales_history").click(function(){
//        var date = new Date();
//        TableToExcel.convert(document.getElementById("item_sales_history"), {
//
//            name: "Account_setteled_details" +date+ ".xlsx",
//            sheet: {
//                name: "ITEMIZED SALES HISTORY"
//            }
//        });
//    });

//});
    $("#download_sales_history").click(function () {
        var date = new Date();
        TableToExcel.convert(document.getElementById("sales_history"), {

            name: "Sales_History_" + date + ".xlsx",
            sheet: {
                name: "Sales History"
            }
        });
    });

    $("#download_account_setteled").click(function () {
        var date = new Date();
        TableToExcel.convert(document.getElementById("account_setteled"), {

            name: "Account_setteled_details" + date + ".xlsx",
            sheet: {
                name: "ACCOUNT SETTLEMENT"
            }
        });
    });
});
function fetchdaysdata(data){
   // dashboardCounts(data);
    //linechartdata(data);
}

function dashboardCounts(days)
    {
        var url = "{{url('dashboardCountsDetails')}}/" + days;
            //console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Counts "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        $('.saledays').html(days);
                        $('#salespercent').html(res.data.PERCENT_SALES);
                        $('#salespercent').css("color", res.data.SALEINCRDECR);
                        $('#salepercent').css("color", res.data.SALEINCRDECR);
                        $('#salesval').html((res.data.THIS_WEEK_SALES).toFixed(2));
                        if(res.data.SALEINCRDECR =='red'){
                            $('#salesimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#salesimg').attr('src', './images/uparrow-green.svg');
                        }

                        $('#toporderpercentl').html(res.data.PERCENT_ORDER);
                        $('#orderspercent').css("color", res.data.ORDERINCRDECR);
                        $('#orderpercent').css("color", res.data.ORDERINCRDECR);
                        $('#totordersvalue').html((res.data.THIS_WEEK));
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#orderimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#orderimg').attr('src', './images/uparrow-green.svg');
                        }


                        $('#incnetpercent').html(res.data.PERCENT_NET);
                        $('#incnetpercent').css("color", res.data.NETINCRDECR);
                        $('#netpercent').css("color", res.data.NETINCRDECR);
                        $('#incnetval').html((res.data.THIS_WEEK_NET).toFixed(2));
                        if(res.data.NETINCRDECR =='red'){
                            $('#incnetimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#incnetimg').attr('src', './images/uparrow-green.svg');
                        }


                        $('#avgpreppercent').html(res.data.AVGPREPTIMEPERCENT);
                        $('#avgpreppercent').css("color", res.data.AVGPREPINCR);
                        $('#avpreppercent').css("color", res.data.AVGPREPINCR);
                        $('#avgprepval').html(res.data.AVGPREPTIME);
                        if(res.data.AVGPREPINCR =='red'){
                            $('#avgprepimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#avgprepimg').attr('src', './images/uparrow-green.svg');
                        }


                        $('#waitpreppercent').html(res.data.AVGWAITTIMEPERCENT);
                        $('#waitpreppercent').css("color", res.data.AVGWAITINCR);
                        $('#waitpreppercent').css("color", res.data.AVGWAITINCR);
                        $('#waitprepercent').css("color", res.data.AVGWAITINCR);
                        $('#waitprepval').html(res.data.AVGWAITTIME);
                        if(res.data.AVGWAITINCR =='red'){
                            $('#waitprepimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#waitprepimg').attr('src', './images/uparrow-green.svg');
                        }


                        $('#deliverpreppercent').html(res.data.AVGDELIVERPERCENT);
                        $('#deliverpreppercent').css("color", res.data.AVGDELIVERINCR);
                        $('#deliverpreppercent').css("color", res.data.AVGDELIVERINCR);
                        $('#deliverprepercent').css("color", res.data.AVGDELIVERINCR);
                        $('#deliverprepval').html(res.data.AVGDELIVERTIME);
                        if(res.data.AVGDELIVERINCR =='red'){
                            $('#deliverprepimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#deliverprepimg').attr('src', './images/uparrow-green.svg');
                        }


                    }
                },error: function(res) {

                }
            });

    }



    Chart.pluginService.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    });

Chart.defaults.global.legend.labels.usePointStyle = true;
function linechartdata(days)
    {
        var url = "{{url('linechartDetails')}}/" + days;
            //console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("LINE CHART  "+JSON.stringify(res));
                    if(res.success){
                        linechart_dataDetails(res.data, days);
                    }
                },error: function(res) {

                }
            });

    }
 function linechart_dataDetails(data, days){
     console.log("weew"+JSON.stringify(data));
     console.log("werw"+days);
     console.log("werw"+data.PREVIOUS);
new Chart(document.getElementById("line-chart"), {
        type: 'line',

        data: {
            labels: data.pradatelabel,

            datasets: [{
                data: data.PRESENT,
                pointBackgroundColor: "#52DCED",
                pointBorderColor: "#52DCED",
                label: "Present "+days+" Days",
                backgroundColor: "rgb(82 220 237/24%)",
                borderColor: "#52DCED",
                fill: true
            }, {
                data: data.PREVIOUS,
                pointBackgroundColor: "#5E84EA",
                pointBorderColor: "#5E84EA",
                label: "Previous "+days+" Days",
                backgroundColor: "rgb(94 132 234/24%)",
                borderColor: "#5E84EA",
                fill: true
            }
            ]
        },
        options: {

            legend: {
                display: false,
                position: "top",
                align: "end",
                labels: {
                    usePointStyle: true,

                },

            },
            chartArea: {

            },

            scales: {
                xAxes: [{
                    gridLines: {
                        drawBorder: false,
                        scaleLineColor: "rgba(0,0,0,0)",
                        borderDash: [0, 0],
                        color: "rgba(0,0,0,0)"
                    }
                }],

                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        scaleLineColor: "rgba(0,0,0,0)",
                        borderDash: [3, 3]
                    },

                    ticks: {
                        stepSize: 100 // this worked as expected
                    }
                }],
            },
            title: {
                display: true,
                text: 'Day Wise Sales Details',
                display: false
            }
        }
    });
}

</script>

<script> 
function TotalOrdersCounts(days)
    {
        var url = "{{url('AjaxTotalOrdersCounts')}}/" + days;
            console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Total orders "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        $('.saledays').html(days);
                        $('#totorderspercent').html(res.data.PERCENT_ORDER);
                        $('#totorderspercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#totordersvalorder').html((res.data.THIS_WEEK));
                        $('#totlastwekordersval').html((res.data.LAST_WEEK));
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#totorderimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#totorderimg').attr('src', './images/uparrow-green.svg');
                        }

                    }
                },error: function(res) {

                }
            });

    }

    function OrdersHistoryCounts(days)
    {
        var url = "{{url('AjaxOrdersHistoryCounts')}}/" + days;
            console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Counts "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        $('.saledays').html(days);
                        $('#topsellingitempercent').html(res.data.PERCENT_ORDER);                        
                        $('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                        $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsellingimg').attr('src', './images/uparrow-green.svg');
                        }

                        $('#topitemearnpercent').html(res.data.PERCENT_TOP_ITEM_EARN);                        
                        $('#topitemearnpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topitemearnpercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#topitemearnThisweek').html((res.data.THIS_WEEK_TOP_ITEM_EARN).toFixed(2));
                        $('#topitemearnLastweek').html((res.data.LAST_WEEK_TOP_ITEM_EARN).toFixed(2));
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topitemearnimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topitemearnimg').attr('src', './images/uparrow-green.svg');
                        }

                        $('#topsectionpercent').html(res.data.PERCENT_ORDER);                       
                        $('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                        $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsectionimg').attr('src', './images/uparrow-green.svg');
                        }

                    }
                },error: function(res) {

                }
            });

    }

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#download_sales_history").click(function(){
            var date = new Date();
            TableToExcel.convert(document.getElementById("sales_history"), {

                name: "sales_history" +date+ ".xlsx",
                sheet: {
                    name: "Sales History"
                }
            });
        });
    });

    function OrdersHistoryCounts(days)
    {
        var url = "{{url('AjaxOrdersHistoryCounts')}}/" + days;
        //console.log("URL "+url);
        $.ajax({
            cache:false,
            type: "GET",
            url: url,
            success: function (res) {
                console.log("Counts "+JSON.stringify(res));
                if(res.success){
                    console.log(res.data);
                    $('.saledays').html(days);
                    $('#topsellingitempercent').html(res.data.PERCENT_ORDER);
                    $('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                    $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
                    if(res.data.ORDERINCRDECR =='red'){
                        $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
                    }else{
                        $('#topsellingimg').attr('src', './images/uparrow-green.svg');
                    }

                    $('#topitemearnpercent').html(res.data.PERCENT_TOP_ITEM_EARN);
                    $('#topitemearnpercent').css("color", res.data.ORDERINCRDECR);
                    $('#topitemearnpercentimg').css("color", res.data.ORDERINCRDECR);
                    $('#topitemearnThisweek').html((res.data.THIS_WEEK_TOP_ITEM_EARN).toFixed(2));
                    $('#topitemearnLastweek').html((res.data.LAST_WEEK_TOP_ITEM_EARN).toFixed(2));
                    if(res.data.ORDERINCRDECR =='red'){
                        $('#topitemearnimg').attr('src', './images/down-arrow-red.svg');
                    }else{
                        $('#topitemearnimg').attr('src', './images/uparrow-green.svg');
                    }

                    $('#topsectionpercent').html(res.data.PERCENT_ORDER);
                    $('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                    $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                    $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
                    if(res.data.ORDERINCRDECR =='red'){
                        $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
                    }else{
                        $('#topsectionimg').attr('src', './images/uparrow-green.svg');
                    }

                }
            },error: function(res) {

            }
        });

    }
    OrdersHistoryCounts(7);
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#download_itemized_sales_history").click(function(){
            var date = new Date();
            TableToExcel.convert(document.getElementById("itemizedsalehistory"), {

                name: "itemizedsalehistory" +date+ ".xlsx",
                sheet: {
                    name: "Itemized Sales History"
                }
            });
        });

    });
</script>