<div>
    <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog"
         aria-labelledby="sidebar-option-lable">
        <div class="modal-dialog" role="sidebar options">
            <div class="modal-content bg-black">

                <div class="modal-body sidebar-options">
                    <header>
                        <img class="logo" src="./images/pekkish-logo.svg" alt="">
                        <button type="button" class="close closebtn " data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </header>
                    <nav class="navbar-options">
                        <ul class="nav flex-column flex-nowrap overflow-hidden">
                            <li class="nav-item">
                                <a class="nav-link text-truncate active-links" href="{{url('dashboard')}}">
                                    <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                    <span class=" menu-link">Dashboard</span></a>
                            </li>
                            {{--@if(session('user_details')->type == 'shop' || in_array(60, json_decode(roles['permissions'])))--}}
                            @if(session('user_details')->type == 'shop')
                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu"
                                       data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('orders')}}"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="{{url('order_history')}}"><span>History</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link text-truncate" href="{{url('menu')}}">
                                    <img class="nav-icons" src="./images/menu.svg" alt="">
                                    <span class="menu-link">Menu</span></a>
                            </li>
                            @if(session('user_details')->type == 'shop')
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('business_hours/') }}">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('accounts/') }}">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span class=" menu-link">Accounts</span></a>
                                </li>
                                <li class="nav-item">
                                    @if(session('user_details')->type == 'shop')
                                        <a class="nav-link collapsed py-1" href="{{ url('notifications/')}}">
                                            <img class="nav-icons" src="./images/side-notification.svg" alt="">
                                            <span class=" menu-link">Notifications</span></a>
                                    @endif
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('feedback_reviews/') }}">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks & Reviews</span></a>
                                </li>

                            @endif
                            @if(session('user_details')->type == 'seller')
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('offer_list/') }}">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>
                            @endif

                            <li class="nav-item ">
                                <a class="nav-link collapsed text-truncate" href="#settings-menu"
                                   data-toggle="collapse"
                                   data-target="#settings-menu">
                                    <img class="nav-icons" src="./images/settings.svg" alt="">
                                    <span class="">Settings</span></a>
                                <div class="collapse" id="settings-menu" aria-expanded="false">
                                    <ul class="flex-column pl-2 nav">
                                        <li class="nav-item">
                                            <a class="nav-link collapsed py-1"
                                               href="{{ url('manage_employees/')}}"><span>Manage Employees</span></a>
                                            <a class="nav-link collapsed py-1" href="{{ url('manage_roles/')}}"><span>Manage Roles</span></a>
                                            @if(session('user_details')->type == 'seller')
                                                <a class="nav-link collapsed py-1"
                                                   href="{{ url('manage_restaurant/')}}"><span>Manage Restaurants</span></a>
                                            @endif
                                            @if(session('user_details')->type == 'shop')
                                                <a class="nav-link collapsed py-1"
                                                   href="{{ url('time_settings/')}}"><span>Time Settings</span></a>
                                            @endif
                                            @if(session('user_details')->type == 'shop')
                                                <a class="nav-link collapsed py-1" href="{{ url('cusines/')}}"><span>Cuisines</span></a>
                                            @endif

                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <div class="mt-2">

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('help/') }}">
                                        <img class="nav-icons" src="./images/help.svg" alt="">
                                        <span class=" menu-link">Help</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('term_conditions/') }}">
                                        <img class="nav-icons" src="./images/terms.svg" alt="">
                                        <span class=" menu-link f-12">Terms & Conditions</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{ url('faqs/') }}">
                                        <img class="nav-icons" src="./images/faq.svg" alt="">
                                        <span class=" menu-link f-12">FAQ</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{url('logout/')}}">
                                        <img class="nav-icons" src="./images/logout.svg" alt="">
                                        <span class=" menu-link">Logout</span></a>
                                </li>
                            </div>


                            <!-- <li class="nav-item">
                              <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse" data-target="#orders-menu"><i class="fa fa-table"></i> <span class="">Reports</span></a>
                              <div class="collapse" id="orders-menu" aria-expanded="false">
                                  <ul class="flex-column pl-2 nav">
                                      <li class="nav-item"><a class="nav-link py-0" href="#"><span>Orders</span></a></li>
                                      <li class="nav-item">
                                          <a class="nav-link collapsed py-1" href="#orders-menusub1" data-toggle="collapse" data-target="#orders-menusub1"><span>Customers</span></a>
                                          <div class="collapse" id="orders-menusub1" aria-expanded="false">
                                              <ul class="flex-column nav pl-4">
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-clock-o"></i> Daily </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-dashboard"></i> Dashboard </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-bar-chart"></i> Charts </a>
                                                  </li>
                                                  <li class="nav-item">
                                                      <a class="nav-link p-1" href="#">
                                                          <i class="fa fa-fw fa-compass"></i> Areas </a>
                                                  </li>
                                              </ul>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                          </li> -->
                        </ul>

                    </nav>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div>
</div>

