<div class="shadow-pk sticky-top">
    <div class="main-header max-content-width">
        <div class="d-flex align-items-center justify-content-center">
            <img class="menuicon" src="{{url('/')}}/images/menuicon.svg" alt="" data-toggle="modal" data-target="#sidebaroptionid">
            <img src="./images/pekkish-logo.svg" alt="">
        </div>



        <div class="d-flex align-items-center justify-content-center">
 
            {{--    <div class="text-center">
                    @if(session()->get('storeid'))
                    <select name="store" id="store" class="select-drop-down" onchange="storechange(this.value);">
                        <option value="">Select Store</option>
                         @foreach ($usershops as $usershop)
                            <option value="{{ $usershop->id }}" @if($usershop->id==session()->get('storeid')) selected @endif>{{ $usershop->name }}</option>
                        @endforeach
                      <!--<option value="323"  @if(session()->get('storeid')==323) selected @endif>Venkat</option>-->
                        </select>
                    @endif
                </div>--}}

                <span class="usericons">
                    @if(session('user_details')->avatar_original == '') 
                        <img src="./images/user.jpg" alt="">
                    @else
                    <img src="{{session('user_details')->avatar_original}}" alt="">
                    @endif
                        <span class="dot bg-red"></span>
                </span>
            <div class="user-details" style="cursor:pointer">
                <div class="mx-2" data-toggle="modal" data-target="#userprofileid">
                    <p class="user-name">Hello, {{session('user_details')->first_name ?? ''}}</p>
                    <div class="role">{{session('user_details')->type ?? ''}}</div>
                </div>
                <img src="./images/downarrowicon.svg" alt="">
            </div>

        </div>
    </div>
</div>

