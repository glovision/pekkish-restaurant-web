<div>
    <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog"
         aria-labelledby="user-profile">
        <div class="modal-dialog" role="profile dialog">
            <div class="modal-content bg-black">

                <!-- <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                              aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="user-profile">Right Sidebar</h4>
                  </div> -->

                <div class="modal-body">
                    @if(session('user_details')->type == 'shop')
                        <div class="outerDivFull">

                            <div class="switchToggle">
                                <input type="checkbox" id="switch" checked value='1' onclick="shopoffline(this.value);">


                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                    @endif
                    {{--@if(session()->has('success'))--}}
                    {{--<div class="alert alert-success alert-dismissable">{{session()->get('success')}}</div>--}}
                    {{--@endif--}}
                    {{--@if(session()->has('failed'))--}}
                    {{--<div class="alert alert-danger alert-dismissable">{{session()->get('failed')}}</div>--}}
                    {{--@endif--}}

                    <div class="text-center">
                        @if(session('user_details')->avatar_original == '')
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                        @else
                            <img src="{{session('user_details')->avatar}}" alt="" class="userprofileimg">
                        @endif
                        <p class="f-16 f-medium text-color-white mt-4 mb-1">
                            Hello, {{session('user_details')->first_name ?? ''}} </p>
                        <p class="f-14 text-color-grey mb-5"> {{session('user_details')->type ?? ''}}</p>
                    </div>
                    <div>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                {{--<li class="nav-item">--}}
                                {{--<a class="nav-link text-truncate" href="#">--}}
                                {{--<span class=" menu-link" onclick="update_email();">Update Email Address</span></a>--}}
                                {{--</li>--}}

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="#">
                                        <span class=" menu-link" onclick="password_reset()">Reset Password</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="#">
                                        <span class=" menu-link" onclick="profupdmodel();">Update</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="{{url('logout/')}}">
                                        <img class="nav-icons" src="./images/logout.svg" alt="">
                                        <span class=" menu-link">Logout</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="passwordreset_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{url('ajaxpasswordreset')}}" method="POST">
                    @csrf
                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="text" id="oldpassword" name="oldpassword" class="form-control" required>
                            <div class="text-danger">{{ $errors->first('oldpassword') }}</div>
                            <label for="name">Old Password</label>
                        </div>
                        <div class="form-group input-material mr-2">
                            <input type="text" id="newpassword" name="newpassword" class="form-control"
                                   onblur="checknewpassword(this.value);" required>
                            <label for="name">New Password</label>
                            <div class="text-danger">{{ $errors->first('newpassword') }}
                                <div class="text-danger" id="newpasswordvaliderror"></div>
                            </div>
                            <div class="form-group input-material mr-2">
                                <input type="text" id="confirmpassword" name="confirmpassword" class="form-control"
                                       onblur="checkconfirmpassword(this.value)" required>
                                <label for="name">Confirm Password</label>
                                <div class="text-danger">{{ $errors->first('confirmpassword') }}</div>
                                <div class="text-danger" id="confirmpasswordvaliderror"></div>
                            </div>

                        </div>
                        <div class="mb-2">
                            <div class=" mb-3 d-flex align-items-center justify-content-around">

                                <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                        aria-label="Close">NO
                                </button>
                                <input type="submit" id="resetbutton" class="addbtn w-100 mx-3 f-medium" value="YES">
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="updateemail_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST" action="{{url('ajaxupdateemailaddres')}}">
                    @csrf
                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>

                        <div class="form-group input-material mr-2">
                            <input type="email" id="newemail" name="newemail" class="form-control"
                                   onblur="emailverification(this.value)" required>
                            <label for="name">Email</label>
                            <div class="text-danger" id="emailerrormsg"></div>
                        </div>

                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">
                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO
                            </button>
                            <button type="submit" id="emailupdatesave" class="addbtn w-100 mx-3 f-medium">YES</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="shopschedule_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="f-20 f-medium">Schedule Timing</div>
                <div class="row modal-body">
                    <div class="col-6">
                        <label class="radio">Current
                            <input type="radio" name="shopoffon" id="current" onclick="currentschedule()">
                            <span class="checkround"></span>
                        </label>
                    </div>
                    <div class="col-6">
                        <label class="radio">Schedule
                            <input type="radio" name="shopoffon" id="schedule" onclick="featureschedule()">
                            <span class="checkround"></span>
                        </label>
                    </div>
                </div>

                <div id="manuallyonoff">
                    <div class="modal-body p-4">
                        <input type="hidden" id="shopcurrsts">
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to put shop <span id="onoffstsnow"></span></p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close" onclick="location.reload()">NO
                            </button>
                            <a class="addbtn w-100 mx-3 f-medium" onclick="manuallychange();">YES</a>
                        </div>
                    </div>
                </div>

                <form action="{{url('ajaxshopscheduleoff')}}" method="POST" id="scheduleoffline">
                    @csrf
                    <div class="modal-body p-4">
                        <div class="form-group input-material mr-2">
                            <input type="text" id="reason" name="reason" class="form-control" required>
                            <label for="name">Reason</label>
                        </div>
                        <div class="mt-2 mb-0">
                            <p class="mb-0 text-color-grey f-14">Set Timings</p>
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group input-material mr-2">
                                            <input type="datetime-local" class="form-control" name="from_time"
                                                   id="from_time" required>
                                            <label for="from_time" class="datetype" style="top: -16px;">From
                                                Time</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group input-material mr-2">
                                            <input type="datetime-local" class="form-control" name="to_time"
                                                   id="to_time" required>
                                            <label for="to_time" class="datetype" style="top: -12px;">To Time</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-5 d-flex align-items-center justify-content-between">
                            <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                            <button class="addbtn">Schedule</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="profupd_model" tabindex="-1" role="dialog" aria-labelledby="profupdTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="signin">
        <div class="modal-content">

            <div class="modal-body p-4 registration-form ">
                <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                    <img src="./images/dialogclose.svg" alt="">
                </div>
                <h6 class="f-24 f-medium">Profile Update</h6>
                <form id="profupdform" method="POST" action="{{url('profupd')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group input-material mr-2">
                        <input type="hidden" class="form-control" name="id" id="profid"
                               value="{{session('user_details')->id}}">
                        <input type="text" class="form-control" name="name" id="profname"
                               data-validation="required" value="{{session('user_details')->first_name ?? ''}}">
                        <label for="Name"> First Name</label>
                        @if ($errors->has('name'))
                            <div class="text-danger">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="last_name" id="proflast_name"
                               value="{{session('user_details')->last_name ?? ''}}" data-validation="required">
                        <label for="Store Address">Last Name</label>
                        <div class="text-danger">{{ $errors->first('last_name') }}</div>
                    </div>

                    <div class="form-group input-material mr-2">
                        <input type="text" class="form-control" name="address" id="profaddress"
                               value="{{session('user_details')->address ?? ''}}">
                        <label for="Address">Address</label>
                    </div>

                    {{--<div class="form-group input-material mr-2">--}}
                    {{--<input type="text" class="form-control"  name="city" id="profcity"--}}
                    {{--value="{{session('user_details')->city ?? ''}}">--}}
                    {{--<label for="City">City</label>--}}
                    {{--</div>--}}

                    <div class="form-group input-material mr-2">
                        <img src="{{session('user_details')->avatar ?? ''}}" class="resturant-add-logo-img" alt=""
                             id="image-profile" class="img-fluid">
                        <input type="file" id="avatar" name="avatar" class="form-control"
                               onchange="readavatarURL(this);">
                        <label for="avatar" style="top:-14px;">logo<span style="color:red">*</span></label>
                        <div class="text-danger">{{ $errors->first('avatar') }}</div>
                        <div class="text-danger" id="imagesizevaliderror"></div>
                    </div>

                    <div class="mt-5 d-flex align-items-center justify-content-between">
                        <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                        <button class="addbtn">Submit</button>
                    </div>

                </form>

            </div>


        </div>
    </div>
</div>


