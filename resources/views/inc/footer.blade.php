<footer>

    <div class="max-content-width footer-section">
        <img src="{{url('/')}}/images/pekkish-logo.svg" alt="">
        <div class="copyright">
            Copyright © 2021-2021 pekkish.com
        </div>
    </div>
</footer>