@extends('layouts.app')
@section('content')
    <div class="container-fluid top-bannerbg">
        <div class="row">
            <div class="col-xl-4">
                <div class="d-flex justify-content-center">
                    <h1 class="banner-text mt-5 pl-5">Discover delicious
                        <br>food near you.</h1>
                    <p class="banner-smalltext pl-5">Fast, easy and reliable delivery</p>
                </div>

            </div>
            <div class="col-xl-8">
                <div id="owl-carousel" class="owl-carousel owl-theme pt-xl-5 pb-xl-4 pr-xl-5">
                    <div>
                        <img src="images/top_slider1.png">
                    </div>
                    <div>

                        <img src="images/top_slider2.png">

                    </div>
                    <div>
                        <img src="images/top_slider1.png">
                    </div>
                    <div >
                        <img src="images/top_slider2.png">
                    </div>
                    <!-- <div class="item">
                                  5
                                </div>
                                <div class="item">
                                  6
                                </div> -->


                </div>

            </div>
        </div>
    </div>
    <!-- top banner slider end -->
    <section class="layout-bg">




        <!-- restaurants  start-->
        <div class="container-fluid pt-5 ">

            <div class="d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <div class="mr-auto p-2">
                    <h1 class="f-24">48 restaurants</h1>
                </div>
                <div class="p-2">
                    <img src="images/res_img1.png" class="mr-4">
                    <p class="f-12-R pt-2 pl-2">Best in<br>
                        Safety</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img2.png"  class="mr-4">
                    <p class="f-12-R pt-2">&nbsp;&nbsp; &nbsp;Free
                        <br> &nbsp;&nbsp;Delivery</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img3.png"  class="mr-4 mt-1">
                    <p class="f-12-R "> &nbsp;&nbsp; &nbsp;Newly <br>Launched</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img4.png"  class="mr-4">
                    <p class="f-12-R pt-2">Healthy<br>&nbsp;&nbsp; Picks</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img5.png"  class="mr-4">
                    <p class="f-12-R pt-2">Non-Veg <br>&nbsp;&nbsp; &nbsp;Only</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img6.png"  class="mr-4">
                    <p class="f-12-R pt-2">Express <br> Delivery</p>
                </div>
                <div class="p-2">
                    <img src="images/res_img7.png"  class="mr-4">
                    <p class="f-12-R pt-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Top <br>&nbsp;&nbsp; Rated</p>
                </div>
            </div>
            <hr>
        </div>

        <!-- restaurants end -->

        <!-- Pekkish Picks start -->
        <div class="container-fluid  pt-xl-2 mt-xl-2 pt-sm-2 mt-sm-2">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Pekkish Picks</h3>
                </div>
                <div class="p-2 bd-highlight pr-sm-1 pr-md-3">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
        <span class="my-next-button" id="pekkishpicks-next-button">
          <i><img src="images/slider_left.svg" alt="" title=""></i>
        </span>
                        <span class="my-prev-button" id="pekkishpicks-prev-button">
          <i><img src="images/slider_right.svg" alt="" title=""></i>
        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="pekkishpicks">
                <div class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1">  <img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"> <img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1">  <img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="#"  class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png">
                            <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                            <div class="card-body">
                                <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                             src="images/clock.png">40 - 50 mins</p>

                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pekkish Picks end -->

        <!-- Download our mobile app now!!! -->

        <div class="container-fluid download-appsbg mt-xl-3 mt-sm-0 mt-md-2 pb-sm-5">

            <div class="d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <div class="mr-auto p-2">
                    <h1 class="mt-xl-5 mt-sm-0 mt-md-1 pl-4"> <img src ="images/download_1.svg"  alt="" title="" class="download-appimg"><span class="download-apptext">Download our mobile app now!!!</span></h1>
                </div>
                <div class="p-2 mt-xl-5 mt-sm-0 mt-md-2">
                    <img src="images/playstore.png">
                </div>
                <div class="p-2 pr-xl-5 mt-xl-5 mt-sm-0 mt-md-1 ">
                    <img src="images/ios.png">
                </div>

            </div>

        </div>

        <!-- Download our mobile app now!!! end -->

        <!-- food near you start -->
        <div class="container-fluid  pt-4 mt-4">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Food Near You</h3>
                </div>
                <div class="p-2 bd-highlight pr-sm-1 pr-md-3">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
        <span class="my-next-button" id="toppicks-next-button">
          <i><img src="images/slider_left.svg" alt="" title=""></i>
        </span>
                        <span class="my-prev-button" id="toppicks-prev-button">
          <i><img src="images/slider_right.svg" alt="" title=""></i>
        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="toppicks">
                <div class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/freedelivery.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- food near you end -->

        <!-- Today’s Promos start -->
        <div class="container-fluid  pt-4 mt-4">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight">
                    <h3 class="f-18">Today’s Promos</h3>
                </div>
                <div class="p-2 bd-highlight pr-sm-1 pr-md-3">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
        <span class="my-next-button" id="promo-next-button">
          <i><img src="images/slider_left.svg" alt="" title=""></i>
        </span>
                        <span class="my-prev-button" id="promo-prev-button">
          <i><img src="images/slider_right.svg" alt="" title=""></i>
        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="promo">
                <div class="mr-3">
                    <a href="#" class="zoom-effect-1">  <img src="images/promo_1.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"><img src="images/promo_2.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"><img src="images/promo_3.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"> <img src="images/promo_1.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"> <img src="images/promo_2.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"> <img src="images/promo_3.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"> <img src="images/promo_1.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"><img src="images/promo_2.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <img src="images/promo_3.png" class="img-fluid img-radius" alt="" title="">
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"><img src="images/promo_1.png" class="img-fluid img-radius" alt="" title=""></a>

                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"><img src="images/promo_2.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
                <div  class="mr-3">
                    <a href="#" class="zoom-effect-1"> <img src="images/promo_3.png" class="img-fluid img-radius" alt="" title=""></a>
                </div>
            </div>
            <hr>
        </div>
        <!-- Today’s Promos end -->

        <!-- Buy 1 Get 1 Free -->
        <div class="container-fluid pt-4 mt-4">
            <div class="d-flex bd-highlight mb-3">
                <div class="mr-auto p-2 bd-highlight pr-sm-1 pr-md-3">
                    <h3 class="f-18">Buy 1 Get 1 Free</h3>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#" class="view-all">View All</a>
                </div>
                <div class="p-2 bd-highlight">

                    <div class="my-owl-nav">
        <span class="my-next-button" id="free-next-button">
          <i><img src="images/slider_left.svg" alt="" title=""></i>
        </span>
                        <span class="my-prev-button" id="free-prev-button">
          <i><img src="images/slider_right.svg" alt="" title=""></i>
        </span>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme" id="free">
                <div class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>

                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>
                            <button type="button" class="btn rating-btn">
                                <span class="fa fa-star pr-2"></span>
                                <span class="star-text">4.5</span>
                            </button>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>

                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_2.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
                <div  class="mr-3">
                    <div class="card">
                        <a href="restaurant.html" class="zoom-effect-1"><img class="img-fluid img-radius" alt="" src="images/picks_1.png"></a>
                        <a href="#"><img class=" img-display fav-slidertop" src="images/fav.png"></a>
                        <a href="#"><img class=" img-fluid freedelivery-startop img-display" src="images/buy.svg"></a>
                        <div class="card-body">
                            <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                            <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                            <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                         src="images/clock.png">40 - 50 mins</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Buy 1 Get 1 Free end -->

        <!-- Download our mobile app now!!! -->

        <div class="container-fluid download-appsbg mt-xl-4 mt-sm-0 mt-md-2 pb-sm-5 mb-4">

            <div class="d-flex flex-sm-row flex-column justify-content-center align-items-center">
                <div class="mr-auto p-2">
                    <h1 class="mt-xl-5 mt-sm-0 mt-md-1 pl-5"> <img src ="images/download_1.svg"  alt="" title="" class="download-appimg"><span class="download-apptext">Download our mobile app now!!!</span></h1>
                </div>
                <div class="p-2 mt-xl-5 mt-sm-0 mt-md-2">
                    <img src="images/playstore.png">
                </div>
                <div class="p-2 pr-xl-5 mt-xl-5 mt-sm-0 mt-md-1 ">
                    <img src="images/ios.png">
                </div>

            </div>
        </div>

        <!-- Download our mobile app now!!! end -->

        <!-- All Restaurants -->

        <div class="container-fluid pt-4 mt-4">
            <div class="row">
                <div class="col-xl-3">
                    <h1 class="f-24">All Restaurants</h1>
                    <div id="accordion">
                        <div class="">
                            <div class="card-bg" id="heading-1">
                                <h5 class="mb-0 f-18">
                                    <a role="button" class="card-check" data-toggle="collapse" href="#collapse-1" aria-expanded="true" aria-controls="collapse-1" style="text-decoration: none;"> Sort By<i class="fa fa-angle-down dropdown-arrow" aria-hidden="true"></i></a>
                                </h5>
                            </div>
                            <div id="collapse-1" class="collapse show" data-parent="#accordion" aria-labelledby="heading-1">
                                <div class="card-body">

                                    <div class="d-flex flex-column bd-highlight mb-3">
                                        <div class="p-2 bd-highlight">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label pl-3 f-14-labelactive" for="customRadioInline1">Relavance</label>
                                            </div>
                                        </div>
                                        <div class="p-2 bd-highlight">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label pl-3 f-14-label" for="customRadioInline1">Cost</label>
                                            </div>
                                        </div>
                                        <div class="p-2 bd-highlight">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label pl-3 f-14-label" for="customRadioInline1">Delivery Time</label>
                                            </div>
                                        </div>
                                        <div class="p-2 bd-highlight">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                                <label class="custom-control-label pl-3 f-14-label" for="customRadioInline1">Rating</label>
                                            </div>
                                        </div>
                                    </div>






                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="card-bg" id="heading-2">
                                <h5 class="mb-0 f-18">
                                    <a class="collapsed card-check"  role="button" data-toggle="collapse" href="#collapse-2" aria-expanded="false" aria-controls="collapse-2" style="text-decoration: none;"> Cuisines<i class="fa fa-angle-down dropdown-arrow" aria-hidden="true"></i></a>
                                </h5>
                            </div>
                            <div id="collapse-2" class="collapse" data-parent="#accordion" aria-labelledby="heading-2">
                                <div class="card-body">
                                    Text 2
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="card-bg" id="heading-3">
                                <h5 class="mb-0 f-18">
                                    <a class="collapsed card-check" role="button" data-toggle="collapse" href="#collapse-3" aria-expanded="false" aria-controls="collapse-3" style="text-decoration: none;">
                                        Offers<i class="fa fa-angle-down dropdown-arrow" aria-hidden="true"></i>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse-3" class="collapse" data-parent="#accordion" aria-labelledby="heading-3">
                                <div class="card-body">
                                    Text 3
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="card-bg" id="heading-4">
                                <h5 class="mb-0 f-18">
                                    <a class="collapsed card-check"  role="button" data-toggle="collapse" href="#collapse-4" aria-expanded="false" aria-controls="collapse-4" style="text-decoration: none;">
                                        <i class="fa fa-angle-down dropdown-arrow" aria-hidden="true"></i> Search by
                                    </a>
                                </h5>
                            </div>
                            <div id="collapse-4" class="collapse" data-parent="#accordion" aria-labelledby="heading-4">
                                <div class="card-body">
                                    Text 3
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 all-bg">
                    <div class="row mt-3">
                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#" ><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#"><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#" ><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#"><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#"><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 mb-3">
                            <div class="zoom-effect-1">

                                <a href="#" ><img class="img-fluid img-radius" alt="" src="images/toppicks_1.png"></a>
                                <a href="#"><img class=" img-display buy-slidertop" src="images/fav.png"></a>
                                <a href="#"><img class=" img-fluid buyone-startop img-display" src="images/buy.svg"></a>

                                <div class="card-body">
                                    <h4 class="order-delivery favourites-text">South Grill Chicken</h4>
                                    <p class="favourites-p favourites-text">Chinese, Indian, Arabian, Soups</p>
                                    <p class="favourites-p favourites-text"><img class="mr-2 clock-top img-display"
                                                                                 src="images/clock.png">40 - 50 mins</p>
                                    <button type="button" class="btn buy-btn">
                                        <span class="fa fa-star pr-2"></span>
                                        <span class="star-text">4.5</span>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- All Restaurants -->


    </section>
@endsection