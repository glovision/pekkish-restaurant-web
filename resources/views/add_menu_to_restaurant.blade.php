@extends('layouts.app-new',["titlePage" => "Add Menu to Restaurant"])
@push('css')
<style>
    .stv-radio-tabs-wrapper {
        display: inline-block;
        width: 100%;
        border-bottom: 1px solid #428bca;
        padding: 0px;
    }

    input.stv-radio-tab {
        position: absolute;
        left: -99999em;
        top: -99999em;
    }

    input.stv-radio-tab + label {
        cursor: pointer;
        float: left;
        border: 1px solid #aaa;
        border-bottom: 0;
        background-color: #fff;
        margin-right: -1px;
        padding: 0.5em 1em;
        position: relative;
        margin-bottom: 0;
        width: 50%;
    }

    input.stv-radio-tab + label:hover {
        background-color: #eee;
    }

    input.stv-radio-tab:checked + label {
        box-shadow: 0 3px 0 -1px #fff, inset 0 5px 0 -1px #13cd4a;
        background-color: #fff;
        border-color: #428bca;
        z-index: 1;
    }

    .category-scroll {
        min-height: calc(100vh - 239px);
        max-height: calc(100vh - 239px);
        overflow-y: scroll;
        -ms-overflow-style: none;
        scrollbar-width: thin;
        mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        mask-size: 100% 20000px;
        mask-position: left bottom;
        -webkit-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -webkit-mask-size: 100% 20000px;
        -webkit-mask-position: left bottom;
        -moz-mask-image: linear-gradient(to top, black, black), linear-gradient(to left, transparent 17px, black 17px);
        -moz-mask-size: 100% 20000px;
        -moz-mask-position: left bottom;
        transition: mask-position 0.3s, -webkit-mask-position 0.3s;
    }

    .category-scroll::-webkit-scrollbar {
        display: none;

    }

    .svg-invert { /* svg on an img tag */
        -webkit-filter: invert(.75); /* safari 6.0 - 9.0 */
        filter: invert(.75);
    }

    .dataTable-wrapper {
        position: relative;
    }

    .dataTable-top {
        position: absolute;
        bottom: 0;
    }

    .dataTable-bottom {
        margin: 0px 18% !important;
    }

    .dataTable-info {
        margin: 12px 0;
    }

    .dataTable-bottom, .dataTable-top {
        padding: 0px 20px;
        font-size: 12px;
        margin-top: 2rem;
    }

    .dataTable-wrapper.no-footer .dataTable-container {
        border-bottom: none;
    }
</style>
<style>
    .avatar-upload {
        position: relative;
        max-width: 100%;
        height:50%;
        margin: 5px;
    }
    .avatar-upload .avatar-edit {
        position: absolute;
        right: 0px;
        z-index: 1;
        top: 0px;
        width: 100%;
        height: 100%;
    }
    .avatar-upload .avatar-edit input {
        display: none;
    }
    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 100%;
        height: 100%;
        margin-bottom: 0;
        background: transparent;
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
        background: transparent;
    }
    .avatar-upload .avatar-edit input + label:after {
        position: absolute;
        top: 6px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }
    .avatar-upload .avatar-preview {
        width: 100%;
        height: 100%;
        position: relative;
        font-size: 14px;
    }
    .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 0%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

</style>
<style>
    .modal-header {
        padding: 2rem 2rem 1rem 2rem;
    }
    .modal-body {
        padding: 0rem 2rem !important;
    }
    .modal-footer {
        background-color: transparent;
        border-top: 1px solid transparent;
    }
    .modal-header .close {
        padding: 0rem 0rem;
        margin: -1.5rem -1rem -1rem auto;
    }
    .form-group.input-material {
        position: relative;
        margin-top: 0px;
        margin-bottom: 26px;
    }
    .add-on-div{
        padding-top: 20px;
        margin: 20px 0;
    }
    .slim-scroll{
        height:100% !important;
    }
    .slimScrollDiv{
        height:100% !important;
    }
    .max-length_word {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 45px;
        display: inline-block;
        top: 2px;
        position: relative;
        right: 4px;
    }
    .vcenter {
        display: inline-block;
        vertical-align: middle;
        float: none;
    }

    .p_a_sub_add_on_img{
        background-color:white;
        color:#a7a796;
        border-radius:10px !important;
        vertical-align:center;
        text-align: center;
        padding-top: 30%;
    }
    .select2-container .select2-selection--multiple {
        border-top: 0px;
        border-right: 0px;
        border-bottom: 1px solid black;
        border-left: 0px;
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border-top: 0px;
        border-right: 0px;
        border-bottom: 1px solid black;
        border-left: 0px;
    }
</style>
<link rel="stylesheet" href="{{asset('css/landing-page.css')}}">
@endpush
@section('content')
    <form id="manage-restaurant-form" method="POST" action="{{url('addSellerItemsToStore')}}" enctype="multipart/form-data">
        @csrf
    <div class="container-fluid">
        <div class="row" style="height: calc(100vh - 60px);">
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 bg-black pt-3 menu-list-fixed" style="max-height: 95vh;overflow: auto">
                    <div class="slim-scroll">
                    <nav class="navbar-options menu-list-items" style="min-height: 83vh;">

                        <ul class="nav md-pills pills-secondary flex-column">
                            <span style="display:none;">{{$i=0}}</span>
                            @foreach($items["menu"] ?? [] as $key=>$category)
                                @if($category["add_ons_count"] > 0)
                                    <li class="nav-item">
                                            <span class="d-flex">
                                                <a class="mr-auto nav-link collapsed text-truncate" href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu"
                                                   data-toggle="collapse"
                                                   data-target="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu">
                                                    <span class="">{{$category['name']}}</span></a>
                                    <a class="nav-link">
                                            <span class="d-flex">
                                              <input type="checkbox" name="category[]" value="{{$category['id']}}" id="checkAll{{$category['id']}}" onclick="allSellermeneu({{$category['id']}});" checked/>
                                            </span>
                                    </a>
                                            </span>
                                        <div class="collapse mb-auto" id="{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}-menu" aria-expanded="false">
                                            <ul class="flex-column pl-2 nav">
                                                <li class="nav-item">
                                                    <a class="nav-link @if($i++==0) active @endif" data-toggle="tab"
                                                       href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}"
                                                       role="tab">All Items</a>

                                                    <a class="nav-link" data-toggle="tab"
                                                       href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))."-add-on"}}"
                                                       role="tab">All Add ons</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                @else
                                    <li class="nav-item d-flex">
                                        <a class="mr-auto nav-link @if($i++==0) active @endif" data-toggle="tab"
                                           href="#{{strtolower(preg_replace('/\PL/u', '', $category['name']))}}"
                                           role="tab">{{$category['name']}}</a>
                                        <a class="nav-link">
                                            <span class="d-flex">
                                                <input type="checkbox" name="category[]" value="{{$category['id']}}" id="checkAll{{$category['id']}}" onclick="allSellermeneu({{$category['id']}});" checked/>
                                            </span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10 main-section-padding">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
                @endif
                @if(session()->has('failed'))
                    <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
                @endif
                <div class="tab-content pt-0">
                    <span style="display:none;">{{$i=0}}</span>
                    @foreach($items["products"] ?? [] as $product)
                        <div class="tab-pane fade @if($i++==0)in show active @endif"
                             id="{{strtolower(preg_replace('/\PL/u', '',$product["category"]))}}" role="tabpanel">
                            <div class="res-cards d-flex align-items-center justify-content-between pb-2">
                                <h4 class="heading"><span id="selectedcatg">{{$product["category"]}}</span></h4>
                                    <div class="justify-content-end">
                                        <input type="hidden" id="shopid" name="shopid" class="form-control" value="{{ request()->get('shopid') }}" required>
                                        <button class="addbtn"><span id="savebtn"></span> Submit</button>
                                    </div>
                            </div>

                            <div class="tab-content pt-0">
                                <div class="table-responsive">
                                    <table class="table" id="products">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Item Name</th>
                                            <th>Available Days</th>
                                            <th>Category</th>
                                            <th>Price</th>
                                            <th>Description</th>
                                            <th>Photo</th>
                                            <th>Accessibility</th>
                                            <th>Override</th>
                                            <th class="table-controls-button" id="table-button">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <ul class="controls-dropdown-box" id="controls-dropdown-menu"
                                                    style="display: none;"></ul>
                                            </th>

                                        </tr>
                                        </thead>
                                        @if(count($product["items"] ?? []) ==0 )
                                            <tbody>
                                            <tr>
                                                <td colspan="9">
                                                    No data available!
                                                </td>
                                            </tr>
                                            </tbody>
                                        @else
                                            <tbody>
                                            @foreach($product["items"] ?? [] as $item)
                                                <tr>
                                                    <td>
                                                        {{$item['name']}}
                                                    </td>
                                                    <td>
                                                        {{$item['name']}}
                                                    </td>
                                                    <td>
                                                        {{$product["category"]}}
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <span>{{$item['price_symbol']}}</span><span>{{$item['base_price']}}</span>
                                                        </div>
                                                    </td>
                                                    <td>{{$item['description'] ?? ""}}
                                                    </td>
                                                    <td>
                                                        <img class="tbl-img" src="{{$item['thumbnail_img']}}"
                                                             alt="">
                                                    </td>
                                                    <td>
                                                        <div class="checkbox checbox-switch switch-primary">
                                                            <label>
                                                                <input type="checkbox" name="item_access[]" value="{{$item['id']}}"
                                                                       class="access{{$item["category_id"]}}" {{($item['status']=='1')?"checked":""}} />
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="checkbox checbox-switch switch-primary">
                                                            <label>
                                                                <input type="checkbox" name="item_override[]" value="{{$item['id']}}"
                                                                       class="override{{$item["category_id"]}}"/>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @foreach($items["add_ons"] ?? [] as $add_on)
                        <div class="tab-pane fade"
                             id="{{strtolower(preg_replace('/\PL/u', '',$add_on["category"]))."-add-on"}}" role="tabpanel">
                            <div class="tab-content pt-0">
                                <div class="table-responsive">
                                    <table class="table" id="add_ons">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Add On Name</th>
                                            <th>Category</th>
                                            <th>Add On Description</th>
                                            <th>Accessibility</th>
                                            <th>Override</th>
                                            <th class="table-controls-button" id="table-button">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <ul class="controls-dropdown-box" id="controls-dropdown-menu"
                                                    style="display: none;"></ul>
                                            </th>

                                        </tr>
                                        </thead>
                                        @if(count($add_on["add_ons_data"] ?? []) ==0 )
                                            <tbody>
                                            <tr>
                                                <td colspan="6">
                                                    No data available!
                                                </td>
                                            </tr>
                                            </tbody>
                                        @else
                                            <tbody>
                                            @foreach($add_on["add_ons_data"] ?? [] as $add_on_data)
                                                <tr>
                                                    <td>
                                                        {{$add_on_data['name']}}
                                                    </td>
                                                    <td>
                                                        {{$add_on["category"]}}
                                                    </td>
                                                    <td>
                                                        {{$add_on_data['description'] ?? ""}}
                                                    </td>
                                                    <td>
                                                        <div class="checkbox checbox-switch switch-primary">
                                                            <label>
                                                                <input type="checkbox" name="add_on_access[]" value="{{$add_on_data['id']}}" class="access{{$add_on_data["id"]}}" {{($add_on_data['status']=='1')?"checked":""}}/>
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="checkbox checbox-switch switch-primary">
                                                            <label>
                                                                <input type="checkbox" name="add_on_override[]" value="{{$add_on_data['id']}}"
                                                                       class="override{{$add_on_data["id"]}}" />
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </form>
@endsection


@section('script')

    <script>
        var options = [];

        $('.dropdown-menu a').on('click', function (event) {

            var $target = $(event.currentTarget),
                val = $target.attr('data-value'),
                $inp = $target.find('input'),
                idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    $inp.prop('checked', false)
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    $inp.prop('checked', true)
                }, 0);
            }

            $(event.target).blur();

            console.log(options);
            return false;
        });
    </script>
    <script type="text/javascript">
        function allSellermeneu(data){
            var isChecked = $('#checkAll'+data).is(':checked');
            console.log('#access'+data+ '' +JSON.stringify(isChecked));
            if (isChecked) {
                $('.access'+data).prop('checked',true);
            }else{
                $('.access'+data).prop('checked', false);
            }
        }
    </script>

@endsection