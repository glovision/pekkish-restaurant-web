@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        <div class="container-fluid">
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 border-right-table ">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <form class="w-100" action="{{url('addWorkTiming')}}" method="POST">
                            @csrf
                            @if(session()->has('message'))
                                <div class="alert alert-danger alert-dismissable">{{ session()->get('message') }}</div>
                            @endif
                            <div>
                                <h4 class="heading mb-4">WORKING HOURS</h4>
                                <label class="radio">Always Open
                                    <input type="radio" name="working_status" id="allwaysopen" value="1">
                                    <span class="checkround"></span>
                                </label>
                                <label class="radio">Selected Business Hours
                                    <input type="radio" name="working_status" id="busineshours" value="2">
                                    <span class="checkround"></span>
                                </label>
                                <label class="radio">I will Turn On Manually
                                    <input type="radio" name="working_status" id="turnmanually" value="3">
                                    <span class="checkround"></span>
                                </label>
                            </div>
                            <div id="weakdays">
                                <h4 class="heading mb-4 mt-5">WORKING HOURS</h4>
                                <label class="d-flex align-items-center flex-wrap chk-toolbar">

                                    {{--<label class="employee-options" id="chk-roleAll" onclick="addalldaystiming()">
                                        All
                                        <input type="checkbox" name="days_to_run[]" id="chk-roleAll" value="All" style="display: none">
                                    </label>--}}
                                    @foreach ($days as $day)
                                        <label class="employee-options" id="chk-role{{$loop->index}}" onclick="addremovestyleoffer({{$loop->index}});">
                                            {{$day['name']}}
                                            <input type="checkbox" name="days_to_run[]" id="chkbtnoffer{{$loop->index}}" value="{{$day['id']}}" style="display: none">
                                        </label>
                                    @endforeach
                                    {{--<div class="text-danger">{{ $errors->first('days_to_run') }}</div>--}}
                                </div>
                                <div class="container-fluid p-0">
                                    <div class="row working-hours mt-3" id="addtimg">
                                    <div id="addtimgedit">
                                       <div class="col-12 col-sm-12 col-md-4" id="defaultall">
                                            <p class="f-10 mb-2">For</p>
                                            <select name="timing[0][workingdays]" id="workingdays" class="select-drop-down mb-4" data-validation="required">
                                                <option value="">Select</option>
                                                {{--<option value="all">All</option>--}}
                                                <option value="0" class="working-hours-selection working-hoursselection0">Sun</option>
                                                <option value="1" class="working-hours-selection working-hoursselection1">Mon</option>
                                                <option value="2" class="working-hours-selection working-hoursselection2">Tue</option>
                                                <option value="3" class="working-hours-selection working-hoursselection3">Wed</option>
                                                <option value="4" class="working-hours-selection working-hoursselection4">Thu</option>
                                                <option value="5" class="working-hours-selection working-hoursselection5">Fri</option>
                                                <option value="6" class="working-hours-selection working-hoursselection6">Sat</option>

                                            </select>
                                           <div class="text-danger">{{ $errors->first('workingdays') }}</div>
                                        </div>

                                        <div class="col-12 col-sm-12 col-md-4">
                                            <p class="f-10 mb-2">Form</p>
                                            <select name="timing[0][start]" id="fromtime" class="select-drop-down mb-4" data-validation="required">
                                                <option value="7">7 AM</option>
                                                <option value="8">8 AM</option>
                                                <option value="9">9 AM</option>
                                                <option value="10">10 AM</option>
                                                <option value="11">11 AM</option>
                                            </select>
                                            <div class="text-danger">{{ $errors->first('start') }}</div>
                                        </div>

                                        <div class="col-12 col-sm-12 col-md-4">
                                            <p class="f-10 mb-2">To</p>
                                            <select name="timing[0][end]" id="totime" class="select-drop-down mb-4" data-validation="required">
                                                <option value="7">7 PM</option>
                                                <option value="8">8 PM</option>
                                                <option value="9">9 PM</option>
                                                <option value="10">10 PM</option>
                                                <option value="11">11 PM</option>
                                            </select>
                                            <div class="text-danger">{{ $errors->first('end') }}</div>
                                        </div>
                                    </div>
                                    </div>

                                </div>

            {{--         <div class="d-flex justify-content-end">
                                <input type="hidden" class="form-control" id="dynamicaddonincr">
                                <p id="addmoretiming" class="add-more-working-hours" >+ Add More</p>
                            </div>--}}
                       {{--     <div id="foralldayscheck">
                                <label class="check">Mark this for all the days
                                    <input type="checkbox" name="foralldays" value="1" data-validation="required" required>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="text-danger">{{ $errors->first('foralldays') }}</div>--}}

                            <div class="mt-5 d-flex align-items-center justify-content-between">
                                {{--<input type="button" class="cancelbtn" value="CANCEL" />--}}
                                <button class="addbtn">ADD </button>
                            </div>

                        </form>

                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 border-left-table">
                    <div class="d-flex align-items-center justify-content-between pb-2">
                        <h4 class="heading">DUTY STATUS HISTORY</h4>
                        <form>
                            <select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>
                                <option value="14">last 14 days</option>
                                <option value="21">last 21 days</option>
                                <option value="28">last 28 days</option>

                            </select>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table" id="shop_timings">
                            <thead class="thead-light">
                            <tr>
                                <th>Date</th>
                                <th>Status</th>
                                <th>From - To</th>
                                <th>Duration</th>
                                <th>Reason</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dutyhistory as $dutyhist)
                            <tr>
                                <td>{{$dutyhist->from_time}}</td>
                                @if($dutyhist->status == 1)
                                <td class="text-color-green">Online</td>
                                @else($dutyhist->status == 0)
                                <td class="text-color-red">Offline</td>
                                @endif
                                <td>{{$dutyhist->from_time}} - {{$dutyhist->to_time}}</td>
                                <td>{{(new Carbon\Carbon($dutyhist->to_time))->diffInHours(new Carbon\Carbon($dutyhist->from_time))}} Hours</td>
                                <td>{{$dutyhist->reason}}</td>
                                <td>

                                    <a id="{{$dutyhist->id}}" onclick="update_status({{$dutyhist->id}},{{$dutyhist->status}});">
                                        <img class="mx-3" src="./images/card-edit.svg" alt="">
                                    </a>

                                </td>
                            </tr>
                            @endforeach



                            </tbody>
                        </table>
                    </div>
                    <div class="row float-right">

                    <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                        <div class="d-flex ">
                        </div>
                        <div id="download_shop_timings">
                            <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                            <img src="./images/download.svg" alt="">
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection

@section('modal')
    <div>
        @include('inc.side_nav')
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    console.log($('#days_to_run').val());

    $(document).ready(function () {
        $("#download_shop_timings").click(function(){
            var date = new Date();
            TableToExcel.convert(document.getElementById("shop_timings"), {

                name: "Shop_Timings_" +date+ ".xlsx",
                sheet: {
                    name: "Shop Timings"
                }
            });
        });
    });
    var selecteddays= [];
    function addremovestyleoffer(param){

        $('.schalldays').html('');
        $('#chk-roleAll').hide('');
        $('#chk-roleAll').removeClass('option-selected');
        $('#chkbtnofferAll').prop('checked', false);
        $('.working-hoursselectionAll').hide();

        if($('#chkbtnoffer'+param).is(":checked")){
            console.log("chkbtnoffer"+param);
            $('#chk-role'+param).addClass('option-selected');
            $('#chkbtnoffer'+param).prop('checked', true);
            $('.working-hoursselection'+param).show();
            selecteddays.push(param);
            console.log(selecteddays);
                if(jQuery.inArray( param, selecteddays )  != -1) {
                    console.log(param);
                    addweekrows(param);
                }
            
            //$('#addmoretiming').trigger('click');
            //for (var i = 0; i < selecteddays.length; i++) {                
                //$('#all').val(param);
           // }
        }else {
            console.log("chkbtnoffer"+param);
            $('#chk-role'+param).removeClass('option-selected');
            $('#chkbtnoffer'+param).prop('checked', false);
            $('.working-hoursselection'+param).hide();
            //selecteddays.push(param);
            //console.log(selecteddays);
        }
    }
    function unique(array){
        return array.filter(function(el, index, arr) {
            return index == arr.indexOf(el);
        });
    }
        


    $(".cancelbtn").click(function () {
        console.log("ererer");
        fetchshoptiming();
        $('.schalldays').html('');
        $('#chk-roleAll').hide('');
        $('#chk-roleAll').removeClass('option-selected');
        $('#chkbtnofferAll').prop('checked', false);
        $('.working-hoursselectionAll').hide();
    });
    var b = 0;
    $( document ).ready(function() {
        if($('#allwaysopen').val()==1){
            $('#weakdays').hide();
            $('#foralldayscheck').hide();
        }
        $('#allwaysopen').click(function(){       
            $('#addmoretiming').hide();
            $('#weakdays').hide();
            $('#addtimg').hide();
            $('#foralldayscheck').hide();
                   

            $('input[name="foralldays"][value="1"]').prop("checked", true);            
        });
        $('#turnmanually').click(function(){       
            $('#addmoretiming').hide();
            $('#weakdays').hide();
            $('#addtimg').hide();

            $('#foralldayscheck').hide();
            $('input[name="foralldays"][value="1"]').prop("checked", true);  
        });        
        $('#busineshours').click(function(){       
            $('#addmoretiming').show();
            $('#weakdays').show();
            $('#addtimg').show();

            $('#foralldayscheck').show();
            $('input[name="foralldays"]').prop("checked", false); 

            console.log(b);
                if(b==0){
                    $('#addmoretiming').trigger('click');
                }
            b++;     
        });
        fetchshoptiming();       
    });

    function fetchshoptiming(){
       var url = "{{url('ajaxBusinessTiming')}}";
        console.log(url);
        $.ajax({
            cache:false,
            type: "GET",
            url: url,
            success: function (res) {
               if(res.success) {
                   console.log("fun_invoice " + JSON.stringify(res.data.SHOP));
                   if(res.data.SHOP.status == 0){                    
                    $('input[name="working_status"][value=3]').prop('checked', 'checked');
                    $('#addmoretiming').hide();
                   }
                   if(res.data.SHOP.is_all_time_avaliable == 1){                    
                    $('input[name="working_status"][value=1]').prop('checked', 'checked');
                    $('#addmoretiming').hide();
                   }
                   $("#addtimgedit").html("");
                   var dataarry = res.data.TIMING;
                   if (res.data.SHOP.is_all_time_avaliable == 0 && res.data.SHOP.status == 1 && dataarry.length > 0){
                    $('#addmoretiming').show();
                       var data = res.data.TIMING[0];
                       console.log("fun_invoice2 " + data);
                       $('input[name="working_status"][value=2]').prop('checked', 'checked');
                   //var wdata = JSON.parse(data.timings);
                  // console.log("fun_invoice2 " + dataarry);
                   console.log("fun_invoice3 " + JSON.stringify(dataarry));
                    

                   for (i = 0; i < dataarry.length; i++) {
                    if ((dataarry[i]['day_id']) == 0) {
                           var dayname = "Sun";
                       }
                       if ((dataarry[i]['day_id']) == 1) {
                           var dayname = "Mon";
                       }
                       if ((dataarry[i]['day_id']) == 2) {
                           var dayname = "Tue";
                       }
                       if ((dataarry[i]['day_id']) == 3) {
                           var dayname = "Wed";
                       }
                       if ((dataarry[i]['day_id']) == 4) {
                           var dayname = "Thu";
                       }
                       if ((dataarry[i]['day_id']) == 5) {
                           var dayname = "Fri";
                       }
                       if ((dataarry[i]['day_id']) == 6) {
                           var dayname = "Sat";
                       }
                       
                       var dbtiming = dataarry[i]['timings'];
                       console.log("outside i loop "+JSON.stringify(dbtiming));//dbtiming.length > 0 &&
                       if(dbtiming != null){
                        var timings ='';
                       for (j = 0; j < dbtiming.length; j++) {
                        console.log("in j loop "+JSON.stringify(dataarry[i]['timings'][j]));
                     timings += '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 timing'+i+'">' +
                           '<p class="f-10 mb-2">For</p>' +
                           '<select name="timing[' + i + '][workingdays]" id="all" class="select-drop-down mb-4" data-validation="required">' +

                           '<option value=' + (dataarry[i]['day_id']) + '>' + dayname + '</option>' +

                           '</select>' +
                           '</div>' +
                           '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 timing'+i+'">' +
                           '<p class="f-10 mb-2">Form</p>' +
                           '<select id="fromtime" name="timing[' + i + '][start]" class="select-drop-down mb-4" data-validation="required">' +
                           '<option value=' + (dataarry[i]['timings'][j]['start']) + '>' + (dataarry[i]['timings'][j]['start']) + ' </option>' +
                           '</select>' +
                           '</div>' +
                           '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 timing'+i+'">' +
                           '<p class="f-10 mb-2">To</p>' +
                           '<select id="totime" name="timing[' + i + '][end]" class="select-drop-down mb-4" data-validation="required">' +
                           '<option value=' + (dataarry[i]['timings'][j]['end']) + '>' + (dataarry[i]['timings'][j]['end']) + ' </option>' +
                           '</select>' +
                           '</div>' +
                           '<div id="'+i+'" class="col-12 col-sm-12 col-md-3 timing'+i+'">' +
                           '<p class="f-10 mb-2">Remove</p>' +
                           '<a id="timing'+i+'" onclick="deleterow(this.id, '+dataarry[i]['day_id']+');">' +
                           '<img class="mx-3" src="./images/delete.svg" alt="" >' +
                           '</a>' +
                           '</div>';                           
                       }
                       $('#addtimg').append(timings);
                       $('#weakdays').show();
                       $('#chk-role' + dataarry[i]['day_id']).addClass('option-selected');
                    $('#chkbtnoffer' + dataarry[i]['day_id']).prop('checked', true);
                    $('.working-hoursselection' + dataarry[i]['day_id']).show();
                    selecteddays.push(dataarry[i]['day_id']);
                    $("#dynamicaddonincr").val(i + 1); 
                       }
                   }

//                    return false;
         
                      
                  
               }
               }
            },error: function(res) {

            }
        });

    }

    function deleterow(val, param) {
        console.log(val);
        $("."+val).remove();

        $('#chk-role'+param).removeClass('option-selected');
        $('#chkbtnoffer'+param).prop('checked', false);
        $('.working-hoursselection'+param).hide();
    }

    function update_status(id,status)
    {

        swal({
                title: "Are you sure?",
                text: "You want to change shop available status!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#dd6b55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    var send = {"id":id,status:status, _token: '{{csrf_token()}}'};
                   // send=$(this).serialize()+" & "+$.param(send);
                    console.log(send);
                    //return false;
                    $.ajax({
                        cache:false,
                        type: "POST",
                        url: "{{url('shopAvailableStatusChange')}}",
                        data:send,
                        success: function (res) {
                            if(res.success){
                                swal({title: "success", text: "Shop Available Status updated"});
                                location.reload();
                            }else{
                                swal({title: "failed", text: res.message});
                            }
                        },error: function(res) {
                            return res;
                        }
                    });
                    //    swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                } else {
                    swal("Cancelled", "Failed", "error");
                }
            });


    }
</script>


<script>
    function addalldaystiming() {
        
        $('#addtimg').html('');
        var i=0;
            $('#addtimg').append('<div id="' + i + '" class="col-12 col-sm-12 col-md-3 schalldays">' +
                '<p class="f-10 mb-2">For</p>' +
                '<select name="timing[' + i + '][workingdays]" id="all" class="select-drop-down mb-4" data-validation="required">' +

                '<option value=' + i + '>All</option>' +

                '</select>' +
                '</div>' +
                '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 schalldays">' +
                '<p class="f-10 mb-2">Form</p>' +
                '<select id="fromtime" name="timing[' + i + '][start]" class="select-drop-down mb-4" data-validation="required">' +
                '<option value="07:00">7 AM</option>' +
                '<option value="08:00">8 AM</option>' +
                '<option value="09:00">9 AM</option>' +
                '<option value="10:00">10 AM</option>' +
                '<option value="11:00">11 AM</option>' +
                '</select>' +
                '</div>' +
                '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 schalldays">' +
                '<p class="f-10 mb-2">To</p>' +
                '<select id="totime" name="timing[' + i + '][end]" class="select-drop-down mb-4" data-validation="required">' +
                '<option value="19:00">7 PM</option>' +
                '<option value="20:00">8 PM</option>' +
                '<option value="21:00">9 PM</option>' +
                '<option value="22:00">10 PM</option>' +
                '<option value="23:00">11 PM</option>' +
                '</select>' +
                '</div>' +
                '<div id="' + i + '" class="col-12 col-sm-12 col-md-3 schalldays">' +
                '<p class="f-10 mb-2">Remove</p>' +
                '<a id="timing' + i + '" onclick="deleterow(this.id, '+i+');">' +
                '<img class="mx-3" src="./images/delete.svg" alt="" >' +
                '</a>' +
                '</div>');

        $('#chk-roleAll').addClass('option-selected');
        $('#chkbtnofferAll').prop('checked', true);
        $('.working-hoursselectionAll').show();
        for (i = 0; i < 7; i++) {
            $('#chk-role' + i).removeClass('option-selected');
            $('#chkbtnoffer' + i).prop('checked', false);
            $('.working-hoursselection' + i).hide();
        }
        return false;
    }


 





    function addweekrows(param){
 
     var formselectdata ="";
     if(param ==0){
        var day = "Sun";
     }
     if(param ==1){
        var day = "Mon";
     }
     if(param ==2){
        var day = "Tue";
     }
     if(param ==3){
        var day = "Wed";
     }
     if(param ==4){
        var day = "Thu";
     }
     if(param ==5){
        var day = "Fri";
     }
     if(param ==6){
        var day = "Sat";
     }

      formselectdata +=  '<div id="'+param+'" class="col-12 col-sm-12 col-md-3 timing'+param+'" style="padding:5px;">' +
         '<p class="f-10 mb-2">For</p>' +
         '<select name="timing['+param+'][workingdays]" id="all" class="select-drop-down mb-4" data-validation="required" required>' +         
         '<option value='+param+'>'+day+'</option>' +
         '</select>' +
         '</div>' +
         '<div id="working'+param+'" class="col-12 col-sm-12 col-md-3 timing'+param+'" style="padding:5px;">' +
         '<p class="f-10 mb-2">Form</p>' +
         '<select id="fromtime" name="timing['+param+'][start]" class="select-drop-down mb-4" data-validation="required" required>' ;
         <?php for($i = 0; $i < 24; $i++): ?>
             formselectdata += ' <option value="<?= $i; ?>:00"><?= $i % 12 ? $i % 12 : 12 ?>:00 <?= $i >= 12 ? 'pm' : 'am' ?></option>';
         <?php endfor ?>
formselectdata += '</select>' +
         '</div>' +
         '<div id="'+param+'" class="col-12 col-sm-12 col-md-3 timing'+param+'" style="padding:5px;">' +
         '<p class="f-10 mb-2">To</p>' +
         '<select id="totime" name="timing['+param+'][end]" class="select-drop-down mb-4" data-validation="required">' ;
         <?php for($i = 0; $i < 24; $i++): ?>
             formselectdata += ' <option value="<?= $i; ?>:00"><?= $i % 12 ? $i % 12 : 12 ?>:00 <?= $i >= 12 ? 'pm' : 'am' ?></option>';
         <?php endfor ?>
formselectdata += '</select>' +
         '</div>' +
         '<div id="'+param+'" class="col-12 col-sm-12 col-md-3 timing'+param+'" style="padding:5px;">' +
         '<p class="f-10 mb-2">Remove</p>' +
         '<a id="timing'+param+'" onclick="deleterow(this.id, '+param+');">' +
         '<img class="mx-3" src="./images/delete.svg" alt="" >' +
         '</a>' +
         '</div>' ;
         $('#addtimg').append(formselectdata);

}




</script>       


