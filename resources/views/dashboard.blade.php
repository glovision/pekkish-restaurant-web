@extends('layouts.app')
@section('content')
<style>
    #donut {
        max-width: 650px;
        /* margin: 35px auto; */
    }

    .box__card {
        box-shadow: 1px 1px 8px #00000029;
        border-radius: 8px;
    }
    
.custom__tbl__for__graph {
  /* min-width: 599px;
  overflow: auto; */
  position: relative;
}

.custom__tbl__for__graph .custom__tbl__data__card {
  max-height: 200px;
  overflow: auto;
}
.custom__tbl__for__graph .custom__tbl__headings {
  /* position: absolute; */
  top: 0px;
  height: 70px;
}

.custom__tbl__for__graph .custom__tbl__data {
  /* max-height: 200px;
  overflow: scroll; */
}
</style>

<div class="orders-notifications">
    @if(session()->get('user_details'))
    <input type="hidden" id="restname" value="{{session()->get('user_details')->first_name}}">
    @endif

    <!-- <div class="bg-black">
          <div class="searching-for-orders max-content-width">
            Searching for new orders
          </div>
        </div> -->


    <!-- <div class="bg-green">
          <div class="received-orders max-content-width">
            <div class="received-orders-content">
              <img src="./images/usericon.svg" alt="">
              <div class="d-flex flex-wrap justify-content-center align-items-center">
                <div class="mx-3 new-order-text">You have new order!</div>

                <div class="mx-3 d-flex"><span>Total 4 Items </span> <span class="mx-1">|</span><span>$</span>
                  <span>2500.25</span>
                </div>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-center">
              <img class="mr-2" src="./images/alret.svg" alt="">
              <button class="see-details-btn">SEE THE DETAILS</button>
            </div>
          </div>
        </div> -->
</div>
@if(session()->has('message'))
<div class="alert alert-danger alert-dismissable">{{ session()->get('message') }}</div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
@endif
@if(session()->has('failed'))
<div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
@endif
<div class="main-section-padding">
    <div class="d-flex align-items-center justify-content-between pb-2 ">
        <h4 class="heading">SALES INSIGHTS</h4>
        <form class="d-flex align-items-center justify-content-between w-100 flex-wrap">
            @if(session('user_details')->type == 'seller')
            <select name="restaurant" id="restaurant" class="select-drop-down" >
                <option value="">Select Restaurant</option>
                <option value="All" selected>All</option>
               @foreach($shops as $shop)
                <option value="{{$shop['id']}}">{{$shop['name']}}</option>
                @endforeach
                @endif
                {{--<option value="30">last 30 days</option>--}}
            </select>
                @if(session('user_details')->type == 'shop')
                <input type="text" name="restaurant" id="restaurant" value="{{session()->get('user_details')->shop_data->id}}" class="select-drop-down" style="visibility: hidden;">
                @endif

            <div class="d-flex align-items-center flex-wrap custom__radio_filter">
                {{--<input type="radio" class="filter__btns filter__btns__active">compared to last 7 days</input>--}}
                @foreach ($days as $key=>$value)
                <label class="filter__btns" id="day{{$key}}" onchange="load_dashboard_counts({{$key}})">
                <input type="radio" name="days" id="days{{$key}}"   value="{{$key}}" >{{$value}}</label>
                @endforeach
                    {{--<label class="filter__btns">--}}
                {{--<input type="radio" name="days" id="days"   value="5" onclick="load_dashboard_counts(this.value)">5d</label>--}}
                {{--<label class="filter__btns">--}}
                    {{--<input type="radio" name="days" id="days"   value="30" onclick="load_dashboard_counts(this.value)">1M</label>--}}
                {{--<label class="filter__btns">--}}
                    {{--<input type="radio" name="days" id="days"   value="60" onclick="load_dashboard_counts(this.value)">2M</label>--}}
                {{--<input type="radio" name="days" id="days"  class="filter__btns" value="7">7d--}}
                {{--<input type="radio" name="days" class="filter__btns" value="5d">5d--}}
                {{--<button class="filter__btns">7d</button>--}}
                {{--<button class="filter__btns">1m</button>--}}
                {{--<button class="filter__btns">3m</button>--}}
                {{--<button class="filter__btns">5m</button>--}}
                {{--<button class="filter__btns">ytd</button>--}}

            </div>


    </form>
    </div>
    <div class="container-fluid p-0 sales">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-3">
                <div class="sales-card">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="sale-type">Total Food Sales</div>
                            <img class="mt-2" src="./images/netsales.svg" alt="">
                        </div>

                        <div class="text-to-end">
                            <div class="sale-percent-up">
                                {{--<span id="salespercent"></span>--}}
                                {{--<span id="salepercent">%</span>--}}
                                {{--<img src="./images/uparrow-green.svg" alt="" id="salesimg">--}}
                            </div>
                            <div class="sale-value"><span>$</span><span id="salesval"></span></div>
                            {{--<div class="sale-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                        </div>
                    </div>
                </div>
                <div class="sales-card">

                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="sale-type">Net Income</div>
                            <img class="mt-2" src="./images/netincome.svg" alt="">
                        </div>

                        <div class="text-to-end">
                            <div class="sale-percent-down">
                                {{--<span id="orderspercent">100</span>--}}
                                {{--<span id="orderpercent">%</span>--}}
                                {{--<img src="./images/down-arrow-red.svg" alt="" id="orderimg">--}}
                            </div>
                            <div class="sale-value"><span>$</span><span id="netincome"></span></div>
                            {{--<div class="sale-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                        </div>
                    </div>
                </div>

                {{--<a href="{{url('/order_history')}}" style="text-decoration: none;color:black;">--}}
                    <div class="sales-card">

                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="sale-type">Total Orders</div>
                                <img class="mt-2" src="./images/totalorders.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="sale-percent-down">
                                    {{--<span id="orderspercent"></span>--}}
                                    {{--<span id="orderpercent">%</span>--}}
                                    {{--<img src="./images/down-arrow-red.svg" alt="" id="orderimg">--}}
                                </div>
                                <div class="sale-value"><span id="ordersval"></span></div>
                                {{--<div class="sale-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                            </div>
                        </div>
                    </div>
                {{--</a>--}}

                {{--<div class="sales-card">--}}
                {{--<div class="d-flex align-items-center justify-content-between">--}}
                {{--<div class="d-grid">--}}
                {{--<div class="sale-type">New Income</div>--}}
                {{--<img class="mt-2" src="./images/netincome.svg" alt="">--}}
                {{--</div>--}}

                {{--<div class="text-to-end">--}}
                {{--<div class="sale-percent-up">--}}
                {{--<span id="incnetpercent"></span>--}}
                {{--<span id="netpercent">%</span>--}}
                {{--<img src="./images/uparrow-green.svg" alt="" id="incnetimg"></div>--}}
                {{--<div class="sale-value"><span>$</span><span id="incnetval"></span></div>--}}
                {{--<div class="sale-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-9">

                <div style="box-shadow: rgb(235 235 235) 0px 0px 10px; border-radius: 8px;">
                    <div class="d-flex align-items-center justify-content-between pl-2 pr-3 pt-3 pb-4">
                        <form>
                            {{--<select name="days" id="days" class="select-drop-down">
                                <option value="7">last 7 days</option>

                            </select>--}}
                        </form>
                        <div class="d-flex align-items-center justify-content-between">
                            <span class="d-flex align-items-center">
                                <span class="present-7-days" style="background-color:#ff8000;"></span>
                                <span class="f-10 text-color-grey">Total Food Sales 
                            </span>

                            <span class="d-flex align-items-center">
                                <span class="previous-7-days" style="background-color:#00b336;"></span>
                                <span class="f-10 text-color-grey">Total Net Income 
                            </span>

                            <span class="d-flex align-items-center">
                                <span class="previous-7-days" style="background-color:#10acf6;"></span>
                                <span class="f-10 text-color-grey">Total Orders 
                            </span>

                        </div>
                    </div>
                    <canvas id="line-chart" style="height:319px"></canvas>
                </div>
            </div>

            <div class="col-12">
                <div class="d-flex justify-content-end download mt-0">
                    <div onclick="exportToLineGraph();exportDashboardcnts();" style="cursor:pointer"> <span class="text-color-green mx-2 f-14 f-medium " id="download">Download Report</span> <img src="./images/download.svg"></div>
                </div>
            </div>
        </div>

        <div class="row avg-time">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="avg-time-card ">
                    <div class="avg-title">Avg. Preparation  Time</div>
                    <div>
                        <div class="text-to-end">
                            <div class="avg-time-percent-up">
                                {{--<span id="avgpreppercent"></span>--}}
                                {{--<span id="avpreppercent">%</span>--}}
                                {{--<img src="./images/uparrow-green.svg" alt="" id="avgprepimg">--}}
                            </div>
                            <div class="avg-time-value" id="avgprepval"></div>
                            {{--<div class="avg-time-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="avg-time-card">
                    <div class="avg-title">Avg. Waiting &nbsp;&nbsp;&nbsp;&nbsp; Time</div>
                    <div>
                        <div class="text-to-end">
                            <div class="avg-time-percent-down">
                                {{--<span id="waitpreppercent"></span>--}}
                                {{--<span id="waitprepercent">%</span>--}}
                                {{--<img src="./images/uparrow-green.svg" alt="" id="waitprepimg">--}}
                            </div>
                            <div class="avg-time-value" id="waitprepval"></div>
                            {{--<div class="avg-time-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <div class="avg-time-card">
                    <div class="avg-title">Avg. Delivery &nbsp;&nbsp;&nbsp;&nbsp; Time</div>
                    <div>
                        <div class="text-to-end">
                            <div class="avg-time-percent-up">
                                {{--<span id="deliverpreppercent"></span>--}}
                                {{--<span id="deliverprepercent">%</span>--}}
                                {{--<img src="./images/uparrow-green.svg" alt="" id="deliverprepimg">--}}
                            </div>
                            <div class="avg-time-value" id="deliverprepval"></div>
                            {{--<div class="avg-time-compare">Compared to previous <span class="saledays"></span> days</div>--}}
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <div class="row banner-section">

            <div class="col-12">
                <div class="position-relative ">
                    <img class="banner" src="./images/banner.png" alt="">
                    <div class="cta">
                        <button class="learn-more">learn more</button>
                        <button class="start-now">start now</button>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row mt-4">--}}
            {{--<div class="col-12 col-sm-12 col-md-5 ">--}}
                {{--<div class=" box__card">--}}
                {{--<div class="p-3">--}}
                {{--<canvas id="donut"></canvas></div>--}}
                   {{----}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-12 col-sm-12 col-md-7 ">--}}
            {{--<div class="box__card ">--}}
            {{--<div class="row">--}}
                    {{--<div class="col-3 canvas__title align-items-start text-left" >--}}
                        {{--Branch--}}
                    {{--</div>--}}
                    {{--<div class="col-3 canvas__title ">--}}
                        {{--No. of Items--}}
                    {{--</div>--}}
                    {{--<div class="col-3 canvas__title">--}}
                        {{--Orders--}}
                    {{--</div>--}}
                    {{--<div class="col-3 canvas__title">--}}
                        {{--Sales Volume--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row m-0">--}}
                    {{--<div class="col-12 canvas__options">--}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                        {{--<div class="row mb-3">--}}
                            {{--<div class="col-3 f-14 text-color-grey location">Branch 1 - Location 1</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">22</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">288</div>--}}
                            {{--<div class="col-3 text-right f-18 f-bold">$99,283.22</div>--}}
                        {{--</div>                     --}}
                      {{----}}
                    {{--</div>--}}
                   {{----}}
                {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-12 mt-5 mb-2">
                <div class="d-flex align-items-center justify-content-between pb-2">
                    <h4 class="heading">ITEMIZED INSIGHT REPORT</h4>
                    {{--<form>--}}
                    {{--<select name="days" id="days" class="select-drop-down">--}}
                    {{--<option value="7">last 7 days</option>--}}

                    {{--</select>--}}
                    {{--</form>--}}
                </div>
            </div>

        </div>
        <div class="row itemized-report">
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Popular Item (Most Selling)</div>
                    <div class="itemized-report-percent-up">
                        {{--<span><img src="./images/uparrow-green.svg" alt="" id="topsellingimg"></span>--}}
                        {{--<span id="topsellingitempercent"></span>--}}
                        {{--<span id="topsellingpercent">%</span>--}}
                    </div>
                    <div class="itemized-report-value" id="topsellingitemthisweek"></div>
                    <div class="itemized-report-compare">
                        {{--<span>Compared to previous 7 days</span> ---}}
                        <span class="font-weight-bold text-color-black" id="topsellingitemlastweek"></span>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Popular Category</div>
                    <div class="itemized-report-percent-up">
                        {{--<span><img src="./images/uparrow-green.svg" alt="" id="topsectionimg"></span>--}}
                        {{--<span id="topsectionpercent"></span>--}}
                        {{--<span id="topsectionpercentimg">%</span>--}}
                    </div>
                    <div class="itemized-report-value" id="thisweektopsection"></div>
                    <div class="itemized-report-compare">
                        {{--<span>Compared to previous 7 days</span> ---}}
                        {{--<span class="font-weight-bold text-color-black text-uppercase" id="lastweektopsection"> </span>--}}
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Popular Offer</div>
                    <div class="itemized-report-percent-up">
                        {{--<span><img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">23.29%</span>--}}
                    </div>
                    <div class="itemized-report-value text-uppercase">-</div>
                    <div class="itemized-report-compare">
                        {{--<span>Compared to previous 7 days</span> ---}}
                        {{--<span class="font-weight-bold text-color-black text-uppercase"> SALDAY</span>--}}
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3 py-3">
                <div>
                    <div class="itemized-report-title">Sales Through Offers</div>
                    <div class="itemized-report-percent-up">
                        {{--<span><img src="./images/uparrow-green.svg" alt=""></span><span class="mx-1">2.25%</span>--}}
                    </div>
                    <div class="itemized-report-value d-flex"><span></span><span>-</span></div>
                    <div class="itemized-report-compare">
                        {{--<span>Compared to previous 7 days</span> ---}}
                        {{--<span class="font-weight-bold text-color-black d-flex"> <span>$</span><span>3900</span></span>--}}
                    </div>
                </div>
            </div>

        </div>
        <div class="row">

            {{--<div class="col-12">--}}
            {{--<div class="d-flex justify-content-end download">--}}
            {{--<div onclick="exportToOrderCount()" style="cursor:pointer"><span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg" alt="">--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}

        </div>


        <div class="line"></div>

        <div class="d-flex align-items-center justify-content-between pb-2">
            <h4 class="heading">POPULAR TIMINGS FOR BUSINESS</h4>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4">
                <div class="timing-card">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="timing-type">Breakfast Time</div>
                            <img class="mt-2" src="./images/breakfast.png" alt="">
                        </div>

                        <div class="d-flex align-items-center" style="margin-top: -15px;">
                            <div class="from-time">
                                <div class="f-20">6</div>
                                <div class="am-pm">am</div>
                            </div>
                            <div class="mx-2">to</div>
                            <div class="to-time">
                                <div class="f-20">11</div>
                                <div class="am-pm">am</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="timing-card">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="timing-type">Lunch Time</div>
                            <img class="mt-2" src="./images/lunch.png" alt="">
                        </div>

                        <div class="d-flex align-items-center" style="margin-top: -15px;">
                            <div class="from-time">
                                <div class="f-20">11</div>
                                <div class="am-pm">am</div>
                            </div>
                            <div class="mx-2">to</div>
                            <div class="to-time">
                                <div class="f-20">3</div>
                                <div class="am-pm">pm</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="timing-card">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="timing-type">Dinner Time</div>
                            <img class="mt-2" src="./images/dinner.png" alt="">
                        </div>

                        <div class="d-flex align-items-center" style="margin-top: -15px;">
                            <div class="from-time">
                                <div class="f-20">6</div>
                                <div class="am-pm">pm</div>
                            </div>
                            <div class="mx-2">to</div>
                            <div class="to-time">
                                <div class="f-20">11</div>
                                <div class="am-pm">pm</div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8">

                <div style="background: black; border-radius: 8px;">
                    <div class="d-flex align-items-center justify-content-between pl-2 pr-3 pt-3 pb-4">
                        <div class="text-color-white f-12 f-medium pl-1"> Day Wise Sales Details</div>
                        <div class="d-flex align-items-center justify-content-between">
                            <span class="d-flex align-items-center">
                                <span class="Breakfast"></span>
                                <span class="f-10 text-color-white">Breakfast</span>
                            </span>

                            <span class="d-flex align-items-center">
                                <span class="Lunch"></span>
                                <span class="f-10 text-color-white">Lunch</span>
                            </span>

                            <span class="d-flex align-items-center">
                                <span class="Dinner"></span>
                                <span class="f-10 text-color-white">Dinner</span>
                            </span>

                        </div>
                    </div>
                    <canvas id="canvas" style="height:319px"></canvas>
                </div>
            </div>
            <div class="col-12">
            <div class="d-flex justify-content-end download">
            <div onclick="exportToCsv()" style="cursor:pointer"><span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg" alt=""></div>
            </div>
            </div>
        </div>
        <div class="line"></div>

        <div class="d-flex align-items-center justify-content-between pb-2">
            <h4 class="heading d-flex">CUSTOMERS & RATINGS</h4>
        </div>
        <div class="row customer-rating">
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                <div class="customer-card">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-grid">
                            <div class="customer-type">New Customers</div>
                            <img class="mt-2" src="./images/newcustomer.svg" alt="">
                        </div>

                        <div class="text-to-end">
                            <div class="customer-percent-up"><span class="mx-2">23.29%</span> <img src="./images/uparrow-green.svg" alt=""></div>
                            <div class="customer-value"><span>

                                    {{$ratings->users->total ?? ''}}

                                </span></div>
                            <div class="customer-compare">Compared to previous 7 days</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                <div class="overall-rating">
                    <div class="row">
                        <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                            <div class="overall-rating-star f-14">
                                <p class="f-medium text-color-grey d-flex">Overall Rating</p>
                                <div class="d-flex align-items-baseline">
                                    <span class="font-weight-bold f-24"></span>
                                    <span class="mx-2">
                                        <div>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="comment-margin col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="f-10 text-color-grey rating-word">Excellent - <span>20</span></div>
                                <div class="rating-percentage-bg">
                                    <div class="actual-rating-value excellent"></div>
                                </div>
                            </div>

                            <div class="d-flex align-items-center justify-content-between">
                                <div class="f-10 text-color-grey rating-word">Good - <span>10</span></div>
                                <div class="rating-percentage-bg">
                                    <div class="actual-rating-value good"></div>
                                </div>
                            </div>

                            <div class="d-flex align-items-center justify-content-between">
                                <div class="f-10 text-color-grey rating-word">Average - <span>5</span></div>
                                <div class="rating-percentage-bg">
                                    <div class="actual-rating-value average"></div>
                                </div>
                            </div>

                            <div class="d-flex align-items-center justify-content-between">
                                <div class="f-10 text-color-grey rating-word">Below Avg - <span>2</span></div>
                                <div class="rating-percentage-bg">
                                    <div class="actual-rating-value below-avg"></div>
                                </div>
                            </div>

                            <div class="d-flex align-items-center justify-content-between">
                                <div class="f-10 text-color-grey rating-word">Poor - <span>1</span></div>
                                <div class="rating-percentage-bg">
                                    <div class="actual-rating-value poor"></div>
                                </div>
                            </div>

                        </div>
                        <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-4">
                            <div class="overall-rating-comment">
                                <div class="f-12 d-flex">Its really good that you have used proper care while delivering my order
                                </div>
                                <div class="comment-gap d-flex align-items-center justify-content-between">
                                    <div>
                                        <span class="fa fa-star star-checked"></span>
                                        <span class="fa fa-star star-checked"></span>
                                        <span class="fa fa-star star-checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                    <span class="f-10 text-color-grey">21 JAN 21</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            {{--<div class="col-12">--}}
            {{--<div class="d-flex justify-content-end download mt-0">--}}
            {{--<span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg" alt="">--}}
            {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>
</div>
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
    <script>
    $(document).ready(function() {
        load_dashboard_counts(30);
    });

    function dashboardCounts(days) {
        var url = "{{url('dashboardCountsDetails')}}/" + days;
        //console.log("URL "+url);
        $.ajax({
            cache: false,
            type: "GET",
            url: url,
            success: function(res) {
                console.log("Counts " + JSON.stringify(res));
                if (res.success) {
                    console.log(res.data);
                    $('.saledays').html(days);
                    $('#salespercent').html(res.data.PERCENT_SALES);
                    $('#salespercent').css("color", res.data.SALEINCRDECR);
                    $('#salepercent').css("color", res.data.SALEINCRDECR);
                    $('#salesval').html((res.data.THIS_WEEK_SALES).toFixed(2));
                    if (res.data.SALEINCRDECR == 'red') {
                        $('#salesimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#salesimg').attr('src', './images/uparrow-green.svg');
                    }

                    $('#orderspercent').html(res.data.PERCENT_ORDER);
                    $('#orderspercent').css("color", res.data.ORDERINCRDECR);
                    $('#orderpercent').css("color", res.data.ORDERINCRDECR);
                    $('#ordersval').html((res.data.THIS_WEEK));
                    if (res.data.ORDERINCRDECR == 'red') {
                        $('#orderimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#orderimg').attr('src', './images/uparrow-green.svg');
                    }


                    $('#incnetpercent').html(res.data.PERCENT_NET);
                    $('#incnetpercent').css("color", res.data.NETINCRDECR);
                    $('#netpercent').css("color", res.data.NETINCRDECR);
                    $('#incnetval').html((res.data.THIS_WEEK_NET).toFixed(2));
                    if (res.data.NETINCRDECR == 'red') {
                        $('#incnetimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#incnetimg').attr('src', './images/uparrow-green.svg');
                    }


                    $('#avgpreppercent').html(res.data.AVGPREPTIMEPERCENT);
                    $('#avgpreppercent').css("color", res.data.AVGPREPINCR);
                    $('#avpreppercent').css("color", res.data.AVGPREPINCR);
                    $('#avgprepval').html(res.data.AVGPREPTIME);
                    if (res.data.AVGPREPINCR == 'red') {
                        $('#avgprepimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#avgprepimg').attr('src', './images/uparrow-green.svg');
                    }


                    $('#waitpreppercent').html(res.data.AVGWAITTIMEPERCENT);
                    $('#waitpreppercent').css("color", res.data.AVGWAITINCR);
                    $('#waitpreppercent').css("color", res.data.AVGWAITINCR);
                    $('#waitprepercent').css("color", res.data.AVGWAITINCR);
                    $('#waitprepval').html(res.data.AVGWAITTIME);
                    if (res.data.AVGWAITINCR == 'red') {
                        $('#waitprepimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#waitprepimg').attr('src', './images/uparrow-green.svg');
                    }


                    $('#deliverpreppercent').html(res.data.AVGDELIVERPERCENT);
                    $('#deliverpreppercent').css("color", res.data.AVGDELIVERINCR);
                    $('#deliverpreppercent').css("color", res.data.AVGDELIVERINCR);
                    $('#deliverprepercent').css("color", res.data.AVGDELIVERINCR);
                    $('#deliverprepval').html(res.data.AVGDELIVERTIME);
                    if (res.data.AVGDELIVERINCR == 'red') {
                        $('#deliverprepimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#deliverprepimg').attr('src', './images/uparrow-green.svg');
                    }


                }
            },
            error: function(res) {

            }
        });

    }



    {{--function dashboardData(days) {--}}
        {{--var url = "{{url('dayWiseSalesDetails')}}/" + days;--}}
        {{--//console.log("URL "+url);--}}
        {{--$.ajax({--}}
            {{--cache: false,--}}
            {{--type: "GET",--}}
            {{--url: url,--}}
            {{--success: function(res) {--}}
                {{--console.log("Sale_details " + JSON.stringify(res));--}}
                {{--if (res.success) {--}}
                    {{--dayWise_SalesDetails(res.data);--}}
                    {{--downloadbargraph(res.data);--}}
                {{--}--}}
            {{--},--}}
            {{--error: function(res) {--}}

            {{--}--}}
        {{--});--}}

    {{--}--}}

    function dayWise_SalesDetails(data) {
        console.log(1);
//        if (window.barChartData != undefined)
//            window.barChartData.destroy();
        console.log(JSON.stringify(data));
        window.barChartData = {

            labels: data.datelabel.reverse(),
            datasets: [{
                    label: "Breakfast",
                    backgroundColor: "#52DCED",
                    //   borderColor: "red",
                    borderWidth: 1,
                    data: data.BREAKFAST.reverse()
                },
                {
                    label: "Lunch",
                    backgroundColor: "#C295FF",
                    //   borderColor: "blue",
                    borderWidth: 1,
                    data: data.LUNCH.reverse()
                },
                {
                    label: "Dinner",
                    backgroundColor: "#504ADB",
                    //   borderColor: "green",
                    borderWidth: 1,
                    data: data.DINNER.reverse()
                }
            ]
        };

        var chartOptions = {
            responsive: true,

            legend: {
                position: "top",
                display: false
            },
            title: {
                display: true,
                text: "Day Wise Sales Details",
                display: false
            },
            scales: {
                xAxes: [{
                    cornerRadius: 20,
                    barPercentage: 0.4,
                    gridLines: {
                        drawBorder: false,
                        scaleLineColor: "rgba(0,0,0,0)",
                        borderDash: [0, 0],
                        color: "rgba(0,0,0,0)"
                    }
                }],
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                        scaleLineColor: "rgba(0,0,0,0)",
                        borderDash: [3, 3],
                        color: "rgba(25,25,25,1)"
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 100
                    }
                }]
            },

        }

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: chartOptions,

        });
        $('#canvas').empty().append('<canvas id="canvas" style="height:319px"></canvas>');
    }

    function downloadbargraph(data, days) {
		var period ="";
		if(days == 1){
			period = "Yesterday orders";
		}
		if(days == 5){
			period = "Last 5 days orders";
		}
		if(days == 7){
			period = "Last week orders";
		}
		if(days == 30){
			period = "Last month orders";
		}
		if(days == 90){
			period = "Last 3 months orders";
		}
		if(days == 180){
			period = "Last 6 months orders";
		}
        var restname = $('#restname').val();
		var currdate = new Date().toISOString().slice(0, 10);
        console.log("export daywise sale"+JSON.stringify(data));
        var Heading = ['', 'BREAKFAST', 'LUNCH', 'DINNER'];
        var results = [
            data.datelabel, data.BREAKFAST, data.LUNCH, data.DINNER
        ];
        exportToCsv = function() {
            var CsvString = "";
            CsvString += "Name of the Report:, Day Wise Order Details";
			CsvString += "\r\n";
            CsvString += "Report Date:, "+currdate;
			CsvString += "\r\n";
            CsvString += "Report Period:, "+period;
            CsvString += "\r\n";
            results.forEach(function(RowItem, RowIndex) {
                CsvString += JSON.stringify(Heading[RowIndex]) + ',';
                RowItem.forEach(function(ColItem, ColIndex) {
                    CsvString += ColItem + ',';
                });
                CsvString += "\r\n";
            });
            CsvString = "data:application/csv," + encodeURIComponent(CsvString);
            var x = document.createElement("A");
            x.setAttribute("href", CsvString);            
            x.setAttribute("download", "Restaurant " + restname + "Day Wise Sales Details " + currdate + ".csv");
            document.body.appendChild(x);
            x.click();
        }

    }

    function downloadlinegraph(data, days) {
        console.log("linechart" + JSON.stringify(data));
		var period = "";
		if(days == 1){
			period = "Yesterday orders";
		}
		if(days == 5){
			period = "Last 5 days orders";
		}
		if(days == 7){
			period = "Last week orders";
		}
		if(days == 30){
			period = "Last month orders";
		}
		if(days == 90){
			period = "Last 3 months orders";
		}		
		if(days == 180){
			period = "Last 6 months orders";
		}else{
            period = "Year to date orders";
        }
        var restname = $('#restname').val();
        var currdate = new Date();
        var datevalue = currdate.toISOString().slice(0, 10);
		var Heading = ['', 'Total Sales', 'Net Income', 'Total Orders'];
        var results = [
            data.pradatelabel, data.PRESENT, data.NETINCOME, data.PREVIOUS
        ];
			exportToLineGraph = function() {
				var CsvString = "";
				CsvString += "Name of the Report:, Order Details";
				CsvString += "\r\n";
				CsvString += "Report Date:, "+datevalue;
				CsvString += "\r\n";
				CsvString += "Report Period:, "+period;
				CsvString += "\r\n";
				CsvString += "Total Orders and Total Sales";
				CsvString += "\r\n";				
				results.forEach(function(RowItem, RowIndex) {
					CsvString += JSON.stringify(Heading[RowIndex]) + ',';
					RowItem.forEach(function(ColItem, ColIndex) {
						CsvString += ColItem + ',';
					});
					CsvString += "\r\n";
				});
				CsvString = "data:application/csv," + encodeURIComponent(CsvString);
				var x = document.createElement("A");
				x.setAttribute("href", CsvString);
				x.setAttribute("download", "Day Wise Sales Details " + datevalue + ".csv");
				document.body.appendChild(x);
				x.click();
			}
		//}
    }

	function dashboardcountExport(data, days) {		
        //var currdate = new Date();
		var period ="";
		if(days == 1){
			period = "Yesterday Business";
		}
		if(days == 5){
			period = "Last 5 days Business";
		}
		if(days == 7){
			period = "Last week Business";
		}
		if(days == 30){
			period = "Last month Business";
		}
		if(days == 90){
			period = "Last 3 months Business";
		}		
		if(days == 180){
			period = "Last 6 months Business";
		}else{
            period = "Year to date orders";
        }
		
		var currdate = new Date().toISOString().slice(0, 10);
        console.log("export Counts Sales Stores  "+JSON.stringify(data));
		
        var Heading = ['Sales', 'Net Income', 'No.of Orders'];
        var results = [
            data.Net_Sales, data.Net_Income, data.Total_Orders
        ];
        exportDashboardcnts = function() {			
            var CsvString = "";
            CsvString += "Name of the Report:, Restaurant Business Insights";
			CsvString += "\r\n";
            CsvString += "Report Date:, "+currdate;
			CsvString += "\r\n";
            CsvString += "Report Period:, "+period;
            CsvString += "\r\n";
				
				CsvString += JSON.stringify(Heading[0]) + ',' + JSON.stringify(Heading[1]) + ',' + JSON.stringify(Heading[2]) + ',';
				CsvString += "\r\n";
                CsvString += JSON.stringify(results[0]) + ',' + JSON.stringify(results[1]) + ',' + JSON.stringify(results[2]) + ',';
	   
            CsvString = "data:application/csv," + encodeURIComponent(CsvString);
            var x = document.createElement("A");
            x.setAttribute("href", CsvString);
            x.setAttribute("download", "Restaurant Business Insights " + currdate + ".csv");
            document.body.appendChild(x);
            x.click();
        }

    }

</script>
<script>
    Chart.pluginService.register({
        beforeDraw: function(chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    });

    Chart.defaults.global.legend.labels.usePointStyle = true;


    function linechart_dataDetails(data, days) {
        console.log("weew" + JSON.stringify(data));
        console.log("werw" + days);
        console.log("werw" + data.PREVIOUS);
        //if (window.linechart != undefined)
         //   window.linechart.destroy();
        window.linechart = new Chart(document.getElementById("line-chart"), {
            type: 'line',

            data: {
                labels: data.pradatelabel,

                datasets: [{
                    data: data.PRESENT,
                    yAxesGroup: 'A',
                    //yAxisID:'A',
                    pointBackgroundColor: "#ff8000",//"#52DCED",
                    pointBorderColor: "#ff8000",//"#52DCED",
                    label: "Sales",
                    backgroundColor: "rgb(82 220 237/24%)",
                    borderColor: "#ff8000",//"#52DCED",
                    fill: false
                },{
                    data: data.NETINCOME,
                    yAxesGroup: 'A',
                    //yAxisID:'A',
                    pointBackgroundColor: "#00b336",//"#52DCED",
                    pointBorderColor: "#00b336",//"#52DCED",
                    label: "Net Income",
                    backgroundColor: "rgb(82 220 237/24%)",
                    borderColor: "#00b336",//"#52DCED",
                    fill: false
                }, {

                    yAxesGroup: 'B',
                    //yAxisID:'B',
                    pointBackgroundColor: "#10acf6",//"#5E84EA",
                    pointBorderColor: "#10acf6",//"#5E84EA",
                    label: "Orders",
                    backgroundColor: "rgb(94 132 234/24%)",
                    borderColor: "#10acf6",//"#5E84EA",
                    fill: false,
                    data: data.PREVIOUS
                }]
            },
            options: {

                legend: {
                    display: false,
                    position: "top",
                    align: "end",
                    labels: {
                        usePointStyle: true,

                    },

                },
                chartArea: {

                },

                scales: {
                    xAxes: [{
                        ticks: {
                            maxTicksLimit: 3
                        },
                        gridLines: {
                            drawBorder: false,
                            scaleLineColor: "rgba(0,0,0,0)",
                            borderDash: [0, 0],
                            color: "rgba(0,0,0,0)"
                        }
                    }],

                    yAxes: [{


                        gridLines: {
                            drawBorder: false,
                            scaleLineColor: "rgba(0,0,0,0)",
                            borderDash: [3, 3]
                        },

                            id: 'A',
                            type: 'linear',
                            position: 'left',
                            ticks: {
                                beginAtZero: true,
                                userCallback: function(label, index, labels) {
                                    // when the floored value is the same as the value we have a whole number
                                    if (Math.floor(label) === label) {
                                        return label;
                                    }

                                },
                            }
                        }, {
                            id: 'B',
                            type: 'linear',
                            position: 'right',
                            ticks: {
                                beginAtZero: true,
                                userCallback: function(label, index, labels) {
                                    // when the floored value is the same as the value we have a whole number
                                    if (Math.floor(label) === label) {
                                        return label;
                                    }

                                },
                            }


                    }],
                },
                title: {
                    display: true,
                    text: 'Day Wise Sales Details',
                    display: false
                }
            }
        });
        //     $('#line-chart').empty().append('<canvas id="line-chart" style="height:319px"></canvas>');

    }



    function itemizedInsightReport(days) {
        var url = "{{url('AjaxOrdersHistoryCounts')}}/" + days;
        console.log("URL " + url);
        $.ajax({
            cache: false,
            type: "GET",
            url: url,
            success: function(res) {
                console.log("Counts " + JSON.stringify(res));
                if (res.success) {
                    console.log(res.data);
                    downloadOrderHistory(res.data);
                    $('.saledays').html(days);
                    $('#topsellingitempercent').html(res.data.PERCENT_ORDER);
                    $('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                    $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
                    if (res.data.ORDERINCRDECR == 'red') {
                        $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#topsellingimg').attr('src', './images/uparrow-green.svg');
                    }



                    $('#topsectionpercent').html(res.data.PERCENT_CATEGORY);
                    $('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                    $('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                    $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                    $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
                    if (res.data.ORDERINCRDECR == 'red') {
                        $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
                    } else {
                        $('#topsectionimg').attr('src', './images/uparrow-green.svg');
                    }

                }
            },
            error: function(res) {

            }
        });

    }

    function downloadOrderHistory(data) {
        var results = data;

        exportToOrderCount = function() {
            var CsvString = '';
            Object.keys(results).forEach(function(key) {
                CsvString += key + ',';
            });
            CsvString += "\r\n";
            Object.values(results).forEach(function(key) {
                CsvString += key + ',';
            });
            CsvString = "data:application/csv," + encodeURIComponent(CsvString);
            var x = document.createElement("A");
            x.setAttribute("href", CsvString);
            x.setAttribute("download", "OrderHistory.csv");
            document.body.appendChild(x);
            x.click();
        }
    }
</script>


<script>
function itemizedInsightReport(days)
    {
        var url = "{{url('AjaxOrdersHistoryCounts')}}/" + days;
            console.log("URL "+url);
            $.ajax({
                cache:false,
                type: "GET",
                url: url,
                success: function (res) {
                    console.log("Counts "+JSON.stringify(res));
                    if(res.success){
                        console.log(res.data);
                        downloadOrderHistory(res.data);

                        $('.saledays').html(days);
                        $('#topsellingitempercent').html(res.data.PERCENT_ORDER);                        
                        $('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                        $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsellingimg').attr('src', './images/uparrow-green.svg');
                        }

                        

                        $('#topsectionpercent').html(res.data.PERCENT_CATEGORY);                       
                        $('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                        $('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                        $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                        $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
                        if(res.data.ORDERINCRDECR =='red'){
                            $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
                        }else{
                            $('#topsectionimg').attr('src', './images/uparrow-green.svg');
                        }

                    }
                },error: function(res) {

                }
            });

    }

</script>
<script>
    function load_dashboard_data(value){
        var days = value;
        var shop_id = $('#restaurant').val();
        var dashboard_params = {"shop_id":shop_id, "days":days, _token: '{{csrf_token()}}'};
        $.ajax({
            cache:false,
            type: "POST",
            url:  "{{url('linechartDetails')}}",
            data:dashboard_params,
            success: function (res) {
                console.log(res);
                linechart_dataDetails(res.data, days);
                downloadlinegraph(res.data, days); 
            },error: function(res) {

            }
        })

    }
    function load_dashboard_counts(value){
        $('.filter__btns').removeClass('filter__btns__active');
        $('#days'+value).prop('checked', true);
        $('#day'+value).addClass('filter__btns__active');


        var days = value;//$('#days').val();
        var shop_id = $('#restaurant').val();
        var dashboard_params = {"shop_id":shop_id, "days":days, _token: '{{csrf_token()}}'};
        $.ajax({
            cache:false,
            type: "POST",
            url:  "{{url('dashboardCountsDetails')}}",
            data:dashboard_params,
            success: function (res) {
                console.log(res);
                load_dashboard_data(days);
                load_day_wise_sales(days);
                load_itemized_counts(days);
                $('#ordersval').html((res.data.Total_Orders));               
                $('#salesval').html(Number((res.data.Net_Sales).toFixed(2)).toLocaleString());
                $('#netincome').html(Number((res.data.Net_Income).toFixed(2)).toLocaleString());
                $('#avgprepval').html(res.data.AVGPREPTIME);
                $('#waitprepval').html(res.data.AVGWAITTIME);
                $('#deliverprepval').html(res.data.AVGDELIVERTIME);
                dashboardcountExport(res.data, days);
            },error: function(res) {

            }
        });

    }

    function load_day_wise_sales(value) {
        var days = value;//$('#days').val();
        var shop_id = $('#restaurant').val();
        var dashboard_params = {"shop_id":shop_id, "days":days, _token: '{{csrf_token()}}'};
        $.ajax({
            cache:false,
            type: "POST",
            url:  "{{url('dayWiseSalesDetails')}}",
            data:dashboard_params,
            success: function(res) {
                console.log("Sale_details " + JSON.stringify(res));
                if (res.success) {
                    dayWise_SalesDetails(res.data, days);
                    downloadbargraph(res.data, days);
                }
            },
            error: function(res) {

            }
        });
    }
    function load_itemized_counts(value){
        var days = value;//$('#days').val();
        var shop_id = $('#restaurant').val();
        var dashboard_params = {"shop_id":shop_id, "days":days, _token: '{{csrf_token()}}'};
        $.ajax({
            cache:false,
            type: "POST",
            url:  "{{url('AjaxOrdersHistoryCounts1')}}",
            data:dashboard_params,
            success: function (res) {
                console.log(res);
               // $('#topsellingitempercent').html(res.data.PERCENT_ORDER);
                //$('#topsellingitempercent').css("color", res.data.ORDERINCRDECR);
                //$('#topsellingpercent').css("color", res.data.ORDERINCRDECR);
                $('#topsellingitemthisweek').html(res.data.THIS_WEEK_ITEM);
                $('#topsellingitemlastweek').html(res.data.LAST_WEEK_ITEM);
//                if (res.data.ORDERINCRDECR == 'red') {
//                    $('#topsellingimg').attr('src', './images/down-arrow-red.svg');
//                } else {
//                    $('#topsellingimg').attr('src', './images/uparrow-green.svg');
//                }



                //$('#topsectionpercent').html(res.data.PERCENT_CATEGORY);
                //$('#topsectionpercent').css("color", res.data.ORDERINCRDECR);
                //$('#topsectionpercentimg').css("color", res.data.ORDERINCRDECR);
                $('#thisweektopsection').html(res.data.THIS_WEEK_TOP_SEC);
                $('#lastweektopsection').html(res.data.LAST_WEEK_TOP_SEC);
//                if (res.data.ORDERINCRDECR == 'red') {
//                    $('#topsectionimg').attr('src', './images/down-arrow-red.svg');
//                } else {
//                    $('#topsectionimg').attr('src', './images/uparrow-green.svg');
//                }

            },error: function(res) {

            }
        });
    }

    function storechange(id)
    {
         var url = "{{url('storechange')}}/" + id; 
        console.log("URL "+url);
        $.ajax({
            cache:false,
            type: "GET",
            url: url,
            success: function (res) {
                console.log("session  "+res);                
            },error: function(res) {

            }
        });
    }


</script>

@endsection