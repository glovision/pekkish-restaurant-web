@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        <div class="container-fluid p-0 sales">
            <div class="d-flex align-items-center justify-content-between pb-2">
                <h4 class="heading d-flex">FEEDBACK & REVIEWS</h4>
            </div>

            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif

            <div class="row customer-rating">
                <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                    <div class="customer-card">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class="d-grid">
                                <div class="customer-type">New Customers</div>
                                <img class="mt-2" src="./images/newcustomer.svg" alt="">
                            </div>

                            <div class="text-to-end">
                                <div class="customer-percent-up"><span class="mx-2">23.29%</span> <img
                                        src="./images/uparrow-green.svg" alt=""></div>
                                <div class="customer-value"><span>{{$ratings->users->total ?? ''}}</span></div>
                                <div class="customer-compare">Compared to previous 7 days</div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                    <div class="overall-rating">
                        <div class="row">
                            <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <div class="overall-rating-star f-14">
                                    <p class="f-medium text-color-grey d-flex">Over all Rating</p>
                                    <div class="d-flex align-items-baseline">
                                        <span class="font-weight-bold f-24"></span>
                                        <span class="mx-2">
                        <div>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star star-checked"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                        </div>
                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-margin col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Excellent - <span>20</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value excellent"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Good - <span>10</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value good"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Average - <span>5</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value average"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Below Avg - <span>2</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value below-avg"></div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="f-10 text-color-grey rating-word">Poor - <span>1</span></div>
                                    <div class="rating-percentage-bg">
                                        <div class="actual-rating-value poor"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="comment-margin col-12 col-sm-12 col-md-3 col-lg-3 col-xl-4">
                                <div class="overall-rating-comment">
                                    <div class="f-12 d-flex">Its really good that you have used proper care while delivering my order
                                    </div>
                                    <div class="comment-gap d-flex align-items-center justify-content-between">
                                        <div>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star star-checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                        <span class="f-10 text-color-grey">21 JAN 21</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                {{--<div class="col-12">--}}
                    {{--<div class="d-flex justify-content-end download mt-0">--}}
                        {{--<span class="text-color-green mx-2 f-14 f-medium ">Download Report</span> <img src="./images/download.svg" alt="">--}}
                    {{--</div>--}}
                {{--</div>--}}

            </div>
            <div class="heading">
                RATINGS HISTORY
            </div>
            <div class="table-responsive">
                <table class="table" id="ratings">
                    <thead class="thead-light">
                    <tr>
                        <th>Date</th>
                        <th>Rating</th>
                        <th>Review Comments</th>
                        <th>Customer Status</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(!isset($ratings->review))
                                <tr><td colspan="4">No data Available</td></tr>
                            @else
                        @foreach($ratings->review as $rating)
                    <tr>
                        <td>{{$rating->date}}</td>
                        <td>
                            <div>
                                @if($rating->rating==1)
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                @endif
                                @if($rating->rating==2)
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                @endif
                                @if($rating->rating==3)
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                @endif
                                @if($rating->rating==4)
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star"></span>
                                    @endif
                                    @if($rating->rating==5)
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star star-checked"></span>
                                    @endif
                            </div>
                        </td>
                        <td>{{$rating->review}}</td>

                        <td>{{$rating->customer_status}}</td>
                    </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

            </div>
            <div class="row float-right">

            <div class="col-12">
                        <div class="d-flex justify-content-between download f-12 margin-top-2rem">
                            <div class="d-flex ">
                            </div>

                            <div id="exportBtn1">
                                <span class="text-color-green mx-2 f-14 f-medium "  >Download Report</span>
                                <img src="./images/download.svg" alt="">
                                <div>
                            </div>
                        </div>
                    </div>
                
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('model')
    <div>
        <div class="modal left fade menu-list" id="sidebaroptionid" tabindex="-1" role="dialog" aria-labelledby="sidebar-option-lable">
            <div class="modal-dialog" role="sidebar options">
                <div class="modal-content bg-black">


                    <div class="modal-body sidebar-options">
                        <header>
                            <img class="logo" src="./images/pekkish-logo.svg" alt="">
                            <!-- <button type="button" class="close closebtn" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button> -->
                        </header>
                        <nav class="navbar-options">
                            <ul class="nav flex-column flex-nowrap overflow-hidden">
                                <li class="nav-item">
                                    <a class="nav-link text-truncate " href="./dashboard.html">
                                        <img class="nav-icons" src="./images/dashboard_icon.svg" alt="">
                                        <span class=" menu-link">Dashboard</span></a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#orders-menu" data-toggle="collapse"
                                       data-target="#orders-menu">
                                        <img class="nav-icons" src="./images/orders.svg" alt="">
                                        <span class="">Orders</span></a>
                                    <div class="collapse" id="orders-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./orders.html"><span>Orders</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./order_history.html"><span>History</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./menu.html">
                                        <img class="nav-icons" src="./images/menu.svg" alt="">
                                        <span class="menu-link">Menu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./bussiness_hours.html">
                                        <img class="nav-icons" src="./images/timesettings.svg" alt="">
                                        <span
                                            class=" menu-link">Bussiness Hours</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./accounts.html">
                                        <img class="nav-icons" src="./images/accounts.svg" alt="">
                                        <span
                                            class=" menu-link">Accounts</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate active-links" href="./feedback_reviews.html">
                                        <img class="nav-icons" src="./images/feeback.svg" alt="">
                                        <span class=" menu-link">Feedbacks
                                        & Reviews</span></a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link text-truncate" href="./offer_list.html">
                                        <img class="nav-icons" src="./images/offers.svg" alt="">
                                        <span class=" menu-link">Offers</span></a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link collapsed text-truncate" href="#settings-menu" data-toggle="collapse"
                                       data-target="#settings-menu">
                                        <img class="nav-icons" src="./images/settings.svg" alt="">
                                        <span class="">Settings</span></a>
                                    <div class="collapse" id="settings-menu" aria-expanded="false">
                                        <ul class="flex-column pl-2 nav">
                                            <li class="nav-item">
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_employees.html"><span>Manage Employees</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./manage_restaurant.html"><span>Manage Restaurants</span></a>
                                                <a class="nav-link collapsed py-1"
                                                   href="./time_settings.html"><span>Time Settings</span></a>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <div class="mt-5">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/logout.svg" alt="">
                                            <span class=" menu-link">Logout</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <img class="nav-icons" src="./images/help.svg" alt="">
                                            <span class=" menu-link">Help</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">

                                            <span class=" menu-link f-12 ml-3 mt-4">Terms & Conditions</span></a>
                                    </li>
                                </div>
                            </ul>

                        </nav>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal right fade profile-sidebar" id="userprofileid" tabindex="-1" role="dialog" aria-labelledby="user-profile">
            <div class="modal-dialog" role="profile dialog">
                <div class="modal-content bg-black">


                    <div class="modal-body">

                        <div class="outerDivFull" >
                            <div class="switchToggle">
                                <input type="checkbox" id="switch">
                                <label for="switch">Toggle</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <img src="./images/userprofile.svg" alt="" class="userprofileimg">
                            <p class="f-16 f-medium text-color-white mt-4 mb-1">Hello, Omari </p>
                            <p class="f-14 text-color-grey mb-5"> Account Manager</p>
                        </div>
                        <div>
                            <nav class="navbar-options">
                                <ul class="nav flex-column flex-nowrap overflow-hidden">
                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Update Email Address</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate" href="#">
                                            <span class=" menu-link">Reset Password</span></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-truncate " href="#">
                                            <span class=" menu-link">Update <Address></Address></span></a>
                                    </li>
                                </ul>
                        </div>
                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

    </div>

@endsection
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script>
    $(document).ready(function () {
    $("#exportBtn1").click(function(){
    var date = new Date();
    TableToExcel.convert(document.getElementById("order_history"), {

    name: "Order_history_" +date+ ".xlsx",
    sheet: {
    name: "Order History"
    }
    });
    });
    });
</script>