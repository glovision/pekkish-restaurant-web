<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Pekkish_end user/Create Account</title>
</head>
<body class="login-bg">
<!-- create account start -->
<div class="login-container">
    <div class="form-createaccount">
        <h1 class="mt-4 mb-5 acount-text">Create an account</h1>
        <form action="" class="form">
            <div class="row">

                <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">First Name</label>
                        <input type="text" class="input" placeholder="First Name">
                    </div>
                </div>
                <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">Last Name</label>
                        <input type="text" class="input" placeholder="Last Name">
                    </div>
                </div>
                <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">Mobile Number</label>
                        <input type="text" class="input" placeholder="Mobile Number">

                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">Email</label>
                        <input type="text" class="input" placeholder="Email">

                    </div>
                </div>
                <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">Address</label>
                        <input type="text" class="input" placeholder="Address">
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="input-createaccount">
                        <label for="username" class="label">City</label>
                        <input type="text" class="input" placeholder="City">
                        <!-- <span class="btn-show-account">
                        <img src="images/invalid_icon.svg">
                        </span> -->
                    </div>
                </div>
                <div class="col-md-6 ml-auto mt-5 mb-5">
                    <a href="#" class="btn modal-btn"  data-toggle="modal" data-target="#basicModal" role="button">NEXT</a>
                    <!-- <a href="#" class="btn login-btn" style="background: #f20;">NEXT</a> -->
                    <!-- <button class="login-btn" data-toggle="modal" data-target="#basicModal">NEXT</button> -->

                </div>
            </div>
        </form>



    </div>

</div>
<!--  end -->

<!-- basic modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <h4 class="modal-title pt-3 pl-3 otp-h4" id="myModalLabel">OTP</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body otp-height">
                <div class="d-flex justify-content-center">
                    <p class="otp-text mt-5">OTP has been sent to<br> your mobile number<br><span class="left-otp"> ***123</span></p>
                </div>
                <div class="input-group">

                    <input type="text" class="left-formotp mt-5" placeholder="2&nbsp;&nbsp;2 &nbsp;&nbsp;5 &nbsp;&nbsp;5 &nbsp;&nbsp;6&nbsp;&nbsp;6">

                    <a class="resend-otp mt-3" href="#" >RESEND OTP</a>
                    <a href="logged_home.html" class="btn otp-btn"role="button">SUBMIT</a>
                    <!-- <a href="logged_home.html"><button class="otp-btn">SUBMIT</button></a> -->
                </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>

<!-- large modal -->


<!-- small modal -->


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery-3.4.1.slim.min.js" ></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>