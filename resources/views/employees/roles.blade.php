@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">Manage Roles</span>
                <button class="add-btn" data-toggle="modal" onclick="addrole_modal()">+ Add Role</button>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="table-responsive" style="text-align: -webkit-center;">
                <table class="table" style="max-width: 50%;">
                    <thead class="thead-light">
                    <tr>
                        <th> # </th>
                        <th> Name</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                    <tr>

                        <td>{{ $loop->iteration }}</td>
                        <td>{{$role['name']}}</td>
                        <td>
                            <div class="d-flex">
                                <a id="{{json_encode($role)}}" onclick="update_modal(this.id);">
                                    <img class="mx-3" src="./images/card-edit.svg" alt="">
                                </a>
                                <a onclick="confirm_modal('{{url('role_destroy', $role['id'])}}');">
                                    <img class="mx-3" src="./images/delete.svg" alt="" >
                                </a>
                            </div>
                        </td>

                    </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="container-fluid p-0 mt-5">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <span class="heading">ROLES DEFINITION</span>

                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/account-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Account Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Has complete control over all restaurants including the ability to add new locations and users.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Store Managers will only be able to see the stores assigned to his/her profile.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-employees.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Employee</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Will only receive orders from the store assigned to account.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div>
        @include('inc.side_nav')

        <div class="modal add-restaurant right fade " id="addroleid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewroles" role="add Roles">
                <div class="modal-content model-row-colum ">

                    <div class="modal-body pb-4">
                    <div class="overflow-dialog">
                        <div class="f-20 f-medium"><span id="addedit"></span> Role</div>
                        <form id="manage-roles-form" method="POST" action="{{url('addrole')}}">
                            @csrf

                            <div class="form-group input-material">
                                <input type="hidden" class="form-control" name="id" id="id">
                                <input type="text" class="form-control" name="rolename" id="rolename" data-validation="required" required>
                                <label for="Last Name">Role Name<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('rolename') }}</div>
                            </div>

                            <div class="mt-4">
                                <div class="f-14 text-color-grey mb-2">Select Permissions<span style="color:red">*</span></div>
                                <div class="row">
                                    @foreach ($tblfeatures ?? '' as $tblfeature)
                                        <!-- @if($tblfeature['FeatureType']==0)
                                                <div class="col-12 f-14">
                                        @endif
                                        @if($tblfeature['FeatureType']==1)
                                                <div class="col-11 col-push-1 f-14" style="margin-left: 30px;">
                                        @endif -->
                                            <div class="col-6 f-14">
                                                <label class="check">{{$tblfeature['FeatureName']}}
                                                    <input type="checkbox" name="permission_id[]" id="permission_id{{$tblfeature['FeatureId']}}" value="{{$tblfeature['FeatureId']}}" data-validation="required">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                    @endforeach
                                </div>
                                <div class="text-danger">{{ $errors->first('permission_id') }}</div>
                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">
                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn">ADD ROLE</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <!-- Modal -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to delete it ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function confirm_modal(delete_url)
        {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
        }

    $.fn.materializeInputs = function (selectors) {

        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea";

        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }

        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });

        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };

    /**
     * Material Inputs
     */
    $('body').materializeInputs();


        function addrole_modal()
        {
            $('#addedit').html('Add');
            $('#manage-roles-form').attr('action', '{{url('addrole')}}');
            $('#addroleid').modal('show');
            $('#id').val('');
            $('#rolename').val('');
            $("input[name='permission_id[]']").prop( 'checked',false );
        }
        function update_modal(param) {
            $("input[name='permission_id[]']").prop( 'checked',false );
            console.log(param);
            //var data = JSON.stringify(param);
            var data = JSON.parse(param);
            $('#addedit').html('Edit');
            $('#manage-roles-form').attr('action', '{{url('editrole')}}');
            $('#addroleid').modal('show');
            $('#id').val(data.id);
            $('#rolename').val(data.name);
            console.log(data['name']);
            var permissions = data['permissions'];
            console.log("ABCD " + permissions.length);

            for (var i = 0; i < permissions.length; i++) {
               // $('#permission_id'+permissions[i]).val();
                $('#permission_id'+permissions[i]).prop( 'checked',true );
            }
            $("#permissionsdiv").html(permissions);
        }


    </script>

@endsection
