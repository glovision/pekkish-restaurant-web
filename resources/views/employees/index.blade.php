@extends('layouts.app')
@section('content')

    <div class="main-section-padding">
        <div class="res-cards">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <span class="heading">LIST OF USERS</span>
                <button class="add-btn" data-toggle="modal" onclick="addemp_modal()">+ Add User</button>
            </div>
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">{{ session()->get('success') }}</div>
            @endif
            @if(session()->has('failed'))
                <div class="alert alert-danger alert-dismissable">{{ session()->get('failed') }}</div>
            @endif
            <div class="table-responsive">
                <table class="table" id="employees">
                    <thead class="thead-light">
                    <tr>
                        <th>Employee Name</th>
                        <th>User Email ID</th>
                        <th>Role</th>
                        @if($type!='shop')
                        <th>Store Name</th>
                        @endif
                        {{--<th>Location</th>--}}
                        <th>Reset Password</th>
                        <th>Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if($users =='')
                                <tr><td colspan="7">No data Available</td></tr>
                            @else
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user['name']}}</td>
                        <td>{{$user['email']}}</td>
                        <td>{{$user['rolename']}}</td>
                        @if($type!='shop')shopname
                        <td>{{$user['shopname']}}</td>
                        @endif
                        {{--<td>{{$user['city']}}</td>--}}
                        <td>
                            <a class="reset-password" onclick="userpasswordreset('{{$user['user_id']}}')">Reset</a>
                        </td>
                        <td>
                            <div class="d-flex">
                                <a id="{{json_encode($user)}}" onclick="update_modal(this.id);">
                                    <img class="mx-3" src="./images/card-edit.svg" alt="">
                                </a>
                                <a onclick="confirm_modal('{{url('users_destroy', $user['user_id'])}}');">
                                    <img class="mx-3" src="./images/delete.svg" alt="" >
                                </a>
                            </div>
                        </td>

                    </tr>

                    @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            <div class="container-fluid p-0 mt-5">
                <div class="d-flex align-items-center justify-content-between mb-4">
                    <span class="heading">ROLES DEFINITION</span>

                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/account-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Account Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Has complete control over all restaurants including the ability to add new locations and users.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-manager.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Manager</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Store Managers will only be able to see the stores assigned to his/her profile.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 ">
                        <div class="restaurant-role-card">
                            <div class="d-flex">
                                <img class="role-icon" src="./images/store-employees.svg" alt="">
                            </div>
                            <div>
                                <p class="mt-3 mb-2 d-flex">Store Employee</p>
                                <p class="mb-0 f-12 text-color-grey role-description d-flex">Will only receive orders from the store assigned to account.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div>
        @include('inc.side_nav')


        <div class="modal add-restaurant right fade " id="addemployeesid" tabindex="-1" role="dialog"
             aria-labelledby="add-restaurant">
            <div class="modal-dialog" id="addnewemployees" role="add employees">
                <div class="modal-content">

                    <div class="modal-body pb-4">
                        <div class="f-20 f-medium"><span id="addedit"></span> Employee</div>
                        <form id="manage-employees-form" method="POST" action="{{url('adduser')}}">
                            @csrf
                            <div class="row">
                                <div class="container-fluid" style="margin: 0 0 -28px 0;">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="hidden" class="form-control" name="id" id="empid">
                                                <input type="text" class="form-control" name="firstname" id="firstname" required data-validation="required">
                                                <label for="firstname">First Name<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('firstname') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group input-material mr-2">
                                                <input type="text" class="form-control" name="lastname" id="lastname" required data-validation="required">
                                                <label for="lastname">Last Name<span style="color:red">*</span></label>
                                                <div class="text-danger">{{ $errors->first('lastname') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="form-group input-material">
                                <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10" pattern="[0-9]*" onblur="mobileverification(this.value)" data-validation="required" required>
                                <label for="Last Name">Mobile Number<span style="color:red">*</span></label>
                                <div class="text-danger">{{ $errors->first('phone') }}</div>
                                <div class="text-danger" id="mobileerrormsg"></div>
                            </div>

                            <div class="form-group input-material">
                                <input type="email" class="form-control" name="emailid" id="emailid"  onblur="emailverification_user(this.value)" data-validation="required" required>
                                <label for="emailid">Email Id<span style="color:red">*</span></label>
                                <div class="text-danger" id="emailerror_msg"></div>
                            </div>
                            <div class="mt-4">
                                <div class="f-14 text-color-grey mb-2">Select Role<span style="color:red">*</span></div>
                                <div class="flex radio-toolbar">
                                    @foreach ($roles ?? '' as $role)
                                        <div type="input" class="employee-options" id="radio-role{{$role['id']}}" onclick="addremovestyle({{$role['id']}})">
                                            {{$role['name']}}
                                                <input type="radio" name="role_id" id="radiobtnrole{{$role['id']}}" value="{{$role['id']}}" style="display: none;" data-validation="required" required>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="text-danger">{{ $errors->first('role_id') }}</div>
                            </div>
                            @if($type == 'seller')
                            <div class="mt-4">
                                <div class="f-14 text-color-grey mb-2">Store Name<span style="color:red">*</span></div>
                                <label class="flex chk-toolbar">
                                   {{-- <div type="input" class="employee-options" onclick="addremoveshopstyle({{$loop->index}})">
                                        All
                                        <input type="checkbox" name="shop_id[]" value="{{$shop['id']}}" style="display: none;">
                                    </div>--}}
                                    @foreach ($shops as $shop)
                                        <label class="employee-options" id="radio-shop{{$loop->index}}">
                                            {{$shop['name'] ??''}}
                                            <input type="checkbox" name="shop_id[]" id="radiobtnshop{{$loop->index}}" value="{{$shop['id'] ?? ''}}"  onclick="addremoveshopstyle({{$loop->index}})" style="display: none">
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <div class="form-group input-material w-50 mr-2">
                                <input type="text" class="form-control" name="joiningdate" id="joiningdate" data-validation="required" required autocomplete="off">
                                <label for="joiningdate" class="datetype" style="top: -12px;">Joining Date<span style="color:#ff0000">*</span></label>
                                <div class="text-danger">{{ $errors->first('joiningdate') }}</div>
                            </div>

                            <div class="mt-5 d-flex align-items-center justify-content-between">

                                <button class="cancelbtn" data-dismiss="modal" aria-label="Close">CANCEL</button>
                                <button class="addbtn" id="addbtn"> </button>
                            </div>

                        </form>

                    </div>

                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <!-- Modal -->
        <div class="modal fade" id="confirm_delete" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body p-4">
                        <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                            <img src="./images/dialogclose.svg" alt="">
                        </div>
                        <p class="f-20 f-medium">Are you sure!</p>
                        <p class="f-12 mb-0">You want to delete it ?</p>
                    </div>
                    <div class="mb-2">
                        <div class=" mb-3 d-flex align-items-center justify-content-around">

                            <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                    aria-label="Close">NO</button>
                            <a id="delete_link" class="addbtn w-100 mx-3 f-medium">YES</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="passwordreset_model" tabindex="-1" role="dialog" aria-labelledby="archieveTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form  method="POST">
                        @csrf
                        <div class="modal-body p-4">
                            <div class="d-flex align-items-end dialog-close-icon" data-dismiss="modal" aria-label="Close">
                                <img src="./images/dialogclose.svg" alt="">
                            </div>

                            <div class="form-group input-material mr-2">
                                <input type="text" id="useroldpassword" name="useroldpassword" class="form-control" required>
                                <label for="name">Old Password</label>
                            </div>
                            <div class="form-group input-material mr-2">
                                <input type="text" id="usernewpassword" name="usernewpassword" class="form-control" onblur="userchecknewpassword(this.value);" required>
                                <label for="name">New Password</label>
                                <div class="text-danger" id="usernewpasswordvaliderror"></div>
                            </div>
                            <div class="form-group input-material mr-2">
                                <input type="text" id="userconfirmpassword" name="userconfirmpassword" class="form-control" onblur="usercheckconfirmpassword(this.value);" required>
                                <label for="name">Confirm Password</label>
                                <div class="text-danger" id="userconfirmpasswordvaliderror"></div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <div class=" mb-3 d-flex align-items-center justify-content-around">

                                <button class="cancelbtn w-100 mx-3 f-medium" data-dismiss="modal"
                                        aria-label="Close">NO</button>
                                <button type="submit" class="addbtn w-100 mx-3 f-medium">YES</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        function confirm_modal(delete_url)
        {
            jQuery('#confirm_delete').modal('show', {backdrop: 'static'});
            document.getElementById('delete_link').setAttribute('href' , delete_url);
        }


    function addremovestyle(param){
        $('.radio-toolbar div').removeClass('option-selected');
        $('#radio-role'+param).addClass('option-selected');
        $('#radiobtnrole'+param).prop('checked', true);
    }
    function addremoveshopstyle(param){
//console.log($('#radiobtnshop'+param).prop("checked"));
            if($('#radiobtnshop'+param).is(":checked")){
                //$('.chk-toolbar div').removeClass('option-selected');
                $('#radio-shop'+param).addClass('option-selected');
                $('#radiobtnshop'+param).prop('checked', true);
            }else{
               // $('.chk-toolbar div').addClass('option-selected');
                $('#radio-shop'+param).removeClass('option-selected');
                $('#radiobtnshop'+param).prop('checked', false);
            }


       // $('.chk-toolbar div').removeClass('option-selected');
      //  $('#radio-shop'+param).addClass('option-selected');
      //  $('#radiobtnshop'+param).prop('checked', true);
    }

    $.fn.materializeInputs = function (selectors) {

        // default param with backwards compatibility
        if (typeof (selectors) === 'undefined') selectors = "input, textarea";

        // attribute function
        function setInputValueAttr(element) {
            element.setAttribute('value', element.value);
        }

        // set value attribute at load
        this.find(selectors).each(function () {
            setInputValueAttr(this);
        });

        // on keyup
        this.on("keyup", selectors, function () {
            setInputValueAttr(this);
        });
    };

    /**
     * Material Inputs
     */
    $('body').materializeInputs();


        function addemp_modal()
        {
            $('#addedit').html('Add');
            $('#addbtn').html('ADD EMPLOYEE');
            $('#manage-employees-form').attr('action', '{{url('adduser')}}');
            $('#addemployeesid').modal({backdrop: 'static', keyboard: false});
            $('#id').val('');
            $('#firstname').val('');
            $('#lastname').val('');
            $('#phone').val('');
            $('#emailid').val('');
            $('#radio-role').val('');
            $('#joiningdate').val('');
            addremovestyle('');
        }
        function update_modal(param)
        {
            console.log(param);
            var data = JSON.parse(param);
            $('#addedit').html('Edit');
            $('#addbtn').html('UPDATE EMPLOYEE');
            $('#manage-employees-form').attr('action', '{{url('user_edit')}}');
            $('#addemployeesid').modal({backdrop: 'static', keyboard: false});

            //$('#id').attr('value', data.user_id);
            $('#empid').val(data.user_id);
            console.log(data.user_id);
            $('#firstname').attr('value', data.name);
            $('#firstname').val(data.name);
            $('#lastname').attr('value', data.last_name);
            $('#lastname').val(data.last_name);
            $('#phone').attr('value', data.phone);
            $('#phone').val(data.phone);
            $('#emailid').attr('value', data.email);
            $('#emailid').val(data.email);
            $('input[name="role_id"][value='+data.role_id+']').prop('checked', 'checked');
            $('#radio-role'+data.role_id).addClass('option-selected');
            //console.log($('#radiobtnrole'+data.role_id).prop('checked', true));
            $('#joiningdate').attr('value', data.joining_date);
            $('#joiningdate').val(data.joining_date);
            addremovestyle(data.role_id);
        }


        function password_reset(fooddispatchurl)
        {
            jQuery('#passwordreset_model').modal('show', {backdrop: 'static'});
        }

        function emailverification_user(id){
            var userid =  $('#id').val();
            var send = {"user_id":userid, "user_type":"staff", "email":id, _token: '{{csrf_token()}}'};
            send=$(this).serialize()+" & "+$.param(send);
            var url = "{{url('userEmailIdvarification/')}}"
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data:send,
                success: function (res) {
                    if(res){
                       $('#emailerror_msg').html("");
                    }else{
                        $('#emailerror_msg').html("EmailId alredy exists. Please Enter another EmailId");
                        $('#emailid').focus();
                        return false;
                    }
                },error: function(res) {

                }
            });
        }
        function mobileverification(id){
            var mob_length = id.length;
            if(mob_length != 10){
                $('#mobileerrormsg').html("Please Enter 10 Digit Mobile No.");
                return false;
            }
            var userid =  $('#id').val();
            var send = {"user_id":userid, "user_type":"staff", "mobile":id, _token: '{{csrf_token()}}'};
            send=$(this).serialize()+" & "+$.param(send);
            var url = "{{url('ajaxmobilevarification')}}"
            console.log("3333 "+url);
            $.ajax({
                cache:false,
                type: "POST",
                url: url,
                data:send,
                success: function (res) {
                    if(res){
                        console.log("mobile success");
                        $('#mobileerrormsg').html("");
                    }else{
                        //console.log("email failed");
                        $('#mobileerrormsg').html("Phone No. already exists. Please Enter another Phone No.");
                        $('#phone').focus();
                        return false;
                    }
                },error: function(res) {

                }
            });
        }


    function userpasswordreset(data){
        console.log(data);
        swal({
                title: "Are you sure?",
                text: "You want to reset password!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){

                if (isConfirm){
                    var send = {"userid":data, _token: '{{csrf_token()}}'};
                    send=$(this).serialize()+" & "+$.param(send);
                    console.log("data "+send);
                    $.ajax({
                        cache:false,
                        type: "POST",
                        url: "{{url('ajaxuserpasswordreset/')}}",
                        data:send,
                        success: function (res) {
                            console.log(res);
                            if (res) {
                                swal({title: "success", text: "Successfully updated. Please check Your E-mail"});
                                window.location.href = "";
                            } else {
                                swal({title: "error", text: "Failed to update."});
                            }
                        },error: function(res) {
                            console.log(res);
                            swal({title: "error", text: "Error to update."});
                        }
                    });
                    //    swal("Shortlisted!", "Candidates are successfully shortlisted!", "success");
                } else {
                    swal("Cancelled", "Failed", "error");
                }
            });
    }
    </script>

@endsection
