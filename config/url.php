<?php
/**
 * Created by PhpStorm.
 * User: venkat
 * Date: 22-03-2021
 * Time: 09:39
 */

return [

    'BASE_URL'=>"http://localhost/pekkish/api",
    //'BASE_URL'=>"https://pekkish.glovision.co/pekkish-dev/api",
    //'BASE_URL'=>"https://pekkish.glovision.co/pekkish/api",
    //'BASE_URL'=>"http://localhost/pekkish-dev/api",


    'ALL_COUPONS' => "/v1/cuisines-filter",
    'SIGNIN' => "/v1/auth/restaurantLogin",
    'TWO_WAY_SIGNIN' => "/v1/auth/two_way_login",
    'SIGNUP' => "/v1/shops/signup",
    'LOGOUT' => "/v1/auth/logout",
    'FORGOTPASSWORD' => "/v1/auth/password/create",
    'OTPVERIFICATION' => "/v1/auth/password/otp-validate",

    'PROFEMAILUPDATE' => "/v1/auth/email/update",
    'PROFILEUPDATE' => "/v1/auth/profile/update",
    'PASSWORDRESET' => "/v1/auth/password/reset",
    'SHOPSCHEDULEOFF' => "/v1/restaurant/schedule-off",
    'SHOPMANUALOFF' => "/v1/restaurant/change-status",

    'ORDERS' => "/v1/get-orders",
    'ORDERS_STATUS' => "/v1/orders-with-status",
    'GETINVOICEDETAILS' => "/v1/restaurant/order-details",
    'FOODREADY' => "/v1/order/food-ready",
    'FOODDISPATCH' => "/v1/order/dispatched",
    'FOODREJECT' => "/v1/order/reject",
    'FOODACCEPT' => "/v1/order/accept",
    //'ORDERITEMREJECT' => "/v1/order/orderitemreject",
    'ORDERITEMREJECT' => "/v1/order/reject/item",

    'ORDERHISTORY' => "/v1/order/order_history",
    'ORDERHISTORYCOUNT' => "/v1/order/TopSellingItem",

    'SALESHISTORY' => "/v1/order/sales_history",
    'ITEMIZEDSALESHISTORY' => "/v1/order/item_sales_history",
    'DASHBOARDCOUNTDTLS' => "/v1/order/ordersCount",

    'ORDERHISTORYCOUNT1' => "/v1/order/TopSellingItem1",
    'ORDERCOUNT' => "/v1/order/TotalOrders",
    
    'OFFERS' => "/v1/coupons-test",
    'ADDOFFERS' => "/v1/coupons/store",
    'EDITOFFER' => "/v1/coupons/update",
    'DELETECOUPON' => "/v1/coupons/destroy",

    'MENULIST' => "/v1/restaurant/menu",
    'PRODUCT_STATUS' => "/v1/products/available_status",
    'ITEMLIST' => "/v1/products/list",
    'DELETEITEM' => "/v1/products/destroy",
    'CUISINES' => "/v1/cuisines-filter",
    'CATEGORIES' => "/v1/categories/list",
    'CATEGORYADD' => "/v1/categories/addCategory",

    'CATEGORYPRODUCT' => "/v1/categories/catagoryproductlist",

    'ADDITEM' => "/v1/products/add",
    'EDITITEM' => "/v1/products/update",

    'SHOPS' => "/v1/shops/list",
    'ADDSHOP' => "/v1/shops/insert",
    'EDITSHOP' => "/v1/shops/edit",
    'UPDATESHOP' => "/v1/shops/update",
    'DELETESHOP' => "/v1/shops/destroy",

    'USERS' => "/v1/staff/shopusers",
    'ADDUSER' => "/v1/users/store",
    'EDITUSER' => "/v1/users/update",
    'DELETEUSER' => "/v1/users/destroy",
    'USERPWDRESET' => "/v1/users/userpwdreset",

    'SHOPTIMING' => "/v1/shops/shopTiming",
    'DUTYHISTORY' => "/v1/shops/dutyHistory",
    'ADDBUSINESSHOUR' => "/v1/shops/addBusinessHour",
    'SHOPAVAILABLETATUS' => "/v1/shops/shopAvailableStatus",

    'COUNTRYCODES' => "/v1/country/index",
    'VERIFYEMAIL' => "/v1/shops/verifyemail",
    'EMAILVERIFY' => "/v1/shops/emailverify",
    'MOBILEVERIFY' => "/v1/shops/mobileverify",
    'USERSHOPS' => "/v1/user/shop",

    'RATINGS' => "/v1/restaurant/customer-ratings",
    'DAYWISESALEDTLS' => "/v1/order/DayWiseSalesDetails",
    'LINECHARTORDERDATA' => "/v1/order/DayWiseOrderDetails",
    'TIMINGS' => "/v1/shops/get_average_time",
    'SETAVGTIME' => "/v1/shops/update/edit_average_time",

    'TBLFEATURES' => "/v1/roles/tblfeaures",
    'ROLES' => "/v1/roles/roleList",
    'ADDROLE' => "/v1/roles/addrole",
    'EDITROLE' => "/v1/roles/editrole",
    'DELETEROLE' => "/v1/roles/destroy",
    'DUTYHISTORYSTATUS' => "/v1/shops/ShopHistoryStatus",

    'TERMCONDITIONS' => "/v1/policies/terms",
    'HELP' => "/v1/policies/help",
    'FAQ' => "/v1/policies/faq",

    'GOOGLEAPI' => "/v1/googleapi_restweb",

    'USERROLES' => "/v1/roles/userrole",
    "USERDETAILS"=>"/v1/auth/user",
    "CITIES"=>"/v1/cities",
    "CATEGORYUPDATE"=>"/v1/category/update_category",
    "CATEGORYEDIT"=>"/v1/category/edit",
    "ASSIGNDRIVER"=>"/v1/order/assign-driver",
    "CUSINES"=>"/v1/cuisines/index",
    "ADDCUSINE"=>"/v1/cuisines/store",
    "UPDATECUSINE"=>"/v1/cuisines/update",
    "DELETECUSINE"=>"/v1/cusine/delete",
    "GETDRIVERS"=>"/v1/order/get-drivers",
    "BRANDS"=>"/v1/brands",
    "APKLINKS"=>"/v1/apklinks",
    "NOTIFICATIONS"=>"/v1/notifications",
    "SHOP_CATEGORIES"=>"/v1/categories/restaurantcategories",
    "CATEGORY_ITEMS"=>"/v1/categories/category_items",
    
    "COUPONVERIFY"=>"/v1/couponverify",

    'SUCCESS_STORIES' => "/v1/success_stories",
    'STORY_DETAILS' => "/v1/story_details",
    
    'ITWORKS' => "/v1/itworks",
    'SENDOTPSIGNUP'=>"/v1/mobile/send",
    'VERIFYSINGUPOTP'=>"/v1/mobile/verify",
    "DELETECATEGORY"=>"/v1/category/destroy",
    "RESETPASSWORD"=>"/v1/auth/password/forgot-reset",
    "RESENDOTP"=>"/v1/auth/password/otp-resend",
    "FOODDISPATCHDELIVERY"=>"/v1/order/dispatch-deliver",

    //new urls
    "SELLERITEMS" => "/v2/seller-products",
    "SHOPITEMS" => "/v2/shop-products",
    "ADDSELLERITEM" => "/v2/add-seller-products",
    "ADDSHOPITEM" => "/v2/add-shop-products",
    "EDITSELLERITEM" => "/v2/edit-seller-products",
    "GETPRODUCTDETAILS" => "/v2/get-product-details",
    "EDITSHOPITEM" => "/v2/edit-shop-products",
    "DELETESELLERITEM" => "/v2/destroy-seller-products",
    "DELETESHOPITEM" => "/v2/destroy-shop-products",
    "SHOPCATEGORIES" => "/v2/shop-menu",
    "SELLERCATEGORIES" => "/v2/seller-menu",
    "ADDSELLERCATEGORY" => "/v2/add-seller-menu",
    "ADDSHOPCATEGORY" => "/v2/add-shop-menu",
    "EDITSELLERCATEGORY"=> "/v2/edit-seller-menu",
    "UPDATESELLERCATEGORY" => "/v2/update-seller-menu",
    "EDITSHOPCATEGORY" => "/v2/edit-shop-menu",
    "UPDATESHOPCATEGORY" => "/v2/update-shop-menu",
    "SELLERCATEGORYDESTROY" => "/v2/destroy-seller-menu",
    "DELETESHOPCATEGORY" => "/v2/destroy-shop-menu",
    "SHOPCATEGORYADDONS" => "/v2/shop-addons",
    "SELLERADDONS" => "/v2/seller-addons",
    "SELLERCATEGORYADDONS" => "/v2/seller-category-add-ons",
    "ADDSELLERCATEGORYADDON" => "/v2/add-seller-addons",
    "ADDSHOPCATEGORYADDON" => "/v2/add-shop-category-addons",
    "EDITSELLERCATEGORYADDON" => "/v2/edit-seller-addons",
    "EDITSHOPCATEGORYADDON" => "/v2/edit-shop-category-addons",
    "DELETESELLERCATEGORYADDON" => "/v2/destroy-seller-addons",
    "DELETESHOPCATEGORYADDON" => "/v2/destroy-shop-category-addons",
    "ADDSELLERITEMSTOSTORE" => "/v2/add-seller-items-to-shop"

]

?>

